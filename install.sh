#!/bin/bash
#
# Developed by Deltapath
#

#==================================== Imports ===============================================
# Import base
. base/import.sh

# Import Sytemupdate
. systemupdate/import.sh

# Import Server
. server/import.sh

# Import deltapath built software
. deltapath/import.sh

# Presentation function and options
welcome(){

clear

echo -e "
Enter an option:
init_install - Execute the first time installation
upgrade-install - Execute the upgrade installation
e - Exit
===============================
"
read program

case $program in

    # Performs the function with the name of the variable passed
    e) clear; exit;;
 #   init_install) curl.sh; basicdep.sh; lamp.sh; nginx.sh; 3rdrpm-asterisk.sh; ready;;
    #init_install) curl.sh; basicdep.sh;  lamp.sh; ready;;
  #  init_install) lamp.sh; dbinit.sh;3rdrpm-asterisk.sh; homemade.sh; ready;;
	#init_install) 3rdrpm-asterisk.sh; homemade.sh; ready;;
	#init_install) images-lib.sh;logtools.sh;zoneinfo.sh;ready;;

	#init_install) tools.sh; utilities.sh;mediacoder.sh;audiodecoder.sh; usersetting.sh; fail2ban.sh; configsscripts.sh; ready;;
	#	init_install) configFileEncryption.sh; fail2ban.sh; h323gatekeeper.sh; nginx-rtmp.sh; dolby.sh; ready;;
   # upgrade-install) upgrade.sh; ready;;

	init_install) curl.sh; basicdep.sh; lamp.sh; dbinit.sh;service-staff.sh;monitor.sh; firewall-network.sh; images-lib.sh;logtools.sh;zoneinfo.sh;tools.sh; utilities.sh;mediacoder.sh;audiodecoder.sh; usersetting.sh; fail2ban.sh; configsscripts.sh; 3rdrpm-asterisk.sh; homemade.sh;configFileEncryption.sh; h323gatekeeper.sh; nginx-rtmp.sh; dolby.sh; ready;; 

	upgrade-install) upgrade.sh; ready;;

    *) welcome;;

esac
}

welcome
