#!/bin/bash
#
configsscripts.sh(){
 	/etc/hosts
	chgrp apache /etc/hosts
	#chmod 664 /etc/hosts
	
	mkdir -p /usr/local/frsip
	chown apache /usr/local/frsip

	mkdir -p /usr/local/frsip/license
	chown apache /usr/local/frsip/license
	
	
	mkdir -p /usr/local/frsip/gateway
	chown apache /usr/local/frsip/gateway

	mkdir -p /var/log/DebugProcess
	mkdir -p /frSIP_backup/current/log/DebugProcess
	chown apache /var/log/DebugProcess

	mkdir -p /usr/local/frsip/ssl
	chown apache /usr/local/frsip/ssl

	mkdir -p /usr/local/frsip/ssl-store
	chown apache /usr/local/frsip/ssl-store

	mkdir -p /usr/local/frsip/faxDocument
	chown apache /usr/local/frsip/faxDocument

	mkdir -p /usr/local/openssl
	rm -rf /usr/local/openssl/certs
	ln -s /etc/ssl/certs /usr/local/openssl/certs

	
	openssl req -new -newkey rsa:4096 -x509 -nodes -out /usr/local/frsip/ssl/server.crt -keyout /usr/local/frsip/ssl/server.key -subj "/C=HK/ST=Hong Kong/L=Hong Kong/O=Deltapath/OU=Deltapath/CN=localhost" -days 1825
	cp /usr/local/frsip/ssl/server.crt /usr/local/frsip/ssl/server.ca-bundle
	openssl x509 -in /usr/local/frsip/ssl/server.crt -out /usr/local/frsip/ssl/server.pem -outform PEM
	openssl rsa -in /usr/local/frsip/ssl/server.key -out /usr/local/frsip/ssl/server.nocrypt.key
	cat /usr/local/frsip/ssl/server.nocrypt.key >> /usr/local/frsip/ssl/server.pem
	rm /usr/local/frsip/ssl/server.nocrypt.key
	chown -R apache /usr/local/frsip/ssl
	chown -R apache /usr/local/frsip/ssl-store
	
	mkdir /var/log/sudo
	mkdir -p /frSIP_backup/current/log/sudo

        chgrp apache /etc/localtime
	chmod 775 /etc/localtime
	
	sed -i 's/^net.ipv4.neigh.default.gc_thresh1=.*//g' /etc/sysctl.conf
	sed -i 's/^net.ipv4.neigh.default.gc_thresh2=.*//g' /etc/sysctl.conf
	sed -i 's/^net.ipv4.neigh.default.gc_thresh3=.*//g' /etc/sysctl.conf
	echo "net.ipv4.neigh.default.gc_thresh1=1024" >> /etc/sysctl.conf
	echo "net.ipv4.neigh.default.gc_thresh2=4096" >> /etc/sysctl.conf
	echo "net.ipv4.neigh.default.gc_thresh3=8192" >> /etc/sysctl.conf
	sysctl -p

	if [ -d /usr/share/fonts/corefonts ]; then
	cp deltapath/files/fonts/* /usr/share/fonts/corefonts/
	fi

	mkdir -p /usr/local/frsip/sfbgateway
	
	cp -R deltapath/files/sfbgateway/* /usr/local/frsip/sfbgateway/
	
	# create the recording directory
	mkdir -p /storage-raid/frSIP/current/sfbgateway/recordings/
	mkdir -p /frSIP_backup/current/sfbgateway/recordings
	chmod 755 /storage-raid/frSIP/current/sfbgateway/recordings/
	chown apache:apache /storage-raid/frSIP/current/sfbgateway/recordings/
	rm -rf /usr/local/frsip/sfbgateway
	ln -s /storage-raid/frSIP/current/sfbgateway /usr/local/frsip/sfbgateway

	# create the FaxDocument directory
	mkdir -p /storage-raid/frSIP/current/faxDocument/
	mkdir -p /frSIP_backup/current/faxDocument
	chmod 755 /storage-raid/frSIP/current/faxDocument/
	chown apache:apache /storage-raid/frSIP/current/faxDocument/
	rm -rf /usr/local/frsip/faxDocument
	ln -s /storage-raid/frSIP/current/faxDocument /usr/local/frsip/faxDocument

	# create the IM directory
	mkdir -p /storage-raid/frSIP/current/im/
	mkdir -p /frSIP_backup/current/im
	chmod 755 /storage-raid/frSIP/current/im/
	chown apache:apache /storage-raid/frSIP/current/im/
	rm -rf /usr/local/frsip/im
	ln -s /storage-raid/frSIP/current/im /usr/local/frsip/im

	# create the IM attachment directory
	mkdir -p /usr/local/frsip/im/attachment/
	mkdir -p /frSIP_backup/current/im/attachment
	chmod 755 /usr/local/frsip/im/attachment/
	chown apache:apache /usr/local/frsip/im/attachment/
	# create the IM avatar directory
	mkdir -p /usr/local/frsip/im/avatar/user/
	mkdir -p /frSIP_backup/current/im/avatar/user
	chmod 755 /usr/local/frsip/im/avatar/user/
	chown apache:apache /usr/local/frsip/im/avatar/user/
	mkdir -p /usr/local/frsip/im/avatar/group/
	mkdir -p /frSIP_backup/current/im/avatar/group
	chmod 755 /usr/local/frsip/im/avatar/group/
	chown apache:apache /usr/local/frsip/im/avatar/group/

	# create the ptt recording folder
	mkdir -p /storage-raid/frSIP/current/ptt/
	mkdir -p /frSIP_backup/current/ptt
	chmod 777 /storage-raid/frSIP/current/ptt/
	chown -R apache:apache /storage-raid/frSIP/current/ptt/
	rm -rf /usr/local/frsip/ptt
	ln -s /storage-raid/frSIP/current/ptt /usr/local/frsip/ptt

	# update administrator password
	#	echo "administrator:CheFR63r" | chpasswd

	# fix the ping command for administrator login
	chmod u+s `which ping`

	#APNs
	cp -Rp deltapath/files/APNsWithHTTP2AuthKey/ /usr/local/frsip/
	mv /usr/local/frsip/APNsWithHTTP2AuthKey/APNSAuthKey.p8 /usr/local/frsip/ssl/APNSAuthKey.p8
	mkdir /var/log/ApplePushNotification
	mkdir -p /frSIP_backup/current/log/ApplePushNotification
	touch /var/log/ApplePushNotification/current
	chown apache:apache /var/log/ApplePushNotification/current
	chmod 664 /var/log/ApplePushNotification/current



	# set limits.conf
	sed -i 's/^\*.*soft.*nofile.*//g' /etc/security/limits.conf
	sed -i 's/^\*.*hard.*nofile.*//g' /etc/security/limits.conf
	sed -i 's/^root.*soft.*nofile.*//g' /etc/security/limits.conf
	sed -i 's/^root.*hard.*nofile.*//g' /etc/security/limits.conf
	echo '*                soft    nofile          30000' >> /etc/security/limits.conf
	echo '*                hard    nofile          30000' >> /etc/security/limits.conf
	echo 'root             soft    nofile          30000' >> /etc/security/limits.conf
	echo 'root             hard    nofile          30000' >> /etc/security/limits.conf
	sed -i 's/^kernel.core_pattern.*//g' /etc/sysctl.conf
	echo 'kernel.core_pattern = core.%e.%s.%t' >> /etc/sysctl.conf
	sed -i 's/^\*.*soft.*core.*//g' /etc/security/limits.conf
	sed -i 's/^\*.*hard.*core.*//g' /etc/security/limits.conf
	sed -i 's/^root.*soft.*core.*//g' /etc/security/limits.conf
	sed -i 's/^root.*hard.*core.*//g' /etc/security/limits.conf
	echo '*                soft    core            unlimited' >> /etc/security/limits.conf
	echo '*                hard    core            unlimited' >> /etc/security/limits.conf
	echo 'root             soft    core            unlimited' >> /etc/security/limits.conf
	echo 'root             hard    core            unlimited' >> /etc/security/limits.conf
	sed -i 's/^net.core.rmem_max.*//g' /etc/sysctl.conf
	echo 'net.core.rmem_max = 16777216' >> /etc/sysctl.conf
	sed -i 's/^net.core.wmem_max.*//g' /etc/sysctl.conf
	echo 'net.core.wmem_max = 16777216' >> /etc/sysctl.conf
}

