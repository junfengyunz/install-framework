#!/bin/bash
#
3rdrpm-asterisk.sh(){
  sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
  yum install -y epel-release dmidecode gcc-c++ ncurses-devel libxml2-devel make wget openssl-devel newt-devel kernel-devel sqlite-devel libuuid-devel gtk2-devel jansson-devel binutils-devel
  yum install -y libsrtp libsrtp-devel spandsp-devel
  rpm -Uvh `pwd`/deltapath/files/libpri-1.4.13-5.gf.el7.x86_64.rpm
  rpm -Uvh `pwd`/deltapath/files/libpri-devel-1.4.13-5.gf.el7.x86_64.rpm
  rpm -Uvh `pwd`/deltapath/files/libresample-0.1.3-20.gf.el7.x86_64.rpm
  rpm -Uvh 'pwd'/deltapath/files/libresample-devel-0.1.3-20.gf.el7.x86_64.rpm
}
