#!/bin/bash
#
homemade.sh(){
	rpm -ivh deltapath/files/libpri-1.4.13-5.gf.el7.x86_64.rpm 
	rpm -ivh deltapath/files/libresample-0.1.3-20.gf.el7.x86_64.rpm
	rpm -ivh deltapath/files/libpri-devel-1.4.13-5.gf.el7.x86_64.rpm
	rpm -ivh deltapath/files/libresample-devel-0.1.3-20.gf.el7.x86_64.rpm 
	rpm -ivh ../bigrpmpackages/dahdi-tools-libs-2.10.0-2.gf.el7.x86_64.rpm 
	rpm -ivh ../bigrpmpackages/dahdi-tools-devel-2.10.0-2.gf.el7.x86_64.rpm
	rpm -ivh ../bigrpmpackages/asterisk-1.6.2.17.3-1.x86_64.rpm
	rpm -ivh ../bigrpmpackages/applianceconfigs-1.0-1.x86_64.rpm --force
}

