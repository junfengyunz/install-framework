#!/bin/bash
#
fail2ban.sh(){
	yum install -y fail2ban fail2ban-systemd
	systemctl enable fail2ban
	systemctl start fail2ban
	cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local
	cp deltapath/files/frsip.conf /etc/fail2ban/jail.d/
	cp deltapath/files/paths-frsip.conf /etc/fail2ban/
	# check storage-raid  /var/lib/fail2ban #1995
	
}

