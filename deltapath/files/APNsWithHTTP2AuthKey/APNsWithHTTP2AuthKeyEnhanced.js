var base64 = {};
base64.encode = function(unencoded) {
    return new Buffer(unencoded || '').toString('base64');
};
base64.decode = function(encoded) {
    return new Buffer(encoded || '', 'base64').toString('utf8');
};

var apn = require('apn');
var fs = require('fs');
if (fs.existsSync(process.argv[2])) {
    fs.readFile(process.argv[2], 'utf8', function(err, data) {
        if (err) {
            return console.log(err);
        }

        var bylines = [];
        if (data.length > 0) {
            bylines = data.split(/\r?\n/);
        }
        if (bylines.length === 0) {
            return ;
        }

        var deviceTokens = null,
            payload = null,
            lineparts = null;

        var provider = new apn.Provider({
            token: {
                key: "/usr/local/frsip/ssl/APNSAuthKey.p8",
                keyId: "7FL8ARJXKA",
                teamId: "YRBJ8RSMVM"
            },
            production: true
        });

        for (var i = 0; i < bylines.length; i++) {
            deviceTokens = null;
            payload = null;

            lineparts = bylines[i].split(' ');
            if (lineparts[0]) {
                payload = JSON.parse(base64.decode(lineparts[0]));
            }
            if (lineparts[1]) {
                deviceTokens = JSON.parse(base64.decode(lineparts[1]));
            }

            if (deviceTokens) {
                var notification = new apn.Notification();
                notification.topic = "com.deltapath.frsipmobile";
                if (typeof lineparts[2] === 'string') {
                    notification.topic = lineparts[2];
                }

                if (payload) {
                    if (payload.aps) {
                        notification.aps = payload.aps;
                        delete payload.aps;
                    }
                    Object.keys(payload).forEach(function(key) {
                        notification.payload[key] = payload[key];
                    });
                }

                provider.send(notification, deviceTokens).then( (response) => {
                    // response.sent: Array of device tokens to which the notification was sent succesfully
                    // response.failed: Array of objects containing the device token (`device`) and either an `error`, or a `status` and `response` from the API
                    // print out the fail result for logging
                    console.log('[' + Date.now() + ']', '\n', '[ARGV]', lineparts, '\n', '[SUCCESS]', response.sent, '\n', '[FAILED]', response.failed, '\n');
                });
            }
        }

        provider.shutdown();

        // remove the data file
        fs.unlink(process.argv[2]);
    });
}
