var base64 = {};
base64.encode = function(unencoded) {
    return new Buffer(unencoded || '').toString('base64');
};
base64.decode = function(encoded) {
    return new Buffer(encoded || '', 'base64').toString('utf8');
};

var apn = require('apn');

var deviceTokens = null,
    payload = null;

if (process.argv[3]) {
    deviceTokens = JSON.parse(base64.decode(process.argv[3]));
}
if (process.argv[2]) {
    payload = JSON.parse(base64.decode(process.argv[2]));
}

if (deviceTokens) {
    var provider = new apn.Provider({
        token: {
            key: "/usr/local/frsip/ssl/APNSAuthKey.p8",
            keyId: "7FL8ARJXKA",
            teamId: "YRBJ8RSMVM"
        },
        production: true
    });

    var notification = new apn.Notification();
    notification.topic = "com.deltapath.frsipmobile";
    if (typeof process.argv[4] === 'string') {
        notification.topic = process.argv[4];
    }

    if (payload) {
        if (payload.aps) {
            notification.aps = payload.aps;
            delete payload.aps;
        }
        Object.keys(payload).forEach(function(key) {
            notification.payload[key] = payload[key];
        });
    }

    provider.send(notification, deviceTokens).then( (response) => {
        // response.sent: Array of device tokens to which the notification was sent succesfully
        // response.failed: Array of objects containing the device token (`device`) and either an `error`, or a `status` and `response` from the API
        // print out the fail result for logging
        console.log('[' + Date.now() + ']', '\n', '[ARGV]', process.argv, '\n', '[SUCCESS]', response.sent, '\n', '[FAILED]', response.failed, '\n');
    });

    provider.shutdown();
}
