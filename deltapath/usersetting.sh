#!/bin/bash
#
usersetting.sh(){
	# NEW : Polycom default username and password
	# Default FTP user account for Polycom
	useradd -g apache -d /tftproot -s /bin/false -m plcmspip
	sed -i 's/^plcmspip/PlcmSpIp/g' /etc/passwd
	sed -i 's/^plcmspip/PlcmSpIp/g' /etc/shadow
	echo "PlcmSpIp:PlcmSpIp" | chpasswd
	usermod -L PlcmSpIp

	# add ftp user for polycom
	POLYCOM_USER=spip$RANDOM
	POLYCOM_PASSWORD=$[RANDOM*RANDOM*RANDOM%100000000]
	useradd -g apache -d /tftproot -s /bin/false -m $POLYCOM_USER
	echo "$POLYCOM_USER:$POLYCOM_PASSWORD" | chpasswd
	# NEW : store the polycom username and password into the database
	#Storing the Polycom Username
	mysql -u frsip -p$MYSQL_USERPW frsip -e "INSERT INTO polycom_setting VALUES ('', 'username', '$POLYCOM_USER');"
	#Storing the Polycom User Password
	mysql -u frsip -p$MYSQL_USERPW frsip -e "INSERT INTO polycom_setting VALUES ('', 'password', '$POLYCOM_PASSWORD');"

	# add the admin password for HDX/Group Provisioning
	POLYCOM_HDX_ADMIN_PW_RAND=$RANDOM
	POLYCOM_HDX_ADMIN_PW=$[RANDOM*RANDOM*RANDOM*RANDOM%100000000]
	#Storing the Polycom HDX/Group Provisioning Password
	mysql -u frsip -p$MYSQL_USERPW frsip -e "INSERT INTO polycom_config (\`model\`, \`path\`, \`value\`) VALUES ('all', '{HDXADMINPASSWORD}', '$POLYCOM_HDX_ADMIN_PW');"

	# set the restriction of the vsftp
	#Setting the /etc/pam.d/ftp
	sed -i 's/^account  required  pam_access.so$//g' /etc/pam.d/ftp
	echo 'account  required  pam_access.so' >> /etc/pam.d/ftp
#	my_printf "Setting the /etc/security/access.conf ...\n"
	sed -i 's/^.*PlcmSpIp.*//g' /etc/security/access.conf
	echo '- : PlcmSpIp : ALL EXCEPT 127.0.0.0/8 192.168.0.0/16 10.0.0.0/8 172.16.0.0/12 169.254.0.0/16' >> /etc/security/access.conf

	# set the default open file limit to 4096 instead of original 1024
#	my_printf "Setting the default open file limit in /etc/security/limits.conf ...\n"
	sed -i 's/^\*.*soft.*nofile.*//g' /etc/security/limits.conf
	sed -i 's/^\*.*hard.*nofile.*//g' /etc/security/limits.conf
	echo '*                soft    nofile          4096' >> /etc/security/limits.conf
	echo '*                hard    nofile          8192' >> /etc/security/limits.conf

}

