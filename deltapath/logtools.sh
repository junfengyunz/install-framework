#!/bin/bash
#
logtools.sh(){

        yum install -y syslog-ng logrotate
	cp deltapath/files/syslog-ng_frsip.conf /etc/syslog-ng/syslog-ng_frsip.conf
	chgrp apache /etc/syslog-ng/syslog-ng_frsip.conf
	chmod 775 /etc/syslog-ng/syslog-ng_frsip.conf
	cp deltapath/files/logrotate.conf /etc/logrotate.conf

}

