#!/bin/bash
#
mediacoder.sh(){
	rpm -v --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
	rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
	yum install ffmpeg ffmpeg-devel -y
	yum install libid3tag -y
	yum install madplay -y
	yum install gstreamer -y
	yum install gstreamer-plugins-base -y
	yum install gstreamer-plugins-good -y
	yum install gstreamer-ffmpeg -y
	yum install -y iso-codes
	
}

