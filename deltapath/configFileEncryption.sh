#!/bin/bash
#
configFileEncryption.sh(){
	
	FRSIPTOOLS='/usr/local/frsip'
	MYSQL_USERPW='dc4e8b64'
	if [ ! -d $FRSIPTOOLS/ConfigFileEncryption ]; then
  	  mkdir -p $FRSIPTOOLS/ConfigFileEncryption
	fi
	cp deltapath/files/ConfigFileEncryption/configFileEncrypt $FRSIPTOOLS/ConfigFileEncryption/
	cp deltapath/files/ConfigFileEncryption/configFileEncrypt $FRSIPTOOLS/ConfigFileEncryption/
	cp deltapath/files/ConfigFileEncryption/configFileEncrypt $FRSIPTOOLS/ConfigFileEncryption/
	chmod 777 $FRSIPTOOLS/ConfigFileEncryption -R
	chown apache:apache -R $FRSIPTOOLS/ConfigFileEncryption
	cp deltapath/files/ConfigFileEncryption/polycomEncryptionKey.key $FRSIPTOOLS/ConfigFileEncryption/
	POLYCOM_ENCRYPT_KEY=`cat deltapath/files/ConfigFileEncryption/polycomEncryptionKey.key`

	mysql -u frsip -p$MYSQL_USERPW frsip -e 'DELETE FROM polycom_setting WHERE `key` = "encryption_key";'
	mysql -u frsip -p$MYSQL_USERPW frsip -e "INSERT INTO polycom_setting VALUES ('', 'encryption_key', '$POLYCOM_ENCRYPT_KEY');"

	
	mkdir -p $FRSIPTOOLS/Cisco7912ConfigFile
	cp -p deltapath/files/Cisco7912ConfigFile/cfgfmt.linux $FRSIPTOOLS/Cisco7912ConfigFile/
	cp -p deltapath/files/Cisco7912ConfigFile/sip_ptag.dat $FRSIPTOOLS/Cisco7912ConfigFile/
	chmod 777 $FRSIPTOOLS/Cisco7912ConfigFile -R
	chown apache:apache -R $FRSIPTOOLS/Cisco7912ConfigFile
}

