#!/bin/bash
#

#
basicdep.sh(){
    yum install -y  build-essential
    yum install -y ftp kernel-devel automake
    yum install -y ncurses-devel newt-devel libuuid-devel jansson-devel libxml2-devel sqlite-devel
    yum install -y libtool-ltdl libtool-ltdl-devel
    yum install -y openssh openssh-server openssh-clients openssl-libs
}
