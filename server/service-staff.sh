#!/bin/bash
#

#
service-staff.sh(){
	yum remove sendmail -y
	yum install postfix -y

	alternatives --set mta /usr/sbin/postfix
        #configration
	touch /etc/postfix/main.cf
	chgrp apache /etc/postfix/main.cf
	chmod 664 /etc/postfix/main.cf
	touch /etc/postfix/saslpass
	chgrp apache /etc/postfix/saslpass
	chmod 664 /etc/postfix/saslpass
	service postfix restart
	chkconfig postfix on
	iptables -A INPUT -m state --state NEW -m tcp -p tcp --dport 25 -j ACCEPT
	iptables -A INPUT -m state --state NEW -m udp -p udp --dport 25 -j ACCEPT
}

