#!/bin/bash
#

#Import basicdep
. server/basicdep.sh

# Import curl
. server/curl.sh

# Import lamp
. server/lamp.sh

. server/dbinit.sh

. server/firewall-network.sh

. server/monitor.sh

. server/service-staff.sh

