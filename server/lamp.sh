#!/bin/bash
#
#

lamp.sh(){
  	#rpm -ivh ../install-files/mysql-community-common-5.7.26-1.el7.x86_64.rpm
        #rpm -ivh ../install-files/mysql-community-client-5.7.26-1.el7.x86_64.rpm
        #rpm -ivh ../install-files/mysql-community-server-5.7.26-1.el7.x86_64.rpm
	yum install -y https://repo.mysql.com//mysql80-community-release-el7-3.noarch.rpm
	#yum localinstall mysql80-community-release-el7-3.noarch.rpm
	yum-config-manager --disable mysql80-community
	yum-config-manager --enable mysql56-community
	yum install -y mysql-community-server
        systemctl enable mysqld
        systemctl start mysqld
	mysql_secure_installation
	cp server/files/my.cnf /etc/my.cnf 
	cp server/files/my.cnf /etc/mysql/my.cnf
	cp server/files/mysql /etc/init.d/
	chmod 777 /etc/init.d/mysql
 	       
	yum install -y http://rpms.remirepo.net/enterprise/remi-release-7.rpm
	yum-config-manager --disable remi-php54
	yum-config-manager --enable remi-php73
	yum install -y php httpd php-devel
	yum install -y php-mysql php-xml php-xmlrpc php-soap php-gd php-cli php-cli
	yum install -y php-pecl-dbase
	yum install -y php-pear
	pecl install dbase
	pecl install inotify
	cp server/files/ioncube/ioncube_loader_lin_7.3*.so /usr/lib64/php/modules
	sed -i '$ a zend_extension = /usr/lib64/php/modules/ioncube_loader_lin_7.3.so' /etc/php.ini
	sed -i '$ a extension=inotify.so' /etc/php.ini
	pear install Config # please refer line 553
        systemctl enable httpd.service
	systemctl start httpd.service
	firewall-cmd --permanent --zone=public --add-service=http
	firewall-cmd --permanent --zone=public --add-service=https
	firewall-cmd --reload
        clear;
}

