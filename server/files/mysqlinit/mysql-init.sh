MYSQL_USERPW='dc4e8b64'
MYSQL_PASSWORD='davpathong'
LOGFILE=./mysql.install.log 
function my_printf() {
    exec 1>&6
    printf "$*"
    exec 1>> ${LOGFILE}
    printf "$*"
}

exec 6>&1   # Save stdout to descriptor #6

exec 1>> ${LOGFILE} # redirect stdout to log file
my_printf "\n\n"
my_printf "===============================================================\n"
my_printf "`date`\n"
my_printf "===============================================================\n"

# MySQL
my_printf "\nEstablishing MySQL Database\n"
my_printf "Update the MySQL system table structure\n"
#mysql_fix_privilege_tables  --password=$MYSQL_PASSWORD
my_printf "Creating frsip user account \n"
MYSQL_DROP_DB="drop database if exists frsip; drop database if exists frsip_private; drop database if exists crm; drop database if exists ejabberd;"
MYSQL_CREATE_DB="create database if not exists frsip; create database if not exists frsip_private; create database if not exists crm; create database if not exists ejabberd;"
MYSQL_CREATE_USER="GRANT ALL PRIVILEGES ON frsip.* TO 'frsip'@'localhost' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;GRANT ALL PRIVILEGES ON frsip_private.* TO 'frsip'@'localhost' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;GRANT ALL PRIVILEGES ON ejabberd.* TO 'frsip'@'localhost' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;GRANT ALL PRIVILEGES ON frsip.* TO 'frsip'@'192.168.80.1' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ; GRANT ALL PRIVILEGES ON frsip.* TO 'frsip'@'192.168.80.3' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;GRANT ALL PRIVILEGES ON crm.* TO 'frsip'@'localhost' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;GRANT ALL PRIVILEGES ON crm.* TO 'frsip'@'192.168.80.1' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;GRANT ALL PRIVILEGES ON crm.* TO 'frsip'@'192.168.80.3' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;GRANT ALL PRIVILEGES ON ejabberd.* TO 'frsip'@'192.168.80.1' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;GRANT ALL PRIVILEGES ON ejabberd.* TO 'frsip'@'192.168.80.3' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;FLUSH PRIVILEGES;"
MYSQL_CREATE_SLAVE_USER='GRANT SUPER, REPLICATION SLAVE ON *.* TO "frsip"@"192.168.80.1" IDENTIFIED BY "'$MYSQL_USERPW'"; GRANT SUPER, REPLICATION SLAVE ON *.* TO "frsip"@"192.168.80.3" IDENTIFIED BY "'$MYSQL_USERPW'"; FLUSH PRIVILEGES;'
MYSQL_CREATE_REDUNDANCY='GRANT SUPER, CREATE USER, RELOAD, REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO "frsip"@"localhost" IDENTIFIED BY "'$MYSQL_USERPW'" WITH GRANT OPTION; FLUSH PRIVILEGES;'
echo $MYSQL_DROP_DB | mysql -u root -p$MYSQL_PASSWORD
echo $MYSQL_CREATE_DB | mysql -u root -p$MYSQL_PASSWORD
echo "$MYSQL_CREATE_USER" | mysql -u root -p$MYSQL_PASSWORD
echo "$MYSQL_CREATE_SLAVE_USER" | mysql -u root -p$MYSQL_PASSWORD
echo "$MYSQL_CREATE_REDUNDANCY" | mysql -u root -p$MYSQL_PASSWORD
sed -i 's/^bind-address.*$/bind-address = 0.0.0.0/g' /etc/mysql/my.cnf
sed -i 's/^server-id.*$/server-id = 1/g' /etc/mysql/my.cnf
sed -i 's/^binlog_do_db.*$//g' /etc/mysql/my.cnf
sed -i 's/^auto_increment_increment.*$//g' /etc/mysql/my.cnf
sed -i 's/^auto_increment_offset.*$//g' /etc/mysql/my.cnf
sed -i 's/^master-connect-retry.*$//g' /etc/mysql/my.cnf
sed -i 's/^replicate-do-db.*$//g' /etc/mysql/my.cnf
sed -i 's/^log-bin.*$/log-bin = mysql-bin\nauto_increment_increment = 1\nauto_increment_offset = 1\nmaster-connect-retry = 60\nreplicate-ignore-db = information_schema\nreplicate-ignore-db = mysql\nreplicate-ignore-db = frsip_private\nreplicate-ignore-db = crm\nexpire_logs_days = 10\nmax_binlog_size = 100000000/g' /etc/mysql/my.cnf
sed -i 's/^innodb_data_file_path.*$/innodb_data_file_path = ibdata1:10M:autoextend:max:12800M/g' /etc/mysql/my.cnf
echo "set global max_connections = 1024;" | mysql -u root -p$MYSQL_PASSWORD
DB_MAX_CON=(`grep max_connection /etc/mysql/my.cnf`)
if [ ! -z ${DB_MAX_CON} ]; then
    sed -i 's/^max_connection.*$/max_connection = 1024/g' /etc/mysql/my.cnf
else
    echo "[mysqld]" >> /etc/mysql/my.cnf
    echo "max_connection = 1024" >> /etc/mysql/my.cnf
fi
/etc/init.d/mysql restart
mysql -u frsip -p$MYSQL_USERPW frsip_private < database/tables_private.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/tables.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/d100.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/data.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/hkbu_tables.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/hkbu_storedProcedures.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/updateSQL.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/defaultData.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/storedProcedures.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/storedProcedures_frSIP_Scheduler.sql
mysql -u frsip -p$MYSQL_USERPW frsip < database/d100_storedProcedures.sql
# sugar crm
mysql -u frsip -p$MYSQL_USERPW crm < database/crm.sql

echo "UPDATE watchdog_config SET value='${MODEL}${MODEL_SSD}' WHERE item='model_no';" | mysql -u frsip -p$MYSQL_USERPW frsip

# Customize DB
MYSQL_USER_DBA_PW='qsu987f2'
MYSQL_DROP_USER_ADMIN="DROP USER 'db_admin'@'localhost';"
MYSQL_CREATE_USER_ADMIN="CREATE USER 'db_admin'@'localhost';"
MYSQL_GRANT_USER_ADMIN="GRANT CREATE, DROP, ALTER, INSERT, LOCK TABLES ON \`customize\_db\_%\`.* TO 'db_admin'@'localhost' IDENTIFIED BY '$MYSQL_USER_DBA_PW' WITH GRANT OPTION ;"
echo $MYSQL_DROP_USER_ADMIN | mysql -u root -p$MYSQL_PASSWORD
echo $MYSQL_CREATE_USER_ADMIN | mysql -u root -p$MYSQL_PASSWORD
echo $MYSQL_GRANT_USER_ADMIN | mysql -u root -p$MYSQL_PASSWORD

MYSQL_USER_DATA_DBA_PW='cds1036y'
MYSQL_DROP_USER_DATA_ADMIN="DROP USER 'db_data_admin'@'localhost';"
MYSQL_CREATE_USER_DATA_ADMIN="CREATE USER 'db_data_admin'@'localhost';"
MYSQL_GRANT_USER_DATA_ADMIN="GRANT SELECT, INSERT, UPDATE, DELETE, LOCK TABLES ON \`customize\_db\_%\`.* TO 'db_data_admin'@'localhost' IDENTIFIED BY '$MYSQL_USER_DATA_DBA_PW';"
echo $MYSQL_DROP_USER_DATA_ADMIN | mysql -u root -p$MYSQL_PASSWORD
echo $MYSQL_CREATE_USER_DATA_ADMIN | mysql -u root -p$MYSQL_PASSWORD
echo $MYSQL_GRANT_USER_DATA_ADMIN | mysql -u root -p$MYSQL_PASSWORD

MYSQL_GRANT_USER_FRSIP="GRANT ALL PRIVILEGES ON \`customize\_db\_%\`.* TO 'frsip'@'localhost' IDENTIFIED BY '$MYSQL_USERPW' WITH GRANT OPTION ;"
echo $MYSQL_GRANT_USER_FRSIP | mysql -u root -p$MYSQL_PASSWORD

MYSQL_FLUSH="FLUSH PRIVILEGES;";
echo $MYSQL_FLUSH | mysql -u root -p$MYSQL_PASSWORD

my_printf "Finish establishing MySQL Database\n"
