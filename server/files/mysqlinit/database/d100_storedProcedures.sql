DROP PROCEDURE IF EXISTS d100_acquireAccount;
DELIMITER //
CREATE PROCEDURE d100_acquireAccount(IN in_udid TEXT, IN in_device TEXT)
BEGIN

    DECLARE out_error INT;
    DECLARE out_sip_username TEXT;
    DECLARE out_sip_password TEXT;
    DECLARE val_account_id INT;
    DECLARE val_device_id INT;
    DECLARE val_device_status TEXT;

    SET out_error = 0;
    SET out_sip_username = '';
    SET out_sip_password = '';
    SET val_account_id = 0;
    SET val_device_id = 0;
    SET val_device_status = '';

    -- LOCK TABLE d100_sip_account WRITE;


    SELECT id, status INTO val_device_id, val_device_status FROM d100_user_device WHERE udid = in_udid AND device = in_device LIMIT 1;

    SELECT d100_sip_account.id, d100_sip_account.sip_username, d100_sip_account.sip_password INTO val_account_id, out_sip_username, out_sip_password FROM d100_user_device LEFT JOIN d100_device_account_relation ON d100_user_device.id = d100_device_account_relation.user_device_id LEFT JOIN d100_sip_account ON d100_device_account_relation.sip_account_id = d100_sip_account.id WHERE d100_user_device.udid = in_udid AND d100_user_device.device = in_device AND d100_user_device.status = 'verified' AND d100_sip_account.status != 'locked' LIMIT 1;

    IF val_account_id <= 0 THEN
        IF val_device_id <= 0 THEN
            -- no more guest
            /*
            SET out_error = 1;
            SET out_sip_username = '';
            SET out_sip_password = '';
            */
            -- guest
            SELECT id, sip_username, sip_password INTO val_account_id, out_sip_username, out_sip_password FROM d100_sip_account WHERE status = 'idle' AND id NOT IN (SELECT sip_account_id FROM d100_device_account_relation) ORDER BY last_used LIMIT 1;
            IF val_account_id <= 0 THEN
                -- no idle without mapped to device
                SELECT id, sip_username, sip_password INTO val_account_id, out_sip_username, out_sip_password FROM d100_sip_account WHERE status = 'idle' ORDER BY last_used LIMIT 1;
                IF val_account_id <= 0 THEN
                    -- no idle can use
                    SET out_error = 1;
                    SET out_sip_username = '';
                    SET out_sip_password = '';
                ELSE
                    -- there is an idle but mapped to device, can use
                    DELETE FROM d100_device_account_relation WHERE sip_account_id = val_account_id;
                    UPDATE d100_sip_account SET status = 'inuse', last_used = now(), device = in_device, udid = in_udid WHERE id = val_account_id;
                END IF;
            ELSE
                -- get and idle account
                UPDATE d100_sip_account SET status = 'inuse', last_used = now(), device = in_device, udid = in_udid WHERE id = val_account_id;
            END IF;
        ELSE
            IF val_device_status != 'verified' THEN
                -- need to wait for user
                SET out_error = 2;
                SET out_sip_username = '';
                SET out_sip_password = '';
            ELSE
                -- verified, but no sip account attached
                SELECT id, sip_username, sip_password INTO val_account_id, out_sip_username, out_sip_password FROM d100_sip_account WHERE status = 'idle' AND id NOT IN (SELECT sip_account_id FROM d100_device_account_relation) ORDER BY last_used LIMIT 1;
                IF val_account_id <= 0 THEN
                    -- no idle without mapped to device
                    SELECT id, sip_username, sip_password INTO val_account_id, out_sip_username, out_sip_password FROM d100_sip_account WHERE status = 'idle' ORDER BY last_used LIMIT 1;
                    IF val_account_id <= 0 THEN
                        -- no idle can use
                        SET out_error = 1;
                        SET out_sip_username = '';
                        SET out_sip_password = '';
                    ELSE
                        -- there is an idle but mapped to device, impossible...
                        DELETE FROM d100_device_account_relation WHERE sip_account_id = val_account_id;
                        UPDATE d100_sip_account SET status = 'inuse', last_used = now(), device = in_device, udid = in_udid WHERE id = val_account_id;
                        DELETE FROM d100_device_account_relation WHERE user_device_id = val_device_id;
                        INSERT INTO d100_device_account_relation (id, user_device_id, sip_account_id) VALUES ('', val_device_id, val_account_id);
                    END IF;
                ELSE
                    -- get and idle account
                    UPDATE d100_sip_account SET status = 'inuse', last_used = now(), device = in_device, udid = in_udid WHERE id = val_account_id;
                    DELETE FROM d100_device_account_relation WHERE user_device_id = val_device_id;
                    INSERT INTO d100_device_account_relation (id, user_device_id, sip_account_id) VALUES ('', val_device_id, val_account_id);
                END IF;
            END IF;
        END IF;
    ELSE
        -- already has the sip account mapping
        UPDATE d100_sip_account SET status = 'inuse', last_used = now(), device = in_device, udid = in_udid WHERE id = val_account_id;
        UPDATE d100_user_device SET login_count = login_count + 1, last_login = now() WHERE id = val_device_id;
    END IF;

    -- UNLOCK TABLES;

    SELECT out_error, out_sip_username, out_sip_password;

END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS d100_releaseAccount;
DELIMITER //
CREATE PROCEDURE d100_releaseAccount(IN in_udid TEXT, IN in_device TEXT)
BEGIN

    DECLARE out_error INT;
    DECLARE val_device_id INT;

    SET out_error = 0;
    SET val_device_id = 0;

    SELECT id INTO val_device_id FROM d100_user_device WHERE udid = in_udid AND device = in_device LIMIT 1;

    IF val_device_id > 0 THEN
        UPDATE d100_sip_account SET status = 'idle' WHERE id IN (SELECT sip_account_id FROM d100_device_account_relation WHERE user_device_id = val_device_id);
    ELSE
        UPDATE d100_sip_account SET status = 'idle' WHERE device = in_device AND udid = in_udid;
    END IF;

    SELECT out_error;

END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS d100_removeVerification;
DELIMITER //
CREATE PROCEDURE d100_removeVerification(IN in_udid TEXT, IN in_device TEXT)
BEGIN

    DECLARE out_error INT;
    DECLARE val_device_id INT;
    DECLARE val_user_id INT;
    DECLARE val_device_cnt INT;

    SET out_error = 0;
    SET val_device_id = 0;
    SET val_user_id = 0;
    SET val_device_cnt = 0;

    SELECT id, user_id INTO val_device_id, val_user_id FROM d100_user_device WHERE udid = in_udid AND device = in_device LIMIT 1;

    IF val_device_id > 0 THEN
        DELETE FROM d100_user_device WHERE id = val_device_id;
        SELECT COUNT(*) INTO val_device_cnt FROM d100_user_device WHERE user_id = val_user_id;
        IF val_device_cnt = 0 THEN
            DELETE FROM d100_user WHERE id = val_user_id;
        END IF;
        UPDATE d100_sip_account SET status = 'idle' WHERE id IN (SELECT sip_account_id FROM d100_device_account_relation WHERE user_device_id = val_device_id);
        DELETE FROM d100_device_account_relation WHERE user_device_id = val_device_id;
    END IF;

    SELECT out_error;

END;
//
DELIMITER ;

