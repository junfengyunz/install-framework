DROP TABLE IF EXISTS `uc_user_profile`;
CREATE TABLE `uc_user_profile` (
  `ssoid` varchar(32) NOT NULL,
  `employee_no` varchar(32) NOT NULL,
  `employee_type` varchar(20) NOT NULL,
  `full_name` varchar(127) NOT NULL,
  `faculty_office_resrch_cntr` varchar(10) NOT NULL,
  `dept_unit_code` varchar(10) NOT NULL,
  `staff_role` numeric(2,0) NULL,
  `rec_id` varchar(1) NULL,
  `sty_pgm` varchar(10) NULL,
  `sty_year` integer(4) NULL,
  `sty_level` varchar(4) NULL,
  `admin` varchar(4) NULL,
  `email` varchar(100) NULL,
  `tel_no` varchar(30) NULL,
  `data_src` varchar(20) NULL,
  `acct_status` varchar(1) NOT NULL,
  `ssoid_passkey_hash` varchar(64) NOT NULL,
  `sip_user_profile` varchar(30) NULL,
  `sip_mac_address` varchar(12) NULL,
  `sip_phone_model` varchar(30) NULL,
  `sip_tel_no` varchar(30) NULL,
  `sip_server_ip` varchar(15) NULL,
  `sip_default_status` varchar(30) NULL,
  `sip_line_number` varchar(2) NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`ssoid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_user_feature_list`;
CREATE TABLE `uc_user_feature_list` (
  `ssoid` varchar(32) NOT NULL,
  `employee_no` varchar(32) NOT NULL,
  `audio_call` varchar(1) NOT NULL,
  `video_call` varchar(1) NOT NULL,
  `host_video_conference` varchar(1) NOT NULL,
  `share_home_screen` varchar(1) NOT NULL,
  `white_board_annotation` varchar(1) NOT NULL,
  `dialpad_ssoid` varchar(1) NOT NULL,
  `im_text` varchar(1) NOT NULL,
  `im_emoji` varchar(1) NOT NULL,
  `im_file_attach` varchar(1) NOT NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`ssoid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_hotlines`;
CREATE TABLE `uc_hotlines` (
  `hotline_id` int(11) NOT NULL auto_increment,
  `sort_order` int(11) NOT NULL,
  `hotline_name` varchar(150) NOT NULL,
  `hotline_desc` varchar(150) NOT NULL,
  `hotline_ext` varchar(10) NOT NULL,
  `hotline_email` varchar(100) NOT NULL,
  `resource_path` varchar(255) NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`hotline_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_hotlines_agent`;
CREATE TABLE `uc_hotlines_agent` (
  `hotline_id` int(11) NOT NULL,
  `ssoid` varchar(32) NOT NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`hotline_id`, `ssoid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_hotlines_allow_group`;
CREATE TABLE `uc_hotlines_allow_group` (
  `hotline_id` int(11) NOT NULL,
  `group_name` varchar(128) NOT NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`hotline_id`, `group_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_hotlines_case`;
CREATE TABLE `uc_hotlines_case` (
  `hotline_id` int(11) NOT NULL,
  `student_ssoid` varchar(32) NOT NULL,
  `agent_ssoid` varchar(32) NOT NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`hotline_id`, `student_ssoid`, `agent_ssoid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_group_mast`;
CREATE TABLE `uc_group_mast` (
  `group_name` varchar(128) NOT NULL,
  `group_desc` varchar(128) NOT NULL,
  `group_type` varchar(30) NOT NULL,
  `contact_group` varchar(1) NOT NULL,
  `group_avatar` text NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`group_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_group_member`;
CREATE TABLE `uc_group_member` (
  `group_name` varchar(128) NOT NULL,
  `ssoid` varchar(32) NOT NULL,
  `employee_no` varchar(32) NOT NULL,
  `member_type` varchar(10) NOT NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`group_name`, `ssoid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_blocklist`;
CREATE TABLE `uc_blocklist` (
  `from_ssoid` varchar(32) NOT NULL,
  `to_ssoid` varchar(32) NOT NULL,
  `block_type` varchar(20) NOT NULL,
  `auto_reply_im` varchar(100) NULL,
  `remarks` varchar(100) NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`from_ssoid`, `to_ssoid`, `block_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_mesg_broadcast_group`;
CREATE TABLE `uc_mesg_broadcast_group` (
  `ssoid` varchar(32) NOT NULL,
  `group_name` varchar(128) NOT NULL,
  `employee_no` varchar(32) NOT NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`ssoid`, `group_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_system_config`;
CREATE TABLE `uc_system_config` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `key` text NOT NULL,
  `value` text NULL,
  `last_mod_date` bigint(8) NULL,
  `last_mod_user` varchar(32) NULL,
  `as_of_date` bigint(8) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_user_auth`;
CREATE TABLE `uc_user_auth` (
    `ssoid` varchar(32) NOT NULL,
    `password` varchar(32) NOT NULL,
    `last_login` bigint(8) NOT NULL,
    PRIMARY KEY  (`ssoid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_acquired_account`;
CREATE TABLE `uc_acquired_account` (
    `ssoid` varchar(32) NOT NULL,
    `sip_username` varchar(128) NOT NULL,
    `acquire_time` bigint(8) NOT NULL,
    PRIMARY KEY  (`sip_username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_user_session`;
CREATE TABLE `uc_user_session` (
    `ssoid` varchar(32) NOT NULL,
    `sessionId` varchar(32) NOT NULL,
    `ip` varchar(20) NOT NULL,
    `device` TEXT NULL,
    `model` TEXT NULL,
    `udid` TEXT NULL,
    `as_of_date` bigint(8) NOT NULL,
    PRIMARY KEY  (`sessionId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_chat_log`;
CREATE TABLE `uc_chat_log` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `time` datetime NULL,
    `fromJid` varchar(64) NOT NULL,
    `toJid` varchar(64) NOT NULL,
    `message` TEXT NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_api_action_log`;
CREATE TABLE `uc_api_action_log` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `time` datetime NULL,
    `ssoid` varchar(32) NOT NULL,
    `ip` varchar(20) NOT NULL,
    `client` varchar(10) NOT NULL,
    `action` varchar(32) NOT NULL,
    `arg1` varchar(100) NOT NULL,
    `arg2` varchar(100) NOT NULL,
    `arg3` varchar(100) NOT NULL,
    `arg4` varchar(100) NOT NULL,
    `arg5` varchar(100) NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE uc_chat_log ADD INDEX USING BTREE (fromJid);
ALTER TABLE uc_chat_log ADD INDEX USING BTREE (toJid);

DROP TABLE IF EXISTS `uc_sync_log`;
CREATE TABLE `uc_sync_log` (
    `id` bigint(20) unsigned NOT NULL auto_increment,
    `time` datetime NOT NULL default '0000-00-00 00:00:00',
    `pid` int(8) NOT NULL,
    `type` varchar(20) NOT NULL,
    `state` varchar(20) NOT NULL,
    `message` text NOT NULL,
    PRIMARY KEY  (`id`),
    UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `uc_full_chat_log`;
CREATE TABLE `uc_full_chat_log` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `time` datetime NULL,
    `nanosecond` varchar(20) default '',
    `fromJid` varchar(128) NOT NULL,
    `toJid` varchar(128) NOT NULL,
    `msgId` varchar(128) NOT NULL,
    `msgBody` TEXT NOT NULL,
    `receivedAt` datetime NULL,
    `command` varchar(128) NOT NULL,
    `arg1` varchar(128) NOT NULL,
    `arg2` varchar(128) NOT NULL,
    `arg3` varchar(128) NOT NULL,
    `arg4` varchar(128) NOT NULL,
    `xml` TEXT NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE uc_full_chat_log ADD INDEX USING BTREE (fromJid);
ALTER TABLE uc_full_chat_log ADD INDEX USING BTREE (toJid);
ALTER TABLE uc_full_chat_log ADD INDEX USING BTREE (msgId);
ALTER TABLE uc_full_chat_log ADD INDEX USING BTREE (command);

DROP TABLE IF EXISTS `uc_full_groupchat_log`;
CREATE TABLE `uc_full_groupchat_log` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `time` datetime NULL,
    `nanosecond` varchar(20) default '',
    `fromJid` varchar(128) NOT NULL,
    `toJid` varchar(128) NOT NULL,
    `msgId` varchar(128) NOT NULL,
    `msgBody` TEXT NOT NULL,
    `command` varchar(128) NOT NULL,
    `arg1` varchar(128) NOT NULL,
    `arg2` varchar(128) NOT NULL,
    `arg3` varchar(128) NOT NULL,
    `arg4` varchar(128) NOT NULL,
    `xml` TEXT NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE uc_full_groupchat_log ADD INDEX USING BTREE (fromJid);
ALTER TABLE uc_full_groupchat_log ADD INDEX USING BTREE (toJid);
ALTER TABLE uc_full_groupchat_log ADD INDEX USING BTREE (msgId);
ALTER TABLE uc_full_groupchat_log ADD INDEX USING BTREE (command);

DROP TABLE IF EXISTS `uc_full_groupchat_log_detail`;
CREATE TABLE `uc_full_groupchat_log_detail` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `fromJid` varchar(128) NOT NULL,
    `toJid` varchar(128) NOT NULL,
    `msgId` varchar(128) NOT NULL,
    `receivedAt` datetime NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE uc_full_groupchat_log_detail ADD INDEX USING BTREE (fromJid);
ALTER TABLE uc_full_groupchat_log_detail ADD INDEX USING BTREE (toJid);
ALTER TABLE uc_full_groupchat_log_detail ADD INDEX USING BTREE (msgId);

DROP TABLE IF EXISTS `uc_full_groupchat_log_distribution`;
CREATE TABLE `uc_full_groupchat_log_distribution` (
  `id` int(11) NOT NULL auto_increment,
  `ssoid` varchar(32) NOT NULL,
  `msgId` varchar(128) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `uc_full_groupchat_log_distribution` ADD INDEX USING BTREE (ssoid);
ALTER TABLE `uc_full_groupchat_log_distribution` ADD INDEX USING BTREE (msgId);
