DROP TABLE IF EXISTS `server_info`;
CREATE TABLE `server_info` (
    `server_id` INT(11) NOT NULL,
    PRIMARY KEY (`server_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `server_info` VALUES ('1');

DROP TABLE IF EXISTS `redundancy_provision_info`;
CREATE TABLE `redundancy_provision_info` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `ip` varchar(100) NOT NULL,
    `dataSet` enum('0', '1') NOT NULL DEFAULT '1',
    `role` enum('main', 'backup') NOT NULL DEFAULT 'main',
    `myself` enum('0', '1') NOT NULL DEFAULT '1',
    `ipTakeOver` enum ('0', '1', '2', '3') NOT NULL default '0',
    `autoFallBack` enum('0', '1') NOT NULL default '0',
    `internalEth` varchar(2),
    `dataSetRsync` enum ('0', '1') NOT NULL default '0',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `redundancy_provision_var`;
CREATE TABLE `redundancy_provision_var` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `variable` text NOT NULL,
    `value` text NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
