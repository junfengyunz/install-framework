-- MySQL dump 10.11
--
-- Host: localhost    Database: crm
-- ------------------------------------------------------
-- Server version	5.0.90-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `accounts`
--

DROP TABLE IF EXISTS `accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts` (
  `id` char(36) NOT NULL,
  `name` varchar(150) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `account_type` varchar(50) default NULL,
  `industry` varchar(50) default NULL,
  `annual_revenue` varchar(25) default NULL,
  `phone_fax` varchar(25) default NULL,
  `billing_address_street` varchar(150) default NULL,
  `billing_address_city` varchar(100) default NULL,
  `billing_address_state` varchar(100) default NULL,
  `billing_address_postalcode` varchar(20) default NULL,
  `billing_address_country` varchar(255) default NULL,
  `rating` varchar(25) default NULL,
  `phone_office` varchar(25) default NULL,
  `phone_alternate` varchar(25) default NULL,
  `website` varchar(255) default NULL,
  `ownership` varchar(100) default NULL,
  `employees` varchar(10) default NULL,
  `ticker_symbol` varchar(10) default NULL,
  `shipping_address_street` varchar(150) default NULL,
  `shipping_address_city` varchar(100) default NULL,
  `shipping_address_state` varchar(100) default NULL,
  `shipping_address_postalcode` varchar(20) default NULL,
  `shipping_address_country` varchar(255) default NULL,
  `parent_id` char(36) default NULL,
  `sic_code` varchar(10) default NULL,
  `campaign_id` char(36) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_accnt_id_del` (`id`,`deleted`),
  KEY `idx_accnt_assigned_del` (`deleted`,`assigned_user_id`),
  KEY `idx_accnt_parent_id` (`parent_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts`
--

LOCK TABLES `accounts` WRITE;
/*!40000 ALTER TABLE `accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_audit`
--

DROP TABLE IF EXISTS `accounts_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime default NULL,
  `created_by` varchar(36) default NULL,
  `field_name` varchar(100) default NULL,
  `data_type` varchar(100) default NULL,
  `before_value_string` varchar(255) default NULL,
  `after_value_string` varchar(255) default NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_audit`
--

LOCK TABLES `accounts_audit` WRITE;
/*!40000 ALTER TABLE `accounts_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_bugs`
--

DROP TABLE IF EXISTS `accounts_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_bugs` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) default NULL,
  `bug_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_acc_bug_acc` (`account_id`),
  KEY `idx_acc_bug_bug` (`bug_id`),
  KEY `idx_account_bug` (`account_id`,`bug_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_bugs`
--

LOCK TABLES `accounts_bugs` WRITE;
/*!40000 ALTER TABLE `accounts_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_cases`
--

DROP TABLE IF EXISTS `accounts_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_cases` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) default NULL,
  `case_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_acc_case_acc` (`account_id`),
  KEY `idx_acc_acc_case` (`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_cases`
--

LOCK TABLES `accounts_cases` WRITE;
/*!40000 ALTER TABLE `accounts_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_contacts`
--

DROP TABLE IF EXISTS `accounts_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) default NULL,
  `account_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_account_contact` (`account_id`,`contact_id`),
  KEY `idx_contid_del_accid` (`contact_id`,`deleted`,`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_contacts`
--

LOCK TABLES `accounts_contacts` WRITE;
/*!40000 ALTER TABLE `accounts_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `accounts_opportunities`
--

DROP TABLE IF EXISTS `accounts_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `accounts_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) default NULL,
  `account_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_account_opportunity` (`account_id`,`opportunity_id`),
  KEY `idx_oppid_del_accid` (`opportunity_id`,`deleted`,`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `accounts_opportunities`
--

LOCK TABLES `accounts_opportunities` WRITE;
/*!40000 ALTER TABLE `accounts_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `accounts_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_actions`
--

DROP TABLE IF EXISTS `acl_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_actions` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) default NULL,
  `name` varchar(150) default NULL,
  `category` varchar(100) default NULL,
  `acltype` varchar(100) default NULL,
  `aclaccess` int(3) default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_aclaction_id_del` (`id`,`deleted`),
  KEY `idx_category_name` (`category`,`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_actions`
--

LOCK TABLES `acl_actions` WRITE;
/*!40000 ALTER TABLE `acl_actions` DISABLE KEYS */;
INSERT INTO `acl_actions` VALUES ('48271fe3-9c72-356e-b9b6-4c0355b4078d','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Leads','module',89,0),('4861c268-ebab-8647-5e75-4c0355c56eb0','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Leads','module',90,0),('492997cb-016c-dd5c-2e71-4c03554cae45','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Leads','module',90,0),('49613db0-97e9-438d-6ff6-4c0355ed210d','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Leads','module',90,0),('4996183e-46a0-9228-8ad2-4c0355849dd8','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Leads','module',90,0),('49c9c56e-3526-5207-7a17-4c0355c97b22','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Leads','module',90,0),('49fc7bd4-945c-70a0-f237-4c035581c102','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Leads','module',90,0),('5318c506-9cf4-cc52-78b8-4c0355f04cd1','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Contacts','module',89,0),('5351606e-8640-fc23-4850-4c0355099a1f','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Contacts','module',90,0),('53857c91-6a34-5683-d627-4c0355439e21','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Contacts','module',90,0),('53b8ce9b-6cbf-5d49-93c1-4c03553eb7ab','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Contacts','module',90,0),('53ec6c34-f0e4-333d-4d3b-4c0355720d5f','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Contacts','module',90,0),('541fb89d-8cd6-9c07-4bae-4c0355b95c6c','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Contacts','module',90,0),('5452a501-4f4a-b562-86e5-4c0355f15ab2','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Contacts','module',90,0),('5c0bf1cf-9dfa-939f-a114-4c0355389b28','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Accounts','module',89,0),('5c447a38-769a-d407-cb80-4c03550782bc','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Accounts','module',90,0),('5c786a0c-4f83-1d39-7138-4c0355c33f2a','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Accounts','module',90,0),('5cab98b0-d48f-6d36-5f86-4c03553497f7','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Accounts','module',90,0),('5cde3fb4-145f-9bbe-9c40-4c0355ecace1','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Accounts','module',90,0),('5d12d716-c3c4-9f40-fa40-4c0355c5b25b','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Accounts','module',90,0),('5d461360-e7d7-d7f3-1426-4c0355e8e8ac','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Accounts','module',90,0),('64c5d47f-d30c-eaa8-6f69-4c03555dd181','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Opportunities','module',89,0),('6500d287-d410-1817-5735-4c03559ab839','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Opportunities','module',90,0),('65360c3a-43b9-5c5d-6c42-4c0355757b31','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Opportunities','module',90,0),('656ca192-d6b8-1590-96e7-4c035582ff88','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Opportunities','module',90,0),('65a26fda-38c5-87f6-80ad-4c035576c44c','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Opportunities','module',90,0),('65d6cdd2-a826-9193-c7de-4c03558dc527','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Opportunities','module',90,0),('660b4464-e628-d00a-8745-4c0355d3df28','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Opportunities','module',90,0),('6d55b15f-6a9e-1dcb-52e4-4c035538f381','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Cases','module',89,0),('6d8f4d41-baaa-8b96-a7b4-4c03552a20bc','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Cases','module',90,0),('6dc4902b-28be-4f3f-ad74-4c03558a2489','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Cases','module',90,0),('6df9222f-5ac3-f953-e68c-4c035539a3ce','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Cases','module',90,0),('6e2db2aa-0d6b-fd0a-4489-4c035587285e','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Cases','module',90,0),('6e61e342-5b8e-e2ea-a2e8-4c03550023c3','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Cases','module',90,0),('6e960054-ed35-f89a-5ea5-4c03558aa9a7','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Cases','module',90,0),('75b64ca6-969b-06ed-a220-4c03554b68ce','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Notes','module',89,0),('75eff400-5eda-3737-e9a7-4c0355cc0bc2','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Notes','module',90,0),('7625968e-02f1-6a55-3d0b-4c03559be59a','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Notes','module',90,0),('765b3081-6e3e-251b-093c-4c03552ebb98','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Notes','module',90,0),('769009f8-40c4-c8ef-d705-4c03559e4c4c','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Notes','module',90,0),('76c4d52a-c0db-dd63-8ecf-4c035527c5ba','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Notes','module',90,0),('76f9125f-c49b-fc86-3240-4c035596ed19','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Notes','module',90,0),('7b07e69d-64e0-2840-553c-4c03550ee240','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','EmailTemplates','module',89,0),('7b407073-9996-8004-38b6-4c03555d9b80','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','EmailTemplates','module',90,0),('7b74db00-237b-4edd-6c8a-4c0355beca47','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','EmailTemplates','module',90,0),('7ba9abfe-ff82-a012-8bc7-4c0355956f02','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','EmailTemplates','module',90,0),('7bdd8f62-7231-1056-b8e1-4c0355b284c8','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','EmailTemplates','module',90,0),('7c1459d2-c3e0-adc4-060f-4c0355616053','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','EmailTemplates','module',90,0),('7c4a9f17-6fce-6552-0088-4c0355f9ca6d','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','EmailTemplates','module',90,0),('85049156-245d-ca7d-4651-4c03551c8841','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Calls','module',89,0),('853de36c-6f4b-b4f7-d8e7-4c03557f3619','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Calls','module',90,0),('85722616-67e5-7491-e9da-4c0355ab345e','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Calls','module',90,0),('85a5e37d-2710-10ba-1f97-4c0355c671a5','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Calls','module',90,0),('85d9f188-96a6-a9d8-1abe-4c03553a5f24','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Calls','module',90,0),('860d7487-d2ec-cbac-afdd-4c035571b06d','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Calls','module',90,0),('8641d5e7-3f1c-e0d9-0f81-4c035545324a','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Calls','module',90,0),('8a5dd881-cf4f-ec3c-fd39-4c0355588710','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Emails','module',89,0),('8a97bec5-4313-856f-96a1-4c03556eddcd','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Emails','module',90,0),('8accc110-c055-1858-be9f-4c03551a8896','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Emails','module',90,0),('8b01c394-76e0-96fc-444a-4c0355285445','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Emails','module',90,0),('8b36d14c-271e-f95c-76b2-4c0355b38870','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Emails','module',90,0),('8b6ae604-db09-98d1-777a-4c03552a1890','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Emails','module',90,0),('8ba09692-dd96-c4dc-18fd-4c0355e6ebba','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Emails','module',90,0),('bcba4b63-ccbd-726c-21c1-4c03556d6290','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Meetings','module',89,0),('bcf770b1-2eb9-704e-eae5-4c0355079f2a','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Meetings','module',90,0),('bd2f066f-0e3b-14f6-3249-4c035596cdb0','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Meetings','module',90,0),('bd653a98-4541-1128-1120-4c0355ba079e','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Meetings','module',90,0),('bd9d6413-b474-e744-0eb6-4c0355e7770f','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Meetings','module',90,0),('bdd3335e-7c37-0a76-3f59-4c0355ba2b77','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Meetings','module',90,0),('be09ba8c-da86-ff06-133a-4c03553b4bb7','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Meetings','module',90,0),('c75e6a9b-ec27-4e58-7e8c-4c0355b80a28','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Tasks','module',89,0),('c799cbc1-749b-782a-595f-4c035520ed28','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Tasks','module',90,0),('c7d0be68-8ced-ede7-dfee-4c0355bd9ac4','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Tasks','module',90,0),('c806ff80-e29a-6660-8965-4c0355bdf447','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Tasks','module',90,0),('c83c41ee-1775-4bfb-4fb5-4c035591ac1d','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Tasks','module',90,0),('c872a724-cd93-2dae-9235-4c03555a056f','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Tasks','module',90,0),('c8a82483-aac1-a352-be1f-4c0355e21a3f','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Tasks','module',90,0),('d4d37089-f17a-e36d-92ea-4c03558c09d1','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Trackers','Tracker',-99,0),('d5117722-fe7c-5cba-ba58-4c0355816b1d','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Trackers','Tracker',-99,0),('d54806dd-8ad8-4fd9-2e96-4c03556c890b','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Trackers','Tracker',-99,0),('d57f6654-19b9-bcc5-6468-4c03559bb5d3','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Trackers','Tracker',-99,0),('d5b59d50-7dd7-dc2f-70ca-4c0355830711','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Trackers','Tracker',-99,0),('d5ec1b23-c2da-db85-996e-4c03555bcdbb','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Trackers','Tracker',-99,0),('d62320a5-fd3d-7912-290f-4c0355299d7d','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Trackers','Tracker',-99,0),('ea7ad5da-b27e-d306-8650-4c0355f657fd','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'access','Bugs','module',89,0),('eab51d2b-6ee9-3f80-efaf-4c035508a3c7','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'view','Bugs','module',90,0),('eaeaf4a3-a382-1acd-7bcd-4c03550a2bda','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'list','Bugs','module',90,0),('eb1f9b53-5f51-cf18-2ba8-4c03551d126f','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'edit','Bugs','module',90,0),('eb556469-06f4-f000-8914-4c0355a36bbc','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'delete','Bugs','module',90,0),('eb8a086f-0b67-87e5-2b8b-4c03555fcd22','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'import','Bugs','module',90,0),('ebbff0f1-72c8-ab69-6fe6-4c0355370fd1','2010-05-31 06:20:39','2010-05-31 06:20:39','1',NULL,'export','Bugs','module',90,0),('13e99752-bab3-0c3a-d080-4c0355db1cc4','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'access','Project','module',89,0),('142638a1-4fb1-6a7f-ab4b-4c03553bb4ad','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'view','Project','module',90,0),('145f0508-ea22-2078-bac5-4c0355cc9364','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'list','Project','module',90,0),('149741de-075b-21d7-e281-4c0355089ef3','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'edit','Project','module',90,0),('14cdf600-ea3b-3838-7dfe-4c0355c82436','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'delete','Project','module',90,0),('1503d30b-dec2-6413-d274-4c0355fba7d9','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'import','Project','module',90,0),('153bd4e2-7c5a-6a5e-0701-4c03551edece','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'export','Project','module',90,0),('1a291f75-ac76-7c56-6c44-4c0355babf88','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'access','ProjectTask','module',89,0),('1a64fd40-9628-3a0b-864b-4c03550a7dfc','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'view','ProjectTask','module',90,0),('1a9d695f-6bc9-c261-9805-4c0355eade01','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'list','ProjectTask','module',90,0),('1ad5ae31-dc87-8c4c-26ba-4c035509bf69','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'edit','ProjectTask','module',90,0),('1b0ed37f-453f-e59e-b554-4c0355c07536','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'delete','ProjectTask','module',90,0),('1b47440d-1517-4629-6279-4c0355764534','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'import','ProjectTask','module',90,0),('1b7f57cc-73fa-4b88-d7a9-4c0355c6c79b','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'export','ProjectTask','module',90,0),('25a5f6e1-017b-cde1-04f8-4c03555f072c','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'access','EmailMarketing','module',89,0),('25e23259-8e70-5321-ab8a-4c0355ef53e0','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'view','EmailMarketing','module',90,0),('261920fe-1e55-e88a-ce69-4c0355bac0f3','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'list','EmailMarketing','module',90,0),('26501d6d-b48e-5ec2-ba24-4c035527b4c1','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'edit','EmailMarketing','module',90,0),('2686368f-1934-00a4-9ec0-4c03558214ad','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'delete','EmailMarketing','module',90,0),('26bd8183-62ec-6a89-dbea-4c0355efbd02','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'import','EmailMarketing','module',90,0),('26f3fd22-236a-af22-2be2-4c03557c2b48','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'export','EmailMarketing','module',90,0),('2b54062b-b9a7-078e-9ba1-4c035574fe10','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'access','Campaigns','module',89,0),('2b8eb2d1-6aa0-9d8e-3a1a-4c03554a9425','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'view','Campaigns','module',90,0),('2bc44f11-fd0b-7549-86b0-4c0355cb3de0','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'list','Campaigns','module',90,0),('2bf9d2ce-da6f-cb8b-9fca-4c03555edfd6','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'edit','Campaigns','module',90,0),('2c3199e9-52ab-c961-44d5-4c03552d5202','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'delete','Campaigns','module',90,0),('2c67919f-1add-ddbe-1efe-4c03557fef6d','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'import','Campaigns','module',90,0),('2c9f1624-5676-5927-68d2-4c035537912f','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'export','Campaigns','module',90,0),('33757273-cd13-9e78-0a28-4c0355e3046a','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'access','ProspectLists','module',89,0),('33b3171a-3512-72d9-7f24-4c0355707cf6','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'view','ProspectLists','module',90,0),('33ecaa19-f0b4-3f40-c205-4c03550bf371','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'list','ProspectLists','module',90,0),('3424914d-069b-ef68-0922-4c0355fb6520','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'edit','ProspectLists','module',90,0),('345c39a1-fcfc-e1ff-d309-4c0355767c39','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'delete','ProspectLists','module',90,0),('34949ca4-d4ba-c99a-4d4c-4c0355ca884d','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'import','ProspectLists','module',90,0),('34cc4281-d26c-9270-d1bc-4c035517a67e','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'export','ProspectLists','module',90,0),('396cbd8c-d58a-4ae7-7d33-4c03550c07a4','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'access','Prospects','module',89,0),('39a9411b-6b4c-1550-3f72-4c0355a7d652','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'view','Prospects','module',90,0),('39e268fd-ad0c-2b81-1816-4c03557054bc','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'list','Prospects','module',90,0),('3a1af0cd-3543-2563-7065-4c03556210df','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'edit','Prospects','module',90,0),('3a549cc1-eb92-bd55-a59c-4c0355ee3754','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'delete','Prospects','module',90,0),('3a8d2573-fecb-23c7-ef81-4c0355a42e57','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'import','Prospects','module',90,0),('3ac7163c-ed01-dcd9-fb59-4c03559c8551','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'export','Prospects','module',90,0),('3f135aff-a69a-cbe6-228f-4c035565debc','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'access','Documents','module',89,0),('3f4e0fb2-caab-85c6-17cd-4c0355031fc0','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'view','Documents','module',90,0),('3f8440b2-8920-f0bd-535c-4c0355ce7142','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'list','Documents','module',90,0),('3fba5c9a-22bf-7df4-b8c8-4c0355a85515','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'edit','Documents','module',90,0),('3ff0fa25-0a47-c631-1aea-4c035589d97b','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'delete','Documents','module',90,0),('4028a9e4-f84f-b609-9082-4c035513a53c','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'import','Documents','module',90,0),('4060f490-4743-447d-4ffe-4c0355e9c5ac','2010-05-31 06:20:40','2010-05-31 06:20:40','1',NULL,'export','Documents','module',90,0);
/*!40000 ALTER TABLE `acl_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles`
--

DROP TABLE IF EXISTS `acl_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) default NULL,
  `name` varchar(150) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_aclrole_id_del` (`id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles`
--

LOCK TABLES `acl_roles` WRITE;
/*!40000 ALTER TABLE `acl_roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles_actions`
--

DROP TABLE IF EXISTS `acl_roles_actions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles_actions` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) default NULL,
  `action_id` varchar(36) default NULL,
  `access_override` int(3) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_acl_role_id` (`role_id`),
  KEY `idx_acl_action_id` (`action_id`),
  KEY `idx_aclrole_action` (`role_id`,`action_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles_actions`
--

LOCK TABLES `acl_roles_actions` WRITE;
/*!40000 ALTER TABLE `acl_roles_actions` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_roles_actions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `acl_roles_users`
--

DROP TABLE IF EXISTS `acl_roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `acl_roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) default NULL,
  `user_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_aclrole_id` (`role_id`),
  KEY `idx_acluser_id` (`user_id`),
  KEY `idx_aclrole_user` (`role_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `acl_roles_users`
--

LOCK TABLES `acl_roles_users` WRITE;
/*!40000 ALTER TABLE `acl_roles_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `acl_roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `address_book`
--

DROP TABLE IF EXISTS `address_book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `address_book` (
  `assigned_user_id` char(36) NOT NULL,
  `bean` varchar(50) NOT NULL,
  `bean_id` char(36) NOT NULL,
  KEY `ab_user_bean_idx` (`assigned_user_id`,`bean`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `address_book`
--

LOCK TABLES `address_book` WRITE;
/*!40000 ALTER TABLE `address_book` DISABLE KEYS */;
/*!40000 ALTER TABLE `address_book` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `asterisk_log`
--

DROP TABLE IF EXISTS `asterisk_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `asterisk_log` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `call_record_id` char(36) default NULL,
  `asterisk_id` varchar(45) default NULL,
  `callstate` varchar(10) default NULL,
  `callerID` varchar(45) default NULL,
  `callerName` varchar(45) default NULL,
  `channel` varchar(30) default NULL,
  `timestampCall` datetime default NULL,
  `timestampLink` datetime default NULL,
  `timestampHangup` datetime default NULL,
  `direction` varchar(1) default NULL,
  `hangup_cause` int(11) default NULL,
  `hangup_cause_txt` varchar(45) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `asterisk_log`
--

LOCK TABLES `asterisk_log` WRITE;
/*!40000 ALTER TABLE `asterisk_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `asterisk_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs`
--

DROP TABLE IF EXISTS `bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs` (
  `id` char(36) NOT NULL,
  `name` varchar(255) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `bug_number` int(11) NOT NULL auto_increment,
  `type` varchar(255) default NULL,
  `status` varchar(25) default NULL,
  `priority` varchar(25) default NULL,
  `resolution` varchar(255) default NULL,
  `work_log` text,
  `found_in_release` varchar(255) default NULL,
  `fixed_in_release` varchar(255) default NULL,
  `source` varchar(255) default NULL,
  `product_category` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `bugsnumk` (`bug_number`),
  KEY `bug_number` (`bug_number`),
  KEY `idx_bug_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs`
--

LOCK TABLES `bugs` WRITE;
/*!40000 ALTER TABLE `bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bugs_audit`
--

DROP TABLE IF EXISTS `bugs_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bugs_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime default NULL,
  `created_by` varchar(36) default NULL,
  `field_name` varchar(100) default NULL,
  `data_type` varchar(100) default NULL,
  `before_value_string` varchar(255) default NULL,
  `after_value_string` varchar(255) default NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bugs_audit`
--

LOCK TABLES `bugs_audit` WRITE;
/*!40000 ALTER TABLE `bugs_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `bugs_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls`
--

DROP TABLE IF EXISTS `calls`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls` (
  `id` char(36) NOT NULL,
  `name` varchar(50) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `duration_hours` int(2) default NULL,
  `duration_minutes` int(2) default NULL,
  `date_start` datetime default NULL,
  `date_end` date default NULL,
  `parent_type` varchar(25) default NULL,
  `status` varchar(25) default NULL,
  `direction` varchar(25) default NULL,
  `parent_id` char(36) default NULL,
  `reminder_time` int(4) default '-1',
  `outlook_id` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_call_name` (`name`),
  KEY `idx_status` (`status`),
  KEY `idx_calls_date_start` (`date_start`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls`
--

LOCK TABLES `calls` WRITE;
/*!40000 ALTER TABLE `calls` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_contacts`
--

DROP TABLE IF EXISTS `calls_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_contacts` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) default NULL,
  `contact_id` varchar(36) default NULL,
  `required` varchar(1) default '1',
  `accept_status` varchar(25) default 'none',
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_con_call_call` (`call_id`),
  KEY `idx_con_call_con` (`contact_id`),
  KEY `idx_call_contact` (`call_id`,`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_contacts`
--

LOCK TABLES `calls_contacts` WRITE;
/*!40000 ALTER TABLE `calls_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_cstm`
--

DROP TABLE IF EXISTS `calls_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_cstm` (
  `id_c` char(36) NOT NULL,
  `asterisk_caller_id_c` varchar(45) default NULL,
  PRIMARY KEY  (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_cstm`
--

LOCK TABLES `calls_cstm` WRITE;
/*!40000 ALTER TABLE `calls_cstm` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_leads`
--

DROP TABLE IF EXISTS `calls_leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_leads` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) default NULL,
  `lead_id` varchar(36) default NULL,
  `required` varchar(1) default '1',
  `accept_status` varchar(25) default 'none',
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_lead_call_call` (`call_id`),
  KEY `idx_lead_call_lead` (`lead_id`),
  KEY `idx_call_lead` (`call_id`,`lead_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_leads`
--

LOCK TABLES `calls_leads` WRITE;
/*!40000 ALTER TABLE `calls_leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `calls_users`
--

DROP TABLE IF EXISTS `calls_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `calls_users` (
  `id` varchar(36) NOT NULL,
  `call_id` varchar(36) default NULL,
  `user_id` varchar(36) default NULL,
  `required` varchar(1) default '1',
  `accept_status` varchar(25) default 'none',
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_usr_call_call` (`call_id`),
  KEY `idx_usr_call_usr` (`user_id`),
  KEY `idx_call_users` (`call_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `calls_users`
--

LOCK TABLES `calls_users` WRITE;
/*!40000 ALTER TABLE `calls_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `calls_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_log`
--

DROP TABLE IF EXISTS `campaign_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_log` (
  `id` char(36) NOT NULL,
  `campaign_id` char(36) default NULL,
  `target_tracker_key` varchar(36) default NULL,
  `target_id` varchar(36) default NULL,
  `target_type` varchar(25) default NULL,
  `activity_type` varchar(25) default NULL,
  `activity_date` datetime default NULL,
  `related_id` varchar(36) default NULL,
  `related_type` varchar(25) default NULL,
  `archived` tinyint(1) default '0',
  `hits` int(11) default '0',
  `list_id` char(36) default NULL,
  `deleted` tinyint(1) default '0',
  `date_modified` datetime default NULL,
  `more_information` varchar(100) default NULL,
  `marketing_id` char(36) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_camp_tracker` (`target_tracker_key`),
  KEY `idx_camp_campaign_id` (`campaign_id`),
  KEY `idx_camp_more_info` (`more_information`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_log`
--

LOCK TABLES `campaign_log` WRITE;
/*!40000 ALTER TABLE `campaign_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaign_trkrs`
--

DROP TABLE IF EXISTS `campaign_trkrs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaign_trkrs` (
  `id` char(36) NOT NULL,
  `tracker_name` varchar(30) default NULL,
  `tracker_url` varchar(255) default 'http://',
  `tracker_key` int(11) NOT NULL auto_increment,
  `campaign_id` char(36) default NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `is_optout` tinyint(1) NOT NULL default '0',
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `campaign_tracker_key_idx` (`tracker_key`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaign_trkrs`
--

LOCK TABLES `campaign_trkrs` WRITE;
/*!40000 ALTER TABLE `campaign_trkrs` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaign_trkrs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigns`
--

DROP TABLE IF EXISTS `campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns` (
  `id` char(36) NOT NULL,
  `name` varchar(50) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `tracker_key` int(11) NOT NULL auto_increment,
  `tracker_count` int(11) default '0',
  `refer_url` varchar(255) default 'http://',
  `tracker_text` varchar(255) default NULL,
  `start_date` date default NULL,
  `end_date` date default NULL,
  `status` varchar(25) default NULL,
  `impressions` int(11) default '0',
  `currency_id` char(36) default NULL,
  `budget` double default NULL,
  `expected_cost` double default NULL,
  `actual_cost` double default NULL,
  `expected_revenue` double default NULL,
  `campaign_type` varchar(25) default NULL,
  `objective` text,
  `content` text,
  `frequency` varchar(25) default NULL,
  PRIMARY KEY  (`id`),
  KEY `camp_auto_tracker_key` (`tracker_key`),
  KEY `idx_campaign_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigns`
--

LOCK TABLES `campaigns` WRITE;
/*!40000 ALTER TABLE `campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `campaigns_audit`
--

DROP TABLE IF EXISTS `campaigns_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `campaigns_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime default NULL,
  `created_by` varchar(36) default NULL,
  `field_name` varchar(100) default NULL,
  `data_type` varchar(100) default NULL,
  `before_value_string` varchar(255) default NULL,
  `after_value_string` varchar(255) default NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `campaigns_audit`
--

LOCK TABLES `campaigns_audit` WRITE;
/*!40000 ALTER TABLE `campaigns_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `campaigns_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases` (
  `id` char(36) NOT NULL,
  `name` varchar(255) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `case_number` int(11) NOT NULL auto_increment,
  `type` varchar(255) default NULL,
  `status` varchar(25) default NULL,
  `priority` varchar(25) default NULL,
  `resolution` text,
  `work_log` text,
  `account_id` char(36) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `casesnumk` (`case_number`),
  KEY `case_number` (`case_number`),
  KEY `idx_case_name` (`name`),
  KEY `idx_account_id` (`account_id`),
  KEY `idx_cases_stat_del` (`assigned_user_id`,`status`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases`
--

LOCK TABLES `cases` WRITE;
/*!40000 ALTER TABLE `cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases_audit`
--

DROP TABLE IF EXISTS `cases_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime default NULL,
  `created_by` varchar(36) default NULL,
  `field_name` varchar(100) default NULL,
  `data_type` varchar(100) default NULL,
  `before_value_string` varchar(255) default NULL,
  `after_value_string` varchar(255) default NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_audit`
--

LOCK TABLES `cases_audit` WRITE;
/*!40000 ALTER TABLE `cases_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cases_bugs`
--

DROP TABLE IF EXISTS `cases_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases_bugs` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) default NULL,
  `bug_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_cas_bug_cas` (`case_id`),
  KEY `idx_cas_bug_bug` (`bug_id`),
  KEY `idx_case_bug` (`case_id`,`bug_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cases_bugs`
--

LOCK TABLES `cases_bugs` WRITE;
/*!40000 ALTER TABLE `cases_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `cases_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `category` varchar(32) default NULL,
  `name` varchar(32) default NULL,
  `value` text,
  KEY `idx_config_cat` (`category`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES ('notify','fromaddress','do_not_reply@example.com'),('notify','fromname','SugarCRM'),('notify','send_by_default','1'),('notify','on','0'),('notify','send_from_assigning_user','0'),('info','sugar_version','5.2.0'),('MySettings','tab',''),('portal','on','0'),('Update','CheckUpdates','manual'),('system','name','frSIP CRM');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `id` char(36) NOT NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `salutation` varchar(5) default NULL,
  `first_name` varchar(100) default NULL,
  `last_name` varchar(100) default NULL,
  `title` varchar(100) default NULL,
  `department` varchar(255) default NULL,
  `do_not_call` tinyint(1) default '0',
  `phone_home` varchar(25) default NULL,
  `phone_mobile` varchar(25) default NULL,
  `phone_work` varchar(25) default NULL,
  `phone_other` varchar(25) default NULL,
  `phone_fax` varchar(25) default NULL,
  `primary_address_street` varchar(150) default NULL,
  `primary_address_city` varchar(100) default NULL,
  `primary_address_state` varchar(100) default NULL,
  `primary_address_postalcode` varchar(20) default NULL,
  `primary_address_country` varchar(255) default NULL,
  `alt_address_street` varchar(150) default NULL,
  `alt_address_city` varchar(100) default NULL,
  `alt_address_state` varchar(100) default NULL,
  `alt_address_postalcode` varchar(20) default NULL,
  `alt_address_country` varchar(255) default NULL,
  `assistant` varchar(75) default NULL,
  `assistant_phone` varchar(25) default NULL,
  `lead_source` varchar(100) default NULL,
  `reports_to_id` char(36) default NULL,
  `birthdate` date default NULL,
  `portal_name` varchar(255) default NULL,
  `portal_active` tinyint(1) NOT NULL default '0',
  `portal_app` varchar(255) default NULL,
  `campaign_id` char(36) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_cont_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_contacts_del_last` (`deleted`,`last_name`),
  KEY `idx_cont_del_reports` (`deleted`,`reports_to_id`,`last_name`),
  KEY `idx_reports_to_id` (`reports_to_id`),
  KEY `idx_del_id_user` (`deleted`,`id`,`assigned_user_id`),
  KEY `idx_cont_assigned` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_audit`
--

DROP TABLE IF EXISTS `contacts_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime default NULL,
  `created_by` varchar(36) default NULL,
  `field_name` varchar(100) default NULL,
  `data_type` varchar(100) default NULL,
  `before_value_string` varchar(255) default NULL,
  `after_value_string` varchar(255) default NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_audit`
--

LOCK TABLES `contacts_audit` WRITE;
/*!40000 ALTER TABLE `contacts_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_bugs`
--

DROP TABLE IF EXISTS `contacts_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_bugs` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) default NULL,
  `bug_id` varchar(36) default NULL,
  `contact_role` varchar(50) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_con_bug_con` (`contact_id`),
  KEY `idx_con_bug_bug` (`bug_id`),
  KEY `idx_contact_bug` (`contact_id`,`bug_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_bugs`
--

LOCK TABLES `contacts_bugs` WRITE;
/*!40000 ALTER TABLE `contacts_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_cases`
--

DROP TABLE IF EXISTS `contacts_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_cases` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) default NULL,
  `case_id` varchar(36) default NULL,
  `contact_role` varchar(50) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_con_case_con` (`contact_id`),
  KEY `idx_con_case_case` (`case_id`),
  KEY `idx_contacts_cases` (`contact_id`,`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_cases`
--

LOCK TABLES `contacts_cases` WRITE;
/*!40000 ALTER TABLE `contacts_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts_users`
--

DROP TABLE IF EXISTS `contacts_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts_users` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) default NULL,
  `user_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_con_users_con` (`contact_id`),
  KEY `idx_con_users_user` (`user_id`),
  KEY `idx_contacts_users` (`contact_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts_users`
--

LOCK TABLES `contacts_users` WRITE;
/*!40000 ALTER TABLE `contacts_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `currencies`
--

DROP TABLE IF EXISTS `currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `currencies` (
  `id` char(36) NOT NULL,
  `name` varchar(36) NOT NULL,
  `symbol` varchar(36) NOT NULL,
  `iso4217` varchar(3) NOT NULL,
  `conversion_rate` double NOT NULL default '0',
  `status` varchar(25) default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_by` char(36) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_currency_name` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `currencies`
--

LOCK TABLES `currencies` WRITE;
/*!40000 ALTER TABLE `currencies` DISABLE KEYS */;
/*!40000 ALTER TABLE `currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `custom_fields`
--

DROP TABLE IF EXISTS `custom_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `custom_fields` (
  `bean_id` varchar(36) default NULL,
  `set_num` int(11) default '0',
  `field0` varchar(255) default NULL,
  `field1` varchar(255) default NULL,
  `field2` varchar(255) default NULL,
  `field3` varchar(255) default NULL,
  `field4` varchar(255) default NULL,
  `field5` varchar(255) default NULL,
  `field6` varchar(255) default NULL,
  `field7` varchar(255) default NULL,
  `field8` varchar(255) default NULL,
  `field9` varchar(255) default NULL,
  `deleted` tinyint(1) default '0',
  KEY `idx_beanid_set_num` (`bean_id`,`set_num`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `custom_fields`
--

LOCK TABLES `custom_fields` WRITE;
/*!40000 ALTER TABLE `custom_fields` DISABLE KEYS */;
/*!40000 ALTER TABLE `custom_fields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboards`
--

DROP TABLE IF EXISTS `dashboards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dashboards` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `assigned_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `name` varchar(100) default NULL,
  `description` text,
  `content` text,
  PRIMARY KEY  (`id`),
  KEY `idx_dashboard_name` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboards`
--

LOCK TABLES `dashboards` WRITE;
/*!40000 ALTER TABLE `dashboards` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `document_revisions`
--

DROP TABLE IF EXISTS `document_revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_revisions` (
  `id` varchar(36) NOT NULL,
  `change_log` varchar(255) default NULL,
  `document_id` varchar(36) default NULL,
  `date_entered` datetime default NULL,
  `created_by` char(36) default NULL,
  `filename` varchar(255) NOT NULL,
  `file_ext` varchar(25) default NULL,
  `file_mime_type` varchar(100) default NULL,
  `revision` varchar(25) default NULL,
  `deleted` tinyint(1) default '0',
  `date_modified` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `document_revisions`
--

LOCK TABLES `document_revisions` WRITE;
/*!40000 ALTER TABLE `document_revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `document_revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `documents` (
  `id` char(36) NOT NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `document_name` varchar(255) NOT NULL,
  `active_date` date default NULL,
  `exp_date` date default NULL,
  `category_id` varchar(25) default NULL,
  `subcategory_id` varchar(25) default NULL,
  `status_id` varchar(25) default NULL,
  `document_revision_id` varchar(36) default NULL,
  `related_doc_id` char(36) default NULL,
  `related_doc_rev_id` char(36) default NULL,
  `is_template` tinyint(1) default '0',
  `template_type` varchar(25) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_doc_cat` (`category_id`,`subcategory_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addr_bean_rel`
--

DROP TABLE IF EXISTS `email_addr_bean_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addr_bean_rel` (
  `id` char(36) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `bean_id` char(36) NOT NULL,
  `bean_module` varchar(25) NOT NULL,
  `primary_address` tinyint(1) default '0',
  `reply_to_address` tinyint(1) default '0',
  `date_created` datetime default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_email_address_id` (`email_address_id`),
  KEY `idx_bean_id` (`bean_id`,`bean_module`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addr_bean_rel`
--

LOCK TABLES `email_addr_bean_rel` WRITE;
/*!40000 ALTER TABLE `email_addr_bean_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_addr_bean_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_addresses`
--

DROP TABLE IF EXISTS `email_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_addresses` (
  `id` char(36) NOT NULL,
  `email_address` varchar(255) NOT NULL,
  `email_address_caps` varchar(255) NOT NULL,
  `invalid_email` tinyint(1) default '0',
  `opt_out` tinyint(1) default '0',
  `date_created` datetime default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_ea_caps_opt_out_invalid` (`email_address_caps`,`opt_out`,`invalid_email`),
  KEY `idx_ea_opt_out_invalid` (`email_address`,`opt_out`,`invalid_email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_addresses`
--

LOCK TABLES `email_addresses` WRITE;
/*!40000 ALTER TABLE `email_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_cache`
--

DROP TABLE IF EXISTS `email_cache`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_cache` (
  `ie_id` char(36) NOT NULL,
  `mbox` varchar(60) NOT NULL,
  `subject` varchar(255) default NULL,
  `fromaddr` varchar(100) default NULL,
  `toaddr` varchar(255) default NULL,
  `senddate` datetime default NULL,
  `message_id` varchar(255) default NULL,
  `mailsize` int(10) unsigned NOT NULL,
  `imap_uid` int(10) unsigned NOT NULL,
  `msgno` int(10) unsigned default NULL,
  `recent` tinyint(4) NOT NULL,
  `flagged` tinyint(4) NOT NULL,
  `answered` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `seen` tinyint(4) NOT NULL,
  `draft` tinyint(4) NOT NULL,
  KEY `idx_ie_id` (`ie_id`),
  KEY `idx_mail_date` (`ie_id`,`mbox`,`senddate`),
  KEY `idx_mail_from` (`ie_id`,`mbox`,`fromaddr`),
  KEY `idx_mail_subj` (`subject`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_cache`
--

LOCK TABLES `email_cache` WRITE;
/*!40000 ALTER TABLE `email_cache` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_cache` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_marketing`
--

DROP TABLE IF EXISTS `email_marketing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_marketing` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `name` varchar(255) default NULL,
  `from_name` varchar(100) default NULL,
  `from_addr` varchar(100) default NULL,
  `reply_to_name` varchar(100) default NULL,
  `reply_to_addr` varchar(100) default NULL,
  `inbound_email_id` varchar(36) default NULL,
  `date_start` datetime default NULL,
  `template_id` char(36) NOT NULL,
  `status` varchar(25) NOT NULL,
  `campaign_id` char(36) default NULL,
  `all_prospect_lists` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_emmkt_name` (`name`),
  KEY `idx_emmkit_del` (`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_marketing`
--

LOCK TABLES `email_marketing` WRITE;
/*!40000 ALTER TABLE `email_marketing` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_marketing` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_marketing_prospect_lists`
--

DROP TABLE IF EXISTS `email_marketing_prospect_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_marketing_prospect_lists` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) default NULL,
  `email_marketing_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `email_mp_prospects` (`email_marketing_id`,`prospect_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_marketing_prospect_lists`
--

LOCK TABLES `email_marketing_prospect_lists` WRITE;
/*!40000 ALTER TABLE `email_marketing_prospect_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_marketing_prospect_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_templates`
--

DROP TABLE IF EXISTS `email_templates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_templates` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` varchar(36) default NULL,
  `published` varchar(3) default NULL,
  `name` varchar(255) default NULL,
  `description` text,
  `subject` varchar(255) default NULL,
  `body` text,
  `body_html` text,
  `deleted` tinyint(1) NOT NULL default '0',
  `text_only` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_email_template_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_templates`
--

LOCK TABLES `email_templates` WRITE;
/*!40000 ALTER TABLE `email_templates` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_templates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emailman`
--

DROP TABLE IF EXISTS `emailman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailman` (
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `user_id` char(36) default NULL,
  `id` int(11) NOT NULL auto_increment,
  `campaign_id` char(36) default NULL,
  `marketing_id` char(36) default NULL,
  `list_id` char(36) default NULL,
  `send_date_time` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `in_queue` tinyint(1) default '0',
  `in_queue_date` datetime default NULL,
  `send_attempts` int(11) default '0',
  `deleted` tinyint(1) default '0',
  `related_id` char(36) default NULL,
  `related_type` varchar(100) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_eman_list` (`list_id`,`user_id`,`deleted`),
  KEY `idx_eman_campaign_id` (`campaign_id`),
  KEY `idx_eman_relid_reltype_id` (`related_id`,`related_type`,`campaign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emailman`
--

LOCK TABLES `emailman` WRITE;
/*!40000 ALTER TABLE `emailman` DISABLE KEYS */;
/*!40000 ALTER TABLE `emailman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails`
--

DROP TABLE IF EXISTS `emails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_sent` datetime default NULL,
  `message_id` varchar(255) default NULL,
  `name` varchar(255) default NULL,
  `type` varchar(25) default NULL,
  `status` varchar(25) default NULL,
  `flagged` tinyint(1) default '0',
  `reply_to_status` tinyint(1) default '0',
  `intent` varchar(25) default 'pick',
  `mailbox_id` char(36) default NULL,
  `parent_type` varchar(25) default NULL,
  `parent_id` char(36) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_email_name` (`name`),
  KEY `idx_message_id` (`message_id`),
  KEY `idx_email_parent_id` (`parent_id`),
  KEY `idx_email_assigned` (`assigned_user_id`,`type`,`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails`
--

LOCK TABLES `emails` WRITE;
/*!40000 ALTER TABLE `emails` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_beans`
--

DROP TABLE IF EXISTS `emails_beans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_beans` (
  `id` varchar(36) NOT NULL,
  `email_id` varchar(36) default NULL,
  `bean_id` varchar(36) default NULL,
  `bean_module` varchar(36) default NULL,
  `campaign_data` text,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_emails_beans_bean_id` (`bean_id`),
  KEY `idx_emails_beans_email_bean` (`email_id`,`bean_id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_beans`
--

LOCK TABLES `emails_beans` WRITE;
/*!40000 ALTER TABLE `emails_beans` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_beans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_email_addr_rel`
--

DROP TABLE IF EXISTS `emails_email_addr_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_email_addr_rel` (
  `id` char(36) NOT NULL,
  `email_id` char(36) NOT NULL,
  `address_type` varchar(4) NOT NULL,
  `email_address_id` char(36) NOT NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_eearl_email_id` (`email_id`,`address_type`),
  KEY `idx_eearl_address_id` (`email_address_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_email_addr_rel`
--

LOCK TABLES `emails_email_addr_rel` WRITE;
/*!40000 ALTER TABLE `emails_email_addr_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_email_addr_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emails_text`
--

DROP TABLE IF EXISTS `emails_text`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emails_text` (
  `email_id` varchar(36) NOT NULL,
  `from_addr` varchar(255) default NULL,
  `reply_to_addr` varchar(255) default NULL,
  `to_addrs` text,
  `cc_addrs` text,
  `bcc_addrs` text,
  `description` longtext,
  `description_html` longtext,
  `raw_source` longtext,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`email_id`),
  KEY `emails_textfromaddr` (`from_addr`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emails_text`
--

LOCK TABLES `emails_text` WRITE;
/*!40000 ALTER TABLE `emails_text` DISABLE KEYS */;
/*!40000 ALTER TABLE `emails_text` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feeds`
--

DROP TABLE IF EXISTS `feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feeds` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `assigned_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `title` varchar(100) default NULL,
  `description` text,
  `url` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_feed_name` (`title`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feeds`
--

LOCK TABLES `feeds` WRITE;
/*!40000 ALTER TABLE `feeds` DISABLE KEYS */;
INSERT INTO `feeds` VALUES ('e8bc00cd-cbb7-280c-0aa5-41e2df9c988d',0,'2005-01-10 12:01:58','2005-01-10 12:01:58','1','1',NULL,'Linux Today',NULL,'http://linuxtoday.com/backend/biglt.rss'),('a93b3610-0f3c-a4cb-6985-41e2df65b1c8',0,'2005-01-10 12:02:07','2005-01-10 12:02:07','1','1',NULL,'MacCentral News',NULL,'http://maccentral.macworld.com/mnn.cgi'),('dde361bb-7d4a-cc59-8cce-41e2dfcf3c1b',0,'2005-01-10 12:02:21','2005-01-10 12:02:21','1','1',NULL,'MacMerc.com',NULL,'http://macmerc.com/backend.php'),('925ef6eb-7bbf-2766-cc7a-41e2df228578',0,'2005-01-10 12:02:30','2005-01-10 12:02:30','1','1',NULL,'ABC News: Business',NULL,'http://my.abcnews.go.com/rsspublic/business_rss20.xml'),('dbab5f6c-d0da-4e67-478c-41e2df8b122c',0,'2005-01-10 12:02:34','2005-01-10 12:02:34','1','1',NULL,'ABC News: Entertainment',NULL,'http://my.abcnews.go.com/rsspublic/entertainment_rss20.xml'),('7eb0e2e7-28c5-21cc-31f9-41e2dfbf3713',0,'2005-01-10 12:02:39','2005-01-10 12:02:39','1','1',NULL,'ABC News: GMA',NULL,'http://my.abcnews.go.com/rsspublic/gma_rss20.xml'),('e8066ab5-3b23-fe0d-614e-41e2df558ec8',0,'2005-01-10 12:02:43','2005-01-10 12:02:43','1','1',NULL,'ABC News: Health',NULL,'http://my.abcnews.go.com/rsspublic/health_rss20.xml'),('36b67446-880f-d6a5-e16e-41e2df3bd857',0,'2005-01-10 12:02:48','2005-01-10 12:02:48','1','1',NULL,'ABC News: Nightline',NULL,'http://my.abcnews.go.com/rsspublic/nightline_rss20.xml'),('83235e59-0a98-1777-2a9c-41e2df9a09ce',0,'2005-01-10 12:02:52','2005-01-10 12:02:52','1','1',NULL,'ABC News: Politics',NULL,'http://my.abcnews.go.com/rsspublic/politics_rss20.xml'),('c2e3db4d-c176-53bf-1aac-41e2df1a1450',0,'2005-01-10 12:02:56','2005-01-10 12:02:56','1','1',NULL,'ABC News: Primetime',NULL,'http://my.abcnews.go.com/rsspublic/primetime_rss20.xml'),('4550e09a-b93e-092f-80ec-41e2dfc550f7',0,'2005-01-10 12:03:01','2005-01-10 12:03:01','1','1',NULL,'ABC News: Technology',NULL,'http://my.abcnews.go.com/rsspublic/scitech_rss20.xml'),('a164cc3f-435e-0a59-3e8c-41e2dfebd62f',0,'2005-01-10 12:03:05','2005-01-10 12:03:05','1','1',NULL,'ABC News: Travel',NULL,'http://my.abcnews.go.com/rsspublic/travel_rss20.xml'),('28572e36-b2fa-cd0e-fbec-41e2df1b84c0',0,'2005-01-10 12:03:15','2005-01-10 12:03:15','1','1',NULL,'ABC News: ThisWeek',NULL,'http://my.abcnews.go.com/rsspublic/tw_rss20.xml'),('a50a67a3-73fd-c3b7-5ee6-41e2dffe5ec5',0,'2005-01-10 12:03:19','2005-01-10 12:03:19','1','1',NULL,'ABC News: US',NULL,'http://my.abcnews.go.com/rsspublic/us_rss20.xml'),('ef110198-40fe-1799-363a-41e2df5c1007',0,'2005-01-10 12:03:24','2005-01-10 12:03:24','1','1',NULL,'ABC News: WNT',NULL,'http://my.abcnews.go.com/rsspublic/wnt_rss20.xml'),('5b71af07-470d-95c3-8afd-41e2df76f015',0,'2005-01-10 12:03:28','2005-01-10 12:03:28','1','1',NULL,'ABC News: International',NULL,'http://my.abcnews.go.com/rsspublic/world_rss20.xml'),('16c540cc-c02a-4311-d64c-41e2dfdbd596',0,'2005-01-10 12:03:29','2005-01-10 12:03:29','1','1',NULL,'BBC News | Africa | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/africa/rss091.xml'),('bc2c7c1f-220d-400a-c383-41e2dfc79697',0,'2005-01-10 12:03:29','2005-01-10 12:03:29','1','1',NULL,'BBC News | Americas | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/americas/rss091.xml'),('81bb8625-d0c5-07f1-04da-41e2df5fc5e2',0,'2005-01-10 12:03:30','2005-01-10 12:03:30','1','1',NULL,'BBC News | Asia-Pacific | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/asia-pacific/rss091.xml'),('3a68ade5-eba9-dcf7-b8bc-41e2df75bea6',0,'2005-01-10 12:03:31','2005-01-10 12:03:31','1','1',NULL,'BBC News | Business | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/business/rss091.xml'),('c5ae7e2a-c2c8-a261-8f4f-41e2dfad01f6',0,'2005-01-10 12:03:31','2005-01-10 12:03:31','1','1',NULL,'BBC News | Entertainment | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/entertainment/rss091.xml'),('7da60a3e-c6b3-4d4a-0338-41e2df90838a',0,'2005-01-10 12:03:32','2005-01-10 12:03:32','1','1',NULL,'BBC News | Europe | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/europe/rss091.xml'),('3191a790-8894-ea0a-fad6-41e2df8b7d62',0,'2005-01-10 12:03:33','2005-01-10 12:03:33','1','1',NULL,'BBC News | News Front Page | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/front_page/rss091.xml'),('d0089e80-9da9-93d5-6999-41e2df85a074',0,'2005-01-10 12:03:33','2005-01-10 12:03:33','1','1',NULL,'BBC News | Health | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/health/rss091.xml'),('9582dea7-c0dd-c5cb-07eb-41e2df38591c',0,'2005-01-10 12:03:34','2005-01-10 12:03:34','1','1',NULL,'BBC News | Middle East | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/middle_east/rss091.xml'),('1c853682-6da0-1ebf-5a4b-41e2df0c6ae0',0,'2005-01-10 12:03:35','2005-01-10 12:03:35','1','1',NULL,'BBC News | Programmes | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/programmes/rss091.xml'),('bd3d3b2a-3b2b-1b1c-b833-41e2dfed10b2',0,'2005-01-10 12:03:35','2005-01-10 12:03:35','1','1',NULL,'BBC News | Science/Nature | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/science/nature/rss091.xml'),('77ea97e9-f224-ae60-e2f3-41e2dfa83f37',0,'2005-01-10 12:03:36','2005-01-10 12:03:36','1','1',NULL,'BBC News | South Asia | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/south_asia/rss091.xml'),('15d960df-ef6b-bb2a-6bc7-41e2df301151',0,'2005-01-10 12:03:37','2005-01-10 12:03:37','1','1',NULL,'BBC News | Have Your Say | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/talking_point/rss091.xml'),('a441427a-dded-20ec-14c0-41e2df00b0ca',0,'2005-01-10 12:03:37','2005-01-10 12:03:37','1','1',NULL,'BBC News | Technology | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/technology/rss091.xml'),('4c8f056f-3eeb-7c81-89df-41e2dfdc1c14',0,'2005-01-10 12:03:38','2005-01-10 12:03:38','1','1',NULL,'BBC News | UK News | Magazine | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/uk_news/magazine/rss091.xml'),('3a000f75-1443-8f26-018e-41e2df41bb7e',0,'2005-01-10 12:03:39','2005-01-10 12:03:39','1','1',NULL,'BBC News | UK | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/uk_news/rss091.xml'),('b18a157a-9129-126a-3ae6-41e2df5bf062',0,'2005-01-10 12:03:39','2005-01-10 12:03:39','1','1',NULL,'BBC News | Week at a Glance | World Edition',NULL,'http://news.bbc.co.uk/rss/newsonline_world_edition/week_at-a-glance/rss091.xml'),('5a354efc-6fd6-e3a9-a39d-41e2dfed604c',0,'2005-01-10 12:03:45','2005-01-10 12:03:45','1','1',NULL,'Scotsman.com News - Alcohol & binge drinking',NULL,'http://news.scotsman.com/topics.cfm?tid=585&format=rss'),('52a9de11-f2fe-5a6d-539c-41e2dff841e5',0,'2005-01-10 12:03:46','2005-01-10 12:03:46','1','1',NULL,'CNET News.com - Personal Technology',NULL,'http://rss.com.com/2547-1040-0-5.xml'),('e4a450fe-6602-2a1e-3ef2-41e2df356543',0,'2005-01-10 12:03:46','2005-01-10 12:03:46','1','1',NULL,'Yahoo! News: Technology - Apple/Macintosh',NULL,'http://rss.news.yahoo.com/rss/applecomputer'),('573d09a7-7e57-b158-56bf-41e2df105ef4',0,'2005-01-10 12:03:47','2005-01-10 12:03:47','1','1',NULL,'Yahoo! News: Entertainment - Arts and Stage',NULL,'http://rss.news.yahoo.com/rss/arts'),('a52e6677-1fa7-85fa-e4c6-41e2df9fbcca',0,'2005-01-10 12:03:47','2005-01-10 12:03:47','1','1',NULL,'Yahoo! News: Business',NULL,'http://rss.news.yahoo.com/rss/business'),('a66852ab-d691-b6be-a3e0-41e2dfdb2fd7',0,'2005-01-10 12:03:48','2005-01-10 12:03:48','1','1',NULL,'Yahoo! News: Cellular Phones',NULL,'http://rss.news.yahoo.com/rss/cellphones'),('211ea34b-c571-6bbc-22b0-41e2df130f37',0,'2005-01-10 12:03:49','2005-01-10 12:03:49','1','1',NULL,'Yahoo! News: Entertainment - Dear Abby',NULL,'http://rss.news.yahoo.com/rss/dearabby'),('8d740c93-ee6b-36d6-a86d-41e2df99cf90',0,'2005-01-10 12:03:49','2005-01-10 12:03:49','1','1',NULL,'Yahoo! News: Digital Photography',NULL,'http://rss.news.yahoo.com/rss/digitalimaging'),('7bc75358-93c7-5aeb-88f6-41e2df228bca',0,'2005-01-10 12:03:50','2005-01-10 12:03:50','1','1',NULL,'Yahoo! News: Digital Music',NULL,'http://rss.news.yahoo.com/rss/digitalmusic'),('4537c91f-a52d-a98d-448a-41e2df099679',0,'2005-01-10 12:03:51','2005-01-10 12:03:51','1','1',NULL,'Yahoo! News: Digital Video/TV Technology',NULL,'http://rss.news.yahoo.com/rss/digitalvideo'),('152a8211-c6a2-13b7-e8b6-41e2dfbb8c86',0,'2005-01-10 12:03:52','2005-01-10 12:03:52','1','1',NULL,'Yahoo! News: Business - Earnings',NULL,'http://rss.news.yahoo.com/rss/earnings'),('a6050911-0292-5625-9ce2-41e2dfebccef',0,'2005-01-10 12:03:53','2005-01-10 12:03:53','1','1',NULL,'Yahoo! News: Business - Economy',NULL,'http://rss.news.yahoo.com/rss/economy'),('a8ad7517-8eaa-08ea-f162-41e2dfd39386',0,'2005-01-10 12:03:53','2005-01-10 12:03:53','1','1',NULL,'Yahoo! News: Presidential Election',NULL,'http://rss.news.yahoo.com/rss/elections'),('a3240865-6431-2f39-2057-41e2dfa726a0',0,'2005-01-10 12:03:54','2005-01-10 12:03:54','1','1',NULL,'Yahoo! News: Entertainment - Industry',NULL,'http://rss.news.yahoo.com/rss/enindustry'),('4b590300-8679-61c0-cc2f-41e2dfddd876',0,'2005-01-10 12:03:55','2005-01-10 12:03:55','1','1',NULL,'Yahoo! News: Technology - Enterprise',NULL,'http://rss.news.yahoo.com/rss/enterprise'),('a9cf9f87-69b2-717a-a8d2-41e2dffffa44',0,'2005-01-10 12:03:55','2005-01-10 12:03:55','1','1',NULL,'Yahoo! News: Entertainment',NULL,'http://rss.news.yahoo.com/rss/entertainment'),('c48dea5a-b340-21da-b590-41e2dfadee26',0,'2005-01-10 12:03:56','2005-01-10 12:03:56','1','1',NULL,'Yahoo! News: Business - European Economy',NULL,'http://rss.news.yahoo.com/rss/eurobiz'),('4d575816-f907-80dd-5fe5-41e2dff30297',0,'2005-01-10 12:03:57','2005-01-10 12:03:57','1','1',NULL,'Yahoo! News: Fashion',NULL,'http://rss.news.yahoo.com/rss/fashion'),('9d01947b-4fa4-530b-ae22-41e2df953c95',0,'2005-01-10 12:03:57','2005-01-10 12:03:57','1','1',NULL,'Yahoo! News: Health',NULL,'http://rss.news.yahoo.com/rss/health'),('186244ef-6a28-e5d3-f85f-41e2df10fc8e',0,'2005-01-10 12:03:58','2005-01-10 12:03:58','1','1',NULL,'Yahoo! News: Reader Ratings',NULL,'http://rss.news.yahoo.com/rss/highestrated'),('d0c4a1f1-4235-1e44-137b-41e2dfd0f653',0,'2005-01-10 12:03:58','2005-01-10 12:03:58','1','1',NULL,'Yahoo! News: Technology - Internet',NULL,'http://rss.news.yahoo.com/rss/internet'),('4ae094bf-947f-304a-4659-41e2dfa5d909',0,'2005-01-10 12:04:00','2005-01-10 12:04:00','1','1',NULL,'Yahoo! News: Iraq',NULL,'http://rss.news.yahoo.com/rss/iraq'),('e7f2de1f-52ee-a6d6-4df2-41e2df2a1d06',0,'2005-01-10 12:04:00','2005-01-10 12:04:00','1','1',NULL,'Yahoo! News: Linux/Open Source',NULL,'http://rss.news.yahoo.com/rss/linux'),('942cac2d-f97a-91b9-4073-41e2df6c80d5',0,'2005-01-10 12:04:02','2005-01-10 12:04:02','1','1',NULL,'Yahoo! News: Microsoft',NULL,'http://rss.news.yahoo.com/rss/microsoft'),('bf300aab-0041-415e-788e-41e2df06cba4',0,'2005-01-10 12:04:03','2005-01-10 12:04:03','1','1',NULL,'Yahoo! News: Most Emailed',NULL,'http://rss.news.yahoo.com/rss/mostemailed'),('8368b72f-f4e5-c4c5-f288-41e2df962d3a',0,'2005-01-10 12:04:03','2005-01-10 12:04:03','1','1',NULL,'Yahoo! News: Most Viewed',NULL,'http://rss.news.yahoo.com/rss/mostviewed'),('1c3c47e4-4be0-7987-4ea9-41e2df97209b',0,'2005-01-10 12:04:05','2005-01-10 12:04:05','1','1',NULL,'Yahoo! News: Entertainment - Movies',NULL,'http://rss.news.yahoo.com/rss/movies'),('6f988256-8ca1-1933-3049-41e2df3e60cc',0,'2005-01-10 12:04:06','2005-01-10 12:04:06','1','1',NULL,'Yahoo! News: Entertainment - Music',NULL,'http://rss.news.yahoo.com/rss/music'),('ccd00fec-47d3-7624-d16e-41e2df044894',0,'2005-01-10 12:04:07','2005-01-10 12:04:07','1','1',NULL,'Yahoo! News: Oddly Enough',NULL,'http://rss.news.yahoo.com/rss/oddlyenough'),('a4f2137a-b861-6975-c126-41e2dfb115db',0,'2005-01-10 12:04:07','2005-01-10 12:04:07','1','1',NULL,'Yahoo! News: Op/Ed',NULL,'http://rss.news.yahoo.com/rss/oped'),('94f3d97c-9124-f5ca-4863-41e2dff1e0f1',0,'2005-01-10 12:04:08','2005-01-10 12:04:08','1','1',NULL,'Yahoo! News: Business - Personal Finance',NULL,'http://rss.news.yahoo.com/rss/personalfinance'),('e67a951a-e02a-5e57-cbfe-41e2df1cb9eb',0,'2005-01-10 12:04:10','2005-01-10 12:04:10','1','1',NULL,'Yahoo! News: Technology - Personal Technology',NULL,'http://rss.news.yahoo.com/rss/personaltech'),('53e42a4a-aad1-d0cc-00bc-41e2df76fe22',0,'2005-01-10 12:04:11','2005-01-10 12:04:11','1','1',NULL,'Yahoo! News: Politics',NULL,'http://rss.news.yahoo.com/rss/politics'),('1c5392ff-668f-7a40-70b7-41e2dfca5009',0,'2005-01-10 12:04:12','2005-01-10 12:04:12','1','1',NULL,'Yahoo! News: Entertainment - Reviews',NULL,'http://rss.news.yahoo.com/rss/reviews'),('a048463f-b2d8-a68b-e532-41e2df0a664a',0,'2005-01-10 12:04:12','2005-01-10 12:04:12','1','1',NULL,'Yahoo! News: RSS & Blogging',NULL,'http://rss.news.yahoo.com/rss/rssblog'),('9aa02754-2c0e-e86b-9710-41e2df2e00b9',0,'2005-01-10 12:04:13','2005-01-10 12:04:13','1','1',NULL,'Yahoo! News: Science',NULL,'http://rss.news.yahoo.com/rss/science'),('bf91dda9-fe5c-b0f9-dcdd-41e2dfa2fcc8',0,'2005-01-10 12:04:14','2005-01-10 12:04:14','1','1',NULL,'Yahoo! News: Portals and Search Engines',NULL,'http://rss.news.yahoo.com/rss/search'),('a789b41c-d7d5-211c-5d76-41e2df3eb898',0,'2005-01-10 12:04:15','2005-01-10 12:04:15','1','1',NULL,'Yahoo! News: Computer Security & Viruses',NULL,'http://rss.news.yahoo.com/rss/security'),('70f3c59d-f85f-66aa-a42f-41e2df696f49',0,'2005-01-10 12:04:16','2005-01-10 12:04:16','1','1',NULL,'Yahoo! News: Semiconductor Industry & Servers',NULL,'http://rss.news.yahoo.com/rss/semiconductor'),('8b800cfb-41f6-5740-02bd-41e2df801fd9',0,'2005-01-10 12:04:17','2005-01-10 12:04:17','1','1',NULL,'Yahoo! News: Technology - Software',NULL,'http://rss.news.yahoo.com/rss/software'),('b82277c3-a5bd-e2a2-313a-41e2dff0822e',0,'2005-01-10 12:04:17','2005-01-10 12:04:17','1','1',NULL,'Yahoo! News: Technology - Spam',NULL,'http://rss.news.yahoo.com/rss/spam'),('26498c47-05ac-260e-414b-41e2df9ed30a',0,'2005-01-10 12:04:18','2005-01-10 12:04:18','1','1',NULL,'Yahoo! News: Sports',NULL,'http://rss.news.yahoo.com/rss/sports'),('8d9cbe09-2776-3142-3c67-41e2dfc11a61',0,'2005-01-10 12:04:18','2005-01-10 12:04:18','1','1',NULL,'Yahoo! News: Business - Stock Markets',NULL,'http://rss.news.yahoo.com/rss/stocks'),('df2f6c82-2eb8-80a4-6827-41e2df880575',0,'2005-01-10 12:04:18','2005-01-10 12:04:18','1','1',NULL,'Yahoo! News: Technology',NULL,'http://rss.news.yahoo.com/rss/tech'),('5cf1cf76-42d6-61ba-3889-41e2df406926',0,'2005-01-10 12:04:19','2005-01-10 12:04:19','1','1',NULL,'Yahoo! News: Top Stories',NULL,'http://rss.news.yahoo.com/rss/topstories'),('d9dcddc7-2d6b-df2f-7577-41e2df55a413',0,'2005-01-10 12:04:20','2005-01-10 12:04:20','1','1',NULL,'Yahoo! News: Entertainment - Television',NULL,'http://rss.news.yahoo.com/rss/tv'),('561f4dd0-2359-391b-d126-41e2df4e5438',0,'2005-01-10 12:04:21','2005-01-10 12:04:21','1','1',NULL,'Yahoo! News: U.S. National',NULL,'http://rss.news.yahoo.com/rss/us'),('18874c7d-9299-93cb-2ba2-41e2df1995a8',0,'2005-01-10 12:04:22','2005-01-10 12:04:22','1','1',NULL,'Yahoo! News: Technology - Video Games',NULL,'http://rss.news.yahoo.com/rss/videogames'),('324f0fd4-0eb6-12e2-ecaf-41e2df28bcb9',0,'2005-01-10 12:04:24','2005-01-10 12:04:24','1','1',NULL,'Yahoo! News: Wireless and Mobile Technology',NULL,'http://rss.news.yahoo.com/rss/wireless'),('6493d8af-027e-3cca-c2d6-41e2dfb9aa84',0,'2005-01-10 12:04:24','2005-01-10 12:04:24','1','1',NULL,'Yahoo! News: World',NULL,'http://rss.news.yahoo.com/rss/world'),('eb881ccf-3494-a043-381a-41e2df52c73f',0,'2005-01-10 12:04:29','2005-01-10 12:04:29','1','1',NULL,'PCWorld.com - Most Popular Downloads of the Week',NULL,'http://rss.pcworld.com/rss/downloads.rss?period=week'),('6bf251a7-1139-4c31-3d90-41e2df7ee6d3',0,'2005-01-10 12:04:34','2005-01-10 12:04:34','1','1',NULL,'PCWorld.com - Latest News Stories',NULL,'http://rss.pcworld.com/rss/latestnews.rss'),('391c061c-5472-f753-ba89-41e2df7adaef',0,'2005-01-10 12:04:39','2005-01-10 12:04:39','1','1',NULL,'eWEEK Database',NULL,'http://rssnewsapps.ziffdavis.com/eweekdatabase.xml'),('687e89bc-e055-7cc8-fee6-41e2df89541f',0,'2005-01-10 12:04:44','2005-01-10 12:04:44','1','1',NULL,'eWEEK Developer',NULL,'http://rssnewsapps.ziffdavis.com/eweekdeveloper.xml'),('61ddc6c4-15c2-1e2e-b559-41e2df9ccbd9',0,'2005-01-10 12:04:49','2005-01-10 12:04:49','1','1',NULL,'eWEEK Linux',NULL,'http://rssnewsapps.ziffdavis.com/eweeklinux.xml'),('388d5242-789e-015b-389e-41e2dfa00dc7',0,'2005-01-10 12:04:54','2005-01-10 12:04:54','1','1',NULL,'eWEEK Web Services',NULL,'http://rssnewsapps.ziffdavis.com/eweekwebservices.xml'),('f01c7eac-7b4e-4742-2287-41e2df47daaf',0,'2005-01-10 12:04:58','2005-01-10 12:04:58','1','1',NULL,'eWEEK Windows',NULL,'http://rssnewsapps.ziffdavis.com/eweekwindows.xml'),('cf40c1b2-e2fb-8641-2bb7-41e2dff46a8f',0,'2005-01-10 12:05:03','2005-01-10 12:05:03','1','1',NULL,'eWEEK Technology News',NULL,'http://rssnewsapps.ziffdavis.com/tech.xml'),('65a0f073-85fe-9426-4a06-41e2dfe5a004',0,'2005-01-10 12:05:04','2005-01-10 12:05:04','1','1',NULL,'Seattle Post-Intelligencer: Arts & Entertainment',NULL,'http://seattlepi.nwsource.com/rss/ae.rss'),('f00846ce-dd14-7fb7-9226-41e2df5041f0',0,'2005-01-10 12:05:04','2005-01-10 12:05:04','1','1',NULL,'Seattle Post-Intelligencer: Books',NULL,'http://seattlepi.nwsource.com/rss/books.rss'),('64f66a69-0df0-3188-49bf-41e2dfa7fcf9',0,'2005-01-10 12:05:05','2005-01-10 12:05:05','1','1',NULL,'Seattle Post-Intelligencer: Business News',NULL,'http://seattlepi.nwsource.com/rss/business.rss'),('bbd43fee-d439-694d-44e9-41e2df39ca30',0,'2005-01-10 12:05:05','2005-01-10 12:05:05','1','1',NULL,'Seattle Post-Intelligencer: Classical Music',NULL,'http://seattlepi.nwsource.com/rss/classical.rss'),('2bf6471a-aca7-ad06-07a2-41e2df156469',0,'2005-01-10 12:05:06','2005-01-10 12:05:06','1','1',NULL,'Seattle Post-Intelligencer: Cougar Football',NULL,'http://seattlepi.nwsource.com/rss/cougars.rss'),('813bddc2-bf31-dad5-1b09-41e2dff8520f',0,'2005-01-10 12:05:06','2005-01-10 12:05:06','1','1',NULL,'Seattle Post-Intelligencer: Dining',NULL,'http://seattlepi.nwsource.com/rss/dining.rss'),('d15c347f-dcce-83f5-3dba-41e2df339a4f',0,'2005-01-10 12:05:06','2005-01-10 12:05:06','1','1',NULL,'Seattle Post-Intelligencer: Food',NULL,'http://seattlepi.nwsource.com/rss/food.rss'),('439337e2-9bc7-465b-4f0e-41e2dfda55c8',0,'2005-01-10 12:05:07','2005-01-10 12:05:07','1','1',NULL,'Seattle Post-Intelligencer: Gardening',NULL,'http://seattlepi.nwsource.com/rss/gardening.rss'),('8858e7a9-9e84-ee76-1486-41e2df13c205',0,'2005-01-10 12:05:07','2005-01-10 12:05:07','1','1',NULL,'Seattle Post-Intelligencer: Health and Fitness',NULL,'http://seattlepi.nwsource.com/rss/health.rss'),('ca29844f-e128-1e28-a2fd-41e2dfeba1b6',0,'2005-01-10 12:05:07','2005-01-10 12:05:07','1','1',NULL,'Seattle Post-Intelligencer: Husky Football',NULL,'http://seattlepi.nwsource.com/rss/huskies.rss'),('32892ebf-9522-7358-8e3b-41e2dff2b85c',0,'2005-01-10 12:05:08','2005-01-10 12:05:08','1','1',NULL,'Seattle Post-Intelligencer: John Levesque',NULL,'http://seattlepi.nwsource.com/rss/levesque.rss'),('887bcac4-2f06-7430-894f-41e2df3ae78f',0,'2005-01-10 12:05:08','2005-01-10 12:05:08','1','1',NULL,'Seattle Post-Intelligencer: Lifestyle',NULL,'http://seattlepi.nwsource.com/rss/lifestyle.rss'),('ead74e28-ee83-4f0d-c681-41e2df62d666',0,'2005-01-10 12:05:08','2005-01-10 12:05:08','1','1',NULL,'Seattle Post-Intelligencer: Local News',NULL,'http://seattlepi.nwsource.com/rss/local.rss'),('5308fdd0-fe86-a7d4-6b56-41e2df0aee0f',0,'2005-01-10 12:05:09','2005-01-10 12:05:09','1','1',NULL,'Seattle Post-Intelligencer: Mariners',NULL,'http://seattlepi.nwsource.com/rss/mariners.rss'),('e6ca8ca3-5734-d264-de97-41e2df6ae7da',0,'2005-01-10 12:05:09','2005-01-10 12:05:09','1','1',NULL,'Seattle Post-Intelligencer: Jim Moore',NULL,'http://seattlepi.nwsource.com/rss/moore.rss'),('78a04e91-dd06-a9d5-33d8-41e2df6aa326',0,'2005-01-10 12:05:10','2005-01-10 12:05:10','1','1',NULL,'Seattle Post-Intelligencer: Movies',NULL,'http://seattlepi.nwsource.com/rss/movies.rss'),('d6e1961e-025e-03bc-c2eb-41e2df215811',0,'2005-01-10 12:05:10','2005-01-10 12:05:10','1','1',NULL,'Seattle Post-Intelligencer: Music',NULL,'http://seattlepi.nwsource.com/rss/music.rss'),('44ac9e37-caa2-74f1-f2b2-41e2df8451fd',0,'2005-01-10 12:05:11','2005-01-10 12:05:11','1','1',NULL,'Seattle Post-Intelligencer: Opinion',NULL,'http://seattlepi.nwsource.com/rss/opinion.rss'),('b59a1298-cecc-56fd-78af-41e2dfbd7ac3',0,'2005-01-10 12:05:11','2005-01-10 12:05:11','1','1',NULL,'Seattle Post-Intelligencer: High School Sports',NULL,'http://seattlepi.nwsource.com/rss/preps.rss'),('daa10bd9-2c50-667b-ad93-41e2df16b642',0,'2005-01-10 12:05:12','2005-01-10 12:05:12','1','1',NULL,'Seattle Post-Intelligencer: Seahawks',NULL,'http://seattlepi.nwsource.com/rss/seahawks.rss'),('896a8ce4-0a22-f2e4-cb46-41e2df90b5d5',0,'2005-01-10 12:05:12','2005-01-10 12:05:12','1','1',NULL,'Seattle Post-Intelligencer: Sonics',NULL,'http://seattlepi.nwsource.com/rss/sonics.rss'),('456ffec6-3c8a-c963-4a45-41e2df7c5b60',0,'2005-01-10 12:05:13','2005-01-10 12:05:13','1','1',NULL,'Seattle Post-Intelligencer: Theater',NULL,'http://seattlepi.nwsource.com/rss/theater.rss'),('92184695-82da-6b2e-0c48-41e2dfbbecac',0,'2005-01-10 12:05:13','2005-01-10 12:05:13','1','1',NULL,'Seattle Post-Intelligencer: Art Thiel',NULL,'http://seattlepi.nwsource.com/rss/thiel.rss'),('e10d1442-e58c-4a7a-cc76-41e2df151d40',0,'2005-01-10 12:05:13','2005-01-10 12:05:13','1','1',NULL,'Seattle Post-Intelligencer: TV & Radio',NULL,'http://seattlepi.nwsource.com/rss/tv.rss'),('52c2c540-9646-b6b4-0690-41e2dfd3c33c',0,'2005-01-10 12:05:14','2005-01-10 12:05:14','1','1',NULL,'Seattle Post-Intelligencer: Video Games',NULL,'http://seattlepi.nwsource.com/rss/videogames.rss'),('1e3e0549-9ae4-d6e4-c152-41e2df2c0b60',0,'2005-01-10 12:05:15','2005-01-10 12:05:15','1','1',NULL,'Seattle Post-Intelligencer: Wheels',NULL,'http://seattlepi.nwsource.com/rss/wheels.rss'),('83921b19-86dd-0a41-74b3-41e2df9dc09c',0,'2005-01-10 12:05:19','2005-01-10 12:05:19','1','1',NULL,'Slashdot: Developers',NULL,'http://slashdot.org/developers.rdf'),('e10afa8d-ec55-fb12-7db5-41e2e01be5c7',0,'2005-01-10 12:05:23','2005-01-10 12:05:23','1','1',NULL,'Slashdot:',NULL,'http://slashdot.org/slashdot.rss'),('b7809dbf-4e01-b6e2-fa54-41e2e0b93efd',0,'2005-01-10 12:05:28','2005-01-10 12:05:28','1','1',NULL,'Slate Magazine',NULL,'http://slate.msn.com/rss/'),('25016a8b-3c84-d029-d4f0-41e2e0c4a154',0,'2005-01-10 12:05:34','2005-01-10 12:05:34','1','1',NULL,'SourceForge.net Project News',NULL,'http://sourceforge.net/export/rss_sfnews.php'),('f40305df-0c9b-bd01-a424-41e2e06687ee',0,'2005-01-10 12:05:34','2005-01-10 12:05:34','1','1',NULL,'Boston Globe -- Business News',NULL,'http://syndication.boston.com/news/globe/business?mode=rss_10'),('4c7e5a97-0790-4a84-1c34-41e2e0395694',0,'2005-01-10 12:05:36','2005-01-10 12:05:36','1','1',NULL,'Boston Globe -- City/Region News',NULL,'http://syndication.boston.com/news/globe/city_region?mode=rss_10'),('54949bd8-5a1d-9a47-7249-41e2e055c4db',0,'2005-01-10 12:05:37','2005-01-10 12:05:37','1','1',NULL,'Boston Globe -- Ideas Section',NULL,'http://syndication.boston.com/news/globe/ideas?mode=rss_10'),('4f7b5f7c-0e5c-6f17-3a7b-41e2e0ddfc44',0,'2005-01-10 12:05:38','2005-01-10 12:05:38','1','1',NULL,'Boston Globe -- Living / Arts News',NULL,'http://syndication.boston.com/news/globe/living?mode=rss_10'),('228dcfab-6c30-2cf6-f7e3-41e2e0488a17',0,'2005-01-10 12:05:39','2005-01-10 12:05:39','1','1',NULL,'Boston Globe -- National News',NULL,'http://syndication.boston.com/news/globe/nation?mode=rss_10'),('ea614325-af09-f851-f120-41e2e0490fa2',0,'2005-01-10 12:05:39','2005-01-10 12:05:39','1','1',NULL,'Boston Globe -- Front Page',NULL,'http://syndication.boston.com/news/globe/pageone?mode=rss_10'),('4b850ec7-8c12-16fa-e99f-41e2e01c0942',0,'2005-01-10 12:05:41','2005-01-10 12:05:41','1','1',NULL,'Boston Globe -- Sports News',NULL,'http://syndication.boston.com/news/globe/sports?mode=rss_10'),('158909fe-4028-662e-c57d-41e2e0cb2054',0,'2005-01-10 12:05:42','2005-01-10 12:05:42','1','1',NULL,'Boston Globe -- World News',NULL,'http://syndication.boston.com/news/globe/world?mode=rss_10'),('ec7000ad-6b84-81b2-c632-41e2e0e9f21c',0,'2005-01-10 12:05:42','2005-01-10 12:05:42','1','1',NULL,'Odds and Ends',NULL,'http://syndication.boston.com/news/odd?mode=rss_10'),('44f8d79e-0fae-a328-d8f8-41e2e0abc373',0,'2005-01-10 12:05:44','2005-01-10 12:05:44','1','1',NULL,'Boston.com / News',NULL,'http://syndication.boston.com/news?mode=rss_10'),('d208a0cd-fb70-a0f2-fa96-41e2e01c4cf5',0,'2005-01-10 12:05:48','2005-01-10 12:05:48','1','1',NULL,'Techdirt',NULL,'http://techdirt.com/techdirt_rss.xml'),('8d018850-7e9b-e970-cbd1-41e2e0c1a765',0,'2005-01-10 12:05:53','2005-01-10 12:05:53','1','1',NULL,'Techno File',NULL,'http://time.blogs.com/technofile/index.rdf'),('eaf51d0c-834a-5593-ee8c-41e2e05e449f',0,'2005-01-10 12:05:54','2005-01-10 12:05:54','1','1',NULL,'BBC News | News Front Page | UK Edition',NULL,'http://www.bbc.co.uk/syndication/feeds/news/ukfs_news/front_page/rss091.xml'),('28193b0e-fb4e-3197-70fb-41e2e0da0a44',0,'2005-01-10 12:05:56','2005-01-10 12:05:56','1','1',NULL,'BBC News | Technology | UK Edition',NULL,'http://www.bbc.co.uk/syndication/feeds/news/ukfs_news/technology/rss091.xml'),('8610b5a9-75cc-c84c-fadc-41e2e088b0df',0,'2005-01-10 12:05:57','2005-01-10 12:05:57','1','1',NULL,'BBC News | UK | UK Edition',NULL,'http://www.bbc.co.uk/syndication/feeds/news/ukfs_news/uk/rss091.xml'),('c313e1b3-db1d-26ec-84af-41e2e00160d9',0,'2005-01-10 12:06:01','2005-01-10 12:06:01','1','1',NULL,'InfoWorld: Application development',NULL,'http://www.infoworld.com/rss/appdev.xml'),('2ff65a56-6bd5-c60c-9f76-41e2e0df96b8',0,'2005-01-10 12:06:06','2005-01-10 12:06:06','1','1',NULL,'InfoWorld: Java',NULL,'http://www.infoworld.com/rss/appdev_java.xml'),('df955186-98c3-2983-2135-41e2e01ab985',0,'2005-01-10 12:06:10','2005-01-10 12:06:10','1','1',NULL,'InfoWorld: Web Applications',NULL,'http://www.infoworld.com/rss/appdev_webapp.xml'),('3c0798b8-fb41-2fb1-3d23-41e2e01b8d2e',0,'2005-01-10 12:06:15','2005-01-10 12:06:15','1','1',NULL,'InfoWorld: Wireless Applications',NULL,'http://www.infoworld.com/rss/appdev_wireapps.xml'),('7e235e26-b2f2-e691-b6be-41e2e07e66b9',0,'2005-01-10 12:06:19','2005-01-10 12:06:19','1','1',NULL,'InfoWorld: XML',NULL,'http://www.infoworld.com/rss/appdev_xml.xml'),('ba005fe6-b497-6dd0-2ead-41e2e047f344',0,'2005-01-10 12:06:23','2005-01-10 12:06:23','1','1',NULL,'InfoWorld: Applications',NULL,'http://www.infoworld.com/rss/applications.xml'),('2d0effdd-a0df-b611-fe99-41e2e01d157f',0,'2005-01-10 12:06:28','2005-01-10 12:06:28','1','1',NULL,'InfoWorld: Application Management',NULL,'http://www.infoworld.com/rss/apps_appmgmt.xml'),('8848e6de-196b-a37f-acc1-41e2e0bded0b',0,'2005-01-10 12:06:32','2005-01-10 12:06:32','1','1',NULL,'InfoWorld: Collaboration',NULL,'http://www.infoworld.com/rss/apps_collab.xml'),('d78da415-8df2-c73f-3706-41e2e03e5563',0,'2005-01-10 12:06:36','2005-01-10 12:06:36','1','1',NULL,'InfoWorld: CRM',NULL,'http://www.infoworld.com/rss/apps_crm.xml'),('31114cb7-c113-29bf-9f20-41e2e0a5687a',0,'2005-01-10 12:06:41','2005-01-10 12:06:41','1','1',NULL,'InfoWorld: Enterprise Integration',NULL,'http://www.infoworld.com/rss/apps_einteg.xml'),('7ea784e8-46ad-8979-dd73-41e2e0241926',0,'2005-01-10 12:06:45','2005-01-10 12:06:45','1','1',NULL,'InfoWorld: ERP',NULL,'http://www.infoworld.com/rss/apps_erp.xml'),('d75c137d-0aae-3fbd-2906-41e2e0de0fe0',0,'2005-01-10 12:06:49','2005-01-10 12:06:49','1','1',NULL,'InfoWorld: Columnists',NULL,'http://www.infoworld.com/rss/columnists.xml'),('288dd54e-4460-658f-f7c3-41e2e031d2f8',0,'2005-01-10 12:06:54','2005-01-10 12:06:54','1','1',NULL,'InfoWorld: Business',NULL,'http://www.infoworld.com/rss/ebizstra.xml'),('5fb588ad-8d0a-c82e-920d-41e2e02745bf',0,'2005-01-10 12:06:58','2005-01-10 12:06:58','1','1',NULL,'InfoWorld: Business to Business',NULL,'http://www.infoworld.com/rss/ebustrat_btob.xml'),('9794dfd2-c784-d326-2ad7-41e2e00e2a81',0,'2005-01-10 12:07:02','2005-01-10 12:07:02','1','1',NULL,'InfoWorld: Business to Consumer',NULL,'http://www.infoworld.com/rss/ebustrat_btoc.xml'),('f162f572-b6da-7189-90be-41e2e02042de',0,'2005-01-10 12:07:06','2005-01-10 12:07:06','1','1',NULL,'InfoWorld: Portals',NULL,'http://www.infoworld.com/rss/ebustrat_portal.xml'),('3817838b-4dbf-84b6-1549-41e2e0b0995a',0,'2005-01-10 12:07:11','2005-01-10 12:07:11','1','1',NULL,'InfoWorld: Handheld Devices',NULL,'http://www.infoworld.com/rss/eusrhw_handdevc.xml'),('66f7b3eb-104a-dba5-7a66-41e2e065f261',0,'2005-01-10 12:07:15','2005-01-10 12:07:15','1','1',NULL,'InfoWorld: Mobile PC',NULL,'http://www.infoworld.com/rss/eusrhw_mobilepc.xml'),('9e00fb2a-6efe-1d1f-ad34-41e2e03a4ca6',0,'2005-01-10 12:07:19','2005-01-10 12:07:19','1','1',NULL,'InfoWorld: PCs',NULL,'http://www.infoworld.com/rss/eusrhw_pc.xml'),('d4c6cbba-d698-5f1f-d06a-41e2e05ba76d',0,'2005-01-10 12:07:23','2005-01-10 12:07:23','1','1',NULL,'InfoWorld: Processors & Components',NULL,'http://www.infoworld.com/rss/eusrhw_proccomp.xml'),('1c467be4-a384-2226-b896-41e2e07a8bf5',0,'2005-01-10 12:07:28','2005-01-10 12:07:28','1','1',NULL,'InfoWorld: Hardware',NULL,'http://www.infoworld.com/rss/hardware.xml'),('5915c10c-ea6e-ed29-b748-41e2e0feb9f0',0,'2005-01-10 12:07:32','2005-01-10 12:07:32','1','1',NULL,'InfoWorld: Grid Computing',NULL,'http://www.infoworld.com/rss/netwking_gridcmp.xml'),('916d98b1-a0df-59e6-d252-41e2e0a0fce5',0,'2005-01-10 12:07:36','2005-01-10 12:07:36','1','1',NULL,'InfoWorld: Network Infrastructure',NULL,'http://www.infoworld.com/rss/netwking_netinfra.xml'),('dfc0f4c7-69e2-3310-0a2a-41e2e0195d74',0,'2005-01-10 12:07:40','2005-01-10 12:07:40','1','1',NULL,'InfoWorld: Network Management',NULL,'http://www.infoworld.com/rss/netwking_netmgmt.xml'),('29007367-92cc-7976-501d-41e2e037017b',0,'2005-01-10 12:07:45','2005-01-10 12:07:45','1','1',NULL,'InfoWorld: Utilities Component',NULL,'http://www.infoworld.com/rss/netwking_utilcomp.xml'),('6abf823f-4b0d-e9a8-635c-41e2e039d4bf',0,'2005-01-10 12:07:49','2005-01-10 12:07:49','1','1',NULL,'InfoWorld: Networking',NULL,'http://www.infoworld.com/rss/networking.xml'),('e8f2ad3b-48dd-d577-2362-41e2e0e96eec',0,'2005-01-10 12:07:57','2005-01-10 12:07:57','1','1',NULL,'InfoWorld: Top News',NULL,'http://www.infoworld.com/rss/news.rdf'),('36a48d6e-be52-025b-aff8-41e2e0a360c5',0,'2005-01-10 12:08:02','2005-01-10 12:08:02','1','1',NULL,'InfoWorld: Top News',NULL,'http://www.infoworld.com/rss/news.xml'),('74662412-16b7-140f-bf08-41e2e088ee14',0,'2005-01-10 12:08:06','2005-01-10 12:08:06','1','1',NULL,'InfoWorld: Platforms',NULL,'http://www.infoworld.com/rss/platforms.xml'),('adf6ac68-003f-395e-c410-41e2e0e5463f',0,'2005-01-10 12:08:10','2005-01-10 12:08:10','1','1',NULL,'InfoWorld: Application Servers',NULL,'http://www.infoworld.com/rss/platform_appserv.xml'),('ebb409b9-3336-e5da-16d1-41e2e0a346ab',0,'2005-01-10 12:08:14','2005-01-10 12:08:14','1','1',NULL,'InfoWorld: Databases',NULL,'http://www.infoworld.com/rss/platform_database.xml'),('2c602f5d-5c2e-2b0a-558d-41e2e016671c',0,'2005-01-10 12:08:19','2005-01-10 12:08:19','1','1',NULL,'InfoWorld: Open Source',NULL,'http://www.infoworld.com/rss/platform_opensrc.xml'),('64050dae-dd95-8a54-a7f4-41e2e027805a',0,'2005-01-10 12:08:23','2005-01-10 12:08:23','1','1',NULL,'InfoWorld: Platforms',NULL,'http://www.infoworld.com/rss/platform_os.xml'),('9866d35a-406f-e916-f8c9-41e2e0214c5d',0,'2005-01-10 12:08:27','2005-01-10 12:08:27','1','1',NULL,'InfoWorld: Server Hardware',NULL,'http://www.infoworld.com/rss/platform_servhw.xml'),('d244ac22-d651-84d4-9726-41e2e029cbde',0,'2005-01-10 12:08:31','2005-01-10 12:08:31','1','1',NULL,'InfoWorld: Test Center Reviews',NULL,'http://www.infoworld.com/rss/reviews.xml'),('173d9c3d-a132-efbc-3653-41e2e07f665f',0,'2005-01-10 12:08:36','2005-01-10 12:08:36','1','1',NULL,'InfoWorld: Security',NULL,'http://www.infoworld.com/rss/security.xml'),('6362dd39-39e6-dd5e-96a5-41e2e07c3534',0,'2005-01-10 12:08:40','2005-01-10 12:08:40','1','1',NULL,'InfoWorld: Firewall',NULL,'http://www.infoworld.com/rss/security_firewall.xml'),('a8f67506-9b3c-92bc-046d-41e2e06b7fbc',0,'2005-01-10 12:08:44','2005-01-10 12:08:44','1','1',NULL,'InfoWorld: Security Appliances',NULL,'http://www.infoworld.com/rss/security_secapp.xml'),('dc99ca3c-a068-afcc-41f0-41e2e06d71e5',0,'2005-01-10 12:08:48','2005-01-10 12:08:48','1','1',NULL,'InfoWorld: VPN',NULL,'http://www.infoworld.com/rss/security_vpn.xml'),('29ac7be2-6a12-5710-435f-41e2e03416e1',0,'2005-01-10 12:08:53','2005-01-10 12:08:53','1','1',NULL,'InfoWorld: Vulnerability Management',NULL,'http://www.infoworld.com/rss/security_vulmgmt.xml'),('6776e391-4544-b6e6-caf4-41e2e07f46b9',0,'2005-01-10 12:08:57','2005-01-10 12:08:57','1','1',NULL,'InfoWorld: Wireless Security',NULL,'http://www.infoworld.com/rss/security_wiresec.xml'),('a24dcc9a-3fda-e158-9bcc-41e2e049a496',0,'2005-01-10 12:09:01','2005-01-10 12:09:01','1','1',NULL,'InfoWorld: Standards',NULL,'http://www.infoworld.com/rss/standards.xml'),('f336c24c-3349-c630-9cbd-41e2e08442a9',0,'2005-01-10 12:09:05','2005-01-10 12:09:05','1','1',NULL,'InfoWorld: Application Development',NULL,'http://www.infoworld.com/rss/stanprot_appdev.xml'),('300c6790-d2a3-75f0-8fa0-41e2e08b960b',0,'2005-01-10 12:09:10','2005-01-10 12:09:10','1','1',NULL,'InfoWorld: Internet',NULL,'http://www.infoworld.com/rss/stanprot_inet.xml'),('69babb1f-fe7c-c364-83ab-41e2e0e2b049',0,'2005-01-10 12:09:14','2005-01-10 12:09:14','1','1',NULL,'InfoWorld: Networking',NULL,'http://www.infoworld.com/rss/stanprot_netwking.xml'),('b97c4759-c783-6990-f94a-41e2e00fca1d',0,'2005-01-10 12:09:18','2005-01-10 12:09:18','1','1',NULL,'InfoWorld: Security',NULL,'http://www.infoworld.com/rss/stanprot_security.xml'),('27b8f326-6eba-c459-8a66-41e2e0c88453',0,'2005-01-10 12:09:23','2005-01-10 12:09:23','1','1',NULL,'InfoWorld: Storage',NULL,'http://www.infoworld.com/rss/stanprot_storage.xml'),('5ede0b55-899c-ccf0-a22b-41e2e0473f50',0,'2005-01-10 12:09:27','2005-01-10 12:09:27','1','1',NULL,'InfoWorld: Wireless',NULL,'http://www.infoworld.com/rss/stanprot_wireless.xml'),('9ba85759-7a84-79d2-5d5b-41e2e0215d1e',0,'2005-01-10 12:09:31','2005-01-10 12:09:31','1','1',NULL,'InfoWorld: Storage',NULL,'http://www.infoworld.com/rss/storage.xml'),('d9cde4fd-28f6-faed-2555-41e2e04de5cb',0,'2005-01-10 12:09:35','2005-01-10 12:09:35','1','1',NULL,'InfoWorld: Business Continuity',NULL,'http://www.infoworld.com/rss/storage_buscont.xml'),('2bdd0ae4-0ddc-8c7c-6bbe-41e2e1f0faeb',0,'2005-01-10 12:09:40','2005-01-10 12:09:40','1','1',NULL,'InfoWorld: Networked Storage',NULL,'http://www.infoworld.com/rss/storage_netstor.xml'),('68d12716-8c75-08d5-0785-41e2e1f8d408',0,'2005-01-10 12:09:44','2005-01-10 12:09:44','1','1',NULL,'InfoWorld: Storage Hardware',NULL,'http://www.infoworld.com/rss/storage_storhw.xml'),('a6502372-e900-cd75-587f-41e2e1a348db',0,'2005-01-10 12:09:48','2005-01-10 12:09:48','1','1',NULL,'InfoWorld: Storage Management',NULL,'http://www.infoworld.com/rss/storage_stormgmt.xml'),('e494037a-c6c4-8793-108b-41e2e15f0f95',0,'2005-01-10 12:09:52','2005-01-10 12:09:52','1','1',NULL,'InfoWorld: Telecom',NULL,'http://www.infoworld.com/rss/telecomm.xml'),('42778fab-3037-1280-059b-41e2e149966a',0,'2005-01-10 12:09:57','2005-01-10 12:09:57','1','1',NULL,'InfoWorld: Broadband',NULL,'http://www.infoworld.com/rss/telecom_broadband.xml'),('82a29d82-9ad5-65ca-b74d-41e2e1b9cc06',0,'2005-01-10 12:10:01','2005-01-10 12:10:01','1','1',NULL,'InfoWorld: Telephony',NULL,'http://www.infoworld.com/rss/telecom_telephony.xml'),('d05fc611-e074-96ea-4186-41e2e1577cea',0,'2005-01-10 12:10:05','2005-01-10 12:10:05','1','1',NULL,'InfoWorld: xSPs',NULL,'http://www.infoworld.com/rss/telecom_xsp.xml'),('21f3db54-63ce-67b8-999d-41e2e11b3dfb',0,'2005-01-10 12:10:10','2005-01-10 12:10:10','1','1',NULL,'InfoWorld: Web services',NULL,'http://www.infoworld.com/rss/webservices.xml'),('586629ae-ecab-f5d5-41f9-41e2e1eb8af6',0,'2005-01-10 12:10:14','2005-01-10 12:10:14','1','1',NULL,'InfoWorld: Web Services Applications',NULL,'http://www.infoworld.com/rss/websvcs_websvcap.xml'),('8a5a7213-cc37-924c-1fef-41e2e1607a6a',0,'2005-01-10 12:10:18','2005-01-10 12:10:18','1','1',NULL,'InfoWorld: Web Services Development',NULL,'http://www.infoworld.com/rss/websvcs_websvcdv.xml'),('d30ec90d-86e4-6b11-05dd-41e2e155d05a',0,'2005-01-10 12:10:22','2005-01-10 12:10:22','1','1',NULL,'InfoWorld: Web Services Integration',NULL,'http://www.infoworld.com/rss/websvcs_websvcin.xml'),('22f69fbf-65ef-2790-262f-41e2e17a93e2',0,'2005-01-10 12:10:27','2005-01-10 12:10:27','1','1',NULL,'InfoWorld: Web Management',NULL,'http://www.infoworld.com/rss/websvcs_websvcmg.xml'),('7ce83cab-8e9d-f80f-0013-41e2e17e3a32',0,'2005-01-10 12:10:31','2005-01-10 12:10:31','1','1',NULL,'InfoWorld: Wireless',NULL,'http://www.infoworld.com/rss/wireless.xml'),('bcaa2681-9bf1-9ef1-6be9-41e2e1c982e2',0,'2005-01-10 12:10:35','2005-01-10 12:10:35','1','1',NULL,'InfoWorld: Wireless Applications',NULL,'http://www.infoworld.com/rss/wireless_wireapps.xml'),('53e80be9-2d4c-d3d4-9e83-41e2e1a6d6be',0,'2005-01-10 12:10:40','2005-01-10 12:10:40','1','1',NULL,'InfoWorld: Wireless Network Infrastructure',NULL,'http://www.infoworld.com/rss/wireless_wirenetinfra.xml'),('53714c90-4807-b021-8e0a-41e2e16e168f',0,'2005-01-10 12:10:44','2005-01-10 12:10:44','1','1',NULL,'InfoWorld: Wireless Network Management',NULL,'http://www.infoworld.com/rss/wireless_wirenetmgmt.xml'),('a63fa605-f6f5-fdb5-9b4b-41e2e1f0b3dd',0,'2005-01-10 12:10:48','2005-01-10 12:10:48','1','1',NULL,'Linux Magazine',NULL,'http://www.linux-mag.com/lm.rss'),('5bd9c58c-b0a7-bdef-ebca-41e2e150faf6',0,'2005-01-10 12:10:57','2005-01-10 12:10:57','1','1',NULL,'Linux Journal - The Original Magazine of the Linux Community',NULL,'http://www.linuxjournal.com/news.rss'),('49e7d772-950e-9f18-b39e-41e2e1344dd3',0,'2005-01-10 12:11:06','2005-01-10 12:11:06','1','1',NULL,'macdailynews.com',NULL,'http://www.macdailynews.com/rss/rss.xml'),('b15d917f-6220-4792-f3cf-41e2e1295e2d',0,'2005-01-10 12:11:06','2005-01-10 12:11:06','1','1',NULL,'MacRumors',NULL,'http://www.macrumors.com/macrumors.xml'),('90634cab-75d2-4b3f-4424-41e2e18d34f1',0,'2005-01-10 12:11:11','2005-01-10 12:11:11','1','1',NULL,'GoogleGuy Says - Google Ranking Info',NULL,'http://www.markcarey.com/googleguy-says/index.rdf'),('3b548447-e8e8-b3d1-c4c7-41e2e1c46b85',0,'2005-01-10 12:11:12','2005-01-10 12:11:12','1','1',NULL,'CBS MarketWatch.com - Financial Services Industry News',NULL,'http://www.marketwatch.com/rss/financial/'),('dff224ac-4ccf-aed7-43b9-41e2e194fb1e',0,'2005-01-10 12:11:12','2005-01-10 12:11:12','1','1',NULL,'CBS MarketWatch.com - Internet Industry News',NULL,'http://www.marketwatch.com/rss/internet/'),('9abe4795-81da-da4b-c27d-41e2e1f12209',0,'2005-01-10 12:11:13','2005-01-10 12:11:13','1','1',NULL,'CBS MarketWatch.com - Personal Finance News',NULL,'http://www.marketwatch.com/rss/pf/'),('4dfb388a-1305-c6a6-bb1b-41e2e1ec0ea0',0,'2005-01-10 12:11:14','2005-01-10 12:11:14','1','1',NULL,'CBS MarketWatch.com - Software Industry News',NULL,'http://www.marketwatch.com/rss/software/'),('5a554d53-5f02-0030-fa51-41e2e1c4ec0b',0,'2005-01-10 12:11:15','2005-01-10 12:11:15','1','1',NULL,'CBS MarketWatch.com - Top Stories',NULL,'http://www.marketwatch.com/rss/topstories/'),('70a60db8-b993-ffdc-c7a2-41e2e1f9f2cc',0,'2005-01-10 12:11:16','2005-01-10 12:11:16','1','1',NULL,'Reuters: Business',NULL,'http://www.microsite.reuters.com/rss/businessNews'),('8bd7e961-fade-e048-969b-41e2e15a0523',0,'2005-01-10 12:11:16','2005-01-10 12:11:16','1','1',NULL,'Reuters: US Domestic News',NULL,'http://www.microsite.reuters.com/rss/domesticNews'),('157fe8ea-d139-355e-b64d-41e2e1d60dd1',0,'2005-01-10 12:11:17','2005-01-10 12:11:17','1','1',NULL,'Reuters: Politics',NULL,'http://www.microsite.reuters.com/rss/ElectionCoverage'),('92d037ee-06a3-418f-74c4-41e2e19c22f5',0,'2005-01-10 12:11:17','2005-01-10 12:11:17','1','1',NULL,'Reuters: Entertainment',NULL,'http://www.microsite.reuters.com/rss/Entertainment'),('1e69fa33-09c2-f086-1f49-41e2e130cd40',0,'2005-01-10 12:11:18','2005-01-10 12:11:18','1','1',NULL,'Reuters: Health',NULL,'http://www.microsite.reuters.com/rss/healthNews'),('9f9d5276-26a0-aadc-1e03-41e2e13d5fc3',0,'2005-01-10 12:11:18','2005-01-10 12:11:18','1','1',NULL,'Reuters: Life & Leisure',NULL,'http://www.microsite.reuters.com/rss/lifeAndLeisureNews'),('22ae3feb-5b25-0c61-7d1c-41e2e134a898',0,'2005-01-10 12:11:19','2005-01-10 12:11:19','1','1',NULL,'Reuters: Oddly Enough',NULL,'http://www.microsite.reuters.com/rss/oddlyEnoughNews'),('a446695a-c6d2-e970-0d13-41e2e10b4f0a',0,'2005-01-10 12:11:19','2005-01-10 12:11:19','1','1',NULL,'Reuters: Politics',NULL,'http://www.microsite.reuters.com/rss/politicsNews'),('4360b39e-9f38-94ab-68f5-41e2e1f8ebfc',0,'2005-01-10 12:11:20','2005-01-10 12:11:20','1','1',NULL,'Reuters: Science',NULL,'http://www.microsite.reuters.com/rss/scienceNews'),('bdbfe8e9-980b-c873-e3b0-41e2e1426554',0,'2005-01-10 12:11:20','2005-01-10 12:11:20','1','1',NULL,'Reuters: Sports',NULL,'http://www.microsite.reuters.com/rss/sportsNews'),('56773914-9860-cc40-5763-41e2e126f83f',0,'2005-01-10 12:11:21','2005-01-10 12:11:21','1','1',NULL,'Reuters: Technology',NULL,'http://www.microsite.reuters.com/rss/technologyNews'),('d098cf04-c313-867c-11d9-41e2e1fd4fe3',0,'2005-01-10 12:11:21','2005-01-10 12:11:21','1','1',NULL,'Reuters: Top News',NULL,'http://www.microsite.reuters.com/rss/topNews'),('563ae3f7-df64-9e60-d02a-41e2e182eacb',0,'2005-01-10 12:11:22','2005-01-10 12:11:22','1','1',NULL,'Reuters: World',NULL,'http://www.microsite.reuters.com/rss/worldNews'),('e52e8793-7b64-d778-4382-41e2e11bda85',0,'2005-01-10 12:11:26','2005-01-10 12:11:26','1','1',NULL,'NYT > Arts',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Arts.xml'),('76395a02-40cb-98c1-7e38-41e2e1cfef20',0,'2005-01-10 12:11:31','2005-01-10 12:11:31','1','1',NULL,'NYT > Automobiles',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Automobiles.xml'),('f329defa-0a0b-72bd-874b-41e2e1d870e6',0,'2005-01-10 12:11:35','2005-01-10 12:11:35','1','1',NULL,'NYT > Books',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Books.xml'),('7bd169d6-5a81-5486-a70b-41e2e1bdb372',0,'2005-01-10 12:11:40','2005-01-10 12:11:40','1','1',NULL,'NYT > Business',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Business.xml'),('32da2be6-a2be-a445-baeb-41e2e10be2b4',0,'2005-01-10 12:11:59','2005-01-10 12:11:59','1','1',NULL,'NYT > Circuits',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Circuits.xml'),('abc4b934-4df7-b840-2c71-41e2e1b5c624',0,'2005-01-10 12:12:03','2005-01-10 12:12:03','1','1',NULL,'NYT > Fashion and Style',NULL,'http://www.nytimes.com/services/xml/rss/nyt/FashionandStyle.xml'),('3e368e6c-a9c8-9db5-8c58-41e2e13063cc',0,'2005-01-10 12:12:08','2005-01-10 12:12:08','1','1',NULL,'NYT > Health',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Health.xml'),('ad6f00c5-4d44-a7f3-3956-41e2e13cb440',0,'2005-01-10 12:12:12','2005-01-10 12:12:12','1','1',NULL,'NYT > Home Page',NULL,'http://www.nytimes.com/services/xml/rss/nyt/HomePage.xml'),('3243b29a-5bf0-73e7-ff62-41e2e10d3f1e',0,'2005-01-10 12:12:17','2005-01-10 12:12:17','1','1',NULL,'NYT > International',NULL,'http://www.nytimes.com/services/xml/rss/nyt/International.xml'),('c0a95969-5b4c-0813-5070-41e2e1dc098d',0,'2005-01-10 12:12:21','2005-01-10 12:12:21','1','1',NULL,'NYT > Magazine',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Magazine.xml'),('b26227be-c4ac-d0db-3534-41e2e1e3c2bd',0,'2005-01-10 12:12:31','2005-01-10 12:12:31','1','1',NULL,'NYT > Media and Advertising',NULL,'http://www.nytimes.com/services/xml/rss/nyt/MediaandAdvertising.xml'),('949123f7-e6b3-9c9b-6d3f-41e2e1ac24ca',0,'2005-01-10 12:12:41','2005-01-10 12:12:41','1','1',NULL,'NYT > Movie Reviews',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Movies.xml'),('1daf4a0b-1a1a-3f5b-9114-41e2e1ac5bd2',0,'2005-01-10 12:12:46','2005-01-10 12:12:46','1','1',NULL,'NYT > Multimedia',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Multimedia.xml'),('93f6049a-0b8c-368f-84db-41e2e10cf8dc',0,'2005-01-10 12:12:50','2005-01-10 12:12:50','1','1',NULL,'NYT > National',NULL,'http://www.nytimes.com/services/xml/rss/nyt/National.xml'),('162fe62d-82ad-3acb-e40b-41e2e1ca3a42',0,'2005-01-10 12:12:55','2005-01-10 12:12:55','1','1',NULL,'NYT > New York Region',NULL,'http://www.nytimes.com/services/xml/rss/nyt/NYRegion.xml'),('8b3b6aa8-afe3-f354-6934-41e2e1289783',0,'2005-01-10 12:12:59','2005-01-10 12:12:59','1','1',NULL,'NYT > Opinion',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Opinion.xml'),('16f15af7-8097-d8ff-e71b-41e2e1e3e56d',0,'2005-01-10 12:13:04','2005-01-10 12:13:04','1','1',NULL,'NYT > Most E-mailed Articles',NULL,'http://www.nytimes.com/services/xml/rss/nyt/pop_top.xml'),('8c824e5a-a19e-3d18-f0b3-41e2e1347681',0,'2005-01-10 12:13:08','2005-01-10 12:13:08','1','1',NULL,'NYT > Real Estate',NULL,'http://www.nytimes.com/services/xml/rss/nyt/RealEstate.xml'),('18a8f2f9-a4bb-40a0-c2fa-41e2e1a22c5b',0,'2005-01-10 12:13:13','2005-01-10 12:13:13','1','1',NULL,'NYT > Science',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Science.xml'),('8b5a69c3-7872-7621-978d-41e2e16ed94a',0,'2005-01-10 12:13:17','2005-01-10 12:13:17','1','1',NULL,'NYT > Sports',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Sports.xml'),('2c248cf4-cbb0-1ae3-98dc-41e2e1f4b908',0,'2005-01-10 12:13:22','2005-01-10 12:13:22','1','1',NULL,'NYT > Technology',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Technology.xml'),('b87e7042-bb54-8c71-eb00-41e2e1fe56f3',0,'2005-01-10 12:13:26','2005-01-10 12:13:26','1','1',NULL,'NYT > Theater',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Theater.xml'),('56a41258-299c-b87a-f11f-41e2e1dea02e',0,'2005-01-10 12:13:40','2005-01-10 12:13:40','1','1',NULL,'NYT > Travel',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Travel.xml'),('cad9e735-4edb-cf76-fa57-41e2e1bd9573',0,'2005-01-10 12:13:44','2005-01-10 12:13:44','1','1',NULL,'NYT > Washington',NULL,'http://www.nytimes.com/services/xml/rss/nyt/Washington.xml'),('6c4ffca7-d6e0-762b-7319-41e2e1942650',0,'2005-01-10 12:13:49','2005-01-10 12:13:49','1','1',NULL,'NYT > Week in Review',NULL,'http://www.nytimes.com/services/xml/rss/nyt/WeekinReview.xml'),('8fc8560f-f3b2-2f3f-7a8e-41e2e2b89d5a',0,'2005-01-10 12:13:54','2005-01-10 12:13:54','1','1',NULL,'Telegraph Arts | Books',NULL,'http://www.telegraph.co.uk/newsfeed/rss/arts_books.xml'),('5ed58c84-963b-dc9e-9192-41e2e286de6b',0,'2005-01-10 12:13:59','2005-01-10 12:13:59','1','1',NULL,'Telegraph Arts',NULL,'http://www.telegraph.co.uk/newsfeed/rss/arts_main.xml'),('3f8d6369-2cb1-34ff-2a84-41e2e2ad2838',0,'2005-01-10 12:14:04','2005-01-10 12:14:04','1','1',NULL,'Telegraph Connected',NULL,'http://www.telegraph.co.uk/newsfeed/rss/connected.xml'),('26c18e81-99a1-dd1d-2971-41e2e2b1f154',0,'2005-01-10 12:14:09','2005-01-10 12:14:09','1','1',NULL,'Telegraph Education',NULL,'http://www.telegraph.co.uk/newsfeed/rss/education.xml'),('f09bdcc6-8e18-f9b6-a868-41e2e26e388c',0,'2005-01-10 12:14:13','2005-01-10 12:14:13','1','1',NULL,'Telegraph Expat',NULL,'http://www.telegraph.co.uk/newsfeed/rss/expat.xml'),('caa98477-7101-c99e-018d-41e2e2c7c022',0,'2005-01-10 12:14:18','2005-01-10 12:14:18','1','1',NULL,'Telegraph Fashion',NULL,'http://www.telegraph.co.uk/newsfeed/rss/fashion.xml'),('abf7648d-d5c0-31cd-b450-41e2e294e388',0,'2005-01-10 12:14:23','2005-01-10 12:14:23','1','1',NULL,'Telegraph Gardening',NULL,'http://www.telegraph.co.uk/newsfeed/rss/gardening.xml'),('a1762049-8d34-762a-49ff-41e2e23d820e',0,'2005-01-10 12:14:28','2005-01-10 12:14:28','1','1',NULL,'Telegraph Health',NULL,'http://www.telegraph.co.uk/newsfeed/rss/health.xml'),('3f266fb5-3e33-91e2-73be-41e2e2dd9764',0,'2005-01-10 12:14:33','2005-01-10 12:14:33','1','1',NULL,'Telegraph Opinion | Leaders',NULL,'http://www.telegraph.co.uk/newsfeed/rss/leaders.xml'),('108906cd-58af-9a2e-a04e-41e2e23db4a1',0,'2005-01-10 12:14:38','2005-01-10 12:14:38','1','1',NULL,'Telegraph Opinion | Letters',NULL,'http://www.telegraph.co.uk/newsfeed/rss/letters.xml'),('b4eff965-86bf-e571-fef0-41e2e234081f',0,'2005-01-10 12:14:42','2005-01-10 12:14:42','1','1',NULL,'Telegraph Money | Markets',NULL,'http://www.telegraph.co.uk/newsfeed/rss/money_markets.xml'),('9bebccf6-65fe-7240-9802-41e2e2a5d2b6',0,'2005-01-10 12:14:47','2005-01-10 12:14:47','1','1',NULL,'Telegraph Money | Personal Finance',NULL,'http://www.telegraph.co.uk/newsfeed/rss/money_pf.xml'),('6268c9b4-82b3-74b5-be2d-41e2e280945c',0,'2005-01-10 12:14:52','2005-01-10 12:14:52','1','1',NULL,'Telegraph Business | Small Business',NULL,'http://www.telegraph.co.uk/newsfeed/rss/money_smallbus.xml'),('6313eb76-e22b-cb06-b80b-41e2e25b1270',0,'2005-01-10 12:14:57','2005-01-10 12:14:57','1','1',NULL,'Telegraph Motoring',NULL,'http://www.telegraph.co.uk/newsfeed/rss/motoring.xml'),('42131d7e-df56-90a2-6ff1-41e2e21b63d4',0,'2005-01-10 12:15:02','2005-01-10 12:15:02','1','1',NULL,'Telegraph News | Breaking News',NULL,'http://www.telegraph.co.uk/newsfeed/rss/news_breaking.xml'),('13dd2763-7cff-361b-6864-41e2e2f367cb',0,'2005-01-10 12:15:07','2005-01-10 12:15:07','1','1',NULL,'Telegraph News | International News',NULL,'http://www.telegraph.co.uk/newsfeed/rss/news_international.xml'),('e00bd292-8c0c-4f96-49b0-41e2e2ff2f9a',0,'2005-01-10 12:15:11','2005-01-10 12:15:11','1','1',NULL,'Telegraph News | Front Page News',NULL,'http://www.telegraph.co.uk/newsfeed/rss/news_main.xml'),('b2da8888-120f-bfb7-67d6-41e2e28c5532',0,'2005-01-10 12:15:16','2005-01-10 12:15:16','1','1',NULL,'Telegraph News | UK News',NULL,'http://www.telegraph.co.uk/newsfeed/rss/news_uk.xml'),('885e54fd-09ba-9cdc-a9e3-41e2e2ebb292',0,'2005-01-10 12:15:21','2005-01-10 12:15:21','1','1',NULL,'Telegraph Opinion',NULL,'http://www.telegraph.co.uk/newsfeed/rss/opinion.xml'),('689b064d-8821-4b66-9788-41e2e2e48838',0,'2005-01-10 12:15:26','2005-01-10 12:15:26','1','1',NULL,'Telegraph Property',NULL,'http://www.telegraph.co.uk/newsfeed/rss/property.xml'),('c36c0a13-8f54-a6c5-6b0b-41e2e27050c6',0,'2005-01-10 12:15:31','2005-01-10 12:15:31','1','1',NULL,'Telegraph Sport | Cricket',NULL,'http://www.telegraph.co.uk/newsfeed/rss/sport_cricket.xml'),('4ecabb98-a3cd-4b96-d288-41e2e2943a21',0,'2005-01-10 12:15:36','2005-01-10 12:15:36','1','1',NULL,'Telegraph Sport | Football',NULL,'http://www.telegraph.co.uk/newsfeed/rss/sport_football.xml'),('e911ad05-13d6-869d-6e85-41e2e24224d4',0,'2005-01-10 12:15:40','2005-01-10 12:15:40','1','1',NULL,'Telegraph Sport | Golf',NULL,'http://www.telegraph.co.uk/newsfeed/rss/sport_golf.xml'),('ba80d353-4126-d39e-363e-41e2e29b93b6',0,'2005-01-10 12:15:45','2005-01-10 12:15:45','1','1',NULL,'Telegraph Sport',NULL,'http://www.telegraph.co.uk/newsfeed/rss/sport_main.xml'),('bd5e4012-976e-3cdb-bacb-41e2e2409362',0,'2005-01-10 12:15:50','2005-01-10 12:15:50','1','1',NULL,'Telegraph Sport | Rugby Union',NULL,'http://www.telegraph.co.uk/newsfeed/rss/sport_rugu.xml'),('c7d5ddd0-3e83-5b31-5158-41e2e2c8eeaf',0,'2005-01-10 12:15:55','2005-01-10 12:15:55','1','1',NULL,'Telegraph Travel',NULL,'http://www.telegraph.co.uk/newsfeed/rss/travel_main.xml'),('933a0c4c-b2b9-cef9-a3a1-41e2e2afe273',0,'2005-01-10 12:16:00','2005-01-10 12:16:00','1','1',NULL,'Telegraph Wine',NULL,'http://www.telegraph.co.uk/newsfeed/rss/wine.xml'),('3ac7981d-0c22-8f87-72c3-41e2e2965536',0,'2005-01-10 12:16:05','2005-01-10 12:16:05','1','1',NULL,'washingtonpost.com - Business',NULL,'http://www.washingtonpost.com/wp-srv/business/rssheadlines.xml'),('c09c8abc-7992-2dc4-face-41e2e268cfb9',0,'2005-01-10 12:16:09','2005-01-10 12:16:09','1','1',NULL,'washingtonpost.com - Education',NULL,'http://www.washingtonpost.com/wp-srv/education/rssheadlines.xml'),('6de7e3cf-20a5-8ae9-9b91-41e2e2531c32',0,'2005-01-10 12:16:14','2005-01-10 12:16:14','1','1',NULL,'washingtonpost.com - Health',NULL,'http://www.washingtonpost.com/wp-srv/health/rssheadlines.xml'),('e6962597-5923-ce09-9db4-41e2e2864635',0,'2005-01-10 12:16:18','2005-01-10 12:16:18','1','1',NULL,'washingtonpost.com - Metro',NULL,'http://www.washingtonpost.com/wp-srv/metro/rssheadlines.xml'),('644ba5b6-6a86-c78c-9fc7-41e2e213bcc8',0,'2005-01-10 12:16:23','2005-01-10 12:16:23','1','1',NULL,'washingtonpost.com - Nation',NULL,'http://www.washingtonpost.com/wp-srv/nation/rssheadlines.xml'),('e06ad79d-0a57-7366-1e30-41e2e2e06c22',0,'2005-01-10 12:16:27','2005-01-10 12:16:27','1','1',NULL,'washingtonpost.com - Opinion',NULL,'http://www.washingtonpost.com/wp-srv/opinion/rssheadlines.xml'),('b04689c2-b068-7223-d8af-41e2e2687b82',0,'2005-01-10 12:16:32','2005-01-10 12:16:32','1','1',NULL,'washingtonpost.com - 2004 Election',NULL,'http://www.washingtonpost.com/wp-srv/politics/elections/2004/rssheadlines.xml'),('4660f0e9-37bf-af65-dbb3-41e2e2e3aef3',0,'2005-01-10 12:16:37','2005-01-10 12:16:37','1','1',NULL,'washingtonpost.com - Politics',NULL,'http://www.washingtonpost.com/wp-srv/politics/rssheadlines.xml'),('d549ff0c-6666-a153-9804-41e2e2dea5de',0,'2005-01-10 12:16:41','2005-01-10 12:16:41','1','1',NULL,'washingtonpost.com - Real Estate',NULL,'http://www.washingtonpost.com/wp-srv/realestate/rssheadlines.xml'),('6204cd5b-0bf2-eda1-1c1b-41e2e2475997',0,'2005-01-10 12:16:46','2005-01-10 12:16:46','1','1',NULL,'washingtonpost.com - Sports',NULL,'http://www.washingtonpost.com/wp-srv/sports/rssheadlines.xml'),('227a0fa6-21fb-a275-66f8-41e2e2b91da7',0,'2005-01-10 12:16:51','2005-01-10 12:16:51','1','1',NULL,'washingtonpost.com - Technology',NULL,'http://www.washingtonpost.com/wp-srv/technology/rssheadlines.xml'),('b8ef9e66-846e-8c8a-5e37-41e2e2139fb6',0,'2005-01-10 12:16:55','2005-01-10 12:16:55','1','1',NULL,'washingtonpost.com - World',NULL,'http://www.washingtonpost.com/wp-srv/world/rssheadlines.xml'),('1426c66e-f338-2f01-6c37-41e2e250f9c6',0,'2005-01-10 12:17:09','2005-01-10 12:17:09','1','1',NULL,'Wired News',NULL,'http://www.wired.com/news_drop/netcenter/netcenter.rdf'),('dad0faed-5e63-a3d3-2600-41e2e25b3432',0,'2005-01-10 12:17:13','2005-01-10 12:17:13','1','1',NULL,'African News from World Press Review',NULL,'http://www.worldpress.org/feeds/Africa.xml'),('975ea85d-b5e3-147d-9a4e-41e2e2bd803c',0,'2005-01-10 12:17:18','2005-01-10 12:17:18','1','1',NULL,'Latin American and Canadian News from World Press Review',NULL,'http://www.worldpress.org/feeds/Americas.xml'),('5c4cc320-8083-64c3-3e26-41e2e2f6b657',0,'2005-01-10 12:17:23','2005-01-10 12:17:23','1','1',NULL,'Asian News from World Press Review',NULL,'http://www.worldpress.org/feeds/Asia.xml'),('2b354b70-9a4f-f7ce-a98e-41e2e21f0c8f',0,'2005-01-10 12:17:28','2005-01-10 12:17:28','1','1',NULL,'European News from World Press Review',NULL,'http://www.worldpress.org/feeds/Europe.xml'),('dc142b87-1213-1748-38c8-41e2e23eb448',0,'2005-01-10 12:17:32','2005-01-10 12:17:32','1','1',NULL,'Middle Eastern News from World Press Review',NULL,'http://www.worldpress.org/feeds/Mideast.xml'),('ade6b2d5-1c1d-f17c-564c-41e2e20caaa3',0,'2005-01-10 12:17:37','2005-01-10 12:17:37','1','1',NULL,'Top Headlines from World Press Review',NULL,'http://www.worldpress.org/feeds/topstories.xml'),('3c13d9ed-6b2e-ffc0-432a-41e2e2bd099b',0,'2005-01-10 12:17:42','2005-01-10 12:17:42','1','1',NULL,'Breaking News Headlines from Around the World, Powered by Worldpress.org',NULL,'http://www.worldpress.org/feeds/worldpresswire.xml'),('cb27f088-bcd4-6378-d754-41e2e2c815b8',0,'2005-01-10 12:17:50','2005-01-10 12:17:50','1','1',NULL,'Linux Journal - The Original Magazine of the Linux Community',NULL,'http://www3.linuxjournal.com/news.rss'),('d3b812de-b09b-d19d-1ef7-41e2e2c7175b',0,'2005-01-10 12:17:51','2005-01-10 12:17:51','1','1',NULL,'ZDNet News - Front Door',NULL,'http://zdnet.com.com/2036-2_2-0.xml'),('4a1301d9-c4fe-f6be-4edb-41e2e22b41b0',0,'2005-01-10 12:17:52','2005-01-10 12:17:52','1','1',NULL,'ZDNet News - Hardware',NULL,'http://zdnet.com.com/2509-1103_2-0-10.xml'),('b5cc579b-0de1-f1fb-2f4c-41e2e23dc415',0,'2005-01-10 12:17:52','2005-01-10 12:17:52','1','1',NULL,'ZDNet News - Software',NULL,'http://zdnet.com.com/2509-1104_2-0-10.xml'),('36085453-2d39-d3b9-1b9c-41e2e2ab4ee7',0,'2005-01-10 12:17:53','2005-01-10 12:17:53','1','1',NULL,'ZDNet News - Security',NULL,'http://zdnet.com.com/2509-1105_2-0-10.xml'),('ae0619d9-4e7e-f462-6d85-41e2e21bfd46',0,'2005-01-10 12:17:53','2005-01-10 12:17:53','1','1',NULL,'ZDNet News - Commentary',NULL,'http://zdnet.com.com/2509-1107_2-0-10.xml'),('30d86812-bc01-d5d3-faca-41e2e2d9bd0d',0,'2005-01-10 12:17:54','2005-01-10 12:17:54','1','1',NULL,'ZDNet News - Latest Headlines',NULL,'http://zdnet.com.com/2509-11_2-0-20.xml'),('90df24f4-9717-7a1b-ed5b-41e77fbcfba7',0,'2005-01-14 00:16:00','2005-01-14 00:16:00','1','1','1','SourceForge.net: SF.net Recent Project Donations: SugarCRM',NULL,'http://sourceforge.net/export/rss2_projdonors.php?group_id=107819'),('4bbca87f-2017-5488-d8e0-41e7808c2553',0,'2005-01-14 00:17:00','2005-01-14 00:17:00','1','1','1','SourceForge.net: SF.net Project News: SugarCRM',NULL,'http://sourceforge.net/export/rss2_projnews.php?group_id=107819'),('c03b64eb-d0d4-a188-e203-41e7804677bd',0,'2005-01-14 00:17:00','2005-01-14 00:17:00','1','1','1','SourceForge.net: SF.net Project News: SugarCRM  (including full news text)',NULL,'http://sourceforge.net/export/rss2_projnews.php?group_id=107819&rss_fulltext=1'),('42210fc0-d21c-cff6-2c72-41e780195bfc',0,'2005-01-14 00:17:00','2005-01-14 00:17:00','1','1','1','SourceForge.net: Project File Releases: SugarCRM',NULL,'http://sourceforge.net/export/rss2_projfiles.php?group_id=107819'),('d410145c-e217-8ad3-0e99-41e78038cf4a',0,'2005-01-14 00:17:00','2005-01-14 00:17:00','1','1','1','SourceForge.net: Project Documentation (DocManager) Updates: SugarCRM',NULL,'http://sourceforge.net/export/rss2_projdocs.php?group_id=107819'),('db197b9c-9158-d779-0be3-41e780eda0f6',0,'2005-01-14 00:17:00','2005-01-14 00:17:00','1','1','1','SourceForge.net: Project Summary: SugarCRM  (sugarcrm project)',NULL,'http://sourceforge.net/export/rss2_projsummary.php?group_id=107819');
/*!40000 ALTER TABLE `feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fields_meta_data`
--

DROP TABLE IF EXISTS `fields_meta_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fields_meta_data` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) default NULL,
  `vname` varchar(255) default NULL,
  `comments` varchar(255) default NULL,
  `help` varchar(255) default NULL,
  `custom_module` varchar(255) default NULL,
  `type` varchar(255) default NULL,
  `len` int(11) default NULL,
  `required` tinyint(1) default '0',
  `default_value` varchar(255) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  `audited` tinyint(1) default '0',
  `massupdate` tinyint(1) default '0',
  `duplicate_merge` smallint(6) default '0',
  `reportable` tinyint(1) default '1',
  `importable` varchar(255) default NULL,
  `ext1` varchar(255) default NULL,
  `ext2` varchar(255) default NULL,
  `ext3` varchar(255) default NULL,
  `ext4` text,
  PRIMARY KEY  (`id`),
  KEY `idx_meta_id_del` (`id`,`deleted`),
  KEY `idx_meta_cm_del` (`custom_module`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fields_meta_data`
--

LOCK TABLES `fields_meta_data` WRITE;
/*!40000 ALTER TABLE `fields_meta_data` DISABLE KEYS */;
INSERT INTO `fields_meta_data` VALUES ('Usersasterisk_outbound_c','asterisk_outbound_c','LBL_ASTERISK_OUTBOUND',NULL,NULL,'Users','bool',45,0,'0','2010-05-31 06:22:55',0,0,0,0,1,'true',NULL,NULL,NULL,NULL),('Usersasterisk_inbound_c','asterisk_inbound_c','LBL_ASTERISK_INBOUND',NULL,NULL,'Users','bool',45,0,'0','2010-05-31 06:22:55',0,0,0,0,1,'true',NULL,NULL,NULL,NULL),('Usersasterisk_ext_c','asterisk_ext_c','LBL_ASTERISK_EXT',NULL,NULL,'Users','varchar',45,0,NULL,'2010-05-31 06:22:55',0,0,0,0,1,'true',NULL,NULL,NULL,NULL),('Callsasterisk_caller_id_c','asterisk_caller_id_c','LBL_ASTERISK_CALLER_ID',NULL,'trimmed callerId','Calls','varchar',45,0,NULL,'2010-05-31 06:22:55',0,0,0,0,0,'true',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `fields_meta_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders`
--

DROP TABLE IF EXISTS `folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders` (
  `id` char(36) NOT NULL,
  `name` varchar(25) NOT NULL,
  `folder_type` varchar(25) default NULL,
  `parent_folder` char(36) default NULL,
  `has_child` tinyint(1) default '0',
  `is_group` tinyint(1) default '0',
  `is_dynamic` tinyint(1) default '0',
  `dynamic_query` text,
  `assign_to_id` char(36) default NULL,
  `created_by` char(36) NOT NULL,
  `modified_by` char(36) NOT NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_parent_folder` (`parent_folder`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders`
--

LOCK TABLES `folders` WRITE;
/*!40000 ALTER TABLE `folders` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_rel`
--

DROP TABLE IF EXISTS `folders_rel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_rel` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `polymorphic_module` varchar(25) NOT NULL,
  `polymorphic_id` char(36) NOT NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_poly_module_poly_id` (`polymorphic_module`,`polymorphic_id`),
  KEY `idx_folders_rel_folder_id` (`folder_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_rel`
--

LOCK TABLES `folders_rel` WRITE;
/*!40000 ALTER TABLE `folders_rel` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_rel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `folders_subscriptions`
--

DROP TABLE IF EXISTS `folders_subscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `folders_subscriptions` (
  `id` char(36) NOT NULL,
  `folder_id` char(36) NOT NULL,
  `assigned_user_id` char(36) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_folder_id_assigned_user_id` (`folder_id`,`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `folders_subscriptions`
--

LOCK TABLES `folders_subscriptions` WRITE;
/*!40000 ALTER TABLE `folders_subscriptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `folders_subscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `iframes`
--

DROP TABLE IF EXISTS `iframes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `iframes` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `placement` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL default '0',
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_by` char(36) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_cont_name` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `iframes`
--

LOCK TABLES `iframes` WRITE;
/*!40000 ALTER TABLE `iframes` DISABLE KEYS */;
/*!40000 ALTER TABLE `iframes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `import_maps`
--

DROP TABLE IF EXISTS `import_maps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `import_maps` (
  `id` char(36) NOT NULL,
  `name` varchar(254) NOT NULL,
  `source` varchar(36) NOT NULL,
  `enclosure` varchar(1) NOT NULL default ' ',
  `delimiter` varchar(1) NOT NULL default ',',
  `module` varchar(36) NOT NULL,
  `content` blob,
  `default_values` blob,
  `has_header` tinyint(1) NOT NULL default '1',
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) default NULL,
  `is_published` varchar(3) NOT NULL default 'no',
  PRIMARY KEY  (`id`),
  KEY `idx_owner_module_name` (`assigned_user_id`,`module`,`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `import_maps`
--

LOCK TABLES `import_maps` WRITE;
/*!40000 ALTER TABLE `import_maps` DISABLE KEYS */;
/*!40000 ALTER TABLE `import_maps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email`
--

DROP TABLE IF EXISTS `inbound_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `name` varchar(255) default NULL,
  `status` varchar(25) NOT NULL default 'Active',
  `server_url` varchar(100) NOT NULL,
  `email_user` varchar(100) NOT NULL,
  `email_password` varchar(100) NOT NULL,
  `port` int(5) NOT NULL,
  `service` varchar(50) NOT NULL,
  `mailbox` text NOT NULL,
  `delete_seen` tinyint(1) default '0',
  `mailbox_type` varchar(10) default NULL,
  `template_id` char(36) default NULL,
  `stored_options` text,
  `group_id` char(36) default NULL,
  `is_personal` tinyint(1) NOT NULL default '0',
  `groupfolder_id` char(36) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email`
--

LOCK TABLES `inbound_email` WRITE;
/*!40000 ALTER TABLE `inbound_email` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email_autoreply`
--

DROP TABLE IF EXISTS `inbound_email_autoreply`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email_autoreply` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `autoreplied_to` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_ie_autoreplied_to` (`autoreplied_to`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email_autoreply`
--

LOCK TABLES `inbound_email_autoreply` WRITE;
/*!40000 ALTER TABLE `inbound_email_autoreply` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email_autoreply` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbound_email_cache_ts`
--

DROP TABLE IF EXISTS `inbound_email_cache_ts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inbound_email_cache_ts` (
  `id` varchar(255) NOT NULL,
  `ie_timestamp` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbound_email_cache_ts`
--

LOCK TABLES `inbound_email_cache_ts` WRITE;
/*!40000 ALTER TABLE `inbound_email_cache_ts` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbound_email_cache_ts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads`
--

DROP TABLE IF EXISTS `leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads` (
  `id` char(36) NOT NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `salutation` varchar(5) default NULL,
  `first_name` varchar(100) default NULL,
  `last_name` varchar(100) default NULL,
  `title` varchar(100) default NULL,
  `department` varchar(100) default NULL,
  `do_not_call` tinyint(1) default '0',
  `phone_home` varchar(25) default NULL,
  `phone_mobile` varchar(25) default NULL,
  `phone_work` varchar(25) default NULL,
  `phone_other` varchar(25) default NULL,
  `phone_fax` varchar(25) default NULL,
  `primary_address_street` varchar(150) default NULL,
  `primary_address_city` varchar(100) default NULL,
  `primary_address_state` varchar(100) default NULL,
  `primary_address_postalcode` varchar(20) default NULL,
  `primary_address_country` varchar(255) default NULL,
  `alt_address_street` varchar(150) default NULL,
  `alt_address_city` varchar(100) default NULL,
  `alt_address_state` varchar(100) default NULL,
  `alt_address_postalcode` varchar(20) default NULL,
  `alt_address_country` varchar(255) default NULL,
  `assistant` varchar(75) default NULL,
  `assistant_phone` varchar(25) default NULL,
  `converted` tinyint(1) NOT NULL default '0',
  `refered_by` varchar(100) default NULL,
  `lead_source` varchar(100) default NULL,
  `lead_source_description` text,
  `status` varchar(100) default NULL,
  `status_description` text,
  `reports_to_id` char(36) default NULL,
  `account_name` varchar(255) default NULL,
  `account_description` text,
  `contact_id` char(36) default NULL,
  `account_id` char(36) default NULL,
  `opportunity_id` char(36) default NULL,
  `opportunity_name` varchar(255) default NULL,
  `opportunity_amount` varchar(50) default NULL,
  `campaign_id` char(36) default NULL,
  `portal_name` varchar(255) default NULL,
  `portal_app` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_lead_acct_name_first` (`account_name`,`deleted`),
  KEY `idx_lead_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_lead_del_stat` (`last_name`,`status`,`deleted`,`first_name`),
  KEY `idx_lead_opp_del` (`opportunity_id`,`deleted`),
  KEY `idx_leads_acct_del` (`account_id`,`deleted`),
  KEY `idx_del_user` (`deleted`,`assigned_user_id`),
  KEY `idx_lead_assigned` (`assigned_user_id`),
  KEY `idx_lead_contact` (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads`
--

LOCK TABLES `leads` WRITE;
/*!40000 ALTER TABLE `leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `leads_audit`
--

DROP TABLE IF EXISTS `leads_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `leads_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime default NULL,
  `created_by` varchar(36) default NULL,
  `field_name` varchar(100) default NULL,
  `data_type` varchar(100) default NULL,
  `before_value_string` varchar(255) default NULL,
  `after_value_string` varchar(255) default NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `leads_audit`
--

LOCK TABLES `leads_audit` WRITE;
/*!40000 ALTER TABLE `leads_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `leads_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `linked_documents`
--

DROP TABLE IF EXISTS `linked_documents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `linked_documents` (
  `id` varchar(36) NOT NULL,
  `parent_id` varchar(36) default NULL,
  `parent_type` varchar(25) default NULL,
  `document_id` varchar(36) default NULL,
  `document_revision_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_parent_document` (`parent_type`,`parent_id`,`document_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `linked_documents`
--

LOCK TABLES `linked_documents` WRITE;
/*!40000 ALTER TABLE `linked_documents` DISABLE KEYS */;
/*!40000 ALTER TABLE `linked_documents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings`
--

DROP TABLE IF EXISTS `meetings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings` (
  `id` char(36) NOT NULL,
  `name` varchar(50) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `location` varchar(50) default NULL,
  `duration_hours` int(2) default NULL,
  `duration_minutes` int(2) default NULL,
  `date_start` datetime default NULL,
  `date_end` date default NULL,
  `parent_type` varchar(25) default NULL,
  `status` varchar(25) default NULL,
  `parent_id` char(36) default NULL,
  `reminder_time` int(11) default '-1',
  `outlook_id` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_mtg_name` (`name`),
  KEY `idx_meet_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_meet_stat_del` (`assigned_user_id`,`status`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings`
--

LOCK TABLES `meetings` WRITE;
/*!40000 ALTER TABLE `meetings` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_contacts`
--

DROP TABLE IF EXISTS `meetings_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_contacts` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) default NULL,
  `contact_id` varchar(36) default NULL,
  `required` varchar(1) default '1',
  `accept_status` varchar(25) default 'none',
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_con_mtg_mtg` (`meeting_id`),
  KEY `idx_con_mtg_con` (`contact_id`),
  KEY `idx_meeting_contact` (`meeting_id`,`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_contacts`
--

LOCK TABLES `meetings_contacts` WRITE;
/*!40000 ALTER TABLE `meetings_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_leads`
--

DROP TABLE IF EXISTS `meetings_leads`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_leads` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) default NULL,
  `lead_id` varchar(36) default NULL,
  `required` varchar(1) default '1',
  `accept_status` varchar(25) default 'none',
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_lead_meeting_meeting` (`meeting_id`),
  KEY `idx_lead_meeting_lead` (`lead_id`),
  KEY `idx_meeting_lead` (`meeting_id`,`lead_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_leads`
--

LOCK TABLES `meetings_leads` WRITE;
/*!40000 ALTER TABLE `meetings_leads` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_leads` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meetings_users`
--

DROP TABLE IF EXISTS `meetings_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meetings_users` (
  `id` varchar(36) NOT NULL,
  `meeting_id` varchar(36) default NULL,
  `user_id` varchar(36) default NULL,
  `required` varchar(1) default '1',
  `accept_status` varchar(25) default 'none',
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_usr_mtg_mtg` (`meeting_id`),
  KEY `idx_usr_mtg_usr` (`user_id`),
  KEY `idx_meeting_users` (`meeting_id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meetings_users`
--

LOCK TABLES `meetings_users` WRITE;
/*!40000 ALTER TABLE `meetings_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `meetings_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notes`
--

DROP TABLE IF EXISTS `notes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notes` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `name` varchar(255) default NULL,
  `filename` varchar(255) default NULL,
  `file_mime_type` varchar(100) default NULL,
  `parent_type` varchar(25) default NULL,
  `parent_id` char(36) default NULL,
  `contact_id` char(36) default NULL,
  `portal_flag` tinyint(1) NOT NULL default '0',
  `embed_flag` tinyint(1) NOT NULL default '0',
  `description` text,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_note_name` (`name`),
  KEY `idx_notes_parent` (`parent_id`,`parent_type`),
  KEY `idx_note_contact` (`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notes`
--

LOCK TABLES `notes` WRITE;
/*!40000 ALTER TABLE `notes` DISABLE KEYS */;
/*!40000 ALTER TABLE `notes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities`
--

DROP TABLE IF EXISTS `opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities` (
  `id` char(36) NOT NULL,
  `name` varchar(50) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `opportunity_type` varchar(255) default NULL,
  `campaign_id` char(36) default NULL,
  `lead_source` varchar(50) default NULL,
  `amount` double default NULL,
  `amount_usdollar` double default NULL,
  `currency_id` char(36) default NULL,
  `date_closed` date default NULL,
  `next_step` varchar(100) default NULL,
  `sales_stage` varchar(25) default NULL,
  `probability` double default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_opp_name` (`name`),
  KEY `idx_opp_assigned` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities`
--

LOCK TABLES `opportunities` WRITE;
/*!40000 ALTER TABLE `opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities_audit`
--

DROP TABLE IF EXISTS `opportunities_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime default NULL,
  `created_by` varchar(36) default NULL,
  `field_name` varchar(100) default NULL,
  `data_type` varchar(100) default NULL,
  `before_value_string` varchar(255) default NULL,
  `after_value_string` varchar(255) default NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_audit`
--

LOCK TABLES `opportunities_audit` WRITE;
/*!40000 ALTER TABLE `opportunities_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `opportunities_contacts`
--

DROP TABLE IF EXISTS `opportunities_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `opportunities_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) default NULL,
  `opportunity_id` varchar(36) default NULL,
  `contact_role` varchar(50) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_con_opp_con` (`contact_id`),
  KEY `idx_con_opp_opp` (`opportunity_id`),
  KEY `idx_opportunities_contacts` (`opportunity_id`,`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `opportunities_contacts`
--

LOCK TABLES `opportunities_contacts` WRITE;
/*!40000 ALTER TABLE `opportunities_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `opportunities_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outbound_email`
--

DROP TABLE IF EXISTS `outbound_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `outbound_email` (
  `id` char(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `type` varchar(6) NOT NULL default 'user',
  `user_id` char(36) NOT NULL,
  `mail_sendtype` varchar(8) NOT NULL default 'sendmail',
  `mail_smtpserver` varchar(100) default NULL,
  `mail_smtpport` int(5) default NULL,
  `mail_smtpuser` varchar(100) default NULL,
  `mail_smtppass` varchar(100) default NULL,
  `mail_smtpauth_req` tinyint(1) default '0',
  `mail_smtpssl` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `oe_user_id_idx` (`id`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outbound_email`
--

LOCK TABLES `outbound_email` WRITE;
/*!40000 ALTER TABLE `outbound_email` DISABLE KEYS */;
INSERT INTO `outbound_email` VALUES ('dc5878e5-fd4f-3200-4a97-4c035568f2ea','system','system','1','SMTP','',25,'','',0,0);
/*!40000 ALTER TABLE `outbound_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `name` varchar(50) NOT NULL,
  `description` text,
  `deleted` tinyint(1) NOT NULL default '0',
  `estimated_start_date` date NOT NULL,
  `estimated_end_date` date NOT NULL,
  `status` varchar(255) default NULL,
  `priority` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task`
--

DROP TABLE IF EXISTS `project_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_task` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `project_id` char(36) default NULL,
  `project_task_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `status` varchar(255) default NULL,
  `description` text,
  `predecessors` text,
  `date_start` date default NULL,
  `time_start` int(11) default NULL,
  `time_finish` int(11) default NULL,
  `date_finish` date default NULL,
  `duration` int(11) NOT NULL,
  `duration_unit` text NOT NULL,
  `actual_duration` int(11) default NULL,
  `percent_complete` int(11) default NULL,
  `parent_task_id` int(11) default NULL,
  `assigned_user_id` char(36) default NULL,
  `modified_user_id` char(36) default NULL,
  `priority` varchar(255) default NULL,
  `created_by` char(36) default NULL,
  `milestone_flag` tinyint(1) default '0',
  `order_number` int(11) default '1',
  `task_number` int(11) default NULL,
  `estimated_effort` int(11) default NULL,
  `actual_effort` int(11) default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `utilization` int(11) default '100',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task`
--

LOCK TABLES `project_task` WRITE;
/*!40000 ALTER TABLE `project_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task_audit`
--

DROP TABLE IF EXISTS `project_task_audit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_task_audit` (
  `id` char(36) NOT NULL,
  `parent_id` char(36) NOT NULL,
  `date_created` datetime default NULL,
  `created_by` varchar(36) default NULL,
  `field_name` varchar(100) default NULL,
  `data_type` varchar(100) default NULL,
  `before_value_string` varchar(255) default NULL,
  `after_value_string` varchar(255) default NULL,
  `before_value_text` text,
  `after_value_text` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task_audit`
--

LOCK TABLES `project_task_audit` WRITE;
/*!40000 ALTER TABLE `project_task_audit` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_task_audit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_accounts`
--

DROP TABLE IF EXISTS `projects_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_accounts` (
  `id` varchar(36) NOT NULL,
  `account_id` varchar(36) default NULL,
  `project_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_proj_acct_proj` (`project_id`),
  KEY `idx_proj_acct_acct` (`account_id`),
  KEY `projects_accounts_alt` (`project_id`,`account_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_accounts`
--

LOCK TABLES `projects_accounts` WRITE;
/*!40000 ALTER TABLE `projects_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_bugs`
--

DROP TABLE IF EXISTS `projects_bugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_bugs` (
  `id` varchar(36) NOT NULL,
  `bug_id` varchar(36) default NULL,
  `project_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_proj_bug_proj` (`project_id`),
  KEY `idx_proj_bug_bug` (`bug_id`),
  KEY `projects_bugs_alt` (`project_id`,`bug_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_bugs`
--

LOCK TABLES `projects_bugs` WRITE;
/*!40000 ALTER TABLE `projects_bugs` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_bugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_cases`
--

DROP TABLE IF EXISTS `projects_cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_cases` (
  `id` varchar(36) NOT NULL,
  `case_id` varchar(36) default NULL,
  `project_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_proj_case_proj` (`project_id`),
  KEY `idx_proj_case_case` (`case_id`),
  KEY `projects_cases_alt` (`project_id`,`case_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_cases`
--

LOCK TABLES `projects_cases` WRITE;
/*!40000 ALTER TABLE `projects_cases` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_cases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_contacts`
--

DROP TABLE IF EXISTS `projects_contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_contacts` (
  `id` varchar(36) NOT NULL,
  `contact_id` varchar(36) default NULL,
  `project_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_proj_con_proj` (`project_id`),
  KEY `idx_proj_con_con` (`contact_id`),
  KEY `projects_contacts_alt` (`project_id`,`contact_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_contacts`
--

LOCK TABLES `projects_contacts` WRITE;
/*!40000 ALTER TABLE `projects_contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_opportunities`
--

DROP TABLE IF EXISTS `projects_opportunities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_opportunities` (
  `id` varchar(36) NOT NULL,
  `opportunity_id` varchar(36) default NULL,
  `project_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_proj_opp_proj` (`project_id`),
  KEY `idx_proj_opp_opp` (`opportunity_id`),
  KEY `projects_opportunities_alt` (`project_id`,`opportunity_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_opportunities`
--

LOCK TABLES `projects_opportunities` WRITE;
/*!40000 ALTER TABLE `projects_opportunities` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_opportunities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projects_products`
--

DROP TABLE IF EXISTS `projects_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_products` (
  `id` varchar(36) NOT NULL,
  `product_id` varchar(36) default NULL,
  `project_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_proj_prod_project` (`project_id`),
  KEY `idx_proj_prod_product` (`product_id`),
  KEY `projects_products_alt` (`project_id`,`product_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projects_products`
--

LOCK TABLES `projects_products` WRITE;
/*!40000 ALTER TABLE `projects_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `projects_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_list_campaigns`
--

DROP TABLE IF EXISTS `prospect_list_campaigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_list_campaigns` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) default NULL,
  `campaign_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_pro_id` (`prospect_list_id`),
  KEY `idx_cam_id` (`campaign_id`),
  KEY `idx_prospect_list_campaigns` (`prospect_list_id`,`campaign_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_list_campaigns`
--

LOCK TABLES `prospect_list_campaigns` WRITE;
/*!40000 ALTER TABLE `prospect_list_campaigns` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_list_campaigns` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_lists`
--

DROP TABLE IF EXISTS `prospect_lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_lists` (
  `id` char(36) NOT NULL,
  `name` varchar(50) default NULL,
  `list_type` varchar(25) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `assigned_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `description` text,
  `domain_name` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_prospect_list_name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_lists`
--

LOCK TABLES `prospect_lists` WRITE;
/*!40000 ALTER TABLE `prospect_lists` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospect_lists_prospects`
--

DROP TABLE IF EXISTS `prospect_lists_prospects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospect_lists_prospects` (
  `id` varchar(36) NOT NULL,
  `prospect_list_id` varchar(36) default NULL,
  `related_id` varchar(36) default NULL,
  `related_type` varchar(25) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_plp_pro_id` (`prospect_list_id`),
  KEY `idx_plp_rel_id` (`related_id`,`related_type`,`prospect_list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospect_lists_prospects`
--

LOCK TABLES `prospect_lists_prospects` WRITE;
/*!40000 ALTER TABLE `prospect_lists_prospects` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospect_lists_prospects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prospects`
--

DROP TABLE IF EXISTS `prospects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prospects` (
  `id` char(36) NOT NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `salutation` varchar(5) default NULL,
  `first_name` varchar(100) default NULL,
  `last_name` varchar(100) default NULL,
  `title` varchar(100) default NULL,
  `department` varchar(255) default NULL,
  `do_not_call` tinyint(1) default '0',
  `phone_home` varchar(25) default NULL,
  `phone_mobile` varchar(25) default NULL,
  `phone_work` varchar(25) default NULL,
  `phone_other` varchar(25) default NULL,
  `phone_fax` varchar(25) default NULL,
  `primary_address_street` varchar(150) default NULL,
  `primary_address_city` varchar(100) default NULL,
  `primary_address_state` varchar(100) default NULL,
  `primary_address_postalcode` varchar(20) default NULL,
  `primary_address_country` varchar(255) default NULL,
  `alt_address_street` varchar(150) default NULL,
  `alt_address_city` varchar(100) default NULL,
  `alt_address_state` varchar(100) default NULL,
  `alt_address_postalcode` varchar(20) default NULL,
  `alt_address_country` varchar(255) default NULL,
  `assistant` varchar(75) default NULL,
  `assistant_phone` varchar(25) default NULL,
  `tracker_key` int(11) NOT NULL auto_increment,
  `birthdate` date default NULL,
  `lead_id` char(36) default NULL,
  `account_name` varchar(150) default NULL,
  `campaign_id` char(36) default NULL,
  PRIMARY KEY  (`id`),
  KEY `prospect_auto_tracker_key` (`tracker_key`),
  KEY `idx_prospects_last_first` (`last_name`,`first_name`,`deleted`),
  KEY `idx_prospecs_del_last` (`last_name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prospects`
--

LOCK TABLES `prospects` WRITE;
/*!40000 ALTER TABLE `prospects` DISABLE KEYS */;
/*!40000 ALTER TABLE `prospects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `relationships`
--

DROP TABLE IF EXISTS `relationships`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `relationships` (
  `id` char(36) NOT NULL,
  `relationship_name` varchar(150) NOT NULL,
  `lhs_module` varchar(100) NOT NULL,
  `lhs_table` varchar(64) NOT NULL,
  `lhs_key` varchar(64) NOT NULL,
  `rhs_module` varchar(100) NOT NULL,
  `rhs_table` varchar(64) NOT NULL,
  `rhs_key` varchar(64) NOT NULL,
  `join_table` varchar(64) default NULL,
  `join_key_lhs` varchar(64) default NULL,
  `join_key_rhs` varchar(64) default NULL,
  `relationship_type` varchar(64) default NULL,
  `relationship_role_column` varchar(64) default NULL,
  `relationship_role_column_value` varchar(50) default NULL,
  `reverse` tinyint(1) default '0',
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_rel_name` (`relationship_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `relationships`
--

LOCK TABLES `relationships` WRITE;
/*!40000 ALTER TABLE `relationships` DISABLE KEYS */;
INSERT INTO `relationships` VALUES ('5aee0cf5-0201-380e-04cd-4c0355553105','leads_modified_user','Users','users','id','Leads','leads','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5b00865b-4b10-ad22-4b53-4c0355f11fc3','leads_created_by','Users','users','id','Leads','leads','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5b0ffa4a-acf5-51e5-6c6d-4c0355aa1c4f','leads_assigned_user','Users','users','id','Leads','leads','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5b1eea1c-a77c-40b6-ffce-4c0355266f85','leads_email_addresses_primary','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many',NULL,NULL,0,0),('5b2e9fe1-71a3-2ff9-e087-4c03557ccc50','lead_direct_reports','Leads','leads','id','Leads','leads','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5b3cfd07-62a2-888d-3f30-4c0355986040','lead_tasks','Leads','leads','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('5b4b7567-18ce-6de5-6ca2-4c035548dcc7','lead_notes','Leads','leads','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('5b59f366-0411-07f1-c190-4c035512abbf','lead_meetings','Leads','leads','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('5b68e386-c68c-38d7-78f8-4c0355fff372','lead_calls','Leads','leads','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('5b779a24-8a38-a567-c163-4c035591fcf4','lead_emails','Leads','leads','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Leads',0,0),('5b861de5-fa7f-d99c-c85f-4c035558fc22','lead_campaign_log','Leads','leads','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5dd62da4-99f7-ed5b-730a-4c035525299b','contacts_modified_user','Users','users','id','Contacts','contacts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5de86cda-6219-e385-57c4-4c03556e5f7d','contacts_created_by','Users','users','id','Contacts','contacts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5df6cb68-2ea5-4494-d6ba-4c0355920cf5','contacts_assigned_user','Users','users','id','Contacts','contacts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5e051129-8f1b-3ec0-f66f-4c035550d754','contacts_email_addresses_primary','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many',NULL,NULL,0,0),('5e14c83f-884d-74e8-b292-4c0355617c78','contact_direct_reports','Contacts','contacts','id','Contacts','contacts','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5e24534e-93b6-43b6-99c0-4c03559cdcf8','contact_leads','Contacts','contacts','id','Leads','leads','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5e32bcfe-fd6c-6268-9ae0-4c03559ff72b','contact_notes','Contacts','contacts','id','Notes','notes','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5e40ea39-32e2-49f4-ae79-4c03552a7b7c','contact_tasks','Contacts','contacts','id','Tasks','tasks','contact_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('5e4f104e-0143-b31d-5a54-4c03557f4afd','contact_campaign_log','Contacts','contacts','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('60550bd7-3fa2-66c9-8db1-4c0355aed267','accounts_modified_user','Users','users','id','Accounts','accounts','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6066f04a-5e2a-517c-1b8c-4c0355ebb261','accounts_created_by','Users','users','id','Accounts','accounts','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('60757f77-4063-b004-b266-4c03552100ba','accounts_assigned_user','Users','users','id','Accounts','accounts','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6084d670-4735-53e0-4393-4c0355b3eb59','accounts_email_addresses_primary','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many',NULL,NULL,0,0),('60955f84-015f-457f-2556-4c03550076a8','member_accounts','Accounts','accounts','id','Accounts','accounts','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('60a5b879-7fd9-d403-f6c3-4c0355a2be3a','account_cases','Accounts','accounts','id','Cases','cases','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('60b412d3-7c7e-1b90-acf8-4c0355faef7b','account_tasks','Accounts','accounts','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('60c4092e-cc21-c643-1ec3-4c0355915523','account_notes','Accounts','accounts','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('60d3445f-3647-ca39-a41f-4c0355620bfa','account_meetings','Accounts','accounts','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('60e221de-edde-662d-dfec-4c03557f11f9','account_calls','Accounts','accounts','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('60f0c5a3-fd3c-32dd-c4c2-4c03557202d0','account_emails','Accounts','accounts','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Accounts',0,0),('6100c8ca-5a15-7672-ac6f-4c035525b986','account_leads','Accounts','accounts','id','Leads','leads','account_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6299f73e-be35-dbd2-a217-4c0355c86594','opportunities_modified_user','Users','users','id','Opportunities','opportunities','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('62aca0d4-fdb0-6494-c0e2-4c0355e17e4e','opportunities_created_by','Users','users','id','Opportunities','opportunities','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('62bcfe56-c1e9-a314-7958-4c035596c73b','opportunities_assigned_user','Users','users','id','Opportunities','opportunities','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('62cd02ba-aea5-9865-cf0e-4c03554340db','opportunity_calls','Opportunities','opportunities','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('62dd1e7c-3396-f162-48df-4c0355b69976','opportunity_meetings','Opportunities','opportunities','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('62ed9776-38d7-2e2e-1573-4c03557d23d9','opportunity_tasks','Opportunities','opportunities','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('62fe65cd-dbbd-3ed3-ec0f-4c0355a8d41a','opportunity_notes','Opportunities','opportunities','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('630e768e-11fb-a48a-05b2-4c035504f293','opportunity_emails','Opportunities','opportunities','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Opportunities',0,0),('631e6d23-245e-5f03-0e40-4c0355c06ad6','opportunity_leads','Opportunities','opportunities','id','Leads','leads','opportunity_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('632ee88d-3285-432d-9f1c-4c0355c630ac','opportunity_currencies','Opportunities','opportunities','currency_id','Currencies','currencies','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('633f07a5-512d-4f78-1c13-4c03550f97b6','opportunities_campaign','campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('64b352aa-571c-ff08-07c3-4c0355a339a3','cases_modified_user','Users','users','id','Cases','cases','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('64c5db23-1410-e9cd-4c8e-4c035592783e','cases_created_by','Users','users','id','Cases','cases','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('64d4f7a1-ea9a-ef66-3c63-4c03554b29d4','cases_assigned_user','Users','users','id','Cases','cases','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('64e43f47-d507-87b3-184f-4c0355c3e855','case_calls','Cases','cases','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('64f45bdb-bb0e-e434-d365-4c0355a40dfe','case_tasks','Cases','cases','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('6503d458-9d62-8d56-7b07-4c0355ca2bdd','case_notes','Cases','cases','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('6513870f-ae8f-6a1a-bf24-4c03553a9523','case_meetings','Cases','cases','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('65235c22-fa95-49b0-ae3a-4c03552cd701','case_emails','Cases','cases','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Cases',0,0),('661fc8cd-0e26-2586-c373-4c035511a641','notes_modified_user','Users','users','id','Notes','notes','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('66336c1d-3f66-e026-ca64-4c035591f48b','notes_created_by','Users','users','id','Notes','notes','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('68d544d3-5859-9a5d-19b3-4c0355d82a08','calls_modified_user','Users','users','id','Calls','calls','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('68e879e6-3c17-f76c-0dc6-4c03550a4ab4','calls_created_by','Users','users','id','Calls','calls','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('68f7d620-bac2-23ca-8774-4c035530fe0f','calls_assigned_user','Users','users','id','Calls','calls','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6906d35d-964b-cae7-e97d-4c035507e88d','calls_notes','Calls','calls','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6a201f46-b024-1b0f-08b4-4c0355849c5c','emails_assigned_user','Users','users','id','Emails','emails','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6a326f6c-f0e9-3fe4-f8b5-4c03551e060b','emails_modified_user','Users','users','id','Emails','emails','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6a42d083-41b9-1b18-1661-4c0355aebb82','emails_created_by','Users','users','id','Emails','emails','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6a53643d-5dca-ee5a-3bdd-4c03556a9a53','emails_notes_rel','Emails','emails','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6bad05e0-84c4-c31b-572b-4c035565f85c','meetings_modified_user','Users','users','id','Meetings','meetings','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6bc2178a-6178-7edb-239b-4c0355c06614','meetings_created_by','Users','users','id','Meetings','meetings','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6bd32e21-5f63-b384-65f5-4c03557167c5','meetings_assigned_user','Users','users','id','Meetings','meetings','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6be35e57-5658-9b79-6e79-4c0355857952','meetings_notes','Meetings','meetings','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Meetings',0,0),('6d5477d0-d3ad-843b-3cc6-4c0355f504b0','tasks_modified_user','Users','users','id','Tasks','tasks','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6d68043f-1ec6-7446-4dfd-4c0355ae15d0','tasks_created_by','Users','users','id','Tasks','tasks','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6d79f543-2de1-1708-7664-4c03550afe16','tasks_assigned_user','Users','users','id','Tasks','tasks','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('6dd2bc49-dbe8-0bea-f539-4c0355d837fd','user_direct_reports','Users','users','id','Users','users','reports_to_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('733bb0bf-8ee3-3712-28bd-4c0355d784be','bugs_modified_user','Users','users','id','Bugs','bugs','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('734e8813-3411-a218-081e-4c035512967f','bugs_created_by','Users','users','id','Bugs','bugs','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('735e44d5-ac62-1676-c25a-4c03551c18b3','bugs_assigned_user','Users','users','id','Bugs','bugs','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('736df8e1-5d6b-e3fd-fc12-4c0355c54fdc','bug_tasks','Bugs','bugs','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('737df33c-aef3-afd1-2da9-4c035597648b','bug_meetings','Bugs','bugs','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('738da554-2842-28b7-50c6-4c035526f535','bug_calls','Bugs','bugs','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('739d86d6-cd99-b496-b678-4c03552ba15b','bug_emails','Bugs','bugs','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('73adb33a-7d9c-ed69-3c06-4c0355b10ef5','bug_notes','Bugs','bugs','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Bugs',0,0),('73bcdb77-24f5-c3c9-6e56-4c035527c8b0','bugs_release','Releases','releases','id','Bugs','bugs','found_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('73cc278b-4ec5-a8a5-e81e-4c035589223c','bugs_fixed_in_release','Releases','releases','id','Bugs','bugs','fixed_in_release',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('75a75942-da95-c00f-76b3-4c0355511ac5','feeds_assigned_user','Users','users','id','Feeds','feeds','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('75bb0160-c3f7-24c6-8ebe-4c0355d8a523','feeds_modified_user','Users','users','id','Feeds','feeds','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('75cc5bd9-bf2c-3009-015e-4c035501166e','feeds_created_by','Users','users','id','Feeds','feeds','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('772882b4-c0fc-575f-c91c-4c03552699da','projects_notes','Project','project','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('773d43ff-86cc-8fea-b684-4c0355833b24','projects_tasks','Project','project','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('774f8c56-e974-b95f-1174-4c035578e997','projects_meetings','Project','project','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('7761733f-6985-8d92-4e66-4c03558683f7','projects_calls','Project','project','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('777414b5-1801-03d4-d76e-4c0355c93e2c','projects_emails','Project','project','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','Project',0,0),('7784aa7f-2eac-6642-968d-4c03550674d4','projects_project_tasks','Project','project','id','ProjectTask','project_task','project_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7795606f-ff15-3aa0-ca12-4c0355b694d7','projects_assigned_user','Users','users','id','Project','project','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('77a51860-93b4-b622-ba3a-4c03553b57ee','projects_modified_user','Users','users','id','Project','project','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('77b5e65a-3f14-9fb4-ca06-4c0355f2a016','projects_created_by','Users','users','id','Project','project','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('78c8a58c-5643-e811-ac11-4c0355a4f1d8','project_tasks_notes','ProjectTask','project_task','id','Notes','notes','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('78dbe05b-470a-8c9e-fa12-4c03555a9257','project_tasks_tasks','ProjectTask','project_task','id','Tasks','tasks','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('78ecf9f0-a99b-b94e-a2e0-4c03554ef42a','project_tasks_meetings','ProjectTask','project_task','id','Meetings','meetings','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('78ffc8d0-338f-0311-55a8-4c035537bfd6','project_tasks_calls','ProjectTask','project_task','id','Calls','calls','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('791078fc-916d-676c-1747-4c03552e8ef7','project_tasks_emails','ProjectTask','project_task','id','Emails','emails','parent_id',NULL,NULL,NULL,'one-to-many','parent_type','ProjectTask',0,0),('7920e2e3-1bc1-7b06-606a-4c0355b6e32e','project_tasks_assigned_user','Users','users','id','ProjectTask','project_task','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7935bd3d-ed85-2e58-26ba-4c035572a39b','project_tasks_modified_user','Users','users','id','ProjectTask','project_task','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('79461498-e58b-860a-78a4-4c03554c1a91','project_tasks_created_by','Users','users','id','ProjectTask','project_task','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7aa1a8d4-d4e5-843a-193a-4c03553f9300','email_template_email_marketings','EmailTemplates','email_templates','id','EmailMarketing','email_marketing','template_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c13d482-74e1-5892-2ceb-4c0355886193','campaigns_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c27d515-0e6a-e161-5474-4c0355de424d','campaigns_created_by','Users','users','id','Campaigns','campaigns','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c382201-39d9-11a5-bb28-4c0355dceb69','campaigns_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c47be18-1ab5-c057-9f73-4c035596010e','campaign_accounts','Campaigns','campaigns','id','Accounts','accounts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c57cb65-f57a-616e-2fc9-4c03558b0821','campaign_contacts','Campaigns','campaigns','id','Contacts','contacts','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c68c977-b820-86e3-a6dc-4c0355c08265','campaign_leads','Campaigns','campaigns','id','Leads','leads','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c78be9d-8bd6-1dac-489d-4c03550e4435','campaign_prospects','Campaigns','campaigns','id','Prospects','prospects','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c886871-cb19-b760-77be-4c035576b4ff','campaign_opportunities','Campaigns','campaigns','id','Opportunities','opportunities','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7c98f11e-6665-02aa-56ac-4c0355b451f4','campaign_email_marketing','Campaigns','campaigns','id','EmailMarketing','email_marketing','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7ca91f19-7033-829b-8739-4c0355dde5bd','campaign_emailman','Campaigns','campaigns','id','EmailMan','emailman','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7cb8caff-6717-c900-1341-4c035536e5be','campaign_campaignlog','Campaigns','campaigns','id','CampaignLog','campaign_log','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7cc84d91-7382-5970-fc70-4c0355ddb59e','campaign_assigned_user','Users','users','id','Campaigns','campaigns','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7cd8c958-8ca5-928c-484b-4c0355b7b259','campaign_modified_user','Users','users','id','Campaigns','campaigns','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7f529a01-bf05-2f61-9d95-4c0355f6a608','prospects_modified_user','Users','users','id','Prospects','prospects','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7f6729c3-8e5d-4aa8-3171-4c03555bed79','prospects_created_by','Users','users','id','Prospects','prospects','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7f7807e6-4241-bf0e-5679-4c0355e366a5','prospects_assigned_user','Users','users','id','Prospects','prospects','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('7f893500-898e-9a81-dc8a-4c0355ef6f8f','prospects_email_addresses_primary','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many',NULL,NULL,0,0),('7f9adc57-e42e-2dd0-8081-4c03550fae63','prospect_campaign_log','Prospects','prospects','id','CampaignLog','campaign_log','target_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('812b0897-e369-159c-5211-4c0355e37fa6','documents_modified_user','Users','users','id','Documents','documents','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('813f54b6-bbeb-583c-0476-4c03558d9979','documents_created_by','Users','users','id','Documents','documents','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('81504927-1e9d-dded-d6ac-4c03551ea940','document_revisions','Documents','documents','id','Documents','document_revisions','document_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('81e3d0bf-19a5-8d5b-c833-4c03553b7ccd','revisions_created_by','Users','users','id','DocumentRevisions','document_revisions','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('83bebeee-07b8-cc18-391c-4c0355fba7e8','schedulers_created_by_rel','Users','users','id','Schedulers','schedulers','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('83d39c05-4875-8798-6ebf-4c0355478d5f','schedulers_modified_user_id_rel','Users','users','id','Schedulers','schedulers','modified_user_id',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('83e4a711-6d95-623d-ebc9-4c0355dfe17d','schedulers_jobs_rel','Schedulers','schedulers','id','SchedulersJobs','schedulers_times','scheduler_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('88b26576-ea02-afcd-447a-4c0355308e19','inbound_email_created_by','Users','users','id','InboundEmail','inbound_email','created_by',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('88c5261d-a036-534b-b811-4c0355553ac5','inbound_email_modified_user_id','Users','users','id','InboundEmail','inbound_email','modified_user_id',NULL,NULL,NULL,'one-to-one',NULL,NULL,0,0),('8a009774-1091-3cc5-c26e-4c0355ed8450','campaignlog_contact','CampaignLog','campaign_log','related_id','Contacts','contacts','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8a14df70-8aca-f86a-d5e2-4c03554e1937','campaignlog_lead','CampaignLog','campaign_log','related_id','Leads','leads','id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8ad465bf-9a4d-dfc0-a17a-4c0355ee72b6','dashboards_assigned_user','Users','users','id','Dashboard','dashboards','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8ae7782d-bb02-12af-e9c6-4c03555eea81','dashboards_modified_user','Users','users','id','Dashboard','dashboards','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8af8da61-cf16-356d-796a-4c0355a710e7','dashboards_created_by','Users','users','id','Dashboard','dashboards','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8ba48db7-5380-5934-47b7-4c03550b6606','campaign_campaigntrakers','Campaigns','campaigns','id','CampaignTrackers','campaign_trkrs','campaign_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8c9c14bc-e083-233f-d5ef-4c0355b4ef4c','saved_search_assigned_user','Users','users','id','SavedSearch','saved_search','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8ed4e459-dc2b-1cbb-d054-4c035556ea28','sugarfeed_modified_user','Users','users','id','SugarFeed','sugarfeed','modified_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8eea2f69-9042-bee9-8df3-4c03559a9d8b','sugarfeed_created_by','Users','users','id','SugarFeed','sugarfeed','created_by',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('8efbe110-4cb2-6baf-18fe-4c03552d0f2b','sugarfeed_assigned_user','Users','users','id','SugarFeed','sugarfeed','assigned_user_id',NULL,NULL,NULL,'one-to-many',NULL,NULL,0,0),('93a96464-cb8e-4db7-f1ba-4c035552fea0','accounts_bugs','Accounts','accounts','id','Bugs','bugs','id','accounts_bugs','account_id','bug_id','many-to-many',NULL,NULL,0,0),('93bebdc0-d06c-4cc6-e27f-4c0355618c53','accounts_contacts','Accounts','accounts','id','Contacts','contacts','id','accounts_contacts','account_id','contact_id','many-to-many',NULL,NULL,0,0),('93d20a70-6f6a-783d-f821-4c0355ed8ba1','accounts_opportunities','Accounts','accounts','id','Opportunities','opportunities','id','accounts_opportunities','account_id','opportunity_id','many-to-many',NULL,NULL,0,0),('93e4d4f5-9106-dc60-814a-4c0355cc68b2','calls_contacts','Calls','calls','id','Contacts','contacts','id','calls_contacts','call_id','contact_id','many-to-many',NULL,NULL,0,0),('93f6e65c-29b7-3a37-5016-4c03550525b6','calls_users','Calls','calls','id','Users','users','id','calls_users','call_id','user_id','many-to-many',NULL,NULL,0,0),('94095849-520f-8f35-6549-4c0355b425ce','calls_leads','Calls','calls','id','Leads','leads','id','calls_leads','call_id','lead_id','many-to-many',NULL,NULL,0,0),('941c0a44-cf8e-2865-283f-4c03556bcbb5','cases_bugs','Cases','cases','id','Bugs','bugs','id','cases_bugs','case_id','bug_id','many-to-many',NULL,NULL,0,0),('942eb298-26c8-1a82-2ebc-4c0355768b1e','contacts_bugs','Contacts','contacts','id','Bugs','bugs','id','contacts_bugs','contact_id','bug_id','many-to-many',NULL,NULL,0,0),('944240cc-2941-02cb-c755-4c0355788963','contacts_cases','Contacts','contacts','id','Cases','cases','id','contacts_cases','contact_id','case_id','many-to-many',NULL,NULL,0,0),('945624c4-56d1-e66d-5191-4c03553077d1','contacts_users','Contacts','contacts','id','Users','users','id','contacts_users','contact_id','user_id','many-to-many',NULL,NULL,0,0),('94691a7c-ff26-5ea5-3ea0-4c0355beb990','accounts_email_addresses','Accounts','accounts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Accounts',0,0),('9484031b-04ca-7cbf-6576-4c0355ea2040','contacts_email_addresses','Contacts','contacts','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Contacts',0,0),('949ef812-2577-682a-85a2-4c0355e52f77','leads_email_addresses','Leads','leads','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Leads',0,0),('94b8f004-79c1-6c12-f065-4c03557bbe5b','prospects_email_addresses','Prospects','prospects','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Prospects',0,0),('94d606a3-cd0a-111b-b6a6-4c03551dc3c9','users_email_addresses','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','bean_module','Users',0,0),('94e7bb91-dda7-6cec-b801-4c0355c84a18','users_email_addresses_primary','Users','users','id','EmailAddresses','email_addresses','id','email_addr_bean_rel','bean_id','email_address_id','many-to-many','primary_address','1',0,0),('94fd2354-c6b4-2a5d-ab57-4c03555efa42','emails_accounts_rel','Emails','emails','id','Accounts','accounts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Accounts',0,0),('95100f8f-55ea-c9c9-ab75-4c0355e41ff6','emails_bugs_rel','Emails','emails','id','Bugs','bugs','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Bugs',0,0),('95220b50-418e-905d-2c69-4c0355856f6c','emails_cases_rel','Emails','emails','id','Cases','cases','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Cases',0,0),('9533bda1-dea4-2643-618f-4c0355037f21','emails_contacts_rel','Emails','emails','id','Contacts','contacts','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Contacts',0,0),('95463480-8929-e719-7472-4c035538ae5c','emails_leads_rel','Emails','emails','id','Leads','leads','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Leads',0,0),('9556ac07-aa23-6ea7-c76a-4c03555a2a5f','emails_opportunities_rel','Emails','emails','id','Opportunities','opportunities','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Opportunities',0,0),('95662f89-547a-055d-ca23-4c03559678cf','emails_tasks_rel','Emails','emails','id','Tasks','tasks','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Tasks',0,0),('9575a025-76c9-c090-41c8-4c03557e0d01','emails_users_rel','Emails','emails','id','Users','users','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Users',0,0),('95865442-359e-0f82-368d-4c035566e129','emails_project_task_rel','Emails','emails','id','ProjectTask','project_task','id','emails_beans','email_id','bean_id','many-to-many','bean_module','ProjectTask',0,0),('959622b6-b770-5d60-0f8e-4c0355ec91d1','emails_projects_rel','Emails','emails','id','Project','project','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Project',0,0),('95a5a9f5-7246-494e-9ce5-4c0355dc20eb','emails_prospects_rel','Emails','emails','id','Prospects','prospects','id','emails_beans','email_id','bean_id','many-to-many','bean_module','Prospects',0,0),('95b998a1-fe60-c457-65e2-4c03551036df','meetings_contacts','Meetings','meetings','id','Contacts','contacts','id','meetings_contacts','meeting_id','contact_id','many-to-many',NULL,NULL,0,0),('95cc4ac9-e485-836b-497a-4c03552cab31','meetings_users','Meetings','meetings','id','Users','users','id','meetings_users','meeting_id','user_id','many-to-many',NULL,NULL,0,0),('95ddf50d-b553-535a-ec5a-4c0355c237ff','meetings_leads','Meetings','meetings','id','Leads','leads','id','meetings_leads','meeting_id','lead_id','many-to-many',NULL,NULL,0,0),('95efd748-c317-cf7d-cdda-4c0355cc3446','opportunities_contacts','Opportunities','opportunities','id','Contacts','contacts','id','opportunities_contacts','opportunity_id','contact_id','many-to-many',NULL,NULL,0,0),('9602c38d-3baf-0304-b75a-4c0355c24f1a','prospect_list_campaigns','ProspectLists','prospect_lists','id','Campaigns','campaigns','id','prospect_list_campaigns','prospect_list_id','campaign_id','many-to-many',NULL,NULL,0,0),('9615623e-e549-1eef-7ebf-4c03558b2983','prospect_list_contacts','ProspectLists','prospect_lists','id','Contacts','contacts','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Contacts',0,0),('9625e057-a541-eeb6-58c2-4c03555e0577','prospect_list_prospects','ProspectLists','prospect_lists','id','Prospects','prospects','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Prospects',0,0),('96371b02-e723-7e02-2b90-4c0355cae0c3','prospect_list_leads','ProspectLists','prospect_lists','id','Leads','leads','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Leads',0,0),('96480b45-6e79-92e1-d4ed-4c035509a0d8','prospect_list_users','ProspectLists','prospect_lists','id','Users','users','id','prospect_lists_prospects','prospect_list_id','related_id','many-to-many','related_type','Users',0,0),('965acfc3-4e6c-d093-46fc-4c0355d822b2','roles_users','Roles','roles','id','Users','users','id','roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),('966d2814-a996-eb4b-d99e-4c03558150d5','projects_bugs','Project','project','id','Bugs','bugs','id','projects_bugs','project_id','bug_id','many-to-many',NULL,NULL,0,0),('967e7cc9-3316-ae2c-c7a9-4c0355eb837c','projects_cases','Project','project','id','Cases','cases','id','projects_cases','project_id','case_id','many-to-many',NULL,NULL,0,0),('9691c398-d363-d2f9-2e4c-4c0355790b48','projects_accounts','Project','project','id','Accounts','accounts','id','projects_accounts','project_id','account_id','many-to-many',NULL,NULL,0,0),('96a2dd58-8bc5-f707-ce7d-4c03551abbe5','projects_contacts','Project','project','id','Contacts','contacts','id','projects_contacts','project_id','contact_id','many-to-many',NULL,NULL,0,0),('96b4b117-ee49-98da-46eb-4c035557e61a','projects_opportunities','Project','project','id','Opportunities','opportunities','id','projects_opportunities','project_id','opportunity_id','many-to-many',NULL,NULL,0,0),('96c6d986-ca10-ff07-6f40-4c03552f0def','acl_roles_actions','ACLRoles','acl_roles','id','ACLActions','acl_actions','id','acl_roles_actions','role_id','action_id','many-to-many',NULL,NULL,0,0),('96d881a5-a577-7309-dfb4-4c0355db5f10','acl_roles_users','ACLRoles','acl_roles','id','Users','users','id','acl_roles_users','role_id','user_id','many-to-many',NULL,NULL,0,0),('96ee8058-9a29-731d-d6a9-4c03551bfe2c','email_marketing_prospect_lists','EmailMarketing','email_marketing','id','ProspectLists','prospect_lists','id','email_marketing_prospect_lists','email_marketing_id','prospect_list_id','many-to-many',NULL,NULL,0,0),('9701fda2-0e6c-7bc4-5e59-4c03554bfcca','leads_documents','Leads','leads','id','Documents','documents','id','linked_documents','parent_id','document_id','many-to-many','parent_type','Leads',0,0);
/*!40000 ALTER TABLE `relationships` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `releases`
--

DROP TABLE IF EXISTS `releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `releases` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) default NULL,
  `name` varchar(50) NOT NULL,
  `list_order` int(4) default NULL,
  `status` varchar(25) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_releases` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `releases`
--

LOCK TABLES `releases` WRITE;
/*!40000 ALTER TABLE `releases` DISABLE KEYS */;
/*!40000 ALTER TABLE `releases` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) default NULL,
  `name` varchar(150) default NULL,
  `description` text,
  `modules` text,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_role_id_del` (`id`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_modules`
--

DROP TABLE IF EXISTS `roles_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_modules` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) default NULL,
  `module_id` varchar(36) default NULL,
  `allow` tinyint(1) default '0',
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_role_id` (`role_id`),
  KEY `idx_module_id` (`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_modules`
--

LOCK TABLES `roles_modules` WRITE;
/*!40000 ALTER TABLE `roles_modules` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles_users`
--

DROP TABLE IF EXISTS `roles_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles_users` (
  `id` varchar(36) NOT NULL,
  `role_id` varchar(36) default NULL,
  `user_id` varchar(36) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_ru_role_id` (`role_id`),
  KEY `idx_ru_user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles_users`
--

LOCK TABLES `roles_users` WRITE;
/*!40000 ALTER TABLE `roles_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `roles_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `saved_search`
--

DROP TABLE IF EXISTS `saved_search`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `saved_search` (
  `id` char(36) NOT NULL,
  `name` varchar(150) default NULL,
  `search_module` varchar(150) default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) default NULL,
  `contents` text,
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `idx_desc` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `saved_search`
--

LOCK TABLES `saved_search` WRITE;
/*!40000 ALTER TABLE `saved_search` DISABLE KEYS */;
/*!40000 ALTER TABLE `saved_search` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedulers`
--

DROP TABLE IF EXISTS `schedulers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulers` (
  `id` varchar(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `created_by` char(36) default NULL,
  `modified_user_id` char(36) default NULL,
  `name` varchar(255) NOT NULL,
  `job` varchar(255) NOT NULL,
  `date_time_start` datetime NOT NULL,
  `date_time_end` datetime default NULL,
  `job_interval` varchar(100) NOT NULL,
  `time_from` time default NULL,
  `time_to` time default NULL,
  `last_run` datetime default NULL,
  `status` varchar(25) default NULL,
  `catch_up` tinyint(1) default '1',
  PRIMARY KEY  (`id`),
  KEY `idx_schedule` (`date_time_start`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedulers`
--

LOCK TABLES `schedulers` WRITE;
/*!40000 ALTER TABLE `schedulers` DISABLE KEYS */;
INSERT INTO `schedulers` VALUES ('15ae28c4-edf2-eb2e-ee77-4c0355662e6d',0,'2010-05-31 06:20:41','2010-05-31 06:20:41',NULL,'1','Prune tracker tables','function::trimTracker','2005-01-01 11:15:00','2020-12-31 15:59:00','0::2::1::*::*',NULL,NULL,NULL,'Active',1),('165bbcfa-f1c1-bf29-32ab-4c0355dd2bf1',0,'2010-05-31 06:20:41','2010-05-31 06:20:41',NULL,'1','Check Inbound Mailboxes','function::pollMonitoredInboxes','2005-01-01 01:45:00','2020-12-31 15:59:00','*::*::*::*::*',NULL,NULL,NULL,'Active',0),('1701caa3-b396-97a5-b926-4c0355450394',0,'2010-05-31 06:20:41','2010-05-31 06:20:41',NULL,'1','Run Nightly Process Bounced Campaign Emails','function::pollMonitoredInboxesForBouncedCampaignEmails','2004-12-31 22:15:00','2020-12-31 15:59:00','0::2-6::*::*::*',NULL,NULL,NULL,'Active',1),('17a9fb36-73cb-7373-c1d2-4c0355e9a684',0,'2010-05-31 06:20:41','2010-05-31 06:20:41',NULL,'1','Run Nightly Mass Email Campaigns','function::runMassEmailCampaign','2005-01-01 08:15:00','2020-12-31 15:59:00','0::2-6::*::*::*',NULL,NULL,NULL,'Active',1),('184f7e5e-71d5-10d2-226f-4c03556697d5',0,'2010-05-31 06:20:41','2010-05-31 06:20:41',NULL,'1','Prune Database on 1st of Month','function::pruneDatabase','2004-12-31 22:45:00','2020-12-31 15:59:00','0::4::1::*::*',NULL,NULL,NULL,'Inactive',0);
/*!40000 ALTER TABLE `schedulers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedulers_times`
--

DROP TABLE IF EXISTS `schedulers_times`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedulers_times` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `scheduler_id` char(36) NOT NULL,
  `execute_time` datetime NOT NULL,
  `status` varchar(25) NOT NULL default 'ready',
  PRIMARY KEY  (`id`),
  KEY `idx_scheduler_id` (`scheduler_id`,`execute_time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedulers_times`
--

LOCK TABLES `schedulers_times` WRITE;
/*!40000 ALTER TABLE `schedulers_times` DISABLE KEYS */;
/*!40000 ALTER TABLE `schedulers_times` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sugarfeed`
--

DROP TABLE IF EXISTS `sugarfeed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sugarfeed` (
  `id` char(36) NOT NULL,
  `name` varchar(255) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` varchar(255) default NULL,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `related_module` varchar(100) default NULL,
  `related_id` char(36) default NULL,
  `link_url` varchar(255) default NULL,
  `link_type` varchar(30) default NULL,
  PRIMARY KEY  (`id`),
  KEY `sgrfeed_date` (`date_entered`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sugarfeed`
--

LOCK TABLES `sugarfeed` WRITE;
/*!40000 ALTER TABLE `sugarfeed` DISABLE KEYS */;
/*!40000 ALTER TABLE `sugarfeed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tasks`
--

DROP TABLE IF EXISTS `tasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tasks` (
  `id` char(36) NOT NULL,
  `name` varchar(50) default NULL,
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `description` text,
  `deleted` tinyint(1) default '0',
  `assigned_user_id` char(36) default NULL,
  `status` varchar(25) default NULL,
  `date_due_flag` tinyint(1) default '1',
  `date_due` datetime default NULL,
  `date_start_flag` tinyint(1) default '1',
  `date_start` datetime default NULL,
  `parent_type` varchar(25) default NULL,
  `parent_id` char(36) default NULL,
  `contact_id` char(36) default NULL,
  `priority` varchar(25) default NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_tsk_name` (`name`),
  KEY `idx_task_con_del` (`contact_id`,`deleted`),
  KEY `idx_task_par_del` (`parent_id`,`parent_type`,`deleted`),
  KEY `idx_task_assigned` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tasks`
--

LOCK TABLES `tasks` WRITE;
/*!40000 ALTER TABLE `tasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `tasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tracker`
--

DROP TABLE IF EXISTS `tracker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tracker` (
  `id` int(11) NOT NULL auto_increment,
  `monitor_id` char(36) NOT NULL,
  `user_id` varchar(36) default NULL,
  `module_name` varchar(255) default NULL,
  `item_id` varchar(36) default NULL,
  `item_summary` varchar(255) default NULL,
  `date_modified` datetime default NULL,
  `action` varchar(255) default NULL,
  `session_id` varchar(36) default NULL,
  `visible` tinyint(1) default '0',
  `deleted` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_tracker_iid` (`item_id`),
  KEY `idx_tracker_userid_vis_id` (`user_id`,`visible`,`id`),
  KEY `idx_tracker_userid_itemid_vis` (`user_id`,`item_id`,`visible`),
  KEY `idx_tracker_monitor_id` (`monitor_id`)
) ENGINE=MyISAM AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tracker`
--

LOCK TABLES `tracker` WRITE;
/*!40000 ALTER TABLE `tracker` DISABLE KEYS */;
INSERT INTO `tracker` VALUES (1,'9ade2fd0-a8b3-9ac6-0150-4c0355055042',NULL,'Users',NULL,NULL,'2010-05-31 06:21:10','login','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(2,'16d97ec2-f0b7-0de0-3a61-4c0355df28e9',NULL,'Users',NULL,NULL,'2010-05-31 06:21:18','authenticate','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(3,'397ac890-fb8b-a5af-39db-4c035543a44a','1','Users',NULL,NULL,'2010-05-31 06:21:18','settimezone','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(4,'e8a03f39-3e8a-4879-48bd-4c0355d8558d','1','Users',NULL,NULL,'2010-05-31 06:21:18','settimezone','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(5,'bfab1690-f9a7-ec26-876b-4c03556a23a7','1','Users',NULL,NULL,'2010-05-31 06:21:25','savetimezone','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(6,'bfab1690-f9a7-ec26-876b-4c03556a23a7','1','UserPreferences','c5ab6ad0-9335-9904-5a5a-4c0355c3432a','Base Implementation.  Should be overridden.','2010-05-31 06:21:25','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(7,'e68b74ec-8ec6-6e81-d6dc-4c0355827f04','1','Home',NULL,NULL,'2010-05-31 06:21:25','index','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(8,'e68b74ec-8ec6-6e81-d6dc-4c0355827f04','1','UserPreferences','5a042b59-e23e-7a46-23c1-4c0355309ae0','Base Implementation.  Should be overridden.','2010-05-31 06:21:26','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(9,'e68b74ec-8ec6-6e81-d6dc-4c0355827f04','1','UserPreferences','5a7d1c65-3e5b-0aad-14ea-4c0355002d1b','Base Implementation.  Should be overridden.','2010-05-31 06:21:26','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(10,'e68b74ec-8ec6-6e81-d6dc-4c0355827f04','1','UserPreferences','5adfb3ef-88b1-3302-de07-4c03558b0793','Base Implementation.  Should be overridden.','2010-05-31 06:21:26','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(11,'e68b74ec-8ec6-6e81-d6dc-4c0355827f04','1','UserPreferences','5b426623-0207-46cd-f6ad-4c0355f5517b','Base Implementation.  Should be overridden.','2010-05-31 06:21:26','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(12,'e68b74ec-8ec6-6e81-d6dc-4c0355827f04','1','UserPreferences','5ba366e9-dd3a-13c8-8f8f-4c0355ba7d09','Base Implementation.  Should be overridden.','2010-05-31 06:21:26','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(13,'e68b74ec-8ec6-6e81-d6dc-4c0355827f04','1','UserPreferences','5c01bfb6-0ff7-eabe-9f4f-4c0355c4484e','Base Implementation.  Should be overridden.','2010-05-31 06:21:26','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(14,'e68b74ec-8ec6-6e81-d6dc-4c0355827f04','1','UserPreferences','5c629bf9-cb58-2a13-4e08-4c0355a6bb6f','Base Implementation.  Should be overridden.','2010-05-31 06:21:26','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(15,'285c03ff-8df3-dcfb-7652-4c0355f5d693','1','Home',NULL,NULL,'2010-05-31 06:21:34','dynamicaction','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(16,'285c03ff-8df3-dcfb-7652-4c0355f5d693','1','UserPreferences','5a042b59-e23e-7a46-23c1-4c0355309ae0','Base Implementation.  Should be overridden.','2010-05-31 06:21:34','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(17,'4cbfe279-0312-e647-ded3-4c0355b840a7','1','Home',NULL,NULL,'2010-05-31 06:21:37','dynamicaction','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(18,'4cbfe279-0312-e647-ded3-4c0355b840a7','1','UserPreferences','5a042b59-e23e-7a46-23c1-4c0355309ae0','Base Implementation.  Should be overridden.','2010-05-31 06:21:37','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(19,'110a3f27-b68d-c354-22bf-4c03551ed0cd','1','Administration',NULL,NULL,'2010-05-31 06:21:41','index','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(20,'a4c88c2c-fab1-47b1-b335-4c035520008d','1','Configurator',NULL,NULL,'2010-05-31 06:22:14','logview','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(21,'4b2dd089-ff9b-205b-9780-4c03551bbbd7','1','Administration',NULL,NULL,'2010-05-31 06:22:21','index','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(22,'44376644-a51a-8702-700c-4c035580968f','1','Administration',NULL,NULL,'2010-05-31 06:22:38','upgradewizard','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(23,'749837ae-d548-aae1-6a8c-4c035538148b','1','Administration',NULL,NULL,'2010-05-31 06:22:46','upgradewizard','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(24,'36c7696b-8f7b-dcce-7dd8-4c0355b59327','1','Administration',NULL,NULL,'2010-05-31 06:22:51','upgradewizard_prepare','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(25,'25c84b81-3918-9aa8-3f62-4c0355c4c2d3','1','Administration',NULL,NULL,'2010-05-31 06:22:55','upgradewizard_commit','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(26,'25c84b81-3918-9aa8-3f62-4c0355c4c2d3','1','EditCustomFields','Usersasterisk_outbound_c','asterisk_outbound_c','2010-05-31 06:22:55','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(27,'25c84b81-3918-9aa8-3f62-4c0355c4c2d3','1','EditCustomFields','Usersasterisk_inbound_c','asterisk_inbound_c','2010-05-31 06:22:55','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(28,'25c84b81-3918-9aa8-3f62-4c0355c4c2d3','1','EditCustomFields','Usersasterisk_ext_c','asterisk_ext_c','2010-05-31 06:22:55','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(29,'25c84b81-3918-9aa8-3f62-4c0355c4c2d3','1','EditCustomFields','Callsasterisk_caller_id_c','asterisk_caller_id_c','2010-05-31 06:22:55','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(30,'25c84b81-3918-9aa8-3f62-4c0355c4c2d3','1','Administration','98358273-ba66-8b71-e74d-4c03551e382f','Base Implementation.  Should be overridden.','2010-05-31 06:22:55','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(31,'a2297bc5-0c80-5913-2314-4c0355d4f35e','1','Administration',NULL,NULL,'2010-05-31 06:23:02','upgradewizard','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(32,'24d93cad-75b9-0e5e-2303-4c0355ea5688','1','Administration',NULL,NULL,'2010-05-31 06:23:10','index','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(33,'e6f536b0-d01c-3011-b8cb-4c03559718cb','1','Users',NULL,NULL,'2010-05-31 06:23:16','index','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(34,'e6f536b0-d01c-3011-b8cb-4c03559718cb','1','UserPreferences','116043c5-3235-1555-0093-4c035557bc5b','Base Implementation.  Should be overridden.','2010-05-31 06:23:17','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(35,'3af29ec1-95e2-42d3-3230-4c035585cd97','1','Users','1','Administrator','2010-05-31 06:23:20','detailview','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(36,'d1a418b0-2aa1-bc62-e955-4c0355526f68','1','Users','1','Administrator','2010-05-31 06:23:29','editview','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(37,'8c6088f7-32d2-3015-f0d6-4c035523ab9c','1','Users','1','Deltapath Administrator','2010-05-31 06:23:58','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(38,'8c6088f7-32d2-3015-f0d6-4c035523ab9c','1','UserPreferences','c5ab6ad0-9335-9904-5a5a-4c0355c3432a','Base Implementation.  Should be overridden.','2010-05-31 06:23:58','save','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(39,'a9a72833-7a1d-fcc3-5ef0-4c0355487984','1','Users','1','Deltapath Administrator','2010-05-31 06:23:58','detailview','d76fcf5a9a6b22a2e86a4d544b9ab3d8',1,0),(40,'24edfd03-0019-aa18-cec8-4c0356e44c40','1','Users',NULL,NULL,'2010-05-31 06:24:06','logout','d76fcf5a9a6b22a2e86a4d544b9ab3d8',0,0),(41,'45bf01ef-06e0-9841-6545-4c0356ca3d79',NULL,'Users',NULL,NULL,'2010-05-31 06:24:06','login','08097328557093884e36b9f8cf681adc',0,0),(42,'68821309-a734-000a-dbc8-4c0356368d71',NULL,'Users',NULL,NULL,'2010-05-31 06:24:10','authenticate','08097328557093884e36b9f8cf681adc',0,0),(43,'8b84bd77-c388-5396-bf8a-4c03568b2db4','1','Home',NULL,NULL,'2010-05-31 06:24:10','index','08097328557093884e36b9f8cf681adc',0,0),(44,'4b89364b-6331-6ed0-bda7-4c0356dd4f54','1','Users',NULL,NULL,'2010-05-31 06:24:28','logout','08097328557093884e36b9f8cf681adc',0,0),(45,'6b76225c-53e5-7566-9bc7-4c035678071b',NULL,'Users',NULL,NULL,'2010-05-31 06:24:28','login','88aa237cd128fe5e4451ae1827ff0b7c',0,0),(46,'ed7258d0-ffc5-1552-4258-4c03565fb38b',NULL,'Users',NULL,NULL,'2010-05-31 06:25:24','login','88aa237cd128fe5e4451ae1827ff0b7c',0,0),(47,'453c6f74-a4ee-8153-c2b3-4c0356f50742',NULL,'Users',NULL,NULL,'2010-05-31 06:27:44','login','88aa237cd128fe5e4451ae1827ff0b7c',0,0),(48,'4e440a02-a43e-fc5f-f5e0-4c03560094fa',NULL,'Users',NULL,NULL,'2010-05-31 06:27:50','authenticate','88aa237cd128fe5e4451ae1827ff0b7c',0,0),(49,'2420811b-ebbd-f903-d748-4c0356b67e5c','1','Home',NULL,NULL,'2010-05-31 06:27:50','index','88aa237cd128fe5e4451ae1827ff0b7c',0,0),(50,'5fd70b1d-1bc3-6920-5785-4c0356649066','1','Users',NULL,NULL,'2010-05-31 06:28:03','logout','88aa237cd128fe5e4451ae1827ff0b7c',0,0),(51,'26bc271e-5d19-e4a6-f2fa-4c0356a0691b',NULL,'Users',NULL,NULL,'2010-05-31 06:28:03','login','eda2b7b7b6ad13220e33ff03c5db47e1',0,0);
/*!40000 ALTER TABLE `tracker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `upgrade_history`
--

DROP TABLE IF EXISTS `upgrade_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `upgrade_history` (
  `id` char(36) NOT NULL,
  `filename` varchar(255) default NULL,
  `md5sum` varchar(32) default NULL,
  `type` varchar(30) default NULL,
  `status` varchar(50) default NULL,
  `version` varchar(10) default NULL,
  `name` varchar(255) default NULL,
  `description` text,
  `id_name` varchar(255) default NULL,
  `manifest` text,
  `date_entered` datetime NOT NULL,
  `enabled` tinyint(1) default '1',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `upgrade_history_md5_uk` (`md5sum`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `upgrade_history`
--

LOCK TABLES `upgrade_history` WRITE;
/*!40000 ALTER TABLE `upgrade_history` DISABLE KEYS */;
INSERT INTO `upgrade_history` VALUES ('98358273-ba66-8b71-e74d-4c03551e382f','cache/upload/upgrades/module/SugarCRM - Asterisk 1.6.zip','4ff729a5dbb983173537b70770158794','module','installed','1.1.1.0-ST','frSIP PBX SugarCRM Connector','Integrates frSIP PBX into SugarCRM.','Asterisk_SugarCRM_Connector','YTozOntzOjg6Im1hbmlmZXN0IjthOjEzOntzOjI1OiJhY2NlcHRhYmxlX3N1Z2FyX3ZlcnNpb25zIjthOjI6e3M6MTM6ImV4YWN0X21hdGNoZXMiO2E6Njp7aTowO3M6NjoiNS4yLjBmIjtpOjE7czo2OiI1LjIuMGgiO2k6MjtzOjY6IjUuMi4waSI7aTozO3M6NjoiNS4yLjBqIjtpOjQ7czo2OiI1LjIuMGsiO2k6NTtzOjY6IjUuMi4wbCI7fXM6MTM6InJlZ2V4X21hdGNoZXMiO2E6MDp7fX1zOjI0OiJhY2NlcHRhYmxlX3N1Z2FyX2ZsYXZvcnMiO2E6MTp7aTowO3M6MjoiQ0UiO31zOjY6InJlYWRtZSI7czoxMDoiUkVBRE1FLnR4dCI7czozOiJrZXkiO3M6MDoiIjtzOjY6ImF1dGhvciI7czo0MToiRGVsdGFwYXRoIENvbW1lcmNlIEFuZCBUZWNobm9sb2d5IExpbWl0ZWQiO3M6MTE6ImRlc2NyaXB0aW9uIjtzOjM1OiJJbnRlZ3JhdGVzIGZyU0lQIFBCWCBpbnRvIFN1Z2FyQ1JNLiI7czo0OiJpY29uIjtzOjA6IiI7czoxNjoiaXNfdW5pbnN0YWxsYWJsZSI7YjoxO3M6NDoibmFtZSI7czoyODoiZnJTSVAgUEJYIFN1Z2FyQ1JNIENvbm5lY3RvciI7czoxNDoicHVibGlzaGVkX2RhdGUiO3M6MTA6IjIwMTAtMDQtMTMiO3M6NDoidHlwZSI7czo2OiJtb2R1bGUiO3M6NzoidmVyc2lvbiI7czoxNDoiMS4xLjEuMC1TVEFCTEUiO3M6MTM6InJlbW92ZV90YWJsZXMiO3M6NjoicHJvbXB0Ijt9czoxMToiaW5zdGFsbGRlZnMiO2E6NTp7czoyOiJpZCI7czoyNzoiQXN0ZXJpc2tfU3VnYXJDUk1fQ29ubmVjdG9yIjtzOjQ6ImNvcHkiO2E6MjY6e2k6MDthOjI6e3M6NDoiZnJvbSI7czo0MDoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Bc3RlcmlzayI7czoyOiJ0byI7czoxNjoibW9kdWxlcy9Bc3RlcmlzayI7fWk6MTthOjI6e3M6NDoiZnJvbSI7czo1MzoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Vc2Vycy9EZXRhaWxWaWV3Lmh0bWwiO3M6MjoidG8iO3M6Mjk6Im1vZHVsZXMvVXNlcnMvRGV0YWlsVmlldy5odG1sIjt9aToyO2E6Mjp7czo0OiJmcm9tIjtzOjUxOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL1VzZXJzL0VkaXRWaWV3Lmh0bWwiO3M6MjoidG8iO3M6Mjc6Im1vZHVsZXMvVXNlcnMvRWRpdFZpZXcuaHRtbCI7fWk6MzthOjI6e3M6NDoiZnJvbSI7czo0NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Vc2Vycy9TYXZlLnBocCI7czoyOiJ0byI7czoyMjoibW9kdWxlcy9Vc2Vycy9TYXZlLnBocCI7fWk6NDthOjI6e3M6NDoiZnJvbSI7czo1MjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Vc2Vycy9EZXRhaWxWaWV3LnBocCI7czoyOiJ0byI7czoyODoibW9kdWxlcy9Vc2Vycy9EZXRhaWxWaWV3LnBocCI7fWk6NTthOjI6e3M6NDoiZnJvbSI7czo1MDoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Vc2Vycy9FZGl0Vmlldy5waHAiO3M6MjoidG8iO3M6MjY6Im1vZHVsZXMvVXNlcnMvRWRpdFZpZXcucGhwIjt9aTo2O2E6Mjp7czo0OiJmcm9tIjtzOjY4OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL0FjY291bnRzL21ldGFkYXRhL2RldGFpbHZpZXdkZWZzLnBocCI7czoyOiJ0byI7czo1MToiY3VzdG9tL21vZHVsZXMvQWNjb3VudHMvbWV0YWRhdGEvZGV0YWlsdmlld2RlZnMucGhwIjt9aTo3O2E6Mjp7czo0OiJmcm9tIjtzOjY4OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL0FjY291bnRzL21ldGFkYXRhL2RldGFpbHZpZXdkZWZzLnBocCI7czoyOiJ0byI7czo1OToiY3VzdG9tL3dvcmtpbmcvbW9kdWxlcy9BY2NvdW50cy9tZXRhZGF0YS9kZXRhaWx2aWV3ZGVmcy5waHAiO31pOjg7YToyOntzOjQ6ImZyb20iO3M6NjY6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQWNjb3VudHMvbWV0YWRhdGEvbGlzdHZpZXdkZWZzLnBocCI7czoyOiJ0byI7czo0OToiY3VzdG9tL21vZHVsZXMvQWNjb3VudHMvbWV0YWRhdGEvbGlzdHZpZXdkZWZzLnBocCI7fWk6OTthOjI6e3M6NDoiZnJvbSI7czo2NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9BY2NvdW50cy9tZXRhZGF0YS9saXN0dmlld2RlZnMucGhwIjtzOjI6InRvIjtzOjU3OiJjdXN0b20vd29ya2luZy9tb2R1bGVzL0FjY291bnRzL21ldGFkYXRhL2xpc3R2aWV3ZGVmcy5waHAiO31pOjEwO2E6Mjp7czo0OiJmcm9tIjtzOjY4OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL0NvbnRhY3RzL21ldGFkYXRhL2RldGFpbHZpZXdkZWZzLnBocCI7czoyOiJ0byI7czo1MToiY3VzdG9tL21vZHVsZXMvQ29udGFjdHMvbWV0YWRhdGEvZGV0YWlsdmlld2RlZnMucGhwIjt9aToxMTthOjI6e3M6NDoiZnJvbSI7czo2ODoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Db250YWN0cy9tZXRhZGF0YS9kZXRhaWx2aWV3ZGVmcy5waHAiO3M6MjoidG8iO3M6NTk6ImN1c3RvbS93b3JraW5nL21vZHVsZXMvQ29udGFjdHMvbWV0YWRhdGEvZGV0YWlsdmlld2RlZnMucGhwIjt9aToxMjthOjI6e3M6NDoiZnJvbSI7czo2NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Db250YWN0cy9tZXRhZGF0YS9saXN0dmlld2RlZnMucGhwIjtzOjI6InRvIjtzOjQ5OiJjdXN0b20vbW9kdWxlcy9Db250YWN0cy9tZXRhZGF0YS9saXN0dmlld2RlZnMucGhwIjt9aToxMzthOjI6e3M6NDoiZnJvbSI7czo2NjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Db250YWN0cy9tZXRhZGF0YS9saXN0dmlld2RlZnMucGhwIjtzOjI6InRvIjtzOjU3OiJjdXN0b20vd29ya2luZy9tb2R1bGVzL0NvbnRhY3RzL21ldGFkYXRhL2xpc3R2aWV3ZGVmcy5waHAiO31pOjE0O2E6Mjp7czo0OiJmcm9tIjtzOjY0OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9pbmNsdWRlL2phdmFzY3JpcHQvanF1ZXJ5L2pxdWVyeS5wYWNrLmpzIjtzOjI6InRvIjtzOjQwOiJpbmNsdWRlL2phdmFzY3JpcHQvanF1ZXJ5L2pxdWVyeS5wYWNrLmpzIjt9aToxNTthOjI6e3M6NDoiZnJvbSI7czo3MDoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Db25maWd1cmF0b3IvYXN0ZXJpc2tfY29uZmlndXJhdG9yLnBocCI7czoyOiJ0byI7czo0NjoibW9kdWxlcy9Db25maWd1cmF0b3IvYXN0ZXJpc2tfY29uZmlndXJhdG9yLnBocCI7fWk6MTY7YToyOntzOjQ6ImZyb20iO3M6NzA6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ29uZmlndXJhdG9yL2FzdGVyaXNrX2NvbmZpZ3VyYXRvci50cGwiO3M6MjoidG8iO3M6NDY6Im1vZHVsZXMvQ29uZmlndXJhdG9yL2FzdGVyaXNrX2NvbmZpZ3VyYXRvci50cGwiO31pOjE3O2E6Mjp7czo0OiJmcm9tIjtzOjMyOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9hc3RlcmlzayI7czoyOiJ0byI7czo4OiJBc3RlcmlzayI7fWk6MTg7YToyOntzOjQ6ImZyb20iO3M6NjM6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ2FsbHMvbWV0YWRhdGEvbGlzdHZpZXdkZWZzLnBocCI7czoyOiJ0byI7czo0NjoiY3VzdG9tL21vZHVsZXMvQ2FsbHMvbWV0YWRhdGEvbGlzdHZpZXdkZWZzLnBocCI7fWk6MTk7YToyOntzOjQ6ImZyb20iO3M6NjY6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ2FsbHMvbWV0YWRhdGEvcXVpY2tjcmVhdGVkZWZzLnBocCI7czoyOiJ0byI7czo0OToiY3VzdG9tL21vZHVsZXMvQ2FsbHMvbWV0YWRhdGEvcXVpY2tjcmVhdGVkZWZzLnBocCI7fWk6MjA7YToyOntzOjQ6ImZyb20iO3M6NjY6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ2FsbHMvbWV0YWRhdGEvcXVpY2tjcmVhdGVkZWZzLnBocCI7czoyOiJ0byI7czo1NzoiY3VzdG9tL3dvcmtpbmcvbW9kdWxlcy9DYWxscy9tZXRhZGF0YS9xdWlja2NyZWF0ZWRlZnMucGhwIjt9aToyMTthOjI6e3M6NDoiZnJvbSI7czo2NToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9DYWxscy9tZXRhZGF0YS9kZXRhaWx2aWV3ZGVmcy5waHAiO3M6MjoidG8iO3M6NDg6ImN1c3RvbS9tb2R1bGVzL0NhbGxzL21ldGFkYXRhL2RldGFpbHZpZXdkZWZzLnBocCI7fWk6MjI7YToyOntzOjQ6ImZyb20iO3M6NjU6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ2FsbHMvbWV0YWRhdGEvZGV0YWlsdmlld2RlZnMucGhwIjtzOjI6InRvIjtzOjU2OiJjdXN0b20vd29ya2luZy9tb2R1bGVzL0NhbGxzL21ldGFkYXRhL2RldGFpbHZpZXdkZWZzLnBocCI7fWk6MjM7YToyOntzOjQ6ImZyb20iO3M6NjM6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ2FsbHMvbWV0YWRhdGEvZWRpdHZpZXdkZWZzLnBocCI7czoyOiJ0byI7czo0NjoiY3VzdG9tL21vZHVsZXMvQ2FsbHMvbWV0YWRhdGEvZWRpdHZpZXdkZWZzLnBocCI7fWk6MjQ7YToyOntzOjQ6ImZyb20iO3M6NjM6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ2FsbHMvbWV0YWRhdGEvZWRpdHZpZXdkZWZzLnBocCI7czoyOiJ0byI7czo1NDoiY3VzdG9tL3dvcmtpbmcvbW9kdWxlcy9DYWxscy9tZXRhZGF0YS9lZGl0dmlld2RlZnMucGhwIjt9aToyNTthOjI6e3M6NDoiZnJvbSI7czo0MzoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvY29uZmlnX292ZXJyaWRlLnBocCI7czoyOiJ0byI7czoxOToiY29uZmlnX292ZXJyaWRlLnBocCI7fX1zOjE0OiJhZG1pbmlzdHJhdGlvbiI7YToxOntpOjA7YToxOntzOjQ6ImZyb20iO3M6NzI6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQWRtaW5pc3RyYXRpb24vYXN0ZXJpc2tfY29uZmlndXJhdG9yLnBocCI7fX1zOjg6Imxhbmd1YWdlIjthOjE1OntpOjA7YTozOntzOjQ6ImZyb20iO3M6NjE6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvVXNlcnMvbGFuZ3VhZ2UvZW5fdXMubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjU6IlVzZXJzIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVuX3VzIjt9aToxO2E6Mzp7czo0OiJmcm9tIjtzOjYxOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL1VzZXJzL2xhbmd1YWdlL2dlX2dlLmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czo1OiJVc2VycyI7czo4OiJsYW5ndWFnZSI7czo1OiJnZV9nZSI7fWk6MjthOjM6e3M6NDoiZnJvbSI7czo2MToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Vc2Vycy9sYW5ndWFnZS9ydV9ydS5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6NToiVXNlcnMiO3M6ODoibGFuZ3VhZ2UiO3M6NToicnVfcnUiO31pOjM7YTozOntzOjQ6ImZyb20iO3M6NzA6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQWRtaW5pc3RyYXRpb24vbGFuZ3VhZ2UvZW5fdXMubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjE0OiJBZG1pbmlzdHJhdGlvbiI7czo4OiJsYW5ndWFnZSI7czo1OiJlbl91cyI7fWk6NDthOjM6e3M6NDoiZnJvbSI7czo3MDoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9BZG1pbmlzdHJhdGlvbi9sYW5ndWFnZS9nZV9nZS5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTQ6IkFkbWluaXN0cmF0aW9uIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImdlX2dlIjt9aTo1O2E6Mzp7czo0OiJmcm9tIjtzOjcwOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL0FkbWluaXN0cmF0aW9uL2xhbmd1YWdlL3J1X3J1LmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxNDoiQWRtaW5pc3RyYXRpb24iO3M6ODoibGFuZ3VhZ2UiO3M6NToicnVfcnUiO31pOjY7YTozOntzOjQ6ImZyb20iO3M6Njg6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ29uZmlndXJhdG9yL2xhbmd1YWdlL2VuX3VzLmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxMjoiQ29uZmlndXJhdG9yIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVuX3VzIjt9aTo3O2E6Mzp7czo0OiJmcm9tIjtzOjY4OiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL0NvbmZpZ3VyYXRvci9sYW5ndWFnZS9nZV9nZS5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTI6IkNvbmZpZ3VyYXRvciI7czo4OiJsYW5ndWFnZSI7czo1OiJnZV9nZSI7fWk6ODthOjM6e3M6NDoiZnJvbSI7czo2ODoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9Db25maWd1cmF0b3IvbGFuZ3VhZ2UvcnVfcnUubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjEyOiJDb25maWd1cmF0b3IiO3M6ODoibGFuZ3VhZ2UiO3M6NToicnVfcnUiO31pOjk7YTozOntzOjQ6ImZyb20iO3M6NjE6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL21vZHVsZXMvQ2FsbHMvbGFuZ3VhZ2UvZW5fdXMubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjU6IkNhbGxzIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVuX3VzIjt9aToxMDthOjM6e3M6NDoiZnJvbSI7czo2MToiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvbW9kdWxlcy9DYWxscy9sYW5ndWFnZS9nZV9nZS5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6NToiQ2FsbHMiO3M6ODoibGFuZ3VhZ2UiO3M6NToiZ2VfZ2UiO31pOjExO2E6Mzp7czo0OiJmcm9tIjtzOjYxOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9tb2R1bGVzL0NhbGxzL2xhbmd1YWdlL3J1X3J1LmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czo1OiJDYWxscyI7czo4OiJsYW5ndWFnZSI7czo1OiJydV9ydSI7fWk6MTI7YTozOntzOjQ6ImZyb20iO3M6NzI6IjxiYXNlcGF0aD4vU3VnYXJNb2R1bGVzL2luY2x1ZGUvbGFuZ3VhZ2UvY2FsbF9zdGF0dXNfZG9tX19lbl91cy5sYW5nLnBocCI7czo5OiJ0b19tb2R1bGUiO3M6MTE6ImFwcGxpY2F0aW9uIjtzOjg6Imxhbmd1YWdlIjtzOjU6ImVuX3VzIjt9aToxMzthOjM6e3M6NDoiZnJvbSI7czo3MjoiPGJhc2VwYXRoPi9TdWdhck1vZHVsZXMvaW5jbHVkZS9sYW5ndWFnZS9jYWxsX3N0YXR1c19kb21fX2dlX2dlLmxhbmcucGhwIjtzOjk6InRvX21vZHVsZSI7czoxMToiYXBwbGljYXRpb24iO3M6ODoibGFuZ3VhZ2UiO3M6NToiZ2VfZ2UiO31pOjE0O2E6Mzp7czo0OiJmcm9tIjtzOjcyOiI8YmFzZXBhdGg+L1N1Z2FyTW9kdWxlcy9pbmNsdWRlL2xhbmd1YWdlL2NhbGxfc3RhdHVzX2RvbV9fcnVfcnUubGFuZy5waHAiO3M6OToidG9fbW9kdWxlIjtzOjExOiJhcHBsaWNhdGlvbiI7czo4OiJsYW5ndWFnZSI7czo1OiJydV9ydSI7fX1zOjEzOiJjdXN0b21fZmllbGRzIjthOjQ6e2k6MDthOjIwOntzOjI6ImlkIjtzOjI0OiJVc2Vyc2FzdGVyaXNrX291dGJvdW5kX2MiO3M6NDoibmFtZSI7czoxOToiYXN0ZXJpc2tfb3V0Ym91bmRfYyI7czo1OiJsYWJlbCI7czoyMToiTEJMX0FTVEVSSVNLX09VVEJPVU5EIjtzOjg6ImNvbW1lbnRzIjtOO3M6NDoiaGVscCI7TjtzOjY6Im1vZHVsZSI7czo1OiJVc2VycyI7czo0OiJ0eXBlIjtzOjQ6ImJvb2wiO3M6ODoibWF4X3NpemUiO3M6MjoiNDUiO3M6MTQ6InJlcXVpcmVfb3B0aW9uIjtzOjE6IjAiO3M6MTM6ImRlZmF1bHRfdmFsdWUiO2k6MDtzOjEzOiJkYXRlX21vZGlmaWVkIjtzOjE5OiIyMDA5LTA1LTIyIDAwOjAwOjAwIjtzOjc6ImRlbGV0ZWQiO3M6MToiMCI7czo3OiJhdWRpdGVkIjtzOjE6IjAiO3M6MTE6Im1hc3NfdXBkYXRlIjtzOjE6IjAiO3M6MTU6ImR1cGxpY2F0ZV9tZXJnZSI7czoxOiIwIjtzOjEwOiJyZXBvcnRhYmxlIjtzOjE6IjEiO3M6NDoiZXh0MSI7TjtzOjQ6ImV4dDIiO047czo0OiJleHQzIjtOO3M6NDoiZXh0NCI7Tjt9aToxO2E6MjA6e3M6MjoiaWQiO3M6MjM6IlVzZXJzYXN0ZXJpc2tfaW5ib3VuZF9jIjtzOjQ6Im5hbWUiO3M6MTg6ImFzdGVyaXNrX2luYm91bmRfYyI7czo1OiJsYWJlbCI7czoyMDoiTEJMX0FTVEVSSVNLX0lOQk9VTkQiO3M6ODoiY29tbWVudHMiO047czo0OiJoZWxwIjtOO3M6NjoibW9kdWxlIjtzOjU6IlVzZXJzIjtzOjQ6InR5cGUiO3M6NDoiYm9vbCI7czo4OiJtYXhfc2l6ZSI7czoyOiI0NSI7czoxNDoicmVxdWlyZV9vcHRpb24iO3M6MToiMCI7czoxMzoiZGVmYXVsdF92YWx1ZSI7aTowO3M6MTM6ImRhdGVfbW9kaWZpZWQiO3M6MTk6IjIwMDktMDUtMjIgMDA6MDA6MDAiO3M6NzoiZGVsZXRlZCI7czoxOiIwIjtzOjc6ImF1ZGl0ZWQiO3M6MToiMCI7czoxMToibWFzc191cGRhdGUiO3M6MToiMCI7czoxNToiZHVwbGljYXRlX21lcmdlIjtzOjE6IjAiO3M6MTA6InJlcG9ydGFibGUiO3M6MToiMSI7czo0OiJleHQxIjtOO3M6NDoiZXh0MiI7TjtzOjQ6ImV4dDMiO047czo0OiJleHQ0IjtOO31pOjI7YToyMDp7czoyOiJpZCI7czoxOToiVXNlcnNhc3Rlcmlza19leHRfYyI7czo0OiJuYW1lIjtzOjE0OiJhc3Rlcmlza19leHRfYyI7czo1OiJsYWJlbCI7czoxNjoiTEJMX0FTVEVSSVNLX0VYVCI7czo4OiJjb21tZW50cyI7TjtzOjQ6ImhlbHAiO047czo2OiJtb2R1bGUiO3M6NToiVXNlcnMiO3M6NDoidHlwZSI7czo3OiJ2YXJjaGFyIjtzOjg6Im1heF9zaXplIjtzOjI6IjQ1IjtzOjE0OiJyZXF1aXJlX29wdGlvbiI7czoxOiIwIjtzOjEzOiJkZWZhdWx0X3ZhbHVlIjtOO3M6MTM6ImRhdGVfbW9kaWZpZWQiO3M6MTk6IjIwMDktMDUtMjIgMDA6MDA6MDAiO3M6NzoiZGVsZXRlZCI7czoxOiIwIjtzOjc6ImF1ZGl0ZWQiO3M6MToiMCI7czoxMToibWFzc191cGRhdGUiO3M6MToiMCI7czoxNToiZHVwbGljYXRlX21lcmdlIjtzOjE6IjAiO3M6MTA6InJlcG9ydGFibGUiO3M6MToiMSI7czo0OiJleHQxIjtOO3M6NDoiZXh0MiI7TjtzOjQ6ImV4dDMiO047czo0OiJleHQ0IjtOO31pOjM7YToyMTp7czoyOiJpZCI7czoyNToiQ2FsbHNhc3Rlcmlza19jYWxsZXJfaWRfYyI7czo0OiJuYW1lIjtzOjIwOiJhc3Rlcmlza19jYWxsZXJfaWRfYyI7czo1OiJsYWJlbCI7czoyMjoiTEJMX0FTVEVSSVNLX0NBTExFUl9JRCI7czo4OiJjb21tZW50cyI7TjtzOjQ6ImhlbHAiO3M6MTY6InRyaW1tZWQgY2FsbGVySWQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjQ6InR5cGUiO3M6NzoidmFyY2hhciI7czo4OiJtYXhfc2l6ZSI7czoyOiI0NSI7czoxNDoicmVxdWlyZV9vcHRpb24iO3M6MToiMCI7czoxMzoiZGVmYXVsdF92YWx1ZSI7TjtzOjEzOiJkYXRlX21vZGlmaWVkIjtzOjE5OiIyMDA5LTA2LTE4IDE1OjM4OjQ4IjtzOjc6ImRlbGV0ZWQiO3M6MToiMCI7czo3OiJhdWRpdGVkIjtzOjE6IjAiO3M6MTE6Im1hc3NfdXBkYXRlIjtzOjE6IjAiO3M6MTU6ImR1cGxpY2F0ZV9tZXJnZSI7czoxOiIwIjtzOjEwOiJyZXBvcnRhYmxlIjtzOjE6IjAiO3M6MTA6ImltcG9ydGFibGUiO3M6NDoidHJ1ZSI7czo0OiJleHQxIjtOO3M6NDoiZXh0MiI7TjtzOjQ6ImV4dDMiO047czo0OiJleHQ0IjtOO319fXM6MTY6InVwZ3JhZGVfbWFuaWZlc3QiO3M6MDoiIjt9','2010-05-31 06:22:55',1);
/*!40000 ALTER TABLE `upgrade_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_preferences`
--

DROP TABLE IF EXISTS `user_preferences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_preferences` (
  `id` char(36) NOT NULL,
  `category` varchar(50) default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `assigned_user_id` char(36) NOT NULL,
  `contents` text,
  PRIMARY KEY  (`id`),
  KEY `idx_userprefnamecat` (`assigned_user_id`,`category`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_preferences`
--

LOCK TABLES `user_preferences` WRITE;
/*!40000 ALTER TABLE `user_preferences` DISABLE KEYS */;
INSERT INTO `user_preferences` VALUES ('c5ab6ad0-9335-9904-5a5a-4c0355c3432a','global',0,'2010-05-31 06:21:25','2010-05-31 06:23:58','1','YTozMjp7czo4OiJncmlkbGluZSI7czozOiJvZmYiO3M6MTI6Im1haWxtZXJnZV9vbiI7czozOiJvZmYiO3M6ODoibWF4X3RhYnMiO3M6MjoiMTIiO3M6MTE6Im1heF9zdWJ0YWJzIjtzOjI6IjEyIjtzOjE2OiJzd2FwX2xhc3Rfdmlld2VkIjtzOjA6IiI7czoxNDoic3dhcF9zaG9ydGN1dHMiO3M6MDoiIjtzOjEzOiJzdWJwYW5lbF90YWJzIjtzOjI6Im9uIjtzOjE0OiJzdWJwYW5lbF9saW5rcyI7czowOiIiO3M6MTk6Im5hdmlnYXRpb25fcGFyYWRpZ20iO3M6MjoiZ20iO3M6MTI6ImRpc3BsYXlfdGFicyI7YToxNjp7aTowO3M6NDoiSG9tZSI7aToxO3M6OToiRGFzaGJvYXJkIjtpOjI7czo4OiJDYWxlbmRhciI7aTozO3M6MTA6IkFjdGl2aXRpZXMiO2k6NDtzOjY6IkVtYWlscyI7aTo1O3M6OToiRG9jdW1lbnRzIjtpOjY7czo4OiJDb250YWN0cyI7aTo3O3M6ODoiQWNjb3VudHMiO2k6ODtzOjk6IkNhbXBhaWducyI7aTo5O3M6NToiTGVhZHMiO2k6MTA7czoxMzoiT3Bwb3J0dW5pdGllcyI7aToxMTtzOjc6IlByb2plY3QiO2k6MTI7czo1OiJDYXNlcyI7aToxMztzOjQ6IkJ1Z3MiO2k6MTQ7czo3OiJpRnJhbWVzIjtpOjE1O3M6NToiRmVlZHMiO31zOjk6ImhpZGVfdGFicyI7YTowOnt9czoxMToicmVtb3ZlX3RhYnMiO2E6MDp7fXM6Nzoibm9fb3BwcyI7czozOiJvZmYiO3M6MTM6InJlbWluZGVyX3RpbWUiO2k6LTE7czo4OiJ0aW1lem9uZSI7czoxNDoiQXNpYS9Ib25nX0tvbmciO3M6MjoidXQiO3M6MToiMSI7czo4OiJjdXJyZW5jeSI7czozOiItOTkiO3M6MzU6ImRlZmF1bHRfY3VycmVuY3lfc2lnbmlmaWNhbnRfZGlnaXRzIjtzOjE6IjIiO3M6MTE6Im51bV9ncnBfc2VwIjtzOjE6IiwiO3M6NzoiZGVjX3NlcCI7czoxOiIuIjtzOjU6ImRhdGVmIjtzOjU6Im0vZC9ZIjtzOjU6InRpbWVmIjtzOjM6Ikg6aSI7czoyNjoiZGVmYXVsdF9sb2NhbGVfbmFtZV9mb3JtYXQiO3M6NToicyBmIGwiO3M6MTY6ImV4cG9ydF9kZWxpbWl0ZXIiO3M6MToiLCI7czoyMjoiZGVmYXVsdF9leHBvcnRfY2hhcnNldCI7czo1OiJVVEYtOCI7czoxNDoidXNlX3JlYWxfbmFtZXMiO3M6Mzoib2ZmIjtzOjE3OiJtYWlsX3NtdHBhdXRoX3JlcSI7czowOiIiO3M6MTI6Im1haWxfc210cHNzbCI7aTowO3M6MTc6InNpZ25hdHVyZV9wcmVwZW5kIjtzOjA6IiI7czoxNToiZW1haWxfbGlua190eXBlIjtzOjA6IiI7czoxNzoiZW1haWxfc2hvd19jb3VudHMiO2k6MDtzOjIwOiJjYWxlbmRhcl9wdWJsaXNoX2tleSI7czowOiIiO30='),('5a042b59-e23e-7a46-23c1-4c0355309ae0','Home',0,'2010-05-31 06:21:26','2010-05-31 06:21:37','1','YToyOntzOjg6ImRhc2hsZXRzIjthOjY6e3M6MzY6IjM1Y2VjMjA2LTdkNmItYTc2Yi0zM2NhLTRjMDM1NWQzNTI5MyI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlDYWxsc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkNhbGxzIjtzOjEyOiJmaWxlTG9jYXRpb24iO3M6NTY6Im1vZHVsZXMvQ2FsbHMvRGFzaGxldHMvTXlDYWxsc0Rhc2hsZXQvTXlDYWxsc0Rhc2hsZXQucGhwIjtzOjc6Im9wdGlvbnMiO2E6MDp7fX1zOjM2OiIzNjhiY2MyZS1kNDliLTdkODAtYjEzYS00YzAzNTU3ZGE5OTciO2E6NDp7czo5OiJjbGFzc05hbWUiO3M6MjI6Ik15T3Bwb3J0dW5pdGllc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjEzOiJPcHBvcnR1bml0aWVzIjtzOjEyOiJmaWxlTG9jYXRpb24iO3M6ODA6Im1vZHVsZXMvT3Bwb3J0dW5pdGllcy9EYXNobGV0cy9NeU9wcG9ydHVuaXRpZXNEYXNobGV0L015T3Bwb3J0dW5pdGllc0Rhc2hsZXQucGhwIjtzOjc6Im9wdGlvbnMiO2E6MDp7fX1zOjM2OiIzNzJkOTFhMy0yNDhjLTk1MmQtYjRiMS00YzAzNTVjNmEzMmYiO2E6NDp7czo5OiJjbGFzc05hbWUiO3M6MTc6Ik15TWVldGluZ3NEYXNobGV0IjtzOjY6Im1vZHVsZSI7czo4OiJNZWV0aW5ncyI7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjY1OiJtb2R1bGVzL01lZXRpbmdzL0Rhc2hsZXRzL015TWVldGluZ3NEYXNobGV0L015TWVldGluZ3NEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319czozNjoiMzgxZTFmYjctMzU4MC1mNGMwLTUzOGYtNGMwMzU1NTVjOTIyIjthOjQ6e3M6OToiY2xhc3NOYW1lIjtzOjE3OiJNeUFjY291bnRzRGFzaGxldCI7czo2OiJtb2R1bGUiO3M6ODoiQWNjb3VudHMiO3M6MTI6ImZpbGVMb2NhdGlvbiI7czo2NToibW9kdWxlcy9BY2NvdW50cy9EYXNobGV0cy9NeUFjY291bnRzRGFzaGxldC9NeUFjY291bnRzRGFzaGxldC5waHAiO3M6Nzoib3B0aW9ucyI7YTowOnt9fXM6MzY6IjM5NDQ5YTQyLTU5ZWYtOWMxNy02ZDgyLTRjMDM1NTUyMjRjMiI7YTo0OntzOjk6ImNsYXNzTmFtZSI7czoxNDoiTXlMZWFkc0Rhc2hsZXQiO3M6NjoibW9kdWxlIjtzOjU6IkxlYWRzIjtzOjEyOiJmaWxlTG9jYXRpb24iO3M6NTY6Im1vZHVsZXMvTGVhZHMvRGFzaGxldHMvTXlMZWFkc0Rhc2hsZXQvTXlMZWFkc0Rhc2hsZXQucGhwIjtzOjc6Im9wdGlvbnMiO2E6MDp7fX1zOjM2OiIzOWViNmUwMC1mOGVkLWUzZDUtNDVlNS00YzAzNTVlM2ZmNTgiO2E6NDp7czo5OiJjbGFzc05hbWUiO3M6MTQ6Ik15Q2FzZXNEYXNobGV0IjtzOjY6Im1vZHVsZSI7czo1OiJDYXNlcyI7czoxMjoiZmlsZUxvY2F0aW9uIjtzOjU2OiJtb2R1bGVzL0Nhc2VzL0Rhc2hsZXRzL015Q2FzZXNEYXNobGV0L015Q2FzZXNEYXNobGV0LnBocCI7czo3OiJvcHRpb25zIjthOjA6e319fXM6NToicGFnZXMiO2E6MTp7aTowO2E6Mzp7czo3OiJjb2x1bW5zIjthOjI6e2k6MDthOjI6e3M6NToid2lkdGgiO3M6MzoiNjAlIjtzOjg6ImRhc2hsZXRzIjthOjM6e2k6MTtzOjM2OiIzNWNlYzIwNi03ZDZiLWE3NmItMzNjYS00YzAzNTVkMzUyOTMiO2k6MjtzOjM2OiIzNzJkOTFhMy0yNDhjLTk1MmQtYjRiMS00YzAzNTVjNmEzMmYiO2k6MztzOjM2OiIzOTQ0OWE0Mi01OWVmLTljMTctNmQ4Mi00YzAzNTU1MjI0YzIiO319aToxO2E6Mjp7czo1OiJ3aWR0aCI7czozOiI0MCUiO3M6ODoiZGFzaGxldHMiO2E6Mzp7aToxO3M6MzY6IjM2OGJjYzJlLWQ0OWItN2Q4MC1iMTNhLTRjMDM1NTdkYTk5NyI7aToyO3M6MzY6IjM4MWUxZmI3LTM1ODAtZjRjMC01MzhmLTRjMDM1NTU1YzkyMiI7aTozO3M6MzY6IjM5ZWI2ZTAwLWY4ZWQtZTNkNS00NWU1LTRjMDM1NWUzZmY1OCI7fX19czoxMDoibnVtQ29sdW1ucyI7czoxOiIyIjtzOjk6InBhZ2VUaXRsZSI7czo4OiJNeSBTdWdhciI7fX19'),('116043c5-3235-1555-0093-4c035557bc5b','Users2_USER',0,'2010-05-31 06:23:17','2010-05-31 06:23:17','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czowOiIiO3M6OToic29ydE9yZGVyIjtzOjA6IiI7fX0='),('5a7d1c65-3e5b-0aad-14ea-4c0355002d1b','Home2_CALL',0,'2010-05-31 06:21:26','2010-05-31 06:21:26','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czowOiIiO3M6OToic29ydE9yZGVyIjtzOjA6IiI7fX0='),('5adfb3ef-88b1-3302-de07-4c03558b0793','Home2_MEETING',0,'2010-05-31 06:21:26','2010-05-31 06:21:26','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czowOiIiO3M6OToic29ydE9yZGVyIjtzOjA6IiI7fX0='),('5b426623-0207-46cd-f6ad-4c0355f5517b','Home2_LEAD',0,'2010-05-31 06:21:26','2010-05-31 06:21:26','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czowOiIiO3M6OToic29ydE9yZGVyIjtzOjA6IiI7fX0='),('5ba366e9-dd3a-13c8-8f8f-4c0355ba7d09','Home2_OPPORTUNITY',0,'2010-05-31 06:21:26','2010-05-31 06:21:26','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czoxMToiZGF0ZV9jbG9zZWQiO3M6OToic29ydE9yZGVyIjtzOjM6IkFTQyI7fX0='),('5c01bfb6-0ff7-eabe-9f4f-4c0355c4484e','Home2_ACCOUNT',0,'2010-05-31 06:21:26','2010-05-31 06:21:26','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czowOiIiO3M6OToic29ydE9yZGVyIjtzOjA6IiI7fX0='),('5c629bf9-cb58-2a13-4e08-4c0355a6bb6f','Home2_CASE',0,'2010-05-31 06:21:26','2010-05-31 06:21:26','1','YToxOntzOjEzOiJsaXN0dmlld09yZGVyIjthOjI6e3M6Nzoib3JkZXJCeSI7czowOiIiO3M6OToic29ydE9yZGVyIjtzOjA6IiI7fX0=');
/*!40000 ALTER TABLE `user_preferences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` char(36) NOT NULL,
  `user_name` varchar(60) default NULL,
  `user_hash` varchar(32) default NULL,
  `authenticate_id` varchar(100) default NULL,
  `sugar_login` tinyint(1) default '1',
  `first_name` varchar(30) default NULL,
  `last_name` varchar(30) default NULL,
  `reports_to_id` char(36) default NULL,
  `is_admin` tinyint(1) default '0',
  `receive_notifications` tinyint(1) default '1',
  `description` text,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) default NULL,
  `created_by` char(36) default NULL,
  `title` varchar(50) default NULL,
  `department` varchar(50) default NULL,
  `phone_home` varchar(50) default NULL,
  `phone_mobile` varchar(50) default NULL,
  `phone_work` varchar(50) default NULL,
  `phone_other` varchar(50) default NULL,
  `phone_fax` varchar(50) default NULL,
  `status` varchar(25) default NULL,
  `address_street` varchar(150) default NULL,
  `address_city` varchar(100) default NULL,
  `address_state` varchar(100) default NULL,
  `address_country` varchar(25) default NULL,
  `address_postalcode` varchar(9) default NULL,
  `user_preferences` text,
  `deleted` tinyint(1) NOT NULL default '0',
  `portal_only` tinyint(1) default '0',
  `employee_status` varchar(25) default NULL,
  `messenger_id` varchar(25) default NULL,
  `messenger_type` varchar(25) default NULL,
  `is_group` tinyint(1) default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_user_name` (`user_name`,`is_group`,`status`,`last_name`,`first_name`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES ('1','deltapath','f99b1e84d05532aec4407217499367b2',NULL,1,'Deltapath','Administrator',NULL,1,1,NULL,'2010-05-31 06:20:41','2010-05-31 06:23:58','1',NULL,'Administrator',NULL,NULL,NULL,NULL,NULL,NULL,'Active',NULL,NULL,NULL,NULL,NULL,NULL,0,0,'Active',NULL,NULL,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_cstm`
--

DROP TABLE IF EXISTS `users_cstm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_cstm` (
  `id_c` char(36) NOT NULL,
  `asterisk_outbound_c` tinyint(1) default '0',
  `asterisk_inbound_c` tinyint(1) default '0',
  `asterisk_ext_c` varchar(45) default NULL,
  PRIMARY KEY  (`id_c`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_cstm`
--

LOCK TABLES `users_cstm` WRITE;
/*!40000 ALTER TABLE `users_cstm` DISABLE KEYS */;
INSERT INTO `users_cstm` VALUES ('1',0,0,'');
/*!40000 ALTER TABLE `users_cstm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_feeds`
--

DROP TABLE IF EXISTS `users_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_feeds` (
  `user_id` varchar(36) default NULL,
  `feed_id` varchar(36) default NULL,
  `rank` int(11) default NULL,
  `date_modified` datetime default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  KEY `idx_ud_user_id` (`user_id`,`feed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_feeds`
--

LOCK TABLES `users_feeds` WRITE;
/*!40000 ALTER TABLE `users_feeds` DISABLE KEYS */;
INSERT INTO `users_feeds` VALUES ('1','4bbca87f-2017-5488-d8e0-41e7808c2553',1,NULL,0);
/*!40000 ALTER TABLE `users_feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_last_import`
--

DROP TABLE IF EXISTS `users_last_import`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_last_import` (
  `id` char(36) NOT NULL,
  `assigned_user_id` char(36) default NULL,
  `import_module` varchar(36) default NULL,
  `bean_type` varchar(36) default NULL,
  `bean_id` char(36) default NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `idx_user_id` (`assigned_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_last_import`
--

LOCK TABLES `users_last_import` WRITE;
/*!40000 ALTER TABLE `users_last_import` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_last_import` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_signatures`
--

DROP TABLE IF EXISTS `users_signatures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_signatures` (
  `id` char(36) NOT NULL,
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `user_id` varchar(36) default NULL,
  `name` varchar(255) default NULL,
  `signature` text,
  `signature_html` text,
  PRIMARY KEY  (`id`),
  KEY `idx_usersig_uid` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_signatures`
--

LOCK TABLES `users_signatures` WRITE;
/*!40000 ALTER TABLE `users_signatures` DISABLE KEYS */;
/*!40000 ALTER TABLE `users_signatures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vcals`
--

DROP TABLE IF EXISTS `vcals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vcals` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime default NULL,
  `date_modified` datetime default NULL,
  `user_id` char(36) NOT NULL,
  `type` varchar(25) default NULL,
  `source` varchar(25) default NULL,
  `content` text,
  PRIMARY KEY  (`id`),
  KEY `idx_vcal` (`type`,`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vcals`
--

LOCK TABLES `vcals` WRITE;
/*!40000 ALTER TABLE `vcals` DISABLE KEYS */;
/*!40000 ALTER TABLE `vcals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `versions`
--

DROP TABLE IF EXISTS `versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `versions` (
  `id` char(36) NOT NULL,
  `deleted` tinyint(1) NOT NULL default '0',
  `date_entered` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `modified_user_id` char(36) NOT NULL,
  `created_by` char(36) default NULL,
  `name` varchar(255) NOT NULL,
  `file_version` varchar(255) NOT NULL,
  `db_version` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `idx_version` (`name`,`deleted`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `versions`
--

LOCK TABLES `versions` WRITE;
/*!40000 ALTER TABLE `versions` DISABLE KEYS */;
INSERT INTO `versions` VALUES ('18c805ac-21eb-d460-534f-4c0355af5fce',0,'2010-05-31 06:20:41','2010-05-31 06:20:41','1',NULL,'Chart Data Cache','3.5.1','3.5.1'),('18fdf687-70da-d034-e1ff-4c0355960fb2',0,'2010-05-31 06:20:41','2010-05-31 06:20:41','1',NULL,'htaccess','3.5.1','3.5.1'),('975fb7fa-0143-6591-b9a9-4c03558351d2',0,'2010-05-31 06:22:55','2010-05-31 06:22:55','1','1','Rebuild Relationships','4.0.0','4.0.0'),('1964193b-3bfb-650e-0148-4c0355767947',0,'2010-05-31 06:20:41','2010-05-31 06:20:41','1',NULL,'Rebuild Extensions','4.0.0','4.0.0');
/*!40000 ALTER TABLE `versions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2010-05-31 14:31:54
