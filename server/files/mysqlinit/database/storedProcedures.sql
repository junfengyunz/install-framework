DROP PROCEDURE IF EXISTS ca_getUserAccount;
DELIMITER //
CREATE PROCEDURE ca_getUserAccount(IN in_username TEXT, IN in_pincode TEXT)
BEGIN

    IF in_username = "" THEN

        -- get all the user account
        SELECT
            ua.useraccount_username, u.password, u.fullname, g.groupCode, 
            IF(ua.useraccount_validate_pin='Profiled', (SELECT user_IDD_valid_PIN FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = ua.useraccount_username limit 1), ua.useraccount_validate_pin) AS useraccount_validate_pin,
            IF(ua.useraccount_allow_others='Profiled', (SELECT user_IDD_allow_other FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = ua.useraccount_username limit 1), ua.useraccount_allow_others) AS useraccount_allow_others,
            IF(ua.useraccount_status='Profiled', (SELECT user_IDD_status FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = ua.useraccount_username limit 1), ua.useraccount_status) AS useraccount_status
        FROM
            ca_useraccount ua INNER JOIN users u ON ua.useraccount_username = u.mailbox 
                LEFT JOIN
                    (SELECT 
                        s.username as account_username,
                        s.group as ba_id
                     FROM 
                        sipfriends s INNER JOIN general_group g ON s.group = g.groupID
                    ) AS a1
                    ON ua.useraccount_username = a1.account_username
                LEFT JOIN
                    (SELECT 
                        d.mailbox as account_username,
                        d.group as ba_id
                     FROM 
                        users d INNER JOIN general_group g ON d.group = g.groupID
                    ) AS a2
                    ON ua.useraccount_username = a2.account_username
            INNER JOIN general_group g ON IF(a1.ba_id is NULL, a2.ba_id=g.groupID, a1.ba_id=g.groupID)
        WHERE 
            g.groupCode != ""
        ORDER BY 
            useraccount_username;

    ELSE
        
        -- get specific user account

        IF in_pincode = "" THEN
        
            -- only check the existence of the user account
            SELECT
                ua.useraccount_username, u.password, u.fullname, g.groupCode, 
                IF(ua.useraccount_validate_pin='Profiled', (SELECT user_IDD_valid_PIN FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = ua.useraccount_username limit 1), ua.useraccount_validate_pin) AS useraccount_validate_pin,
                IF(ua.useraccount_allow_others='Profiled', (SELECT user_IDD_allow_other FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = ua.useraccount_username limit 1), ua.useraccount_allow_others) AS useraccount_allow_others,
                IF(ua.useraccount_status='Profiled', (SELECT user_IDD_status FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = ua.useraccount_username limit 1), ua.useraccount_status) AS useraccount_status
            FROM
                ca_useraccount ua INNER JOIN users u ON ua.useraccount_username = u.mailbox 
                    LEFT JOIN
                        (SELECT 
                            s.username as account_username,
                            s.group as ba_id
                         FROM 
                            sipfriends s INNER JOIN general_group g ON s.group = g.groupID
                         WHERE
                            s.username = in_username
                        ) AS a1
                        ON ua.useraccount_username = a1.account_username
                    LEFT JOIN
                        (SELECT 
                            d.mailbox as account_username,
                            d.group as ba_id
                         FROM 
                            users d INNER JOIN general_group g ON d.group = g.groupID
                         WHERE
                            d.mailbox = in_username
                        ) AS a2
                        ON ua.useraccount_username = a2.account_username
                INNER JOIN general_group g ON IF(a1.ba_id is NULL, a2.ba_id=g.groupID, a1.ba_id=g.groupID)
            WHERE 
                ua.useraccount_username = in_username
            AND
                g.groupCode != ""
            ORDER BY 
                useraccount_username;

        ELSE

            -- also check with the pincode
            SELECT
                ua.useraccount_username, u.password, u.fullname, g.groupCode, 
                IF(ua.useraccount_validate_pin='Profiled', (SELECT user_IDD_valid_PIN FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = ua.useraccount_username limit 1), ua.useraccount_validate_pin) AS useraccount_validate_pin,
                IF(ua.useraccount_allow_others='Profiled', (SELECT user_IDD_allow_other FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = ua.useraccount_username limit 1), ua.useraccount_allow_others) AS useraccount_allow_others,
                IF(ua.useraccount_status='Profiled', (SELECT user_IDD_status FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = ua.useraccount_username limit 1), ua.useraccount_status) AS useraccount_status
            FROM
                ca_useraccount ua INNER JOIN users u ON ua.useraccount_username = u.mailbox 
                    LEFT JOIN
                        (SELECT 
                            s.username as account_username,
                            s.group as ba_id
                         FROM 
                            sipfriends s INNER JOIN general_group g ON s.group = g.groupID
                         WHERE
                            s.username = in_username
                        ) AS a1
                        ON ua.useraccount_username = a1.account_username
                    LEFT JOIN
                        (SELECT 
                            d.mailbox as account_username,
                            d.group as ba_id
                         FROM 
                            users d INNER JOIN general_group g ON d.group = g.groupID
                         WHERE
                            d.mailbox = in_username
                        ) AS a2
                        ON ua.useraccount_username = a2.account_username
                INNER JOIN general_group g ON IF(a1.ba_id is NULL, a2.ba_id=g.groupID, a1.ba_id=g.groupID)
            WHERE 
                ua.useraccount_username = in_username
            AND
                u.password = in_pincode
            AND
                g.groupCode != ""
            ORDER BY 
                useraccount_username;

        END IF;

    END IF;

END;
//
DELIMITER ;


-- determine the tariff rate by specifying tariff code, country code, countryarea code
DROP PROCEDURE IF EXISTS ca_getTariffrate;
DELIMITER //
CREATE PROCEDURE ca_getTariffrate(IN in_datetime DATETIME, IN in_tariff_code TEXT, IN in_country_code TEXT, IN in_countryarea_code TEXT, OUT out_tariffrate_id INT)
BEGIN

    SET out_tariffrate_id = 0;

    -- get the tariff rate
    SELECT
        tariffrate_id INTO out_tariffrate_id
    FROM 
        ca_tariff t INNER JOIN ca_tariffrate tr ON t.tariff_id = tr.tariffrate_tariff_id
    WHERE
        tariff_code = in_tariff_code
    AND
        tariffrate_country_code = in_country_code
    AND
        tariffrate_countryarea_code = in_countryarea_code
    AND
        in_datetime
            BETWEEN
                tariffrate_valid_period_begin
            AND
                tariffrate_valid_period_end
    AND
        (tariffrate_period_weekday like CONCAT('%', WEEKDAY(in_datetime)+1, '%')
        OR  
        tariffrate_period_weekday = '')
    AND
        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_begin="", MONTH(in_datetime), tariffrate_period_month_begin), "-", IF(tariffrate_period_day_begin="", DAY(in_datetime), tariffrate_period_day_begin)), '%Y-%m-%d') > STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_end="", MONTH(in_datetime), tariffrate_period_month_end), "-", IF(tariffrate_period_day_end="", DAY(in_datetime), tariffrate_period_day_end)), '%Y-%m-%d'), 
            -- begin > end
            date(in_datetime)
                BETWEEN
                    IF(tariffrate_valid_period_begin = "", 
                        -- start from day 1
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_end="", MONTH(in_datetime), tariffrate_period_month_end), "-", IF(tariffrate_period_day_end="", DAY(in_datetime), tariffrate_period_day_end)), '%Y-%m-%d')
                    ,
                        -- get the valid start date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_end="", MONTH(in_datetime), tariffrate_period_month_end), "-", IF(tariffrate_period_day_end="", DAY(in_datetime), tariffrate_period_day_end)), '%Y-%m-%d') < tariffrate_valid_period_begin, 
                            -- period end < valid end, so use the valid end
                            STR_TO_DATE(tariffrate_valid_period_begin, '%Y-%m-%d')
                        ,
                            -- period end >= valid end, so use the period end
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_end="", MONTH(in_datetime), tariffrate_period_month_end), "-", IF(tariffrate_period_day_end="", DAY(in_datetime), tariffrate_period_day_end)), '%Y-%m-%d')
                        )
                    )
                AND
                    IF(tariffrate_valid_period_end = "", 
                        -- last forever
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_begin="", MONTH(in_datetime), tariffrate_period_month_begin), "-", IF(tariffrate_period_day_begin="", DAY(in_datetime), tariffrate_period_day_begin)), '%Y-%m-%d')
                    ,
                        -- get the valid end date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_begin="", MONTH(in_datetime), tariffrate_period_month_begin), "-", IF(tariffrate_period_day_begin="", DAY(in_datetime), tariffrate_period_day_begin)), '%Y-%m-%d') > tariffrate_valid_period_begin OR STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_begin="", MONTH(in_datetime), tariffrate_period_month_begin), "-", IF(tariffrate_period_day_begin="", DAY(in_datetime), tariffrate_period_day_begin)), '%Y-%m-%d') < tariffrate_valid_period_end, 
                            -- period begin > valid end or period begin < valid end, so use the valid end
                            STR_TO_DATE(tariffrate_valid_period_end, '%Y-%m-%d')
                        ,
                            -- period begin <= valid begin, so use the period begin
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_begin="", MONTH(in_datetime), tariffrate_period_month_begin), "-", IF(tariffrate_period_day_begin="", DAY(in_datetime), tariffrate_period_day_begin)), '%Y-%m-%d')
                        )
                    )
        ,
            -- begin <= end
            date(in_datetime)
                BETWEEN
                    IF(tariffrate_valid_period_begin = "",
                        -- start from day 1
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_begin="", MONTH(in_datetime), tariffrate_period_month_begin), "-", IF(tariffrate_period_day_begin="", DAY(in_datetime), tariffrate_period_day_begin)), '%Y-%m-%d')
                    ,
                        -- get the valid start date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_begin="", MONTH(in_datetime), tariffrate_period_month_begin), "-", IF(tariffrate_period_day_begin="", DAY(in_datetime), tariffrate_period_day_begin)), '%Y-%m-%d') < tariffrate_valid_period_begin,
                            -- period begin < valid begin, so use the valid begin
                            STR_TO_DATE(tariffrate_valid_period_begin, '%Y-%m-%d')
                        ,
                            -- period begin >= valid begin, so use the period begin
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_begin="", MONTH(in_datetime), tariffrate_period_month_begin), "-", IF(tariffrate_period_day_begin="", DAY(in_datetime), tariffrate_period_day_begin)), '%Y-%m-%d')
                        )
                    )
                AND
                    IF(tariffrate_valid_period_end = "",
                        -- last forever
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_end="", MONTH(in_datetime), tariffrate_period_month_end), "-", IF(tariffrate_period_day_end="", DAY(in_datetime), tariffrate_period_day_end)), '%Y-%m-%d')
                    ,
                        -- get the valid end date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_end="", MONTH(in_datetime), tariffrate_period_month_end), "-", IF(tariffrate_period_day_end="", DAY(in_datetime), tariffrate_period_day_end)), '%Y-%m-%d') > tariffrate_valid_period_end OR STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_end="", MONTH(in_datetime), tariffrate_period_month_end), "-", IF(tariffrate_period_day_end="", DAY(in_datetime), tariffrate_period_day_end)), '%Y-%m-%d') < tariffrate_valid_period_begin,
                            -- period end > valid end or period end < begin end, so use the valid end
                            STR_TO_DATE(tariffrate_valid_period_end, '%Y-%m-%d')
                        ,
                            -- period end <= valid end, so use the period end
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(tariffrate_period_month_end="", MONTH(in_datetime), tariffrate_period_month_end), "-", IF(tariffrate_period_day_end="", DAY(in_datetime), tariffrate_period_day_end)), '%Y-%m-%d')
                        )
                    )
        )
    AND
        IF(IF(tariffrate_period_time_begin="", time(in_datetime), tariffrate_period_time_begin) > IF(tariffrate_period_time_end="", time(in_datetime), tariffrate_period_time_end),
            -- time begin > time end
            time(in_datetime) NOT
                BETWEEN
                    IF(tariffrate_period_time_begin="", time(in_datetime), tariffrate_period_time_begin)
                AND
                    IF(tariffrate_period_time_end="", time(in_datetime), tariffrate_period_time_end)
        ,
            -- time begin <= time end
            time(in_datetime)
                BETWEEN
                    IF(tariffrate_period_time_begin="", time(in_datetime), tariffrate_period_time_begin)
                AND
                    IF(tariffrate_period_time_end="", time(in_datetime), tariffrate_period_time_end)
        )
    ORDER BY
        tariffrate_period_default DESC
    LIMIT 1;

END;
//
DELIMITER ;


-- determine the provider rate by specifying provider code, country code, countryarea code
DROP PROCEDURE IF EXISTS ca_getProviderrate;
DELIMITER //
CREATE PROCEDURE ca_getProviderrate(IN in_datetime DATETIME, IN in_provider_code TEXT, IN in_country_code TEXT, IN in_countryarea_code TEXT, OUT out_providerrate_id INT)
BEGIN

    SET out_providerrate_id = 0;

    -- get the provider rate
    SELECT
        providerrate_id INTO out_providerrate_id
    FROM 
        ca_provider p INNER JOIN ca_providerrate pr ON p.provider_id = pr.providerrate_provider_id
    WHERE
        provider_code = in_provider_code
    AND
        providerrate_country_code = in_country_code
    AND
        providerrate_countryarea_code = in_countryarea_code
    AND
        in_datetime
            BETWEEN
                providerrate_valid_period_begin
            AND
                providerrate_valid_period_end
    AND
        (providerrate_period_weekday like CONCAT('%', WEEKDAY(in_datetime)+1, '%')
        OR  
        providerrate_period_weekday = '')
    AND
        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_begin="", MONTH(in_datetime), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(in_datetime), providerrate_period_day_begin)), '%Y-%m-%d') > STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_end="", MONTH(in_datetime), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(in_datetime), providerrate_period_day_end)), '%Y-%m-%d'), 
            -- begin > end
            date(in_datetime)
                BETWEEN
                    IF(providerrate_valid_period_begin = "", 
                        -- start from day 1
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_end="", MONTH(in_datetime), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(in_datetime), providerrate_period_day_end)), '%Y-%m-%d')
                    ,
                        -- get the valid start date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_end="", MONTH(in_datetime), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(in_datetime), providerrate_period_day_end)), '%Y-%m-%d') < providerrate_valid_period_begin, 
                            -- period end < valid end, so use the valid end
                            STR_TO_DATE(providerrate_valid_period_begin, '%Y-%m-%d')
                        ,
                            -- period end >= valid end, so use the period end
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_end="", MONTH(in_datetime), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(in_datetime), providerrate_period_day_end)), '%Y-%m-%d')
                        )
                    )
                AND
                    IF(providerrate_valid_period_end = "", 
                        -- last forever
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_begin="", MONTH(in_datetime), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(in_datetime), providerrate_period_day_begin)), '%Y-%m-%d')
                    ,
                        -- get the valid end date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_begin="", MONTH(in_datetime), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(in_datetime), providerrate_period_day_begin)), '%Y-%m-%d') > providerrate_valid_period_begin OR STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_begin="", MONTH(in_datetime), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(in_datetime), providerrate_period_day_begin)), '%Y-%m-%d') < providerrate_valid_period_end, 
                            -- period begin > valid end or period begin < valid end, so use the valid end
                            STR_TO_DATE(providerrate_valid_period_end, '%Y-%m-%d')
                        ,
                            -- period begin <= valid begin, so use the period begin
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_begin="", MONTH(in_datetime), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(in_datetime), providerrate_period_day_begin)), '%Y-%m-%d')
                        )
                    )
        ,
            -- begin <= end
            date(in_datetime)
                BETWEEN
                    IF(providerrate_valid_period_begin = "",
                        -- start from day 1
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_begin="", MONTH(in_datetime), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(in_datetime), providerrate_period_day_begin)), '%Y-%m-%d')
                    ,
                        -- get the valid start date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_begin="", MONTH(in_datetime), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(in_datetime), providerrate_period_day_begin)), '%Y-%m-%d') < providerrate_valid_period_begin,
                            -- period begin < valid begin, so use the valid begin
                            STR_TO_DATE(providerrate_valid_period_begin, '%Y-%m-%d')
                        ,
                            -- period begin >= valid begin, so use the period begin
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_begin="", MONTH(in_datetime), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(in_datetime), providerrate_period_day_begin)), '%Y-%m-%d')
                        )
                    )
                AND
                    IF(providerrate_valid_period_end = "",
                        -- last forever
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_end="", MONTH(in_datetime), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(in_datetime), providerrate_period_day_end)), '%Y-%m-%d')
                    ,
                        -- get the valid end date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_end="", MONTH(in_datetime), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(in_datetime), providerrate_period_day_end)), '%Y-%m-%d') > providerrate_valid_period_end OR STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_end="", MONTH(in_datetime), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(in_datetime), providerrate_period_day_end)), '%Y-%m-%d') < providerrate_valid_period_begin,
                            -- period end > valid end or period end < begin end, so use the valid end
                            STR_TO_DATE(providerrate_valid_period_end, '%Y-%m-%d')
                        ,
                            -- period end <= valid end, so use the period end
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(providerrate_period_month_end="", MONTH(in_datetime), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(in_datetime), providerrate_period_day_end)), '%Y-%m-%d')
                        )
                    )
        )
    AND
        IF(IF(providerrate_period_time_begin="", time(in_datetime), providerrate_period_time_begin) > IF(providerrate_period_time_end="", time(in_datetime), providerrate_period_time_end),
            -- time begin > time end
            time(in_datetime) NOT
                BETWEEN
                    IF(providerrate_period_time_begin="", time(in_datetime), providerrate_period_time_begin)
                AND
                    IF(providerrate_period_time_end="", time(in_datetime), providerrate_period_time_end)
        ,
            -- time begin <= time end
            time(in_datetime)
                BETWEEN
                    IF(providerrate_period_time_begin="", time(in_datetime), providerrate_period_time_begin)
                AND
                    IF(providerrate_period_time_end="", time(in_datetime), providerrate_period_time_end)
        )
    ORDER BY
        providerrate_period_default DESC
    LIMIT 1;

END;
//
DELIMITER ;


-- determine the routing by specifying tariff id, country code, countryarea code
DROP PROCEDURE IF EXISTS ca_getRouting;
DELIMITER //
CREATE PROCEDURE ca_getRouting(IN in_datetime DATETIME, IN in_iar_id TEXT, IN in_country_code TEXT, IN in_countryarea_code TEXT, OUT out_routing_id INT, OUT out_routing_lcr TEXT)
BEGIN

    SET out_routing_id = 0;
    SET out_routing_lcr = "";

    -- get the routing
    SELECT 
        routing_id, routing_lcr INTO out_routing_id, out_routing_lcr
    FROM 
        ca_routing r 
        LEFT JOIN ca_routing_group_routing_relation rgrr ON r.routing_id = rgrr.rgrr_routing_id
        LEFT JOIN ca_routing_group_iddprefix_account_relation rgiar ON rgiar.rgiar_routing_group_id = rgrr.rgrr_routing_group_id
    WHERE 
        r.routing_country_code = in_country_code
    AND
        r.routing_countryarea_code = in_countryarea_code
    AND
        rgiar.rgiar_iar_id = in_iar_id
    AND
        in_datetime
            BETWEEN
                routing_valid_period_begin
            AND
                routing_valid_period_end
    AND
        (routing_period_weekday like CONCAT('%', WEEKDAY(in_datetime)+1, '%')
        OR  
        routing_period_weekday = '')
    AND
        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_begin="", MONTH(in_datetime), routing_period_month_begin), "-", IF(routing_period_day_begin="", DAY(in_datetime), routing_period_day_begin)), '%Y-%m-%d') > STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_end="", MONTH(in_datetime), routing_period_month_end), "-", IF(routing_period_day_end="", DAY(in_datetime), routing_period_day_end)), '%Y-%m-%d'), 
            -- begin > end
            date(in_datetime)
                BETWEEN
                    IF(routing_valid_period_begin = "", 
                        -- start from day 1
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_end="", MONTH(in_datetime), routing_period_month_end), "-", IF(routing_period_day_end="", DAY(in_datetime), routing_period_day_end)), '%Y-%m-%d')
                    ,
                        -- get the valid start date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_end="", MONTH(in_datetime), routing_period_month_end), "-", IF(routing_period_day_end="", DAY(in_datetime), routing_period_day_end)), '%Y-%m-%d') < routing_valid_period_begin, 
                            -- period end < valid end, so use the valid end
                            STR_TO_DATE(routing_valid_period_begin, '%Y-%m-%d')
                        ,
                            -- period end >= valid end, so use the period end
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_end="", MONTH(in_datetime), routing_period_month_end), "-", IF(routing_period_day_end="", DAY(in_datetime), routing_period_day_end)), '%Y-%m-%d')
                        )
                    )
                AND
                    IF(routing_valid_period_end = "", 
                        -- last forever
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_begin="", MONTH(in_datetime), routing_period_month_begin), "-", IF(routing_period_day_begin="", DAY(in_datetime), routing_period_day_begin)), '%Y-%m-%d')
                    ,
                        -- get the valid end date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_begin="", MONTH(in_datetime), routing_period_month_begin), "-", IF(routing_period_day_begin="", DAY(in_datetime), routing_period_day_begin)), '%Y-%m-%d') > routing_valid_period_begin OR STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_begin="", MONTH(in_datetime), routing_period_month_begin), "-", IF(routing_period_day_begin="", DAY(in_datetime), routing_period_day_begin)), '%Y-%m-%d') < routing_valid_period_end, 
                            -- period begin > valid end or period begin < valid end, so use the valid end
                            STR_TO_DATE(routing_valid_period_end, '%Y-%m-%d')
                        ,
                            -- period begin <= valid begin, so use the period begin
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_begin="", MONTH(in_datetime), routing_period_month_begin), "-", IF(routing_period_day_begin="", DAY(in_datetime), routing_period_day_begin)), '%Y-%m-%d')
                        )
                    )
        ,
            -- begin <= end
            date(in_datetime)
                BETWEEN
                    IF(routing_valid_period_begin = "",
                        -- start from day 1
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_begin="", MONTH(in_datetime), routing_period_month_begin), "-", IF(routing_period_day_begin="", DAY(in_datetime), routing_period_day_begin)), '%Y-%m-%d')
                    ,
                        -- get the valid start date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_begin="", MONTH(in_datetime), routing_period_month_begin), "-", IF(routing_period_day_begin="", DAY(in_datetime), routing_period_day_begin)), '%Y-%m-%d') < routing_valid_period_begin,
                            -- period begin < valid begin, so use the valid begin
                            STR_TO_DATE(routing_valid_period_begin, '%Y-%m-%d')
                        ,
                            -- period begin >= valid begin, so use the period begin
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_begin="", MONTH(in_datetime), routing_period_month_begin), "-", IF(routing_period_day_begin="", DAY(in_datetime), routing_period_day_begin)), '%Y-%m-%d')
                        )
                    )
                AND
                    IF(routing_valid_period_end = "",
                        -- last forever
                        STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_end="", MONTH(in_datetime), routing_period_month_end), "-", IF(routing_period_day_end="", DAY(in_datetime), routing_period_day_end)), '%Y-%m-%d')
                    ,
                        -- get the valid end date
                        IF(STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_end="", MONTH(in_datetime), routing_period_month_end), "-", IF(routing_period_day_end="", DAY(in_datetime), routing_period_day_end)), '%Y-%m-%d') > routing_valid_period_end OR STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_end="", MONTH(in_datetime), routing_period_month_end), "-", IF(routing_period_day_end="", DAY(in_datetime), routing_period_day_end)), '%Y-%m-%d') < routing_valid_period_begin,
                            -- period end > valid end or period end < begin end, so use the valid end
                            STR_TO_DATE(routing_valid_period_end, '%Y-%m-%d')
                        ,
                            -- period end <= valid end, so use the period end
                            STR_TO_DATE(CONCAT(YEAR(in_datetime), "-", IF(routing_period_month_end="", MONTH(in_datetime), routing_period_month_end), "-", IF(routing_period_day_end="", DAY(in_datetime), routing_period_day_end)), '%Y-%m-%d')
                        )
                    )
        )
    AND
        IF(IF(routing_period_time_begin="", time(in_datetime), routing_period_time_begin) > IF(routing_period_time_end="", time(in_datetime), routing_period_time_end),
            -- time begin > time end
            time(in_datetime) NOT
                BETWEEN
                    IF(routing_period_time_begin="", time(in_datetime), routing_period_time_begin)
                AND
                    IF(routing_period_time_end="", time(in_datetime), routing_period_time_end)
        ,
            -- time begin <= time end
            time(in_datetime)
                BETWEEN
                    IF(routing_period_time_begin="", time(in_datetime), routing_period_time_begin)
                AND
                    IF(routing_period_time_end="", time(in_datetime), routing_period_time_end)
        )
    LIMIT 1;

END;
//
DELIMITER ;


-- Stored Procedures --
DROP PROCEDURE IF EXISTS ca_checkCallSkipPIN;
DELIMITER //
CREATE PROCEDURE ca_checkCallSkipPIN(IN in_callerid TEXT, IN in_dialnum TEXT)
BEGIN
    -- declare variables
    DECLARE out_skip boolean;
    DECLARE out_msg TEXT;
    DECLARE cnt INT;

    -- initialize variables
    SET out_skip = false;

    -- check skip-pin-authentication list
    -- IF (out_skip = false) THEN
        -- SELECT COUNT(*) INTO cnt FROM sipfriends WHERE username = in_dialnum;
        -- IF (cnt > 0) THEN
            -- SET out_skip = true;
            -- SET out_msg = 'skip authenticate the dialed number';
        -- ELSE
            -- SET out_skip = false;
        -- END IF;
    -- END IF;
    
    -- check user settings
    IF (out_skip = false) THEN
        SELECT COUNT(*) INTO cnt FROM ca_useraccount WHERE useraccount_username = in_callerid AND (useraccount_validate_pin = '0' OR (useraccount_validate_pin = 'Profiled' AND (SELECT user_IDD_valid_PIN FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = useraccount_username limit 1) = '0')) AND (useraccount_status = '1' OR (useraccount_status = 'Profiled' AND (SELECT user_IDD_status FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = useraccount_username limit 1) = '1'));
        IF (cnt > 0) THEN
            SET out_skip = true;
            SET out_msg = 'user set to skip pin authentication';
        ELSE
            SET out_skip = false;
        END IF;
    END IF;
    
    -- check unlocked user
    -- IF (out_skip = false) THEN
        -- SELECT COUNT(*) INTO cnt FROM sipfriends WHERE username = in_callerid;
        -- IF (cnt > 0) THEN
            -- SET out_skip = true;
            -- SET out_msg = 'user has unlocked the account';
        -- ELSE
            -- SET out_skip = false;
        -- END IF;
    -- END IF;

    -- output the result
    SELECT out_skip, out_msg;
END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS ca_prepareCall;
DELIMITER //
CREATE PROCEDURE ca_prepareCall(IN in_billingAccount TEXT, IN in_username TEXT, IN in_dialednum TEXT, IN in_cid_num TEXT, IN in_cid_name TEXT)
BEGIN
    -- declare variable
    DECLARE out_status INT DEFAULT 1;
    DECLARE out_duration_limit TEXT DEFAULT "";
    DECLARE out_cid_num TEXT DEFAULT "";
    DECLARE out_cid_name TEXT DEFAULT "";
    DECLARE out_context TEXT DEFAULT "";
    DECLARE out_iddprefix TEXT DEFAULT "";
    DECLARE out_tariff_code TEXT DEFAULT "";
    DECLARE out_tariff_name TEXT DEFAULT "";
    DECLARE out_tariff_adjustment FLOAT DEFAULT 0;
    DECLARE out_dial_num TEXT DEFAULT "";
    DECLARE out_country_code TEXT DEFAULT "";
    DECLARE out_countryarea_code TEXT DEFAULT "";
    DECLARE out_calling_code TEXT DEFAULT "";
    DECLARE out_country_calling_code TEXT DEFAULT "";

    DECLARE out_provider_code TEXT DEFAULT "";
    DECLARE out_sippeer TEXT DEFAULT "";
    DECLARE out_dst_num TEXT DEFAULT "";

    DECLARE val_now datetime;
    DECLARE val_iar_id TEXT;
    DECLARE val_tariff_plan TEXT;
    DECLARE val_tariff_id TEXT;
    DECLARE val_tariffrate_id TEXT;
    DECLARE val_provider_id TEXT;
    DECLARE val_balance FLOAT;
    DECLARE val_threshold FLOAT;
    DECLARE val_remaining_threshold FLOAT;
    DECLARE val_routing_id TEXT;
    DECLARE val_lcr TEXT;
    DECLARE val_sippeer TEXT;

    DECLARE val_tr_rate FLOAT;
    DECLARE val_tr_rut FLOAT;
    DECLARE val_tr_b FLOAT;
    DECLARE val_tr_e FLOAT;
    DECLARE val_dl_nb FLOAT;
    DECLARE val_dl_cnt FLOAT;

    DECLARE cnt INT;
    DECLARE str1 TEXT;
    DECLARE str2 TEXT;
    DECLARE str3 TEXT;

    -- cursor
    DECLARE done INT DEFAULT 0;
    -- fetch the provider under the routing, LCR disabled
    DECLARE cur_provider CURSOR FOR
        SELECT 
            rpr_provider_id
        FROM 
            ca_routing_provider_relation rpr INNER JOIN ca_providerrate pr ON rpr.rpr_provider_id = pr.providerrate_provider_id
        WHERE 
            rpr_routing_id = val_routing_id
        AND
            providerrate_country_code = out_country_code
        AND
            providerrate_countryarea_code = out_countryarea_code
        AND
            val_now
                BETWEEN
                    providerrate_valid_period_begin
                AND
                    providerrate_valid_period_end
        AND
            (providerrate_period_weekday like CONCAT('%', WEEKDAY(val_now)+1, '%')
            OR  
            providerrate_period_weekday = '')
        AND
            IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d') > STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d'), 
                -- begin > end
                date(val_now)
                    BETWEEN
                        IF(providerrate_valid_period_begin = "",
                            -- start from day 1
                            STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d')
                        ,
                            -- get the valid start date
                            IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d') < providerrate_valid_period_begin,
                                -- period end < valid end, so use the valid end
                                providerrate_valid_period_begin
                            ,
                                -- period end >= valid end, so use the period end
                                STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d')
                            )
                        )
                    AND
                        IF(providerrate_valid_period_end = "",
                            -- last forever
                            STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d')
                        ,
                            -- get the valid end date
                            IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d') > providerrate_valid_period_begin OR STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d') < providerrate_valid_period_end,
                                -- period begin > valid end or period begin < valid end, so use the valid end
                                providerrate_valid_period_end
                            ,
                                -- period begin <= valid begin, so use the period begin
                                STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d')
                            )
                        )
            ,
                -- begin <= end
                date(val_now)
                    BETWEEN
                        IF(providerrate_valid_period_begin = "",
                            -- start from day 1
                            STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d')
                        ,
                            -- get the valid start date
                            IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d') < providerrate_valid_period_begin,
                                -- period begin < valid begin, so use the valid begin
                                providerrate_valid_period_begin
                            ,
                                -- period begin >= valid begin, so use the period begin
                                STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d')
                            )
                        )
                    AND
                        IF(providerrate_valid_period_end = "",
                            -- last forever
                            STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d')
                        ,
                            -- get the valid end date
                            IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d') > providerrate_valid_period_end OR STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d') < providerrate_valid_period_begin,
                                -- period end > valid end or period end < begin end, so use the valid end
                                providerrate_valid_period_end
                            ,
                                -- period end <= valid end, so use the period end
                                STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d')
                            )
                        )
            )
        AND
            IF(IF(providerrate_period_time_begin="", time(val_now), providerrate_period_time_begin) > IF(providerrate_period_time_end="", time(val_now), providerrate_period_time_end),
                -- time begin > time end
                time(val_now) NOT
                    BETWEEN
                        IF(providerrate_period_time_begin="", time(val_now), providerrate_period_time_begin)
                    AND
                        IF(providerrate_period_time_end="", time(val_now), providerrate_period_time_end)
            ,
                -- time begin <= time end
                time(val_now)
                    BETWEEN
                        IF(providerrate_period_time_begin="", time(val_now), providerrate_period_time_begin)
                    AND
                        IF(providerrate_period_time_end="", time(val_now), providerrate_period_time_end)
            )
        ORDER BY 
            rpr_id;

    -- fetch the provider under the routing, LCR enabled
    DECLARE cur_provider_lcr CURSOR FOR
        SELECT 
            DISTINCT providerrate_provider_id
        FROM
            ca_providerrate_info pri INNER JOIN 
            (
                SELECT
                    providerrate_id, providerrate_provider_id
                FROM 
                    ca_providerrate r INNER JOIN ca_routing_provider_relation rpr ON r.providerrate_provider_id = rpr.rpr_provider_id
                WHERE
                    rpr.rpr_routing_id = val_routing_id
                AND
                    providerrate_country_code = out_country_code
                AND
                    providerrate_countryarea_code = out_countryarea_code
                AND
                    val_now
                        BETWEEN
                            providerrate_valid_period_begin
                        AND
                            providerrate_valid_period_end
                AND
                    (providerrate_period_weekday like CONCAT('%', WEEKDAY(val_now)+1, '%')
                    OR  
                    providerrate_period_weekday = '')
                AND
                    IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d') > STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d'), 
                        -- begin > end
                        date(val_now)
                            BETWEEN
                                IF(providerrate_valid_period_begin = "",
                                    -- start from day 1
                                    STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d')
                                ,
                                    -- get the valid start date
                                    IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d') < providerrate_valid_period_begin,
                                        -- period end < valid end, so use the valid end
                                        providerrate_valid_period_begin
                                    ,
                                        -- period end >= valid end, so use the period end
                                        STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d')
                                    )
                                )
                            AND
                                IF(providerrate_valid_period_end = "",
                                    -- last forever
                                    STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d')
                                ,
                                    -- get the valid end date
                                    IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d') > providerrate_valid_period_begin OR STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d') < providerrate_valid_period_end,
                                        -- period begin > valid end or period begin < valid end, so use the valid end
                                        providerrate_valid_period_end
                                    ,
                                        -- period begin <= valid begin, so use the period begin
                                        STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d')
                                    )
                                )
                    ,
                        -- begin <= end
                        date(val_now)
                            BETWEEN
                                IF(providerrate_valid_period_begin = "",
                                    -- start from day 1
                                    STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d')
                                ,
                                    -- get the valid start date
                                    IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d') < providerrate_valid_period_begin,
                                        -- period begin < valid begin, so use the valid begin
                                        providerrate_valid_period_begin
                                    ,
                                        -- period begin >= valid begin, so use the period begin
                                        STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_begin="", MONTH(val_now), providerrate_period_month_begin), "-", IF(providerrate_period_day_begin="", DAY(val_now), providerrate_period_day_begin)), '%Y-%m-%d')
                                    )
                                )
                            AND
                                IF(providerrate_valid_period_end = "",
                                    -- last forever
                                    STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d')
                                ,
                                    -- get the valid end date
                                    IF(STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d') > providerrate_valid_period_end OR STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d') < providerrate_valid_period_begin,
                                        -- period end > valid end or period end < begin end, so use the valid end
                                        providerrate_valid_period_end
                                    ,
                                        -- period end <= valid end, so use the period end
                                        STR_TO_DATE(CONCAT(YEAR(val_now), "-", IF(providerrate_period_month_end="", MONTH(val_now), providerrate_period_month_end), "-", IF(providerrate_period_day_end="", DAY(val_now), providerrate_period_day_end)), '%Y-%m-%d')
                                    )
                                )
                    )
                AND
                    IF(IF(providerrate_period_time_begin="", time(val_now), providerrate_period_time_begin) > IF(providerrate_period_time_end="", time(val_now), providerrate_period_time_end),
                        -- time begin > time end
                        time(val_now) NOT
                            BETWEEN
                                IF(providerrate_period_time_begin="", time(val_now), providerrate_period_time_begin)
                            AND
                                IF(providerrate_period_time_end="", time(val_now), providerrate_period_time_end)
                    ,
                        -- time begin <= time end
                        time(val_now)
                            BETWEEN
                                IF(providerrate_period_time_begin="", time(val_now), providerrate_period_time_begin)
                            AND
                                IF(providerrate_period_time_end="", time(val_now), providerrate_period_time_end)
                    )
                ORDER BY
                    providerrate_period_default DESC
            ) AS pr
            ON pri.providerrate_info_providerrate_id = pr.providerrate_id
        WHERE 
            providerrate_info_begin = 0
        ORDER BY
            providerrate_info_rate / providerrate_info_rounduptime ASC;

    DECLARE cur_dur_limit CURSOR FOR
        SELECT 
            tariffrate_info_rate, tariffrate_info_rounduptime, tariffrate_info_begin, tariffrate_info_end
        FROM
            ca_tariffrate_info
        WHERE
            tariffrate_info_tariffrate_id = val_tariffrate_id
        ORDER BY
            tariffrate_info_begin;

    DECLARE cur_sippeer_group CURSOR FOR
        SELECT 
            s.peerID
        FROM 
            sippeer_group_sippeers_relation sgsr LEFT JOIN sippeers s ON sgsr.sippeer_id = s.peerID 
        WHERE 
            sgsr.group_name = val_sippeer 
        ORDER BY 
            sgsr.priority;


    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;


    -- initialize variable
    SET val_now = NOW();
    
    -- default callerID and context
    SELECT 
        IF(context='Profiled', (SELECT user_context FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = in_username limit 1), context) AS context
    INTO 
        out_context 
    FROM 
        sipfriends 
    WHERE 
        username = in_username
    LIMIT 2;

    -- get the IDD prefix, tariff id, number to dial
    SET out_iddprefix = "";
    SELECT 
        iddprefix_prefix, iar_id, iar_tariff_id, iar_percentage, billingaccount_balance, billingaccount_threshold, billingaccount_dl_remaining_threshold INTO out_iddprefix, val_iar_id, val_tariff_id, out_tariff_adjustment, val_balance, val_threshold, val_remaining_threshold
    FROM 
        ca_iddprefix_account_relation iar INNER JOIN ca_billingaccount b ON iar.iar_billingaccount_id = billingaccount_id
        INNER JOIN ca_iddprefix i ON iar.iar_iddprefix_id = i.iddprefix_id
    WHERE 
        billingaccount_code = in_billingAccount
    AND
        billingaccount_status = '1'
    AND
        iddprefix_prefix = SUBSTR(in_dialednum, 1, LENGTH(iddprefix_prefix)) 
    ORDER BY 
        LENGTH(iddprefix_prefix) DESC 
    LIMIT 1;
    SET out_dial_num = SUBSTR(in_dialednum, LENGTH(out_iddprefix)+1);

    -- get the country code and area code
    SET str1 = "";
    SELECT 
        DISTINCT c.country_code, a.countryarea_code, CONCAT(c.country_calling_code, a.countryarea_calling_code) as code, c.country_calling_code INTO out_country_code, out_countryarea_code, out_calling_code, out_country_calling_code
    FROM 
        ca_country c LEFT JOIN ca_countryarea a ON c.country_id = a.countryarea_country_id 
    WHERE 
        SUBSTR(c.country_calling_code, 1, 1) = SUBSTR(out_dial_num, 1, 1)
    HAVING 
        code = SUBSTR(out_dial_num, 1, LENGTH(code)) 
    ORDER BY 
        LENGTH(code) DESC 
    LIMIT 1;


    -- tariff plan enabled?
    SELECT value INTO val_tariff_plan FROM systemConfigs WHERE `key` = 'tariffplan';

    IF val_tariff_plan != "1" THEN

        -- tariff plan disabled
        SET val_tariff_id = NULL;
        SET out_tariff_code = "DISABLED";
        SET out_tariff_name = "DISABLED";
        SET out_tariff_adjustment = "100";
        SET out_duration_limit = -1;
    
    ELSE
    
        -- tariff plan enabled
        -- get the tariff code
        SELECT tariff_code, tariff_name INTO out_tariff_code, out_tariff_name FROM ca_tariff WHERE tariff_id = val_tariff_id;

        -- determine call duration limit
        IF val_remaining_threshold < 0 THEN

            SET out_duration_limit = -1;

        ELSE 

            -- get the tariff rate
            call ca_getTariffrate(val_now, out_tariff_code, out_country_code, out_countryarea_code, val_tariffrate_id);

            IF val_tariffrate_id = 0 THEN

                SET val_tariff_id = NULL;
                SET out_tariff_code = "";
                SET out_tariff_name = "";
                SET out_tariff_adjustment = "";
                SET out_duration_limit = 0;

            ELSE

                SET str1 = 0;
                SET str2 = 0;
                SET cnt = 0 ;
                SET out_duration_limit = 0;
                SET val_balance = val_balance - val_threshold;

                SET done = 0;
                OPEN cur_dur_limit;
                loop1: LOOP
                    FETCH cur_dur_limit INTO val_tr_rate, val_tr_rut, val_tr_b, val_tr_e;
                    IF done THEN LEAVE loop1; END IF;
                    IF val_balance <= 0 THEN LEAVE loop1; END IF;
                    IF out_duration_limit > 86400 THEN LEAVE loop1; END IF;
                    IF val_tr_rate <= 0 THEN 
                        SET out_duration_limit = -1;
                        LEAVE loop1;
                    END IF;
                    IF val_tr_e = -1 THEN
                        SET out_duration_limit = out_duration_limit + FLOOR(val_balance / val_tr_rate) * val_tr_rut;
                        LEAVE loop1;
                    ELSE
                        SET val_dl_nb = (val_tr_e - val_tr_b) / val_tr_rut;
                        SET val_dl_cnt = IF(FLOOR(val_balance / val_tr_rate)>=val_dl_nb, val_dl_nb, FLOOR(val_balance / val_tr_rate));
                        SET val_balance = val_balance - val_dl_cnt * val_tr_rate;
                        SET out_duration_limit = out_duration_limit + val_dl_cnt * val_tr_rut;
                    END IF;
                    SET done = 0;
                END LOOP loop1;
                CLOSE cur_dur_limit;

            END IF;

        END IF;
    
    END IF;

    IF out_duration_limit > 86400 THEN 
        SET out_duration_limit = 86400; 
    END IF;

    -- check the status
    IF 
        out_duration_limit = "" OR
        -- out_cid_num = "" OR
        -- out_cid_name = "" OR
        -- out_context = "" OR
        out_iddprefix = "" OR
        out_tariff_code = "" OR
        out_tariff_name = "" OR
        out_tariff_adjustment < 0 OR
        out_dial_num = "" OR
        out_country_code = "" OR
        out_countryarea_code = "" OR
        out_calling_code = ""
    THEN
        SET out_status = 0;
    END IF;

    -- output the result
    SELECT out_status, out_duration_limit, out_context, out_iddprefix, out_tariff_code, out_tariff_name, out_tariff_adjustment, out_dial_num, out_country_code, out_countryarea_code, out_calling_code;


    -- determine the routing
    call ca_getRouting(val_now, val_iar_id, out_country_code, out_countryarea_code, val_routing_id, val_lcr);

    IF val_lcr = "1" THEN

        SET done = 0;
        OPEN cur_provider_lcr;
        column_loop1: LOOP
            FETCH cur_provider_lcr INTO val_provider_id;
            IF done THEN LEAVE column_loop1; END IF;

            SET val_sippeer = "";
            SELECT 
                provider_code, provider_sippeer, CONCAT(provider_prefix, IF(provider_strip="2", SUBSTR(out_dial_num, LENGTH(out_country_calling_code)+1), IF(provider_strip="1", SUBSTR(out_dial_num, LENGTH(out_calling_code)+1), out_dial_num))) INTO out_provider_code, val_sippeer, out_dst_num 
            FROM 
                ca_provider 
            WHERE 
                provider_id = val_provider_id;

            SET out_sippeer = "";
            SELECT 
                p.provider_sippeer INTO out_sippeer 
            FROM 
                ca_provider p INNER JOIN sippeers s ON p.provider_sippeer=s.peerID 
            WHERE 
                p.provider_sippeer=val_sippeer 
            LIMIT 1;
            IF out_sippeer = "" THEN
                SET done = 0;
                OPEN cur_sippeer_group;
                loop1: LOOP
                    FETCH cur_sippeer_group INTO out_sippeer;
                    IF done THEN LEAVE loop1; select val_sippeer, out_sippeer; END IF;

                    call ca_resolveCallerId(in_username, out_sippeer, in_cid_num, in_cid_name, out_cid_num, out_cid_name);
                    SELECT out_provider_code, out_sippeer, out_dst_num, out_cid_num, out_cid_name;
                END LOOP loop1;
                CLOSE cur_sippeer_group;
            ELSE
                call ca_resolveCallerId(in_username, val_sippeer, in_cid_num, in_cid_name, out_cid_num, out_cid_name);
                SELECT out_provider_code, out_sippeer, out_dst_num, out_cid_num, out_cid_name;
            END IF;
            SET done = 0;
        END LOOP column_loop1;
        CLOSE cur_provider_lcr;

    ELSE

        SET done = 0;
        OPEN cur_provider;
        column_loop1: LOOP
            FETCH cur_provider INTO val_provider_id;
            IF done THEN LEAVE column_loop1; END IF;

            SET val_sippeer = "";
            SELECT 
                provider_code, provider_sippeer, CONCAT(provider_prefix, IF(provider_strip="2", SUBSTR(out_dial_num, LENGTH(out_country_calling_code)+1), IF(provider_strip="1", SUBSTR(out_dial_num, LENGTH(out_calling_code)+1), out_dial_num))) INTO out_provider_code, val_sippeer, out_dst_num 
            FROM 
                ca_provider 
            WHERE 
                provider_id = val_provider_id;

            SET out_sippeer = "";
            SELECT 
                p.provider_sippeer INTO out_sippeer 
            FROM 
                ca_provider p INNER JOIN sippeers s ON p.provider_sippeer=s.peerID 
            WHERE 
                p.provider_sippeer=val_sippeer 
            LIMIT 1;
            IF out_sippeer = "" THEN
                SET done = 0;
                OPEN cur_sippeer_group;
                loop1: LOOP
                    FETCH cur_sippeer_group INTO out_sippeer;
                    IF done THEN LEAVE loop1; select val_sippeer, out_sippeer; END IF;

                    call ca_resolveCallerId(in_username, out_sippeer, in_cid_num, in_cid_name, out_cid_num, out_cid_name);
                    SELECT out_provider_code, out_sippeer, out_dst_num, out_cid_num, out_cid_name;
                END LOOP loop1;
                CLOSE cur_sippeer_group;
            ELSE
                call ca_resolveCallerId(in_username, val_sippeer, in_cid_num, in_cid_name, out_cid_num, out_cid_name);
                SELECT out_provider_code, out_sippeer, out_dst_num, out_cid_num, out_cid_name;
            END IF;
            SET done = 0;
        END LOOP column_loop1;
        CLOSE cur_provider;

    END IF;

END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS ca_startCall;
DELIMITER //
CREATE PROCEDURE ca_startCall(
    IN in_starttime TEXT,
    IN in_src_num TEXT,
    IN in_src_name TEXT,
    IN in_dst_num TEXT,
    IN in_country_code TEXT,
    IN in_countryarea_code TEXT,
    IN in_useraccount_code TEXT,
    IN in_useraccount_name TEXT,
    IN in_billingaccount_code TEXT,
    IN in_billingaccount_name TEXT,
    IN in_billingaccount_type TEXT,
    IN in_tariff_code TEXT,
    IN in_tariff_name TEXT,
    IN in_tariff_adjustment TEXT,
    IN in_provider_code TEXT,
    IN in_provider_name TEXT,
    IN in_uniqueid TEXT,
    IN in_channel TEXT,
    IN in_iddprefix TEXT,
    IN in_calling_code TEXT,
    IN in_dialed_num TEXT,
    IN in_sippeer TEXT,
    IN in_nasipaddress TEXT)
BEGIN

    DECLARE val_cnt INT;
    DECLARE val_id INT;
    DECLARE out_status BOOLEAN DEFAULT TRUE;

    DECLARE CONTINUE HANDLER FOR SQLWARNING SET out_status = FALSE;

    INSERT INTO ca_call (
        starttime, 
        src_num, 
        src_name, 
        dst_num, 
        country_code, 
        countryarea_code, 
        useraccount_code, 
        useraccount_name, 
        billingaccount_code, 
        billingaccount_name, 
        billingaccount_type, 
        tariff_code,
        tariff_name,
        tariff_adjustment,
        provider_code,
        provider_name,
        uniqueid,
        channel,
        iddprefix,
        calling_code,
        dialed_num,
        sippeer,
        nasipaddress,
        status
    ) VALUES (
        in_starttime,
        in_src_num,
        in_src_name,
        in_dst_num,
        in_country_code,
        in_countryarea_code,
        in_useraccount_code,
        in_useraccount_name,
        in_billingaccount_code,
        in_billingaccount_name,
        in_billingaccount_type,
        in_tariff_code,
        in_tariff_name,
        in_tariff_adjustment,
        in_provider_code,
        in_provider_name,
        in_uniqueid,
        in_channel,
        in_iddprefix,
        in_calling_code,
        in_dialed_num,
        in_sippeer,
        in_nasipaddress,
        '0'
    );
    SELECT id INTO val_id FROM ca_call WHERE uniqueid = in_uniqueid ORDER BY id DESC LIMIT 1;

    SELECT out_status, val_id;

END;
//
DELIMITER ;


DROP PROCEDURE IF EXISTS ca_stopCall;
DELIMITER //
CREATE PROCEDURE ca_stopCall(IN in_id TEXT, IN in_stoptime TEXT, IN in_duration FLOAT, IN in_terminatecause TEXT)
BEGIN

    DECLARE out_status BOOLEAN DEFAULT TRUE;
    DECLARE val_cnt INT;
    DECLARE val_tariff_plan INT DEFAULT 0;
    DECLARE val_status INT DEFAULT 0;
    DECLARE val_starttime datetime;
    DECLARE val_tariff_code TEXT;
    DECLARE val_iddprefix TEXT;
    DECLARE val_provider_code TEXT;
    DECLARE val_country_code TEXT;
    DECLARE val_countryarea_code TEXT;
    DECLARE val_billingaccount_code TEXT;
    DECLARE val_billingaccount_type TEXT;
    DECLARE val_tariff_adjustment FLOAT DEFAULT 1;
    DECLARE val_tariffrate_id TEXT DEFAULT "";
    DECLARE val_providerrate_id TEXT DEFAULT "";
    DECLARE val_bill FLOAT DEFAULT 0;
    DECLARE val_cost FLOAT DEFAULT 0;
    DECLARE val_ba_balance FLOAT;
    DECLARE val_amount FLOAT DEFAULT 0;
    DECLARE val_ba_type TEXT;
    DECLARE val_ba_threshold FLOAT;
    DECLARE val_ba_status TEXT;
    DECLARE val_transaction_id INT;

    DECLARE CONTINUE HANDLER FOR SQLWARNING SET out_status = FALSE;

    SET val_bill = 0;
    SET val_cost = 0;
    SET val_status = 0;

    IF in_duration = 0 THEN

        IF in_terminatecause = "Answer" THEN

            -- since the call is answered, it must not have the duration equals to zero
            SET in_duration = 1;

        END IF;

    ELSE 

        IF in_terminatecause != "Answer" THEN

            -- since the call is not answered, the duration must be zero
            SET in_duration = 0;

        END IF;

    END IF;

    IF in_duration = 0 THEN

        UPDATE ca_call
        SET
            stoptime = in_stoptime,
            duration = in_duration,
            terminatecause = in_terminatecause,
            bill = 0,
            cost = 0,
            status = 1
        WHERE
            id = in_id;

        SET out_status = TRUE;

    ELSE 

        SELECT COUNT(id) INTO val_cnt FROM ca_call WHERE id = in_id;
        IF val_cnt = 1 THEN

            -- get back the call details
            SELECT 
                starttime, tariff_code, tariff_adjustment, iddprefix, provider_code, country_code, countryarea_code, billingaccount_code
            INTO
                val_starttime, val_tariff_code, val_tariff_adjustment, val_iddprefix, val_provider_code, val_country_code, val_countryarea_code, val_billingaccount_code
            FROM 
                ca_call 
            WHERE 
                id = in_id 
            LIMIT 1;

            -- calculate bill
            -- tariff plan enabled?
            SELECT value INTO val_tariff_plan FROM systemConfigs WHERE `key` = 'tariffplan';

            IF val_tariff_plan != "1" THEN

                SET val_bill = 0;
            
            ELSE 

                -- get the tariff rate id
                call ca_getTariffrate(val_starttime, val_tariff_code, val_country_code, val_countryarea_code, val_tariffrate_id);

                IF val_tariffrate_id = 0 THEN
                    -- cannot bill the call
                    SET val_bill = 0;
                    SET val_status = -1;
                ELSE
                    -- bill the call
                    SELECT
                        SUM(
                            IF(tariffrate_info_end>tariffrate_info_begin, 
                                tariffrate_info_rate * ceil((least(in_duration, tariffrate_info_end) - tariffrate_info_begin) / tariffrate_info_rounduptime),
                                tariffrate_info_rate * ceil((in_duration - tariffrate_info_begin) / tariffrate_info_rounduptime)
                            ) 
                        ) INTO val_bill
                    FROM
                        ca_tariffrate_info
                    WHERE
                        tariffrate_info_tariffrate_id = val_tariffrate_id
                    AND
                        in_duration > tariffrate_info_begin;

                    -- perform the tariff adjustment
                    SET val_bill = val_bill * val_tariff_adjustment / 100;
                    SET val_status = 1;

                END IF;

            END IF;

            -- calculate cost
            -- get the provider rate id
            call ca_getProviderrate(val_starttime, val_provider_code, val_country_code, val_countryarea_code, val_providerrate_id);

            IF val_providerrate_id = 0 THEN
                -- cannot calculate the cost
                SET val_cost = 0;
                SET val_status = -1;
            ELSE
                -- calculate the cost
                SELECT
                    SUM(
                        IF(providerrate_info_end>providerrate_info_begin, 
                            providerrate_info_rate * ceil((least(in_duration, providerrate_info_end) - providerrate_info_begin) / providerrate_info_rounduptime),
                            providerrate_info_rate * ceil((in_duration - providerrate_info_begin) / providerrate_info_rounduptime)
                        ) 
                    ) INTO val_cost
                FROM
                    ca_providerrate_info
                WHERE
                    providerrate_info_providerrate_id = val_providerrate_id
                AND
                    in_duration > providerrate_info_begin;

                SET val_status = 1;
            END IF;

            -- update the call record
            UPDATE ca_call
            SET
                stoptime = in_stoptime,
                duration = in_duration,
                terminatecause = in_terminatecause,
                bill = val_bill,
                cost = val_cost,
                status = val_status
            WHERE
                id = in_id;

            IF val_status < 0 THEN
                SET out_status = FALSE;
            END IF;
        ELSE

            SET out_status = FALSE;
        
        END IF;

    END IF;

    -- update the balance if the billing account is Prepaid'
    SELECT 
        billingaccount_balance, billingaccount_type, billingaccount_threshold, billingaccount_status INTO val_ba_balance, val_ba_type, val_ba_threshold, val_ba_status
    FROM
        ca_billingaccount
    WHERE
        billingaccount_code = val_billingaccount_code;

    IF val_ba_type = "Prepaid" THEN
        SET val_ba_balance = val_ba_balance - val_bill;

        IF val_ba_balance <= val_ba_threshold THEN
            SET val_ba_status = "0";
        ELSE
            SET val_ba_status = "1";
        END IF;

        -- record the transaction
        -- check the call_id
        SELECT id, `amount` INTO val_transaction_id, val_amount FROM ca_transaction WHERE call_id = in_id  ORDER BY id DESC LIMIT 1;
        IF val_transaction_id > 0 THEN
            -- if got, revert the previous one
            -- read the balance again, try best to avoid getting the wrong balance due to concurrent call
            SELECT
                billingaccount_balance INTO val_ba_balance
            FROM
                ca_billingaccount
            WHERE
                billingaccount_code = val_billingaccount_code;

            -- revert the account balance first
            UPDATE
                ca_billingaccount
            SET
                billingaccount_balance = billingaccount_balance - val_amount
            WHERE
                billingaccount_code = val_billingaccount_code;

            -- then the transaction record
            INSERT INTO ca_transaction
            (`id`, `time`, `billingaccount_type`, `billingaccount_code`, `call_id`, `amount`, `description`, `username`, `balance_remain_ref`)
            (SELECT '', now(), `billingaccount_type`, `billingaccount_code`, `call_id`, val_amount*-1.0, `description`, `username`, val_ba_balance-val_amount FROM ca_transaction WHERE id = val_transaction_id);
        END IF;

        -- then do the new record update
        -- read the balance again, try best to avoid getting the wrong balance due to concurrent call
        SELECT
            billingaccount_balance INTO val_ba_balance
        FROM
            ca_billingaccount
        WHERE
            billingaccount_code = val_billingaccount_code;
        -- account balance first
        UPDATE
            ca_billingaccount
        SET
            billingaccount_balance = billingaccount_balance - val_bill,
            billingaccount_status = val_ba_status
        WHERE
            billingaccount_code = val_billingaccount_code;

        -- then create a new transaction
        INSERT INTO ca_transaction
            (`id`, `time`, `billingaccount_type`, `billingaccount_code`, `call_id`, `amount`, `description`, `username`, `balance_remain_ref`)
        VALUES
            ('', now(), 'Prepaid', val_billingaccount_code, in_id, val_bill*-1, 'Normal Call Billing', '--System--', val_ba_balance - val_bill);


    END IF;

    SELECT out_status;

END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS ca_resolveCallerId;
delimiter //
CREATE PROCEDURE ca_resolveCallerId(IN in_username TEXT, IN in_peerId TEXT, IN in_cid_num TEXT, IN in_cid_name TEXT, OUT out_cid_num TEXT, OUT out_cid_name TEXT)
BEGIN

    -- declare variables

    DECLARE str1 TEXT;
    DECLARE str2 TEXT;
    DECLARE str3 TEXT;

    DECLARE done INT DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

    -- default callerID and context
    SELECT 
        username, name
    INTO 
        out_cid_num, out_cid_name
    FROM 
        sipfriends 
    WHERE 
        username = in_username
    LIMIT 2;

    -- get callerID number and name
    SET str1 = "";
    SET str2 = "";
    SET str3 = "";
    SELECT 
        IF(useraccount_callerID='Profiled', (SELECT user_IDD_callerid FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = useraccount_username limit 1), useraccount_callerID) AS useraccount_callerID,
        IF(useraccount_callerID='Profiled', (SELECT user_IDD_callerid_custom_number FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = useraccount_username limit 1), useraccount_custom_callerID_num) AS useraccount_custom_callerID_num,
        IF(useraccount_callerID='Profiled', (SELECT user_IDD_callerid_custom_name FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = useraccount_username limit 1), useraccount_custom_callerID_name) AS useraccount_custom_callerID_name
    INTO 
        str1, str2, str3 
    FROM 
        ca_useraccount 
    WHERE 
        useraccount_username = in_username 
    AND
        IF(useraccount_status='Profiled', (SELECT user_IDD_status FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = useraccount_username limit 1), useraccount_status) = '1'
    LIMIT 1;
    CASE str1
        WHEN "autoresolve" THEN
            SET str1 = "";
            SET str2 = 0;
            SET done = 0;
            SELECT CONCAT(callerid_prepend, SUBSTR(in_username, callerid_strip+1)) INTO out_cid_num FROM users_callerid_manipulation_relation r LEFT JOIN callerid_manipulation c ON r.manipulation_id = c.id LEFT JOIN sippeers_callerid_manipulation_relation s ON s.manipulation_id = c.id WHERE peerID = in_peerId AND username = in_username;
            IF done THEN
                SET done = 0;
                SELECT CONCAT(callerid_prepend, SUBSTR(in_username, callerid_strip+1)) INTO out_cid_num FROM sippeers_callerid_manipulation_relation r LEFT JOIN callerid_manipulation c ON r.manipulation_id = c.id WHERE peerID = in_peerId AND internal_exten_range_begin <= CAST(in_username AS UNSIGNED) AND internal_exten_range_end >= CAST(in_username AS UNSIGNED);
                IF done THEN
                    SELECT value INTO out_cid_num FROM systemConfigs WHERE `key` = 'defaultcallerid_num' LIMIT 1;
                    SELECT value INTO out_cid_name FROM systemConfigs WHERE `key` = 'defaultcallerid_name' LIMIT 1;
                END IF;
            END IF;
        WHEN "defaultcallerid" THEN
            SELECT value INTO out_cid_num FROM systemConfigs WHERE `key` = 'defaultcallerid_num' LIMIT 1;
            SELECT value INTO out_cid_name FROM systemConfigs WHERE `key` = 'defaultcallerid_name' LIMIT 1;
        WHEN "nonecallerid" THEN
            SET out_cid_num = "";
            SET out_cid_name = "";
        WHEN "customcallerid" THEN
            SET out_cid_num = str2;
            SET out_cid_name = str3;
        WHEN "originalcallerid" THEN
            SET str1 = in_cid_num + 0;
            SET str2 = 0;
            SET done = 0;
            IF in_cid_num = str1 THEN
                SELECT CONCAT(callerid_prepend, SUBSTR(in_cid_num, callerid_strip+1)) INTO out_cid_num FROM users_callerid_manipulation_relation r LEFT JOIN callerid_manipulation c ON r.manipulation_id = c.id LEFT JOIN sippeers_callerid_manipulation_relation s ON s.manipulation_id = c.id WHERE peerID = in_peerId AND username = in_cid_num;
                IF done THEN
                    SET done = 0;
                    SELECT CONCAT(callerid_prepend, SUBSTR(in_cid_num, callerid_strip+1)) INTO out_cid_num FROM sippeers_callerid_manipulation_relation r LEFT JOIN callerid_manipulation c ON r.manipulation_id = c.id WHERE peerID = in_peerId AND internal_exten_range_begin <= CAST(in_cid_num AS UNSIGNED) AND internal_exten_range_end >= CAST(in_cid_num AS UNSIGNED);
                    IF done THEN
                        SET out_cid_num = in_cid_num;
                        SET out_cid_name = in_cid_name;
                    ELSE
                        SELECT name INTO out_cid_name FROM sipfriends WHERE username = in_cid_num LIMIT 1;
                    END IF;
                ELSE
                    SELECT name INTO out_cid_name FROM sipfriends WHERE username = in_cid_num LIMIT 1;
                END IF;
            ELSE
                SET out_cid_num = in_cid_num;
                SET out_cid_name = in_cid_name;
            END IF;
        ELSE
            SET str1 = "";
            SET str2 = 0;
            SET done = 0;
            SELECT CONCAT(callerid_prepend, SUBSTR(in_username, callerid_strip+1)) INTO out_cid_num FROM users_callerid_manipulation_relation r LEFT JOIN callerid_manipulation c ON r.manipulation_id = c.id LEFT JOIN sippeers_callerid_manipulation_relation s ON s.manipulation_id = c.id WHERE peerID = in_peerId AND username = in_username;
            IF done THEN
                SET done = 0;
                SELECT CONCAT(callerid_prepend, SUBSTR(in_username, callerid_strip+1)) INTO out_cid_num FROM sippeers_callerid_manipulation_relation r LEFT JOIN callerid_manipulation c ON r.manipulation_id = c.id WHERE peerID = in_peerId AND internal_exten_range_begin <= CAST(in_username AS UNSIGNED) AND internal_exten_range_end >= CAST(in_username AS UNSIGNED);
                IF done THEN
                    SELECT value INTO out_cid_num FROM systemConfigs WHERE `key` = 'defaultcallerid_num' LIMIT 1;
                    SELECT value INTO out_cid_name FROM systemConfigs WHERE `key` = 'defaultcallerid_name' LIMIT 1;
                END IF;
            END IF;
    END CASE;

END//
delimiter ;

DROP PROCEDURE IF EXISTS disa2_getUserAccount;
DELIMITER //
CREATE PROCEDURE disa2_getUserAccount(IN in_username TEXT, IN in_pincode TEXT)
BEGIN

    IF in_username = "" THEN

        -- get all the user account
        SELECT
            a.username, u.fullname,
            IF(u.context='Profiled', (SELECT user_context FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = a.username limit 1), u.context) AS context,
            IF(a.usepincode='Profiled', (SELECT user_disa_usePIN FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = a.username limit 1), a.usepincode) AS usepincode,
            IF(a.status='Profiled', (SELECT user_disa_status FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = a.username limit 1), a.status) AS status
        FROM
            account a INNER JOIN users u ON a.username = u.mailbox 
        ORDER BY 
            username;

    ELSE
        
        IF in_pincode = "" THEN
            -- get specific user account
            SELECT
                a.username, u.fullname,
                IF(u.context='Profiled', (SELECT user_context FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = a.username limit 1), u.context) AS context,
                IF(a.usepincode='Profiled', (SELECT user_disa_usePIN FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = a.username limit 1), a.usepincode) AS usepincode,
                IF(a.status='Profiled', (SELECT user_disa_status FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = a.username limit 1), a.status) AS status
            FROM
                account a INNER JOIN users u ON a.username = u.mailbox 
            WHERE 
                a.username = in_username
            ORDER BY 
                username;

        ELSE
            -- get specific user account wiith pincode checking
            SELECT
                a.username, u.fullname,
                IF(u.context='Profiled', (SELECT user_context FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = a.username limit 1), u.context) AS context,
                IF(a.usepincode='Profiled', (SELECT user_disa_usePIN FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = a.username limit 1), a.usepincode) AS usepincode,
                IF(a.status='Profiled', (SELECT user_disa_status FROM user_profile_user_relation pur LEFT JOIN user_profile p ON pur.profile_id = p.id WHERE pur.username = a.username limit 1), a.status) AS status
            FROM
                account a INNER JOIN users u ON a.username = u.mailbox 
            WHERE 
                a.username = in_username
            AND
                u.password = in_pincode
            ORDER BY 
                username;

        END IF;

    END IF;

END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS drop_tables_like;
DELIMITER //
CREATE PROCEDURE drop_tables_like(pattern varchar(255), db varchar(255))
BEGIN
SELECT @str_sql:=concat('drop table ', group_concat(table_name ORDER BY LENGTH(table_name) DESC))
FROM information_schema.tables
WHERE table_schema=db AND table_name LIKE pattern AND table_type = 'BASE TABLE';

prepare stmt FROM @str_sql;
EXECUTE stmt;
DROP prepare stmt;
END
//
DELIMITER ;

DROP PROCEDURE IF EXISTS resolveCallerIdName;
DELIMITER //
CREATE PROCEDURE resolveCallerIdName(IN in_cid_num TEXT, IN in_username TEXT)
BEGIN
    DECLARE val_group TEXT DEFAULT "";
    DECLARE val_username TEXT DEFAULT "";
    DECLARE val_i INT;
    DECLARE out_cid_name TEXT DEFAULT "";
    DECLARE val_name_order TEXT DEFAULT "firstname";

    -- name order in general setting
    SELECT value INTO val_name_order FROM systemConfigs WHERE `key` = 'fullnameOrder';

    SELECT LOCATE('-', in_username) INTO val_i;
    IF val_i > 0 THEN
        SELECT SUBSTR(in_username, 1, val_i-1) INTO val_username;
    ELSE
        SET val_username = in_username;
    END IF;

    SELECT `group` INTO val_group FROM sipfriends WHERE username = val_username;
    IF val_group = "" THEN
        IF val_name_order = "lastname" THEN
            SELECT CONCAT(lastname, ' ', firstname) INTO out_cid_name FROM book WHERE (REPLACE(REPLACE(phone_number, "-", ""), " ", "") = in_cid_num OR REPLACE(REPLACE(mobile_number, "-", ""), " ", "") = in_cid_num OR REPLACE(REPLACE(other_number, "-", ""), " ", "") = in_cid_num) AND ((recordType = 'User' AND recordOwner = val_username) OR (recordType != 'User' AND recordType != 'Customer')) ORDER BY FIELD(recordType, 'Site-Internal', 'Site', 'Customer', 'User') DESC LIMIT 1;
        ELSE
            SELECT CONCAT(firstname, ' ', lastname) INTO out_cid_name FROM book WHERE (REPLACE(REPLACE(phone_number, "-", ""), " ", "") = in_cid_num OR REPLACE(REPLACE(mobile_number, "-", ""), " ", "") = in_cid_num OR REPLACE(REPLACE(other_number, "-", ""), " ", "") = in_cid_num) AND ((recordType = 'User' AND recordOwner = val_username) OR (recordType != 'User' AND recordType != 'Customer')) ORDER BY FIELD(recordType, 'Site-Internal', 'Site', 'Customer', 'User') DESC LIMIT 1;
        END IF;
        IF out_cid_name = "" THEN
            SET val_i = LENGTH(in_cid_num);
            SEARCH: WHILE val_i >= 8 DO
                IF val_name_order = "lastname" THEN
                    SELECT CONCAT(lastname, ' ', firstname) INTO out_cid_name FROM book WHERE (REPLACE(REPLACE(phone_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i)) OR REPLACE(REPLACE(mobile_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i)) OR REPLACE(REPLACE(other_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i))) AND ((recordType = 'User' AND recordOwner = val_username) OR (recordType != 'User' AND recordType != 'Customer')) ORDER BY FIELD(recordType, 'Site-Internal', 'Site', 'Customer', 'User') DESC LIMIT 1;
                ELSE
                    SELECT CONCAT(firstname, ' ', lastname) INTO out_cid_name FROM book WHERE (REPLACE(REPLACE(phone_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i)) OR REPLACE(REPLACE(mobile_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i)) OR REPLACE(REPLACE(other_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i))) AND ((recordType = 'User' AND recordOwner = val_username) OR (recordType != 'User' AND recordType != 'Customer')) ORDER BY FIELD(recordType, 'Site-Internal', 'Site', 'Customer', 'User') DESC LIMIT 1;
                END IF;
                IF out_cid_name != "" THEN
                    LEAVE SEARCH;
                END IF;
                SET val_i = val_i - 1;
            END WHILE;
        END IF;
    ELSE
        IF val_name_order = "lastname" THEN
            SELECT CONCAT(lastname, ' ', firstname) INTO out_cid_name FROM book WHERE (REPLACE(REPLACE(phone_number, "-", ""), " ", "") = in_cid_num OR mobile_number = in_cid_num OR REPLACE(REPLACE(mobile_number, "-", ""), " ", "") = in_cid_num OR REPLACE(REPLACE(other_number, "-", ""), " ", "") = in_cid_num) AND ((recordType = 'User' AND recordOwner = val_username) OR (recordType = 'Customer' AND recordOwner = val_group) OR (recordType != 'User' AND recordType != 'Customer')) ORDER BY FIELD(recordType, 'Site-Internal', 'Site', 'Customer', 'User') DESC LIMIT 1;
        ELSE
            SELECT CONCAT(firstname, ' ', lastname) INTO out_cid_name FROM book WHERE (REPLACE(REPLACE(phone_number, "-", ""), " ", "") = in_cid_num OR mobile_number = in_cid_num OR REPLACE(REPLACE(mobile_number, "-", ""), " ", "") = in_cid_num OR REPLACE(REPLACE(other_number, "-", ""), " ", "") = in_cid_num) AND ((recordType = 'User' AND recordOwner = val_username) OR (recordType = 'Customer' AND recordOwner = val_group) OR (recordType != 'User' AND recordType != 'Customer')) ORDER BY FIELD(recordType, 'Site-Internal', 'Site', 'Customer', 'User') DESC LIMIT 1;
        END IF;
        IF out_cid_name = "" THEN
            SET val_i = LENGTH(in_cid_num);
            SEARCH: WHILE val_i >= 8 DO
                IF val_name_order = "lastname" THEN
                    SELECT CONCAT(lastname, ' ', firstname) INTO out_cid_name FROM book WHERE (REPLACE(REPLACE(phone_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i)) OR REPLACE(REPLACE(mobile_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i)) OR REPLACE(REPLACE(other_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i))) AND ((recordType = 'User' AND recordOwner = val_username) OR (recordType = 'Customer' AND recordOwner = val_group) OR (recordType != 'User' AND recordType != 'Customer')) ORDER BY FIELD(recordType, 'Site-Internal', 'Site', 'Customer', 'User') DESC LIMIT 1;
                ELSE
                    SELECT CONCAT(firstname, ' ', lastname) INTO out_cid_name FROM book WHERE (REPLACE(REPLACE(phone_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i)) OR REPLACE(REPLACE(mobile_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i)) OR REPLACE(REPLACE(other_number, "-", ""), " ", "") LIKE CONCAT("%", SUBSTR(in_cid_num, 1, val_i))) AND ((recordType = 'User' AND recordOwner = val_username) OR (recordType = 'Customer' AND recordOwner = val_group) OR (recordType != 'User' AND recordType != 'Customer')) ORDER BY FIELD(recordType, 'Site-Internal', 'Site', 'Customer', 'User') DESC LIMIT 1;
                END IF;
                IF out_cid_name != "" THEN
                    LEAVE SEARCH;
                END IF;
                SET val_i = val_i - 1;
            END WHILE;
        END IF;
    END IF;
    SELECT out_cid_name;
END;
//
DELIMITER ;


DROP PROCEDURE IF EXISTS cc_updateQueueCall;
delimiter //
CREATE PROCEDURE cc_updateQueueCall(IN in_time TEXT, IN in_callid TEXT, IN in_queuename TEXT, IN in_agent TEXT, IN in_event TEXT, IN in_arg1 TEXT, IN in_arg2 TEXT, IN in_arg3 TEXT, IN in_arg4 TEXT, IN in_arg5 TEXT)
BEGIN
    -- declare variables
    DECLARE cnt INT;

    IF (in_event = 'ENTERQUEUE') THEN
        -- check record existance by the time and callid
        SELECT COUNT(*) INTO cnt FROM queue_call WHERE starttime = in_time AND callid = in_callid;
        IF (cnt < 1) THEN
            -- same time and same callid not exist, need insert
            INSERT INTO queue_call (starttime, queue, callid, callerid) VALUES (in_time, in_queuename, in_callid, in_arg2);
        END IF;
    ELSEIF (in_event = 'ABANDON') THEN
        UPDATE queue_call SET stoptime = in_time, agent = in_agent, holdtime = in_arg3, duration = in_arg3, reason = in_event WHERE callid = in_callid AND queue = in_queuename ORDER BY id DESC LIMIT 1;
    ELSEIF (in_event = 'EXITEMPTY') THEN
        UPDATE queue_call SET stoptime = in_time, agent = in_agent, holdtime = in_arg3, duration = in_arg3, reason = in_event WHERE callid = in_callid AND queue = in_queuename ORDER BY id DESC LIMIT 1;
    ELSEIF (in_event = 'EXITWITHTIMEOUT') THEN
        IF (in_arg3 != '') THEN
            UPDATE queue_call SET stoptime = in_time, agent = in_agent, holdtime = in_arg3, duration = in_arg3, reason = in_event WHERE callid = in_callid AND queue = in_queuename ORDER BY id DESC LIMIT 1;
        ELSE
            UPDATE queue_call SET stoptime = in_time, agent = in_agent, holdtime = (FROM_UNIXTIME(in_time) - FROM_UNIXTIME(starttime)), duration = (FROM_UNIXTIME(in_time) - FROM_UNIXTIME(starttime)), reason = in_event WHERE callid = in_callid AND queue = in_queuename ORDER BY id DESC LIMIT 1;
        END IF;
    ELSEIF (in_event = 'EXITWITHKEY') THEN
        UPDATE queue_call SET stoptime = in_time, agent = in_agent, holdtime = (FROM_UNIXTIME(in_time) - FROM_UNIXTIME(starttime)), duration = (FROM_UNIXTIME(in_time) - FROM_UNIXTIME(starttime)), reason = in_event WHERE callid = in_callid ORDER BY id DESC LIMIT 1;
    ELSEIF (in_event = 'TRANSFER') THEN
        UPDATE queue_call SET stoptime = in_time, agent = in_agent, holdtime = in_arg3, calltime = in_arg4, duration = (in_arg3 + in_arg4), reason = in_event WHERE callid = in_callid AND queue = in_queuename ORDER BY id DESC LIMIT 1;
    ELSEIF (in_event = 'COMPLETECALLER') THEN
        UPDATE queue_call SET stoptime = in_time, agent = in_agent, holdtime = in_arg1, calltime = in_arg2, duration = (in_arg1 + in_arg2), reason = in_event WHERE callid = in_callid AND queue = in_queuename ORDER BY id DESC LIMIT 1;
    ELSEIF (in_event = 'COMPLETEAGENT') THEN
        UPDATE queue_call SET stoptime = in_time, agent = in_agent, holdtime = in_arg1, calltime = in_arg2, duration = (in_arg1 + in_arg2), reason = in_event WHERE callid = in_callid AND queue = in_queuename ORDER BY id DESC LIMIT 1;
    ELSEIF (in_event = 'CONNECT') THEN
        UPDATE queue_call SET agent = in_agent, holdtime = in_arg1 WHERE callid = in_callid AND queue = in_queuename ORDER BY id DESC LIMIT 1;
    END IF;
END//
delimiter ;

DROP TRIGGER IF EXISTS `queue_log_insertcascade`;
delimiter //
CREATE TRIGGER `queue_log_insertcascade` AFTER INSERT ON `queue_log`
FOR EACH ROW BEGIN
    call cc_updateQueueCall(NEW.time, NEW.callid, NEW.queuename, NEW.agent, NEW.event, NEW.arg1, NEW.arg2, NEW.arg3, NEW.arg4, NEW.arg5);
END//
delimiter ;

DROP PROCEDURE IF EXISTS account_acquireAccount;
DELIMITER //
CREATE PROCEDURE account_acquireAccount(IN in_udid TEXT, IN in_device TEXT, IN in_userId TEXT)
BEGIN

    DECLARE out_error INT;
    DECLARE out_sip_username TEXT;
    DECLARE out_sip_password TEXT;
    DECLARE val_account_id INT;

    SET out_error = 0;
    SET out_sip_username = '';
    SET out_sip_password = '';
    SET val_account_id = 0;

    -- LOCK TABLE d100_sip_account WRITE;


    SELECT d100_sip_account.id, d100_sip_account.sip_username, d100_sip_account.sip_password INTO val_account_id, out_sip_username, out_sip_password FROM d100_sip_account LEFT JOIN uc_acquired_account ON d100_sip_account.sip_username = uc_acquired_account.sip_username WHERE d100_sip_account.udid = in_udid AND d100_sip_account.device = in_device AND d100_sip_account.status != 'locked' AND uc_acquired_account.ssoid = in_userId LIMIT 1;

    IF val_account_id <= 0 THEN
        SELECT id, sip_username, sip_password INTO val_account_id, out_sip_username, out_sip_password FROM d100_sip_account WHERE status = 'idle' ORDER BY last_used LIMIT 1;
        IF val_account_id <= 0 THEN
            -- no idle without mapped to device
            SELECT id, sip_username, sip_password INTO val_account_id, out_sip_username, out_sip_password FROM d100_sip_account WHERE status = 'idle' ORDER BY last_used LIMIT 1;
            IF val_account_id <= 0 THEN
                -- no idle can use
                SET out_error = 1;
                SET out_sip_username = '';
                SET out_sip_password = '';
            ELSE
                -- there is an idle but mapped to device, can use
                UPDATE d100_sip_account SET status = 'inuse', last_used = now(), device = in_device, udid = in_udid WHERE id = val_account_id;
                DELETE FROM uc_acquired_account WHERE ssoid = in_userId AND sip_username = out_sip_username;
                INSERT INTO uc_acquired_account VALUES (in_userId, out_sip_username, CONCAT(unix_timestamp(), '000'));
            END IF;
        ELSE
            -- get and idle account
            UPDATE d100_sip_account SET status = 'inuse', last_used = now(), device = in_device, udid = in_udid WHERE id = val_account_id;
            DELETE FROM uc_acquired_account WHERE ssoid = in_userId AND sip_username = out_sip_username;
            INSERT INTO uc_acquired_account VALUES (in_userId, out_sip_username, CONCAT(unix_timestamp(), '000'));
        END IF;
    ELSE
        -- already has the sip account mapping
        UPDATE d100_sip_account SET status = 'inuse', last_used = now(), device = in_device, udid = in_udid WHERE id = val_account_id;
        DELETE FROM uc_acquired_account WHERE ssoid = in_userId AND sip_username = out_sip_username;
        INSERT INTO uc_acquired_account VALUES (in_userId, out_sip_username, CONCAT(unix_timestamp(), '000'));
    END IF;

    -- UNLOCK TABLES;

    SELECT out_error, out_sip_username, out_sip_password;

END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS account_releaseAccount;
DELIMITER //
CREATE PROCEDURE account_releaseAccount(IN in_udid TEXT, IN in_device TEXT, IN in_userId TEXT)
BEGIN

    DECLARE out_error INT;
    DECLARE val_sip_username TEXT;

    SET out_error = 0;
    SET val_sip_username = 0;

    SELECT d100_sip_account.sip_username INTO val_sip_username FROM d100_sip_account LEFT JOIN uc_acquired_account ON d100_sip_account.sip_username = uc_acquired_account.sip_username WHERE d100_sip_account.udid = in_udid AND d100_sip_account.device = in_device AND uc_acquired_account.ssoid = in_userId LIMIT 1;
    UPDATE d100_sip_account SET status = 'idle' WHERE device = in_device AND udid = in_udid AND sip_username = val_sip_username;
    DELETE FROM uc_acquired_account WHERE ssoid = in_userId AND sip_username = val_sip_username;

    SELECT out_error;

END;
//
DELIMITER ;

-- Trigger when insert into session table: login
DROP TRIGGER IF EXISTS `session_insertcascade_auth_log`;
delimiter //
CREATE TRIGGER `session_insertcascade_auth_log` AFTER INSERT ON `session`
FOR EACH ROW BEGIN
    IF (NEW.session_id IS NOT NULL) THEN
        INSERT INTO auth_log(session_id, username, login_time, server_id) VALUES (NEW.session_id, NEW.username, NOW(), NEW.server_id);
    END IF;
END//
delimiter ;

-- Trigger when updating a session table: may be a login from attempt
DROP TRIGGER IF EXISTS `session_updatecascade_auth_log`;
delimiter //
CREATE TRIGGER `session_updatecascade_auth_log` AFTER UPDATE ON `session`
FOR EACH ROW BEGIN
    IF (OLD.session_id != NEW.session_id) THEN
        INSERT INTO auth_log(session_id, username, login_time, server_id) VALUES (NEW.session_id, NEW.username, NOW(), NEW.server_id);
    END IF;
END//
delimiter ;

-- Trigger when deleting a session: Logout/expire
DROP TRIGGER IF EXISTS `session_deletecascade_auth_log`;
delimiter //
CREATE TRIGGER `session_deletecascade_auth_log` AFTER DELETE ON `session`
FOR EACH ROW BEGIN
    DECLARE expiryMinutes TEXT;
    DECLARE expireTime DATETIME;

    SET expiryMinutes := 60; -- Default to 60 minutes.
    IF (OLD.credential IS NULL) THEN
        -- Admin Portal session expire/logout
        SELECT `value` INTO expiryMinutes FROM `systemConfigs` WHERE `key` = 'sessionExpiryTime' AND `value` IS NOT NULL LIMIT 1;
    ELSE
        -- Admin Portal session expire/logout
        SELECT `value` INTO expiryMinutes FROM `systemConfigs` WHERE `key` = 'switchboardSessionExpiryTime' AND `value` IS NOT NULL LIMIT 1;
    END IF;

    SET expireTime := FROM_UNIXTIME(OLD.last_access + expiryMinutes * 60);
    IF (NOW() > expireTime) THEN
        -- Session expired.
        UPDATE auth_log SET expire_time = expireTime, logout_time = NOW() WHERE session_id = OLD.session_id AND `username` = OLD.`username` AND server_id IN (OLD.server_id, 0);
    ELSE
        -- User prompted logout.
        UPDATE auth_log SET logout_time = NOW() WHERE session_id = OLD.session_id AND `username` = OLD.`username` AND server_id IN (OLD.server_id, 0);
    END IF;

END//
delimiter ;

DROP TRIGGER IF EXISTS `im_groupchat_log_insertcascade`;
DELIMITER //
CREATE TRIGGER `im_groupchat_log_insertcascade` AFTER INSERT ON `im_groupchat_log`
FOR EACH ROW BEGIN
    -- declare variables
    DECLARE cnt INT;

    IF (NEW.command = 'kick') THEN
        INSERT INTO `im_groupchat_log_distribution` (`username`, `msgId`) VALUES (SUBSTRING_INDEX(NEW.arg1, '@', 1), NEW.msgId);
    END IF;
    IF (NEW.command = 'leave') THEN
        INSERT INTO `im_groupchat_log_distribution` (`username`, `msgId`) VALUES (SUBSTRING_INDEX(NEW.fromJid, '@', 1), NEW.msgId);
    END IF;
    -- check record existance of the group, since we need to handle the uchat
    SELECT COUNT(*) INTO cnt FROM im_groupchat_member WHERE groupchat_unique_id = SUBSTRING_INDEX(NEW.toJid, '@', 1);
    IF (cnt > 0) THEN
        INSERT INTO `im_groupchat_log_distribution` (`username`, `msgId`) SELECT `username`, NEW.msgId AS msgId FROM im_groupchat_member WHERE groupchat_unique_id = SUBSTRING_INDEX(NEW.toJid, '@', 1);
    ELSE
        INSERT INTO `im_groupchat_log_distribution` (`username`, `msgId`) SELECT `ssoid`, NEW.msgId AS msgId FROM uc_group_member WHERE group_name = SUBSTRING_INDEX(NEW.toJid, '@', 1);
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS `sipfriends_insertcascade`;

DROP TRIGGER IF EXISTS `sipfriends_updatecascade`;

DROP TRIGGER IF EXISTS `sipfriends_deletecascade`;

DROP TRIGGER IF EXISTS `acl_user_insertcascade`;
DELIMITER //
CREATE TRIGGER `acl_user_insertcascade` AFTER INSERT ON `acl_user`
FOR EACH ROW BEGIN
    DECLARE val_name_order TEXT DEFAULT "firstname";

    -- name order in general setting
    SELECT value INTO val_name_order FROM systemConfigs WHERE `key` = 'fullnameOrder';

    INSERT INTO ejabberd.users (username, password) VALUES (NEW.username, NEW.password);

    IF val_name_order = "lastname" THEN
        INSERT INTO ejabberd.vcard (username, vcard) SELECT a.username, CONCAT("<vCard xmlns='vcard-temp'><NICKNAME>", TRIM(CONCAT(b.lastname, " ", b.firstname)), "</NICKNAME></vCard>") FROM acl_user a INNER JOIN book b ON a.information_record = b.id WHERE a.username = NEW.username;
        INSERT INTO ejabberd.vcard_search (username, lusername, nickname, lnickname) SELECT a.username, LOWER(a.username), TRIM(CONCAT(b.lastname, " ", b.firstname)), LOWER(TRIM(CONCAT(b.lastname, " ", b.firstname))) FROM acl_user a INNER JOIN book b ON a.information_record = b.id WHERE a.username = NEW.username;
    ELSE
        INSERT INTO ejabberd.vcard (username, vcard) SELECT a.username, CONCAT("<vCard xmlns='vcard-temp'><NICKNAME>", TRIM(CONCAT(b.firstname, " ", b.lastname)), "</NICKNAME></vCard>") FROM acl_user a INNER JOIN book b ON a.information_record = b.id WHERE a.username = NEW.username;
        INSERT INTO ejabberd.vcard_search (username, lusername, nickname, lnickname) SELECT a.username, LOWER(a.username), TRIM(CONCAT(b.firstname, " ", b.lastname)), LOWER(TRIM(CONCAT(b.firstname, " ", b.lastname))) FROM acl_user a INNER JOIN book b ON a.information_record = b.id WHERE a.username = NEW.username;
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS `acl_user_updatecascade`;
DELIMITER //
CREATE TRIGGER `acl_user_updatecascade` AFTER UPDATE ON `acl_user`
FOR EACH ROW BEGIN
    DECLARE val_name_order TEXT DEFAULT "firstname";

    -- name order in general setting
    SELECT value INTO val_name_order FROM systemConfigs WHERE `key` = 'fullnameOrder';

    IF val_name_order = "lastname" THEN
        UPDATE ejabberd.vcard v LEFT JOIN frsip.acl_user a ON v.username = a.username LEFT JOIN frsip.book b ON a.information_record = b.id SET v.vcard = CONCAT("<vCard xmlns='vcard-temp'><NICKNAME>", TRIM(CONCAT(b.lastname, " ", b.firstname)), "</NICKNAME></vCard>") WHERE v.username = NEW.username;
        UPDATE ejabberd.vcard_search v LEFT JOIN frsip.acl_user a ON v.username = a.username LEFT JOIN frsip.book b ON a.information_record = b.id SET v.nickname = TRIM(CONCAT(b.lastname, " ", b.firstname)), lnickname = LOWER(TRIM(CONCAT(b.lastname, " ", b.firstname))) WHERE v.username = NEW.username;
    ELSE
        UPDATE ejabberd.vcard v LEFT JOIN frsip.acl_user a ON v.username = a.username LEFT JOIN frsip.book b ON a.information_record = b.id SET v.vcard = CONCAT("<vCard xmlns='vcard-temp'><NICKNAME>", TRIM(CONCAT(b.firstname, " ", b.lastname)), "</NICKNAME></vCard>") WHERE v.username = NEW.username;
        UPDATE ejabberd.vcard_search v LEFT JOIN frsip.acl_user a ON v.username = a.username LEFT JOIN frsip.book b ON a.information_record = b.id SET v.nickname = TRIM(CONCAT(b.firstname, " ", b.lastname)), lnickname = LOWER(TRIM(CONCAT(b.firstname, " ", b.lastname))) WHERE v.username = NEW.username;
    END IF;
    IF (NEW.password != OLD.password) THEN
        UPDATE ejabberd.users SET password = NEW.password WHERE username = NEW.username;
    END IF;
END//
DELIMITER ;

DROP TRIGGER IF EXISTS `acl_user_deletecascade`;
DELIMITER //
CREATE TRIGGER `acl_user_deletecascade` AFTER DELETE ON `acl_user`
FOR EACH ROW BEGIN

    DECLARE loop0_eof BOOLEAN DEFAULT FALSE;
    DECLARE tmp_group_id VARCHAR(128);
    DECLARE cur0 CURSOR FOR SELECT unique_id FROM frsip.im_groupchat WHERE unique_id NOT IN (SELECT groupchat_unique_id FROM im_groupchat_member WHERE `type` = 'OWNER');
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET loop0_eof = TRUE;

    DELETE FROM ejabberd.vcard WHERE username = OLD.username;
    DELETE FROM ejabberd.vcard_search WHERE username = OLD.username;
    DELETE FROM ejabberd.users WHERE username = OLD.username;
    DELETE FROM frsip.im_groupchat_member WHERE username = OLD.username;
    -- Remove the group which is no more user there
    DELETE FROM frsip.im_groupchat WHERE `unique_id` NOT IN (SELECT `groupchat_unique_id` FROM `im_groupchat_member`);

    -- Fix admin of the group
    OPEN cur0;
        loop0: LOOP
            FETCH cur0 INTO tmp_group_id;
            IF loop0_eof THEN
                LEAVE loop0;
            END IF;
            -- Update one of the User as Admin
            UPDATE frsip.im_groupchat_member SET `type` = 'OWNER' WHERE `groupchat_unique_id` = tmp_group_id LIMIT 1;
        END LOOP loop0;
    CLOSE cur0;
END//
DELIMITER ;
