DROP PROCEDURE IF EXISTS Scheduler_getNextTask;
DELIMITER //
CREATE PROCEDURE Scheduler_getNextTask(IN in_server_id INT)
BEGIN
    -- declare variable
    DECLARE out_id INT DEFAULT 0;
    DECLARE out_job TEXT;
    DECLARE out_data TEXT;
    DECLARE out_parameter TEXT;
    DECLARE val_server_id_mask INT DEFAULT 1;
    DECLARE val_cnt INT DEFAULT 0;
    DECLARE val_curtimestamp INT;

    -- cursor
    DECLARE val_done INT DEFAULT 0;
    -- fetch the next task
    DECLARE cur_scheduled_task CURSOR FOR
        SELECT id, job, data, parameter
        FROM scheduled_task
        WHERE processing & val_server_id_mask = 0
        AND done & val_server_id_mask = 0
        AND trial <= 7
        AND (job,data,parameter) NOT IN (SELECT job,data,parameter FROM frsip_temp_s1)
        AND timestamp <= val_curtimestamp
        ORDER BY timestamp, id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET val_done = 1;

    -- Asterisk Dialplan Reload will be done on every 5 mins interval
    -- UPDATE scheduled_task SET timestamp = timestamp + (300 - timestamp % 300) WHERE job = 'AsteriskReload' AND data = 'dialPlan' AND (timestamp % 300) != 0;

    SET out_id = 0;
    SET out_job = "";
    SET out_data = "";
    SET out_parameter = "";
    SET val_server_id_mask = 1 << in_server_id;
    SET val_curtimestamp = UNIX_TIMESTAMP();

    CREATE temporary TABLE frsip_temp_s1 SELECT DISTINCT job, data, parameter FROM scheduled_task WHERE processing & val_server_id_mask!= 0;

    OPEN cur_scheduled_task;
    loop_task: LOOP
        FETCH cur_scheduled_task INTO out_id, out_job, out_data, out_parameter;
        IF val_done THEN LEAVE loop_task; END IF;

        SELECT count(*) INTO val_cnt
        FROM scheduled_task
        WHERE processing & val_server_id_mask != 0
        AND done & val_server_id_mask = 0
        AND timestamp <= val_curtimestamp
        AND 
            IF (out_job="AsteriskConfig", 
                IF (out_data="dialPlan",
                    "|AsteriskConfig:dialPlanNumber|AsteriskReload:dialPlan|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="dialPlanNumber",
                    "|AsteriskConfig:dialPlan|AsteriskReload:dialPlan|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="ringGroup",
                    "|AsteriskReload:dialPlan|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="ivr",
                    "|AsteriskReload:dialPlan|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="sip",
                    "|AsteriskReload:sip|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="sipPeer",
                    "|AsteriskReload:dialPlan|AsteriskReload:sip|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="sipProvider",
                    "|AsteriskReload:sip|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="sipUser",
                    "|AsteriskReload:sip|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="sipMobile",
                    "|AsteriskReload:sip|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="sipD100",
                    "|AsteriskReload:sip|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="roamingUser",
                    "|AsteriskReload:sip|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="queue",
                    "|AsteriskReload:queue|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="voicemail",
                    "|AsteriskReload:voicemail|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="voicemailUser",
                    "|AsteriskReload:voicemail|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="voicemailEmailTemplate",
                    "|AsteriskReload:voicemail|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="meetme",
                    "|AsteriskReload:meetme|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="feature",
                    "|AsteriskReload:feature|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="manager",
                    "|AsteriskReload:,manager|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="logger",
                    "|AsteriskReload:logger|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="moh",
                    "|AsteriskReload:moh|AsteriskReload:all|",
                IF (out_data="disa2",
                    "|AsteriskReload:disa2|AsteriskReload:all|", ""
                )))))))))))))))))))))
            ,
            IF (out_job="AsteriskReload",
                IF (out_data="dialPlan",
                    "|AsteriskConfig:dialPlan|AsteriskConfig:dialPlanNumber|AsteriskConfig:ringGroup|AsteriskConfig:ivr|AsteriskConfig:sipPeer|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="sip",
                    "|AsteriskConfig:sip|AsteriskConfig:sipPeer|AsteriskConfig:sipProvider|AsteriskConfig:sipUser|AsteriskConfig:sipMobile|AsteriskConfig:sipD100|AsteriskConfig:roamingUser|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="queue",
                    "|AsteriskConfig:queue|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="feature",
                    "|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="logger",
                    "|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="moh",
                    "|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="voicemail",
                    "|AsteriskConfig:voicemail|AsteriskConfig:voicemailUser|AsteriskConfig:voicemailEmailTemplate|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="disa2",
                    "|AsteriskReload:normal|AsteriskReload:all|",
                IF (out_data="normal",
                    CONCAT("|",job,":",data,"|"),
                IF (out_data="all",
                    CONCAT("|",job,":",data,"|"), ""
                )))))))))), ""
            )) 
        LIKE CONCAT("%|",job,":",data,"|%");

        IF val_cnt = 0 THEN
            LEAVE loop_task;
        ELSE
            SET out_id = 0;
            SET out_job = "";
            SET out_data = "";
            SET out_parameter = "";
        END IF;
    END LOOP loop_task;
    CLOSE cur_scheduled_task;

    DROP temporary TABLE frsip_temp_s1;

    SELECT out_id, out_job, out_data, out_parameter;
END;
//
DELIMITER ;


DROP PROCEDURE IF EXISTS scheduler_taskSetProcessing;
DELIMITER //
CREATE PROCEDURE scheduler_taskSetProcessing(IN in_server_id INT, IN in_id TEXT, IN in_job TEXT, IN in_data TEXT, IN in_parameter TEXT)
BEGIN
    DECLARE val_server_id_mask INT DEFAULT 1;
    SET val_server_id_mask = 1 << in_server_id;
    IF in_job = "AsteriskReload" THEN
        UPDATE scheduled_task SET processing = processing|val_server_id_mask WHERE id = in_id;
    ELSE
        UPDATE scheduled_task SET processing = processing|val_server_id_mask WHERE job = in_job AND data = in_data AND parameter = in_parameter AND done&val_server_id_mask = 0;
    END IF;
END;
//
DELIMITER ;

DROP PROCEDURE IF EXISTS scheduler_taskReset;
DELIMITER //
CREATE PROCEDURE scheduler_taskReset(IN in_server_id INT, IN in_id TEXT, IN in_job TEXT, IN in_data TEXT, IN in_parameter TEXT)
BEGIN
    DECLARE val_server_id_mask INT DEFAULT 1;
    SET val_server_id_mask = 1 << in_server_id;
    IF in_job = "AsteriskReload" THEN
        UPDATE scheduled_task SET processing = processing&~val_server_id_mask WHERE id = in_id;
    ELSE
        UPDATE scheduled_task SET processing = processing&~val_server_id_mask, trial = trial+1 WHERE job = in_job AND data = in_data AND parameter = in_parameter AND done&val_server_id_mask = 0;
    END IF;
END;
//
DELIMITER ;


DROP PROCEDURE IF EXISTS scheduler_taskSetFinished;
DELIMITER //
CREATE PROCEDURE scheduler_taskSetFinished(IN in_server_id INT, IN in_id TEXT, IN in_job TEXT, IN in_data TEXT, IN in_parameter TEXT)
BEGIN
    DECLARE val_server_id_mask INT DEFAULT 1;
    DECLARE out_id INT DEFAULT 0;
    DECLARE out_job_data TEXT;
    -- cursor
    DECLARE val_done INT DEFAULT 0;
    DECLARE val_curtimestamp INT;
    DECLARE cur_scheduled_task CURSOR FOR
        SELECT id, CONCAT("|",job,":",data,"|")
        FROM scheduled_task
        WHERE processing & val_server_id_mask = 0
        AND done & val_server_id_mask = 0
        AND id != in_id
        AND timestamp <= val_curtimestamp
        ORDER BY timestamp, id;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET val_done = 1;

    SET val_curtimestamp = UNIX_TIMESTAMP();
    SET out_id = 0;
    SET out_job_data = "";

    SET val_server_id_mask = 1 << in_server_id;
    IF in_job = "AsteriskReload" THEN
        UPDATE scheduled_task SET processing = processing&~val_server_id_mask, done = done|val_server_id_mask WHERE id = in_id;
        OPEN cur_scheduled_task;
        loop_task: LOOP
            FETCH cur_scheduled_task INTO out_id, out_job_data;
            IF val_done THEN LEAVE loop_task; END IF;

            CASE in_data
                WHEN "dialPlan" THEN
                    IF "|AsteriskConfig:dialPlan|AsteriskConfig:dialPlanNumber|AsteriskConfig:ringGroup|AsteriskConfig:ivr|AsteriskConfig:sipPeer|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:dialPlan|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
                WHEN "sip" THEN
                    IF "|AsteriskConfig:sip|AsteriskConfig:sipPeer|AsteriskConfig:sipProvider|AsteriskConfig:sipUser|AsteriskConfig:sipMobile|AsteriskConfig:sipD100|AsteriskConfig:roamingUser|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:sip|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
                WHEN "queue" THEN
                    IF "|AsteriskConfig:queue|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:queue|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
                WHEN "feature" THEN
                    IF "|AsteriskConfig:feature|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:feature|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
                WHEN "logger" THEN
                    IF "|AsteriskConfig:logger|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:logger|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
                WHEN "moh" THEN
                    IF "|AsteriskConfig:moh|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:moh|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
                WHEN "voicemail" THEN
                    IF "|AsteriskConfig:voicemail|AsteriskConfig:voicemailUser|AsteriskConfig:voicemailEmailTemplate|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:voicemail|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
                WHEN "disa2" THEN
                    IF "|AsteriskConfig:disa2|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:disa2|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
                WHEN "normal" THEN
                    IF "|AsteriskConfig:dialPlan|AsteriskConfig:dialPlanNumber|AsteriskConfig:ringGroup|AsteriskConfig:ivr|AsteriskConfig:sip|AsteriskConfig:sipPeer|AsteriskConfig:sipProvider|AsteriskConfig:sipUser|AsteriskConfig:sipMobile|AsteriskConfig:sipD100|AsteriskConfig:roamingUser|AsteriskConfig:feature|AsteriskConfig:logger|AsteriskConfig:disa2|AsteriskConfig:manager|AsteriskConfig:meetme|AsteriskConfig:queue|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:dialPlan|AsteriskReload:sip|AsteriskReload:feature|AsteriskReload:logger|AsteriskReload:voicemail|AsteriskReload:disa2|AsteriskReload:normal|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
                WHEN "all" THEN
                    IF "|AsteriskConfig:dialPlan|AsteriskConfig:dialPlanNumber|AsteriskConfig:ringGroup|AsteriskConfig:ivr|AsteriskConfig:sip|AsteriskConfig:sipPeer|AsteriskConfig:sipProvider|AsteriskConfig:sipUser|AsteriskConfig:sipMobile|AsteriskConfig:sipD100|AsteriskConfig:roamingUser|AsteriskConfig:feature|AsteriskConfig:logger|AsteriskConfig:moh|AsteriskConfig:disa2|AsteriskConfig:manager|AsteriskConfig:meetme|AsteriskConfig:queue|" LIKE CONCAT("%",out_job_data,"%") THEN
                        LEAVE loop_task;
                    ELSE IF "|AsteriskReload:dialPlan|AsteriskReload:sip|AsteriskReload:feature|AsteriskReload:logger|AsteriskReload:voicemail|AsteriskReload:disa2|AsteriskReload:moh|AsteriskReload:normal|AsteriskReload:all|" LIKE CONCAT("%",out_job_data,"%") THEN
                        UPDATE scheduled_task SET done = done|val_server_id_mask WHERE id = out_id;
                    END IF;
                    END IF;
            END CASE;
        END LOOP loop_task;
        CLOSE cur_scheduled_task;
    ELSE
        UPDATE scheduled_task SET processing = processing&~val_server_id_mask, done = done|val_server_id_mask WHERE job = in_job AND data = in_data AND parameter = in_parameter AND processing&val_server_id_mask != 0 AND timestamp <= val_curtimestamp;
    END IF;
    DELETE FROM scheduled_task WHERE done = (SELECT SUM(1<<server_id) FROM all_server_info);
END;
//
DELIMITER ;
