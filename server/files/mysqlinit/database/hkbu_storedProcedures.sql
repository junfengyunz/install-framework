DROP TRIGGER IF EXISTS `uc_full_groupchat_log_insertcascade`;
DELIMITER //
CREATE TRIGGER `uc_full_groupchat_log_insertcascade` AFTER INSERT ON `uc_full_groupchat_log`
FOR EACH ROW BEGIN
    IF (NEW.command = 'kick') THEN
        INSERT INTO `uc_full_groupchat_log_distribution` (`ssoid`, `msgId`) VALUES (SUBSTRING_INDEX(NEW.arg1, '@', 1), NEW.msgId);
    END IF;
    IF (NEW.command = 'leave') THEN
        INSERT INTO `uc_full_groupchat_log_distribution` (`ssoid`, `msgId`) VALUES (SUBSTRING_INDEX(NEW.fromJid, '@', 1), NEW.msgId);
    END IF;
    INSERT INTO `uc_full_groupchat_log_distribution` (`ssoid`, `msgId`) SELECT `ssoid`, NEW.msgId AS msgId FROM uc_group_member WHERE group_name = SUBSTRING_INDEX(NEW.toJid, '@', 1);
END//
DELIMITER ;
