-- SLA Related topology
UPDATE `topology` SET `topology_activate` = '1' WHERE `topology_page` IN ('0207', '020701', '020702', '020703', '020704', '020705');
UPDATE `topology` SET `topology_show` = '1' WHERE `topology_page` IN ('0207');
-- update the max_appearance
UPDATE `sla_group` SET `max_appearance` = '256';
-- manual config line type
ALTER TABLE `equipment_manual_config` ADD `line_type` VARCHAR(20) NULL DEFAULT '';

UPDATE `topology` SET `topology_show` = '1', `topology_url_opt_a` = 'l', `topology_actionType` = 1, `topology_url_opt_m` = 'a' WHERE `topology_page` = '0102';
INSERT INTO `topology` VALUES (NULL,'User','userExtraImport','SM_USER_USEREXTRAIMPORT_TITLE','','0102','010206',60,'','','a','i','7','0','0','1');
INSERT INTO `topology` VALUES ('','User','userExtraEdit','SM_USER_USEREXTRAEDIT_TITLE','','0102','010207',70,'','','a','e','7','0','0','1');
-- migrate third party user
-- delete the topology for the third party user
DELETE FROM `topology` WHERE `topology_page` = '0106' OR `topology_parent` = '0106';
-- drop the third party user table
DROP TABLE `third_party_user`;

-- gavin(12312009) my portal, my feed batch edit
UPDATE `topology` SET `topology_show` = '1' WHERE `topology_page` = '011106';
-- INSERT INTO `topology` VALUES('','User','userMyPortalEdit','SM_USER_MY_PORTAL_EDIT_TITLE','','0111','011108','70','','','p','e','7','1','','1');
-- INSERT INTO `topology` VALUES('','User','userMyFeedEdit','SM_USER_MY_FEED_EDIT_TITLE','','0111','011109','80','','','p','em','7','1','','1');

-- H.323 Provisioning related stuff
DROP TABLE IF EXISTS `equipment_h323_settings`;
CREATE TABLE `equipment_h323_settings` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(80) NOT NULL,
    `h323_server_address` varchar(80) NOT NULL,
    `h323_server_port` varchar(80) NOT NULL,
    `h323_server_expires` varchar(80) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `equipment_manual_config` ADD `protocol` VARCHAR(20) NULL DEFAULT '';
ALTER TABLE `equipment_manual_config` ADD `h323_server_address` VARCHAR(80) NULL DEFAULT '';
ALTER TABLE `equipment_manual_config` ADD `h323_server_port` VARCHAR(80) NULL DEFAULT '';
ALTER TABLE `equipment_manual_config` ADD `h323_server_expires` VARCHAR(80) NULL DEFAULT '';
UPDATE `equipment_manual_config` SET `protocol` = 'sip';

-- added the missing switchboard default status in the initial default data set
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available (default)','1','1','1','Dial(SIP/{EXTENSION},40,t)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available (default)','1','1','2','Voicemail({EXTENSION},u)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available (default)','1','1','3','Hangup()');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Busy (default)','0','0','1','Voicemail({EXTENSION},b)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Busy (default)','0','0','2','Hangup()');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Away (default)','0','0','1','Dial(LOCAL/{MOBILE}@OUTGOINGCONTEXT,40,t)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Away (default)','0','0','2','Voicemail({EXTENSION},u)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Away (default)','0','0','3','Hangup()');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Do Not Disturb (default)','0','0','1','Voicemail({EXTENSION},u)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Do Not Disturb (default)','0','0','2','Hangup()');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available: Follow-me (default)','1','1','1','Dial(SIP/{EXTENSION},30,t)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available: Follow-me (default)','1','1','2','Dial(LOCAL/{MOBILE}@OUTGOINGCONTEXT,30,t)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available: Follow-me (default)','1','1','3','Voicemail({EXTENSION},u)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available: Follow-me (default)','1','1','4','Hangup()');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available: Forward (default)','1','1','1','Dial(LOCAL/{MOBILE}@OUTGOINGCONTEXT,30,t)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available: Forward (default)','1','1','2','Voicemail({EXTENSION},u)');
INSERT INTO `switchboard_user_status_default` (`status_name`,`status_sound`,`status_ans_cc_call`,`priority`,`options`) VALUES ('Available: Forward (default)','1','1','3','Hangup()');

-- added the missing polycom phonebook display attribute in the initial default data set
INSERT INTO `polycom_phonebook_attribute` (`id`,`store_name`,`display_name`,`status`) VALUES (1,'firstname','PB_LIST_FIRSTNAME',1),(2,'lastname','PB_LIST_LASTNAME',1),(3,'othername','PB_LIST_OTHERNAME',1),(4,'nametitle','PB_LIST_NAMETITLE',0),(5,'company','PB_LIST_COMPANY',1),(6,'department','PB_LIST_DEPARTMENT',1),(7,'jobTitle','PB_LIST_JOBTITLE',1),(8,'phone_number','PB_LIST_PHONE_NUMBER',1),(9,'mobile_number','PB_LIST_MOBILE_NUMBER',1),(10,'other_number','PB_LIST_OTHER_NUMBER',0),(11,'fax','PB_LIST_FAX',0),(12,'email','PB_LIST_EMAIL',0),(13,'location','PB_LIST_LOCATION',1);

-- frSIP Version 2.8.7.5
ALTER TABLE `equipment_manual_config` ADD `phonelabel` VARCHAR(40) NULL DEFAULT '';

DROP TABLE IF EXISTS `sla_user_barge_in_user`;
CREATE TABLE `sla_user_barge_in_user` (
    `id` int(11) NOT NULL auto_increment,
    `username` varchar(80) NOT NULL,
    `username_allow_bargein` varchar(80) NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE quintum_analogue_gateway CHANGE physicalPort physicalPort int(11);

-- added for control the voicemail management of the switchboard
ALTER TABLE sipfriends ADD switchboard_voicemail VARCHAR(20) NOT NULL DEFAULT '';
UPDATE sipfriends SET switchboard_voicemail = '1';
ALTER TABLE additional_device ADD switchboard_voicemail VARCHAR(20) NOT NULL DEFAULT '';
UPDATE additional_device SET switchboard_voicemail = '1';
ALTER TABLE user_profile ADD user_switchboard_voicemail VARCHAR(10) NOT NULL DEFAULT '';
UPDATE user_profile SET user_switchboard_voicemail = '1';

INSERT INTO `topology` VALUES ('','User','switchboardStatus','SM_USER_SBSTATUS_TITLE','','01','0114',140,'','','o','l','1','1','1','1');
INSERT INTO `topology` VALUES ('','User','switchboardStatusList','SM_USER_SBSTATUSLIST_TITLE','','0114','011401',10,'','','o','l','1','0','0','1');
INSERT INTO `topology` VALUES ('','User','switchboardStatusView','SM_USER_SBSTATUSVIEW_TITLE','','0114','011402',20,'','','o','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','User','switchboardStatusEdit','SM_USER_SBSTATUSEDIT_TITLE','','0114','011403',30,'','','o','m','3','0','0','1');
INSERT INTO `topology` VALUES ('','User','switchboardStatusAdd','SM_USER_SBSTATUSADD_TITLE','','0114','011404',40,'','','o','a','4','0','0','1');
INSERT INTO `topology` VALUES ('','User','switchboardStatusDelete','SM_USER_SBSTATUSDELETE_TITLE','','0114','011405',50,'','','o','d','5','0','0','1');

-- resend general line pin code
INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ('GeneralLinePincode','','Verdana','10','urgent@deltapath.com','frSIP PBX','General Line Account Password','','Dear $name,\r\n\r\nThe administrator has created an General Line on the PBX.\r\n\r\nGeneral Line Number: $username\r\nPassword: $pincode\r\n\r\nYour password will be used for:\r\n-Checking your voicemail at the phone\r\n-Making IDD Calls (if required)\r\n\r\nPlease remember to keep your password secure.  You may at anytime call into User Management Centre to change your password.\r\n\r\nThank You\r\n\r\n****************************************************************************************\r\nThis is an automated message generated by the phone system. Please do not reply to this message as this account is not monitored by any personnel.\r\n**************************************************************************************** \r\n\r\n','','1');
INSERT INTO `topology` VALUES ('','CRM','tools','SM_CRM_TOOLS_TITLE','','07','0710','100','','','','','0','1','1','1');
INSERT INTO `topology` VALUES ('','CRM','resendpincode','SM_CRM_GENERALLINE_RESENDPINCODE_TITLE','','0710','071001','10','','','sp','a','12','1','0','1');

-- nokia provision
DROP TABLE IF EXISTS `wireless_network`;
CREATE TABLE `wireless_network` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(20) NOT NULL,
  `sip_name` varchar(20) NOT NULL default '',
  `wl_type` enum('WLAN','GSM-GPRS','GSM-CSD') NOT NULL default 'WLAN',
  `wl_name` varchar(20) default NULL,
  `wl_ssid` varchar(20) NOT NULL,
  `wl_sec_mode` varchar(20) NOT NULL,
  `wl_encry_key` text default NULL,
  `wl_encry_key_index` enum('','0','1','2','3') NOT NULL default '',
  `wl_encry_key_length` enum('','64','128') NOT NULL default '',
  `wl_nap_address` varchar(80) NOT NULL default '',
  `wl_nap_addrtype` enum('','APN','GSM-GPRS') NOT NULL default '',
  `wl_authtype` enum('','PAP','CHAP') NOT NULL default '',
  `wl_authname` varchar(80) NOT NULL default '',
  `wl_authsecret` varchar(80) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `wireless_network_shared_user_relation`;
CREATE TABLE `wireless_network_shared_user_relation` (
  `id` int(11) NOT NULL auto_increment,
  `wn_id` int(11) NOT NULL,
  `username` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `nm_wn_pro_relation`;
CREATE TABLE `nm_wn_pro_relation` (
  `id` int(11) NOT NULL auto_increment,
  `nokiaMobile_username` varchar(20) NOT NULL,
  `wirelessNetwork_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `nokia_setting`;
CREATE TABLE `nokia_setting` (
  `id` int(11) NOT NULL auto_increment,
  `key` text NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `equipment` ADD COLUMN `nokia_mobile_number` varchar(35) default '';
ALTER TABLE `equipment` ADD COLUMN `nokia_mobile_server_ip` varchar(35) NOT NULL default '';
INSERT INTO `topology` VALUES ('','Equipment','NokiaSettings','SM_EQUIP_NOKIASETTINGS_TITLE','','03','0306',60,'','','n','v','2','1','1','1');
INSERT INTO `topology` VALUES ('','Equipment','NokiaSettings','SM_EQUIP_NOKIASETTINGS_TITLE','','0306','030601',10,'','','n','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','Equipment','NokiaSettings','SM_EQUIP_NOKIASETTINGS_TITLE','','0306','030602',20,'','','n','m','3','0','0','1');
INSERT INTO `topology` VALUES ('','Equipment','equipProvision','SM_EQUIP_EQPROVISION_TITLE','','0301','030108',60,'','','e','p','11','0','0','1');
INSERT INTO `topology` VALUES ('','User','userMobileNAPSettings','SM_USER_WIRELESS_NETWORK_SETTINGS_TITLE','','01','0112',130,'','','w','l','1','1','1','1');
INSERT INTO `topology` VALUES ('','User','userMobileNAPList','SM_USER_WIRELESS_NETWORK_LIST_TITLE','','0112','011201',10,'','','w','l','1','0','0','1');
INSERT INTO `topology` VALUES ('','User','userMobileNAPView','SM_USER_WIRELESS_NETWORK_VIEW_TITLE','','0112','011202',20,'','','w','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','User','userMobileNAPModify','SM_USER_WIRELESS_NETWORK_MODIFY_TITLE','','0112','011203',30,'','','w','m','3','0','0','1');
INSERT INTO `topology` VALUES ('','User','userMobileNAPAdd','SM_USER_WIRELESS_NETWORK_ADD_TITLE','','0112','011204',40,'','','w','a','4','0','0','1');
INSERT INTO `topology` VALUES ('','User','userMobileNAPDelete','SM_USER_WIRELESS_NETWORK_DELETE_TITLE','','0112','011205',50,'','','w','d','5','0','0','1');
INSERT INTO `topology` VALUES ('','User','userMobileNAPEdit','SM_USER_WIRELESS_NETWORK_EDIT_TITLE','','0112','011207',70,'','','w','e','7','0','0','1');
INSERT INTO `topology` VALUES ('','User','userMobileNAPIMPORT','SM_USER_WIRELESS_NETWORK_IMPORT_TITLE','','0112','011206',60,'','','w','i','7','0','0','1');
INSERT INTO `topology` VALUES ('','User','provisionNokiaMobile','SM_USER_PROVISION_NOKIA_MOBILE','','0105','010504',40,'','','sn','a','12','1','0','1');

-- Polycom VVX1500D support both protocol on same line
ALTER TABLE `equipment` ADD `protocol` VARCHAR(20) NULL DEFAULT '';
UPDATE `equipment` SET `protocol` = '0';
UPDATE `equipment` SET `protocol` = '1' WHERE `username` IN (SELECT `username` FROM `equipment_h323_settings`);
-- General Line Import
INSERT INTO `topology` VALUES ('','CRM','GeneralLineImport','SM_CRM_GENERALLINEIMPORT_TITLE','','0706','070606',60,'','','l','i','7','1','0','1');

-- eric (02032010) : fixed the voice prompt ACL Setting can not show at the ACL Group page
UPDATE `topology` SET `topology_have_child` = '1' WHERE `topology_page` = 1312;

-- no more /tftproot/polycom folder, so no need to sync this folder
DELETE FROM redundancy_sync_dir WHERE path = '/tftproot/polycom/';

-- eric (10032010) : conference join / leave notification
ALTER TABLE `conference` ADD `review` VARCHAR(20) NULL DEFAULT '';
UPDATE `conference` SET `review` = '0';

-- eric (12032010) : create the index of cdr table
ALTER TABLE cdr ADD INDEX USING BTREE (src);
ALTER TABLE cdr ADD INDEX USING BTREE (dst);
ALTER TABLE cdr ADD INDEX USING BTREE (accountcode);

-- eric (16032010) : TCP settings
ALTER TABLE `sippeers` ADD `main_protocol` VARCHAR(20) NULL DEFAULT '';
UPDATE `sippeers` SET `main_protocol` = 'udp';
ALTER TABLE `user_profile` ADD `user_main_protocol` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `user_profile` SET `user_main_protocol` = 'udp';
ALTER TABLE `sipfriends` ADD `main_protocol` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `sipfriends` SET `main_protocol` = 'Profiled';
ALTER TABLE `additional_device` ADD `main_protocol` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `additional_device` SET `main_protocol` = 'udp';

ALTER TABLE cdr ADD INDEX USING BTREE (clid);
ALTER TABLE call_recording_file ADD INDEX USING BTREE (fixed);

-- eric (29032010) : sip peer allow sip info
ALTER TABLE `sippeers` ADD `allowsipinfo` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `sippeers` SET `allowsipinfo` = '0';
-- eric (29032010) : from domain
ALTER TABLE `sippeers` ADD `fromdomain` VARCHAR(80) NULL DEFAULT '';
UPDATE `sippeers` SET `fromdomain` = '';

-- eric (19042010) : default settings for the TOS at the sip.conf
INSERT INTO systemConfigs (`key`, `value`) VALUES ('tos_sip', 'cs3');
INSERT INTO systemConfigs (`key`, `value`) VALUES ('tos_audio', 'ef');
INSERT INTO systemConfigs (`key`, `value`) VALUES ('tos_video', 'af41');
INSERT INTO systemConfigs (`key`, `value`) VALUES ('tos_text', 'af41');
INSERT INTO systemConfigs (`key`, `value`) VALUES ('cos_sip', '3');
INSERT INTO systemConfigs (`key`, `value`) VALUES ('cos_audio', '5');
INSERT INTO systemConfigs (`key`, `value`) VALUES ('cos_video', '4');
INSERT INTO systemConfigs (`key`, `value`) VALUES ('cos_text', '3');

-- new version watchdog
DROP TABLE IF EXISTS `watchdog_config`;
CREATE TABLE `watchdog_config` (
  `id` int(11) NOT NULL auto_increment,
  `timestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  `server_id` int(11) NOT NULL default '0',
  `updated_by` varchar(31) NOT NULL,
  `item` varchar(31) NOT NULL,
  `value` varchar(63) NOT NULL,
  `correctness` varchar(7) NOT NULL,
  `related_services` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
);
INSERT INTO `watchdog_config` VALUES (1,'0000-00-00 00:00:00',1,'frSIP','appliance','no','yes','N/A'),(2,'0000-00-00 00:00:00',1,'frSIP','high_availability','no','yes','Hardware.frSIP_RFS, Hardware.DBsync'),(3,'0000-00-00 00:00:00',1,'frSIP','redundant_supply','no','yes','Hardware.PS'),(4,'0000-00-00 00:00:00',1,'frSIP','server_role','primary','yes','N/A'),(5,'0000-00-00 00:00:00',1,'frSIP','lan_bypass','no','yes','N/A'),(6,'0000-00-00 00:00:00',1,'frSIP','lan_auto_fallback','no','yes','N/A'),(7,'0000-00-00 00:00:00',1,'frSIP','raid','no','yes','Hardware.RAID, Hardware.HDD'),(8,'0000-00-00 00:00:00',1,'frSIP','hd_sn','unknown,unknown','yes','Hardware.RAID, Hardware.HDD'),(9,'0000-00-00 00:00:00',1,'frSIP','fail_tolerance','3','yes','N/A'),(10,'0000-00-00 00:00:00',1,'frSIP','uplink_fail_over','4','yes','N/A'),(11,'0000-00-00 00:00:00',1,'frSIP','platform','software','yes','N/A'),(12,'0000-00-00 00:00:00',1,'frSIP','lan_status','1','yes','N/A');
DROP TABLE IF EXISTS `service_check`;
CREATE TABLE `service_check` (
  id int(11) NOT NULL AUTO_INCREMENT, 
  timestamp datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  server_id int(11) NOT NULL DEFAULT '0',
  service varchar(63) NOT NULL,
  service_code varchar(31) NOT NULL,
  category varchar(15) NOT NULL,
  led varchar(7) NOT NULL,
  status varchar(15) NOT NULL,
  report varchar(7) NOT NULL,
  details varchar(255) NOT NULL,
  debug varchar(127) NOT NULL,
  PRIMARY KEY (id)
);
DROP TABLE IF EXISTS `system_log`;
CREATE TABLE `system_log` (
  id int(11) NOT NULL AUTO_INCREMENT,
  timestamp datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  server_id int(11) NOT NULL DEFAULT '0',
  triggered_by varchar(31) NOT NULL,
  nature varchar(15) NOT NULL,
  service varchar(63) NOT NULL,
  category varchar(15) NOT NULL,
  led varchar(7) NOT NULL,
  details varchar(255) NOT NULL,
  debug varchar(127) NOT NULL,
  PRIMARY KEY (id)
); 

-- delete unwanted, old table
DROP TABLE IF EXISTS `system_check`;
DROP TABLE IF EXISTS `system_err_log`;
DROP TABLE IF EXISTS `appliance_system_log`;
DROP TABLE IF EXISTS `appliance_service_check`;

UPDATE watchdog_config SET related_services = 'Hardware.DBsync' WHERE item = 'high_availability';

ALTER TABLE queue_log CHANGE time time varchar(21) NOT NULL DEFAULT '0000-00-00 00:00:00';

-- 2.8.8
UPDATE admin SET value='2.8.8' WHERE variable='version';

ALTER TABLE queue_log CHANGE time time varchar(21) NOT NULL DEFAULT '0000-00-00 00:00:00';

-- eric (27052010) : call back failed email template
INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ('callBackFailed','','Verdana','10','urgent@deltapath.com','frSIP PBX','Call back request failed','','The system detected the call back request failed.\r\n\r\nDetails:\r\n\r\nCall back number : $callbacknum\r\nDestination number : $destnum\r\nDestination name : $destname\r\nTime : $time\r\nCause of failure : $cause\r\n\r\n','','1');
-- eric (28052010) : Sugar CRM Module
INSERT INTO `topology` VALUES ('','CRM','SugarCRM','SM_CRM_SUGARCRM_TITLE','','07','0711',110,'','','e','l','1','1','1','1');

-- eric (28052010) : Nokia SIP Profile
-- the username data type should be VARCHAR, since the int will truncate the prepend zero
ALTER TABLE `wireless_network_shared_user_relation` CHANGE username username varchar(80) NOT NULL DEFAULT '';

DELETE FROM watchdog_config WHERE item = 'appliance';
DELETE FROM watchdog_config WHERE item = 'platform';
DELETE FROM watchdog_config WHERE item = 'redundant_supply';
DELETE FROM watchdog_config WHERE item = 'lan_bypass';
UPDATE watchdog_config SET value = '3' WHERE item = 'lan_status';
INSERT INTO watchdog_config VALUES ('','0000-00-00 00:00:00',1,'frSIP','model_no','Software','yes','N/A'),('','0000-00-00 00:00:00',1,'frSIP','lan_pair','0','yes','N/A'),('','0000-00-00 00:00:00',1,'frSIP','lan_force_mode','auto','yes','N/A');

ALTER TABLE all_server_info CHANGE ipTakeOver ipTakeOver enum('0', '1', '2', '3');
ALTER TABLE all_server_info ADD internalEth varchar(2);

-- ringtype
ALTER TABLE `book` ADD `ringtype` VARCHAR(35) NOT NULL DEFAULT '';
ALTER TABLE `speedDial_setting` ADD `ringtype` VARCHAR(35) NOT NULL DEFAULT '';
ALTER TABLE `speedDial_contact` ADD `ringtype` VARCHAR(35) NOT NULL DEFAULT '';

UPDATE topology SET topology_have_child='1' WHERE topology_page = '1312';

-- new voicemail option
ALTER TABLE `user_profile` ADD `user_hidefromdir` VARCHAR(10) NOT NULL DEFAULT '';
ALTER TABLE `user_profile` ADD `user_exit_zero` VARCHAR(80) NOT NULL DEFAULT '';
ALTER TABLE `user_profile` ADD `user_exit_star` VARCHAR(80) NOT NULL DEFAULT '';
UPDATE `user_profile` SET `user_hidefromdir` = 'no';

ALTER TABLE `users` ADD `hidefromdir` VARCHAR(10) NOT NULL DEFAULT '';
ALTER TABLE `users` ADD `exit_zero` VARCHAR(80) NOT NULL DEFAULT '';
ALTER TABLE `users` ADD `exit_star` VARCHAR(80) NOT NULL DEFAULT '';
UPDATE `users` SET `hidefromdir` = 'no';
UPDATE `users` SET `hidefromdir` = 'Profiled' WHERE `mailbox` IN (SELECT `username` FROM `sipfriends`);
UPDATE `users` SET `exit_zero` = 'Profiled' WHERE `mailbox` IN (SELECT `username` FROM `sipfriends`);
UPDATE `users` SET `exit_star` = 'Profiled' WHERE `mailbox` IN (SELECT `username` FROM `sipfriends`);

-- ring tone modification
UPDATE `switchboard_user_status_default` SET `options` = REPLACE(options,',tr',',t') WHERE `options` LIKE 'Dial(%' AND `options` LIKE '%,tr%';
UPDATE `switchboard_user_status_callflow` SET `options` = REPLACE(options,',tr',',t') WHERE `options` LIKE 'Dial(%' AND `options` LIKE '%,tr%';
UPDATE `user_profile_routing` SET `ext_options` = REPLACE(ext_options,',tr',',t') WHERE `ext_options` LIKE 'Dial(%' AND `ext_options` LIKE '%,tr%';
UPDATE `extension` SET `options` = REPLACE(options,',tr',',t') WHERE `options` LIKE 'Dial(%' AND `options` LIKE '%,tr%';
-- delete cdr feature
INSERT INTO `topology` VALUES ('','Reporting','cdrDelete','SM_REPORTING_CDRDELETE_TITLE','','1201','120108',80,'','','c','d','5','0','0','1');
-- batch tools for sippeer
INSERT INTO `topology` VALUES ('','Configuration','sipPeerImport','SM_CONFIGURATION_SPIMPORT_TITLE','','1304','130411',54,'','','p','i','7','1','0','1');
INSERT INTO `topology` VALUES ('','Configuration','sipPeerEdit','SM_CONFIGURATION_SPEDIT_TITLE','','1304','130412',57,'','','p','e','7','1','0','1');

DROP TABLE IF EXISTS `system_recovery`;
CREATE TABLE `system_recovery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `server_id` int(5) NOT NULL DEFAULT '0',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `data` int(15) NOT NULL DEFAULT '0',
  `store` int(5) NOT NULL DEFAULT '1',
  `status` int(5) NOT NULL DEFAULT '0',
  `schedule` int(5) NOT NULL DEFAULT '0',
  `hour` int(5) NOT NULL DEFAULT '0',
  `week` int(5) NOT NULL DEFAULT '0',
  `day` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
);

ALTER TABLE `scheduled_task` ADD `parameter` TEXT NULL DEFAULT NULL AFTER `data`;

-- eric (16072010) : support the polycom expansion module
ALTER TABLE `equipment` CHANGE `linenum` `linenum` int(11) NOT NULL;

-- eric (10082010) : added the locationname and address for switchboard
ALTER TABLE `general_group` ADD `locationName` TEXT NOT NULL DEFAULT '';
ALTER TABLE `general_group` ADD `locationAddress` TEXT NOT NULL DEFAULT '';
ALTER TABLE `customer` ADD `locationName` TEXT NOT NULL DEFAULT '';
ALTER TABLE `customer` ADD `locationAddress` TEXT NOT NULL DEFAULT '';

-- eric (16072010) : default no use fqdn for phone config
INSERT INTO `systemConfigs` VALUES ('','phone_config_use_fqdn','0');

-- eric (30072010) : the fqdn for eth
ALTER TABLE `network` ADD `fqdn` VARCHAR(80) NOT NULL DEFAULT '';

-- Quintum Enhancement
ALTER TABLE quintum ADD quintum_mode varchar(50) NOT NULL;
ALTER TABLE quintumport DROP COLUMN quintumport_lineType;
ALTER TABLE quintum ADD quintum_recv_gain int(11) DEFAULT 0;
ALTER TABLE quintum ADD quintum_trans_gain int(11) DEFAULT 0;
ALTER TABLE quintum ADD quintum_qos_type int(11);
ALTER TABLE quintum ADD quintum_qos_val int(11);

-- eric (09092010) : NTP Server and Voicemail Link Address
ALTER TABLE `sipfriends` ADD `ntp_opt` VARCHAR(80) NOT NULL DEFAULT '';
ALTER TABLE `sipfriends` ADD `ntp_opt_custom` VARCHAR(80) NOT NULL DEFAULT '';
ALTER TABLE `user_profile` ADD `user_ntp_opt` VARCHAR(80) NOT NULL DEFAULT '';
ALTER TABLE `user_profile` ADD `user_ntp_opt_custom` VARCHAR(80) NOT NULL DEFAULT '';
UPDATE `user_profile` SET `user_ntp_opt` = '3';

ALTER TABLE `users` ADD `link_address_opt` VARCHAR(80) NOT NULL DEFAULT '';
ALTER TABLE `users` ADD `link_address_opt_value` VARCHAR(80) NOT NULL DEFAULT '';
ALTER TABLE `user_profile` ADD `user_link_address_opt` VARCHAR(80) NOT NULL DEFAULT '';
ALTER TABLE `user_profile` ADD `user_link_address_opt_value` VARCHAR(80) NOT NULL DEFAULT '';
UPDATE `user_profile` SET `user_link_address_opt` = '1';
-- UPDATE `user_profile` SET `user_link_address_opt_value` = (SELECT `value` FROM `admin` WHERE `variable` = 'server_eth' AND `server_id` = '1');

INSERT INTO `systemConfigs` VALUES ('','voicemail_link_address_opt','1');
-- INSERT INTO `systemConfigs` VALUES ('','voicemail_link_address_opt_value',(SELECT `value` FROM `admin` WHERE `variable` = 'server_eth' AND `server_id` = '1'));

-- 2.8.8.5
-- disable miss call related fix
INSERT INTO `polycom_config` (`id`,`model`,`path`,`attribute`,`value`) VALUES ('','all','{DISABLEMISSCALL}', NULL, (SELECT `data` FROM `polycom_setting` WHERE `key` = 'disablemisscall'));
DELETE FROM `polycom_setting` WHERE `key` = 'disablemisscall';

-- eric (20092010) : enhanced the avaya
ALTER TABLE `sippeers` ADD `mwi_on_exten` VARCHAR(30) NOT NULL DEFAULT '';
ALTER TABLE `sippeers` ADD `mwi_off_exten` VARCHAR(30) NOT NULL DEFAULT '';

-- more arguments for queue_log
ALTER TABLE queue_log ADD `arg4` VARCHAR(100) NOT NULL DEFAULT '';
ALTER TABLE queue_log ADD `arg5` VARCHAR(100) NOT NULL DEFAULT '';

-- 2.8.8.5
UPDATE admin SET value='2.8.8.5' WHERE variable='version';

DROP TABLE IF EXISTS `system_recovery`;
CREATE TABLE `system_recovery` (
   `id` int(11) NOT NULL AUTO_INCREMENT,
   `server_id` int(5) NOT NULL DEFAULT '0',
   `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
   `name` varchar(127) NOT NULL DEFAULT '',
   `status` int(5) NOT NULL DEFAULT '0',
   `snapshot` int(5) NOT NULL DEFAULT '0',
   `data` text NOT NULL DEFAULT '',
   `store` int(5) NOT NULL DEFAULT '1',
   `schedule` int(5) NOT NULL DEFAULT '0',
   `hour` int(5) NOT NULL DEFAULT '0',
   `week` int(5) NOT NULL DEFAULT '0',
   `day` int(5) NOT NULL DEFAULT '0',
   `CF_rotate` int(5) NOT NULL DEFAULT '1',
   `RAID_rotate` int(5) NOT NULL DEFAULT '1',
   `FTP_rotate` int(5) NOT NULL DEFAULT '1',
   `ftp_mode` int(5) NOT NULL DEFAULT '1',
   `ftp_info` varchar(255) NOT NULL DEFAULT '',
   `description` text NOT NULL DEFAULT '', 
   PRIMARY KEY (`id`)
);

-- context type
ALTER TABLE context ADD contextType enum('ALL', 'PSTN', 'VoIP') DEFAULT 'ALL';
-- sippeer call type
ALTER TABLE sippeers ADD callType enum('ALL', 'PSTN', 'VoIP') DEFAULT 'ALL';
-- call restrict value
ALTER TABLE `user_profile` ADD `user_call_restrict` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `user_profile` SET `user_call_restrict` = 'no' WHERE `user_call_restrict` = '';
ALTER TABLE `sipfriends` ADD `call_restrict` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `sipfriends` SET `call_restrict` = 'Profiled' WHERE `call_restrict` = '';
ALTER TABLE `additional_device` ADD `call_restrict` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `additional_device` SET `call_restrict` = 'no' WHERE `call_restrict` = '';
-- sippeers
ALTER TABLE `sippeers` ADD `reg_server_option` INT(11) NOT NULL DEFAULT 0 AFTER `password`;
ALTER TABLE `sippeers` ADD `authuser` VARCHAR(80) NULL DEFAULT '' AFTER `password`;
-- DID Range enahncement
ALTER TABLE `did_range` ADD `actual_did_begin` varchar(40) NOT NULL;
ALTER TABLE `did_range` ADD `actual_did_end` varchar(40) NOT NULL;
UPDATE `did_range` SET `actual_did_begin` = `callerid_begin`;
UPDATE `did_range` SET `actual_did_end` = `callerid_end`;
ALTER TABLE queue CHANGE planguage planguage enum('en','en_us','cn','ja');

-- 2.9.0
UPDATE admin SET value='2.9.0' WHERE variable='version';
UPDATE `book` SET `ringtype` = `ringtype` + 2 WHERE `ringtype` > 12;
-- 2.9.1
UPDATE admin SET value='2.9.1' WHERE variable='version';
ALTER TABLE `book` ADD `sms_number` VARCHAR(40) NULL DEFAULT '' AFTER `other_number`;
DROP TABLE IF EXISTS `sms_message`;
CREATE TABLE `sms_message` (
  `id` int(11) NOT NULL auto_increment,
  `timestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  `owner` varchar(80) NOT NULL,
  `sms_recipient` varchar(80) NOT NULL,
  `sms_content` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `topology` VALUES ('','NumberingPlan','CallFilter','SM_NP_CALLFILTER_TITLE','','04','0408',80,'','','f','l','1','1','1','1');
INSERT INTO `topology` VALUES ('','NumberingPlan','CallFilterList','SM_NP_CALLFILTERLIST_TITLE','','0408','040801',10,'','','f','l','1','1','0','1');
INSERT INTO `topology` VALUES ('','NumberingPlan','CallFilterView','SM_NP_CALLFILTERVIEW_TITLE','','0408','040802',20,'','','f','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','NumberingPlan','CallFilterEdit','SM_NP_CALLFILTEREDIT_TITLE','','0408','040803',30,'','','f','m','3','0','0','1');
INSERT INTO `topology` VALUES ('','NumberingPlan','CallFilterAdd','SM_NP_CALLFILTERADD_TITLE','','0408','040804',40,'','','f','a','4','0','0','1');
INSERT INTO `topology` VALUES ('','NumberingPlan','CallFilterDelete','SM_NP_CALLFILTERDELETE_TITLE','','0408','040805',50,'','','f','d','5','0','0','1');

DROP TABLE IF EXISTS `call_filter`;
CREATE TABLE `call_filter` (
  `id` int(11) NOT NULL auto_increment,
  `filter_owner` Varchar(80) NOT NULL,
  `filter_number` Varchar(80) NOT NULL,
  `filter_type` Varchar(2) NOT NULL,
  `filter_callrecording` int(11) NOT NULL,
  `filter_options` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `call_filter_statistic`;
CREATE TABLE `call_filter_statistic` (
  `id` int(11) NOT NULL auto_increment,
  `timestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  `callerid_name` Varchar(80) NOT NULL,
  `callerid_number` Varchar(80) NOT NULL,
  `destination` Varchar(80) NOT NULL,
  `action` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Call Centre Report
UPDATE topology SET topology_show = '1' WHERE topology_page = 1209;
-- add the topology for the Switchboard sms feature
INSERT INTO `topology` VALUES ('','User','UserSwitchBoardSMS','SM_USER_USERSWITCHBOARD_SMS_FEATURE','','01','0115',150,'','','ss','m','11','0','1','1');
-- 2.9.2
UPDATE admin SET value='2.9.2' WHERE variable='version';
ALTER TABLE `call_filter_statistic` ADD COLUMN `owner` VARCHAR(80) NOT NULL DEFAULT '' AFTER `timestamp`;
-- call filter reporting
INSERT INTO `topology` VALUES ('','Reporting','CallFilterReport','SM_REPORTING_CALLFILTERREPORT_TITLE','','12','1210',100,'','','cf','l','1','1','1','1');
INSERT INTO `topology` VALUES ('','Reporting','CallFilterReportView','SM_REPORTING_CALLFILTERREPORT_VIEW_TITLE','','1210','121001',10,'','','cf','v','2','0','0','1');
-- Timezone
ALTER TABLE `user_profile` ADD `user_timezone` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `user_profile` SET `user_timezone` = 'global';
ALTER TABLE `sipfriends` ADD `timezone` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `sipfriends` SET `timezone` = 'Profiled';
ALTER TABLE `additional_device` ADD `timezone` VARCHAR(20) NOT NULL DEFAULT '';
UPDATE `additional_device` SET `timezone` = 'owner';
-- Import AclUser
INSERT INTO `topology` VALUES ('','System','ACLUserImport','SM_SYSTEM_ACL_USER_IMPORT_TITLE','','140102','14010206',60,'','','au','i','7','1','0','1');
-- Express Dial for user mobile
ALTER TABLE `sipfriends` ADD `mobile_express` VARCHAR(80) NOT NULL DEFAULT '';
-- change the session talbe column length
ALTER TABLE `session` CHANGE `current_page` `current_page` VARCHAR(40) NOT NULL;
-- call back feature preserve the previous behaviour
INSERT INTO systemConfigs (`key`, `value`) VALUES ('callbackRingDestFirst','0');
-- default it off since many customer don't want this
INSERT INTO systemConfigs (`key`, `value`) VALUES ('conference_review_name','0');
-- administrator default feature
INSERT INTO `acl_user_features` (`acl_user_id`,`feature`,`data`) VALUES ('1','manager_allow','makeCall');
INSERT INTO `acl_user_features` (`acl_user_id`,`feature`,`data`) VALUES ('1','manager_allow','sendSMS');
INSERT INTO `acl_user_features` (`acl_user_id`,`feature`,`data`) VALUES ('1','manager_allow','getUserDevice');

-- 2.9.3
UPDATE admin SET value='2.9.3' WHERE variable='version';

DROP TABLE IF EXISTS `sip_iphoneClient`;
CREATE TABLE `sip_iphoneClient` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL,
  `sip_username` varchar(50) NOT NULL,
  `sip_password` varchar(50) NOT NULL,
  `UDID` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `topology` VALUES ('','User','iPhoneClient','SM_USER_IPHONECLIENT_TITLE','','01','0116',160,'','','i','l','1','1','1','1');
INSERT INTO `topology` VALUES ('','User','iPhoneClientList','SM_USER_IPHONECLIENTLIST_TITLE','','0116','011601',10,'','','i','l','1','0','0','1');
INSERT INTO `topology` VALUES ('','User','iPhoneClientView','SM_USER_IPHONECLIENTVIEW_TITLE','','0116','011602',20,'','','i','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','User','iPhoneClientEdit','SM_USER_IPHONECLIENTEDIT_TITLE','','0116','011603',30,'','','i','m','3','0','0','1');
INSERT INTO `topology` VALUES ('','User','iPhoneClientAdd','SM_USER_IPHONECLIENTADD_TITLE','','0116','011604',40,'','','i','a','4','0','0','1');
INSERT INTO `topology` VALUES ('','User','iPhoneClientDelete','SM_USER_IPHONECLIENTDELETE_TITLE','','0116','011605',50,'','','i','d','5','0','0','1');
-- Beijing Time zone
-- change the systemConfigs to id first
UPDATE `systemConfigs` SET `value` = (SELECT id FROM `system_time_zone` WHERE `path` = `value`) WHERE `key` = 'time_zone';
INSERT INTO `system_time_zone` VALUES ('','Beijing','Asia/Shanghai','p0800');
-- list equipment topology changes
UPDATE `topology` SET `topology_text` = 'SM_EQUIP_EQLIST_TITLE', `topology_url_opt_m` = 'e', `topology_url_opt_a` = 'l', `topology_actionType` = '1', `topology_show` = '1', `topology_have_child` = '1' WHERE `topology_page` = '0301';
UPDATE `topology` SET `topology_show` = '0' WHERE `topology_page` = '030101';
-- conference limit for user profile used at create user
ALTER TABLE `user_profile` ADD `user_conference_limit` VARCHAR(20) NOT NULL DEFAULT '';
-- statement format strip idd prefix option
ALTER TABLE `ca_statement_format` ADD `format_stripiddprefix` enum('0','1') NOT NULL DEFAULT '1';

-- sippeer related
ALTER TABLE `sippeers` ADD `sessionTimers` INT(11) NOT NULL;
ALTER TABLE `sippeers` ADD `sessionExpires` VARCHAR(10) NOT NULL;
ALTER TABLE `sippeers` ADD `registration_expires` VARCHAR(10) NOT NULL;
ALTER TABLE `sippeers` ADD `inviteRequireAuth` INT(11) NOT NULL;
ALTER TABLE `sippeers` ADD `authRealm` VARCHAR(100) NOT NULL;
ALTER TABLE `sippeers` ADD `ignoresdpversion` VARCHAR(10) NULL;
ALTER TABLE `sippeers` ADD `copyCidNameToNum` VARCHAR(10) NULL;
UPDATE `sippeers` SET `sessionTimers` = 0;
UPDATE `sippeers` SET `inviteRequireAuth` = 0;
UPDATE `sippeers` SET `ignoresdpversion` = 'no';
UPDATE `sippeers` SET `copyCidNameToNum` = 'no';
INSERT INTO `systemConfigs` VALUES ('', 'reserveTxCid', '0');
INSERT INTO `systemConfigs` VALUES ('', 'registertimeout', '20');
INSERT INTO `systemConfigs` VALUES ('', 'sessionTimers', '0');

-- batch tools for call filter
INSERT INTO `topology` VALUES ('','NumberingPlan','CallFilterImport','SM_NP_CALLFILTERIMPORT_TITLE','','0408','040806',60,'','','f','i','7','1','0','1');
INSERT INTO `topology` VALUES ('','NumberingPlan','CallFilterEdit','SM_NP_CALLFILTEREDIT_TITLE','','0408','040807',70,'','','f','e','7','1','0','1');

-- this should be in 2.9.2
ALTER TABLE `conference_owner_relation` DROP `third_party_user_id`;

-- Call Recording enhancement
-- Copy the table
DROP TABLE IF EXISTS `callrecording_call`;
CREATE TABLE `callrecording_call` SELECT * FROM `call_recording_file`;
ALTER TABLE `callrecording_call` CHANGE `id` `id` int(11) NOT NULL auto_increment PRIMARY KEY;
ALTER TABLE `callrecording_call` ADD INDEX USING BTREE (owner);
ALTER TABLE `callrecording_call` CHANGE `owner` `owner` VARCHAR(50) NOT NULL;
ALTER TABLE `callrecording_call` ADD `callername` VARCHAR(50) NOT NULL AFTER `owner`;
ALTER TABLE `callrecording_call` CHANGE `callerID` `callernum` VARCHAR(50) NOT NULL;
ALTER TABLE `callrecording_call` CHANGE `calleeID` `dstnum` VARCHAR(50) NOT NULL;
ALTER TABLE `callrecording_call` DROP `type`;
ALTER TABLE `callrecording_call` ADD `type` VARCHAR(50) NOT NULL AFTER `stoptime`;
ALTER TABLE `callrecording_call` ADD `mediatype` VARCHAR(30) NOT NULL AFTER `type`;
ALTER TABLE `callrecording_call` ADD `duration` TIME NOT NULL AFTER `mediatype`;
ALTER TABLE `callrecording_call` ADD `filename` TEXT NOT NULL AFTER `stoptime`;
ALTER TABLE `callrecording_call` ADD `ondemand` INT(11) DEFAULT 0 AFTER `filename`;

-- default enable all conference call recording
ALTER TABLE `conference` ADD `callrecording` enum('yes','no') DEFAULT 'no';

-- add the call filter name and fix it
ALTER TABLE `call_filter` ADD `filter_name` VARCHAR(50) NOT NULL AFTER `filter_owner`;

-- delete the call recording admin related topology
DELETE FROM `topology` WHERE `topology_modules` = 'callrecordings' AND `topology_page` LIKE '%0902%';

-- call recording archive schedule
INSERT INTO `topology` VALUES ('','CallRecordings','archiveSchedule','SM_CR_ARCHIVESCHEDULE_TITLE','','09','0903',30,'','','s','l','1','1','1','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveScheduleList','SM_CR_ARCHIVESCHEDULELIST_TITLE','','0903','090301',10,'','','s','l','1','0','0','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveScheduleView','SM_CR_ARCHIVESCHEDULEVIEW_TITLE','','0903','090302',20,'','','s','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveScheduleEdit','SM_CR_ARCHIVESCHEDULEEDIT_TITLE','','0903','090303',30,'','','s','m','3','0','0','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveScheduleAdd','SM_CR_ARCHIVESCHEDULEADD_TITLE','','0903','090304',40,'','','s','a','4','0','0','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveScheduleDelete','SM_CR_ARCHIVESCHEDULEDELETE_TITLE','','0903','090305',50,'','','s','d','5','0','0','1');

-- call recording archive
INSERT INTO `topology` VALUES ('','CallRecordings','archive','SM_CR_ARCHIVE_TITLE','','09','0904',40,'','','z','l','1','0','1','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveList','SM_CR_ARCHIVELIST_TITLE','','0904','090401',10,'','','z','l','1','0','0','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveView','SM_CR_ARCHIVEVIEW_TITLE','','0904','090402',20,'','','z','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveEdit','SM_CR_ARCHIVEEDIT_TITLE','','0904','090403',30,'','','z','m','3','0','0','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveAdd','SM_CR_ARCHIVEADD_TITLE','','0904','090404',40,'','','z','a','4','0','0','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveDelete','SM_CR_ARCHIVEDELETE_TITLE','','0904','090405',50,'','','z','d','5','0','0','1');
INSERT INTO `topology` VALUES ('','CallRecordings','archiveDownload','SM_CR_ARCHIVEDOWNLOAD_TITLE','','0904','090406',60,'','','z','x','9','0','0','1');

-- call recording list
UPDATE `topology` SET `topology_url_opt_m` = 'f', `topology_url_opt_a` = 'l', `topology_actionType` = '1', `topology_show` = '1', `topology_have_child` = '1' WHERE `topology_page` = '0901';
UPDATE `topology` SET `topology_show` = '0' WHERE `topology_page` = '090101';

-- archive table
DROP TABLE IF EXISTS `callrecording_archive`;
CREATE TABLE `callrecording_archive` (
  `id` int(11) NOT NULL auto_increment,
  `timestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  `schedule_id` int(11) NOT NULL,
  `filename` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- archive schedule table
DROP TABLE IF EXISTS `callrecording_archive_schedule`;
CREATE TABLE `callrecording_archive_schedule` (
  `id` int(11) NOT NULL auto_increment,
  `schedule_name` Varchar(80) NOT NULL,
  `schedule_desc` TEXT NOT NULL,
  `schedule_interval` Varchar(10) NOT NULL,
  `schedule_interval_time` Varchar(10) NOT NULL,
  `schedule_expire` Varchar(10) NOT NULL,
  `schedule_ftp_status` int(11) NOT NULL,
  `schedule_ftp_address` Varchar(50) NOT NULL,
  `schedule_ftp_username` Varchar(20) NOT NULL,
  `schedule_ftp_password` Varchar(20) NOT NULL,
  `schedule_ftp_directory` Varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- default settings for call recording : general settings
INSERT INTO `systemConfigs` VALUES ('', 'callrecording_expire', '-1');
INSERT INTO `systemConfigs` VALUES ('', 'callrecording_expire_option', '0');
INSERT INTO `systemConfigs` VALUES ('', 'callrecording_video_format', 'mp4');

-- drop the old call recording stuff
ALTER TABLE `callrecording_call` DROP `md5file`;
ALTER TABLE `callrecording_call` DROP `uniqueid`;
ALTER TABLE `callrecording_call` DROP `fixed`;

ALTER TABLE `call_recording` DROP `type`;
ALTER TABLE `call_recording` DROP `email`;
ALTER TABLE `call_recording` DROP `canView`;
ALTER TABLE `call_recording` DROP `canDelete`;

DROP TABLE `call_recording_file`;
DROP TABLE `call_recording_webadmin_member`;

-- Missing the agentgroup generalgroup field for install
ALTER TABLE `agentgroup` ADD `generalgroup` int(11) NOT NULL AFTER `description`;

-- Prepaid
ALTER TABLE ca_call ADD billingaccount_type enum('Postpaid','Prepaid','Wholesale') AFTER billingaccount_name;
UPDATE ca_call set billingaccount_type = 'Postpaid';
DROP TABLE IF EXISTS `ca_transaction`;
CREATE TABLE `ca_transaction` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `time` datetime NOT NULL,
    `billingaccount_type` enum('Postpaid','Prepaid','Wholesale') NOT NULL,
    `billingaccount_code` varchar(100) NOT NULL,
    `call_id` int(11) NOT NULL,
    `amount` float NOT NULL default '0',
    `description` text,
    `username` varchar(100) NOT NULL,
    `balance_remain_ref` float NOT NULL default '0',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- call accounting transaction
INSERT INTO `topology` VALUES ('','CallAccounting','transaction','SM_CA_TRANSACTION_TITLE','','08','0812',120,'','','n','l','1','1','1','1');
INSERT INTO `topology` VALUES ('','CallAccounting','transactionList','SM_CA_TRANSACTIONLIST_TITLE','','0812','081201',10,'','','n','l','1','0','0','1');
INSERT INTO `topology` VALUES ('','CallAccounting','transactionEdit','SM_CA_TRANSACTIONEDIT_TITLE','','0812','081203',30,'','','n','m','3','0','0','1');

-- reporting : report scheduler related, long long time ago...
DELETE FROM `topology` WHERE `topology_page` LIKE '1204%' AND `topology_modules` = 'Reporting;CallAccounting';

-- License
INSERT INTO `topology` VALUES ('','System','License','SM_SYSTEM_LICENSE_TITLE','','14','1407',70,'','','c','v','2','1','1','1');
INSERT INTO `topology` VALUES ('','System','License','SM_SYSTEM_LICENSE_TITLE','','1407','140701',10,'','','c','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','System','License','SM_SYSTEM_LICENSE_TITLE','','1407','140702',20,'','','c','m','3','0','0','1');

-- Call Centre Batch tools
-- agent
INSERT INTO `topology` VALUES ('','CallCentre','CallCentreAgentUserImport','SM_CC_AGENT_USER_IMPORT_TITLE','','1502','150206',60,'','','au','i','7','0','0','1');
INSERT INTO `topology` VALUES ('','CallCentre','CallCentreAgentUserEdit','SM_CC_AGENT_USER_EDIT_TITLE','','1502','150207',70,'','','au','e','7','0','0','1');
-- agent group
INSERT INTO `topology` VALUES ('','CallCentre','CallCentreAgentGroupImport','SM_CC_AGENT_GROUP_IMPORT_TITLE','','1505','150506',60,'','','ag','i','7','0','0','1');
INSERT INTO `topology` VALUES ('','CallCentre','CallCentreAgentGroupEdit','SM_CC_AGENT_GROUP_EDIT_TITLE','','1505','150507',70,'','','ag','e','7','0','0','1');
-- queue
INSERT INTO `topology` VALUES ('','CallCentre','CallCentreQueueImport','SM_CC_QUEUE_IMPORT_TITLE','','1501','150107',70,'','','q','i','7','0','0','1');
INSERT INTO `topology` VALUES ('','CallCentre','CallCentreQueueEdit','SM_CC_QUEUE_EDIT_TITLE','','1501','150108',80,'','','q','e','7','0','0','1');

-- 3.1
UPDATE admin SET value='3.1' WHERE variable='version';
-- set it to standalone frSIP
INSERT INTO admin (`variable`,`value`,`server_id`) VALUES ('frsipType','frSIP',1);
-- SIP Peers checking
ALTER TABLE sippeers ADD `accessible` ENUM('0', '1') NOT NULL DEFAULT '1';
ALTER TABLE sippeers_backup ADD `accessible` ENUM('0', '1') NOT NULL DEFAULT '1';

-- statement format currency symbol
ALTER TABLE ca_statement_format ADD format_currency_symbol VARCHAR(10) DEFAULT '' AFTER `format_stripiddprefix`;
UPDATE ca_statement_format SET format_currency_symbol = '$';

-- Cyberdata Equipment
DROP TABLE IF EXISTS `equipment_cyberdata_settings`;
CREATE TABLE `equipment_cyberdata_settings` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `dialoutextension` varchar(80) NOT NULL,
  `dialoutid` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- sippeer sippeer backup relation
DROP TABLE IF EXISTS `sippeers_sippeers_backup_relation`;
CREATE TABLE `sippeers_sippeers_backup_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `peerID` VARCHAR(30) NOT NULL,
  `backupPeerID` VARCHAR(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `meetme_log`;
CREATE TABLE `meetme_log` (
  `id` int(11) NOT NULL auto_increment,
  `time` varchar(32) NOT NULL default '0000-00-00 00:00:00',
  `room` varchar(100) NOT NULL default '',
  `roomUid` varchar(100) NOT NULL default '',
  `channel` varchar(100) NOT NULL default '',
  `uniqueId` varchar(100) NOT NULL default '',
  `cidNum` varchar(100) NOT NULL default '',
  `cidName` varchar(100) NOT NULL default '',
  `event` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `time` USING BTREE (`time`),
  KEY `room` USING BTREE (`room`),
  KEY `roomUid` USING BTREE (`roomUid`),
  KEY `event` USING BTREE (`event`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `meetme_call`;
CREATE TABLE `meetme_call` (
  `id` int(11) NOT NULL auto_increment,
  `starttime` datetime NOT NULL default '0000-00-00 00:00:00',
  `stoptime` datetime NOT NULL default '0000-00-00 00:00:00',
  `room` varchar(100) NOT NULL default '',
  `roomUid` varchar(100) NOT NULL default '',
  `duration` int(11) NOT NULL default '0',
  `maxParticipant` int(11) NOT NULL default '0',
  `totalTalkTime` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `starttime` USING BTREE (`starttime`),
  KEY `stoptime` USING BTREE (`stoptime`),
  KEY `room` USING BTREE (`room`),
  KEY `roomUid` USING BTREE (`roomUid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Polycom VBP
INSERT INTO `topology` VALUES ('','Configuration','polycomVBP','SM_CONFIGURATION_POLYCOMVBP_TITLE','','13','1313',105,'','','v','l','1','1','1','1');
INSERT INTO `topology` VALUES ('','Configuration','polycomVBPList','SM_CONFIGURATION_POLYCOMVBPLIST_TITLE','','1313','131301',10,'','','v','l','1','1','0','1');
INSERT INTO `topology` VALUES ('','Configuration','polycomVBPView','SM_CONFIGURATION_POLYCOMVBPVIEW_TITLE','','1313','131302',20,'','','v','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','Configuration','polycomVBPEdit','SM_CONFIGURATION_POLYCOMVBPEDIT_TITLE','','1313','131303',30,'','','v','m','3','0','0','1');
INSERT INTO `topology` VALUES ('','Configuration','polycomVBPAdd','SM_CONFIGURATION_POLYCOMVBPADD_TITLE','','1313','131304',40,'','','v','a','4','0','0','1');
INSERT INTO `topology` VALUES ('','Configuration','polycomVBPDelete','SM_CONFIGURATION_POLYCOMVBPDELETE_TITLE','','1313','131305',50,'','','v','d','5','0','0','1');
INSERT INTO `topology` VALUES ('','Configuration','polycomVBPImport','SM_CONFIGURATION_POLYCOMVBPIMPORT_TITLE','','1313','131306',60,'','','v','i','7','1','0','1');
INSERT INTO `topology` VALUES ('','Configuration','polycomVBPEdit','SM_CONFIGURATION_POLYCOMVBPEDIT_TITLE','','1313','131307',70,'','','v','e','7','1','0','1');

DROP TABLE IF EXISTS `polycom_vbp`;
CREATE TABLE `polycom_vbp` (
  `id` INT(11) NOT NULL auto_increment,
  `name` VARCHAR(80) NOT NULL,
  `description` text NOT NULL DEFAULT '', 
  `ip` VARCHAR(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `user_profile` ADD `user_polycom_vbp` Varchar(30) NULL default '';
ALTER TABLE `sipfriends` ADD `polycom_vbp` Varchar(30) NULL default '';
UPDATE `sipfriends` SET `polycom_vbp` = 'Profiled';
ALTER TABLE `quintum` ADD `quintum_polycom_vbp` INT(11) NULL;

-- DNS record
INSERT INTO `topology` VALUES ('','Configuration','dnsRecord','SM_CONFIGURATION_DNSRECORD_TITLE','','13','1314',107,'','','o','l','1','1','1','1');
INSERT INTO `topology` VALUES ('','Configuration','dnsRecordList','SM_CONFIGURATION_DNSRECORDLIST_TITLE','','1314','131401',10,'','','o','l','1','1','0','1');
INSERT INTO `topology` VALUES ('','Configuration','dnsRecordView','SM_CONFIGURATION_DNSRECORDVIEW_TITLE','','1314','131402',20,'','','o','v','2','0','0','1');
INSERT INTO `topology` VALUES ('','Configuration','dnsRecordEdit','SM_CONFIGURATION_DNSRECORDEDIT_TITLE','','1314','131403',30,'','','o','m','3','0','0','1');
INSERT INTO `topology` VALUES ('','Configuration','dnsRecordAdd','SM_CONFIGURATION_DNSRECORDADD_TITLE','','1314','131404',40,'','','o','a','4','0','0','1');
INSERT INTO `topology` VALUES ('','Configuration','dnsRecordDelete','SM_CONFIGURATION_DNSRECORDDELETE_TITLE','','1314','131405',50,'','','o','d','5','0','0','1');
INSERT INTO `topology` VALUES ('','Configuration','dnsRecordImport','SM_CONFIGURATION_DNSRECORDIMPORT_TITLE','','1314','131406',60,'','','o','i','7','1','0','1');
INSERT INTO `topology` VALUES ('','Configuration','dnsRecordEdit','SM_CONFIGURATION_DNSRECORDEDIT_TITLE','','1314','131407',70,'','','o','e','7','1','0','1');
DROP TABLE IF EXISTS `dns_record`;
CREATE TABLE `dns_record` (
  `id` INT(11) NOT NULL auto_increment,
  `eth` VARCHAR(10) NOT NULL,
  `type` VARCHAR(10) NOT NULL,
  `domain` VARCHAR(80) NOT NULL,
  `protocol` VARCHAR(10) NOT NULL,
  `port` VARCHAR(10) NOT NULL,
  `server_id` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
-- eric (28062012) : the fqdn for eth no more here
ALTER TABLE `network` DROP `fqdn`;

-- play call recording permission
INSERT INTO topology VALUES('', 'CallRecordings', 'filePlay', 'SM_CR_FILEPALY_TITLE', '', '0901', '090109', '90', '', '', 'f', 'p', '16', '0', '0', '1');

-- DISA email template
INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ('disaActivate','','Verdana','10','urgent@deltapath.com','frSIP UC','DISA Service Activated','','Dear $name,\r\n\r\nYour DISA Service has been activated.\r\nPlease use your Assoicated Caller Id Number to dial into following number.\r\n\r\nFull DID Number: $fullext\r\nInternal Number: $ext\r\n\r\nAccount Information:\r\nUsername: $username\r\nPassword: $pin\r\nNeed Authentication: $auth\r\nAssociated Caller Id:\r\n$callerid\r\n\r\nThank You.\r\n','','1');

-- change the sippeers table column sync with quintum_name
ALTER TABLE `sippeers` CHANGE `peerID` `peerID` varchar(200) NOT NULL;

-- E911
/* ----- eemergency ----- */
DROP TABLE IF EXISTS `eemergency_ip_location_mapping`;
CREATE TABLE `eemergency_ip_location_mapping` (
  `id` INT(11) NOT NULL auto_increment,
  `ip` TEXT NOT NULL,
  `location` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `eemergency_security_team`;
CREATE TABLE `eemergency_security_team` (
  `id` INT(11) NOT NULL auto_increment,
  `name` VARCHAR(80) NOT NULL,
  `desc` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `eemergency_security_team_member`;
CREATE TABLE `eemergency_security_team_member` (
  `id` INT(11) NOT NULL auto_increment,
  `teamId` INT(11) NOT NULL,
  `username` VARCHAR(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `eemergency_number`;
CREATE TABLE `eemergency_number` (
  `id` INT(11) NOT NULL auto_increment,
  `number` VARCHAR(80) NOT NULL,
  `name` VARCHAR(80) NOT NULL,
  `desc` TEXT NOT NULL,
  `context` VARCHAR(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `eemergency_number_ring_security_team`;
CREATE TABLE `eemergency_number_ring_security_team` (
  `id` INT(11) NOT NULL auto_increment,
  `numberId` INT(11) NOT NULL,
  `teamId` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `eemergency_number_ring_sippeer`;
CREATE TABLE `eemergency_number_ring_sippeer` (
  `id` INT(11) NOT NULL auto_increment,
  `numberId` INT(11) NOT NULL,
  `peerId` VARCHAR(30) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ('emergencyCall','','Verdana','10','urgent@deltapath.com','frSIP UC','Emergency Call','','The system detected the emergency call has been made.\r\n\r\nDetails:\r\n\r\nTime : $time\r\nEmergency number : $enumber\r\nCaller Id : $calleridnum <$calleridname>\r\nLocation : $location\r\n\r\n','','1');

/* ----- debug job list ----- */
DROP TABLE IF EXISTS `debug_process`;
CREATE TABLE `debug_process` (
  `id` INT(11) NOT NULL auto_increment,
  `serverId` INT(11) NOT NULL,
  `username` VARCHAR(80) NOT NULL,
  `starttime` datetime NOT NULL,
  `stoptime` datetime NOT NULL,
  `processId` INT(11) NOT NULL,
  `processType` VARCHAR(40) NOT NULL,
  `parameter` TEXT NULL DEFAULT '',
  `processing` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- sippeers
ALTER TABLE sippeers ADD routingMethod enum('PSTN', 'VoIP', 'VoIP+PSTN', 'PSTN+VoIP') NOT NULL DEFAULT 'VoIP+PSTN';

DROP TABLE IF EXISTS `sippeers_number_range`;
CREATE TABLE `sippeers_number_range` (
  `id` INT(11) NOT NULL auto_increment,
  `peerID` VARCHAR(30) NOT NULL,
  `range_type` VARCHAR(30) NOT NULL,
  `range_begin` VARCHAR(40) NOT NULL,
  `range_end` VARCHAR(40) NOT NULL,
  `internal_exten_range_begin` VARCHAR(40) NOT NULL,
  `internal_exten_range_end` VARCHAR(40) NOT NULL,
  `callerid_range_begin` VARCHAR(40) NOT NULL,
  `callerid_range_end` VARCHAR(40) NOT NULL,
  `defaultcallerid_num` VARCHAR(40) NOT NULL,
  `defaultcallerid_name` VARCHAR(40) NOT NULL,
  `range_context` VARCHAR(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DELETE FROM sippeers_number_range WHERE `range_begin` = '' AND `range_end` = '';

UPDATE sippeers_number_range SET defaultcallerid_num = (SELECT `value` FROM systemConfigs WHERE `key` = 'defaultcallerid_num') WHERE `callerid_range_begin` != '' AND `callerid_range_end` != '';
UPDATE sippeers_number_range SET defaultcallerid_name = (SELECT `value` FROM systemConfigs WHERE `key` = 'defaultcallerid_name') WHERE `callerid_range_begin` != '' AND `callerid_range_end` != '';

DROP TABLE IF EXISTS `sippeers_sippeers_number_range_relation`;
CREATE TABLE `sippeers_sippeers_number_range_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `peerID` VARCHAR(200) NOT NULL,
  `range_id` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE sippeers_number_range CHANGE peerID peerID varchar(200) NOT NULL;

-- new numbering plan
-- idd prefix
ALTER TABLE `ca_iddprefix` ADD `iddprefix_context` VARCHAR(80) NOT NULL DEFAULT '';
UPDATE `ca_iddprefix` SET `iddprefix_context` = (SELECT `context` FROM `extension` WHERE `system` = 'idd' AND `priority` = 1 AND `ext` = CONCAT('_',ca_iddprefix.iddprefix_prefix,'Z.'));

/* ----- time slot ----- */
DROP TABLE IF EXISTS `time_slot`;
CREATE TABLE `time_slot` (
  `id` INT(11) NOT NULL auto_increment,
  `slot_name` VARCHAR(80) NOT NULL,
  `slot_desc` TEXT NOT NULL,
  /* number.id, group.groupID, global */
  `slot_owner` VARCHAR(80) NOT NULL,
  `slot_owner_type` VARCHAR(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `time_slot_detail`;
CREATE TABLE `time_slot_detail` (
  `id` INT(11) NOT NULL auto_increment,
  `time_slot_id` INT(11) NOT NULL,
  `detail_name` VARCHAR(80) NOT NULL,
  `detail_desc` TEXT NOT NULL,
  `time` VARCHAR(20) NOT NULL DEFAULT '',
  `week` VARCHAR(20) NOT NULL DEFAULT '',
  `day` VARCHAR(10) NOT NULL DEFAULT '',
  `month` VARCHAR(10) NOT NULL DEFAULT '',
  `once_start` datetime NOT NULL,
  `once_end` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*
DROP TRIGGER IF EXISTS `time_slot_deletecascade`;
delimiter //
CREATE TRIGGER `time_slot_deletecascade` AFTER DELETE ON `time_slot`
FOR EACH ROW BEGIN
    DELETE FROM `time_slot_detail` WHERE `time_slot_id` = old.id;
END;
//
delimiter ;
*/

/* ----- number ----- */
DROP TABLE IF EXISTS `number`;
CREATE TABLE `number` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(80) NOT NULL DEFAULT '',
  `number_name` VARCHAR(80) NOT NULL DEFAULT '',
  `number_desc` TEXT NOT NULL DEFAULT '',
  `callerid_matching` VARCHAR(80) NOT NULL DEFAULT '',
  /* Extension (User) / Number ? */
  `type` VARCHAR(20) NOT NULL DEFAULT 'Extension',
  `group` VARCHAR(100) NOT NULL DEFAULT '',
  `context` VARCHAR(80) NOT NULL DEFAULT '',
  /* call recording module, need to store the 'Profiled' */
  `call_recording` VARCHAR(80) NOT NULL DEFAULT '',
  /* call accounting module */
  `idd_account` VARCHAR(80) NOT NULL DEFAULT '',
  /* FIXME (need this?) : is the Extension using profile? */
  /*`useprofile` INT(11) NOT NULL,*/
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `number_detail`;
CREATE TABLE `number_detail` (
  `id` INT(11) NOT NULL auto_increment,
  /* number.id */
  `number_id` INT(11) NOT NULL,
  /* number_status.id */
  `number_status_id` INT(11) NOT NULL,
  /* time_slot.id */
  `time_slot_id` INT(11) NOT NULL,
  /* sort priority better than check the overlap */
  `time_slot_priority` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `number_status`;
CREATE TABLE `number_status` (
  `id` INT(11) NOT NULL auto_increment,
  /* number.id, avoid the same number with difference context */
  `number_id` INT(11) NOT NULL,
  `status_name` VARCHAR(80) NOT NULL,
  `status_desc` TEXT NOT NULL DEFAULT '',
  /* marked as default or not, since when there is no time slot matched, we still need to have a routing */
  /* XXX : now able to set no default routing */
  /* we may also need to separate the status added by user / system (like the old My status / Admin define status / etc.) */
  /* default, user, profile */
  `status_type` VARCHAR(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `number_status_routing`;
CREATE TABLE `number_status_routing` (
  `id` INT(11) NOT NULL auto_increment,
  /* number_status.id */
  `number_status_id` INT(11) NOT NULL,
  `routing_name` VARCHAR(80) NOT NULL,
  `routing_desc` TEXT NOT NULL DEFAULT '',
  /* condition routing : default? device status? etc. */
  `routing_type` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `number_status_routing_step`;
CREATE TABLE `number_status_routing_step` (
  `id` INT(11) NOT NULL auto_increment,
  /* number_status_routing.id */
  `number_status_routing_id` INT(11) NOT NULL,
  /* like priority */
  `step` INT(11) NOT NULL DEFAULT '1',
  /* store the type of the step, since something like Dial(XXX) may custom input */
  /* value : custom, voicemail, call (Dial, Queue), misc (MusicOnHold, Playback), redirect (Goto), etc. */
  `step_type` VARCHAR(30) NOT NULL,
  /* Dial, Queue, MusicOnHold, Voicemail, etc. */
  `app` VARCHAR(20) NOT NULL,
  /* SIP/XXX, 8160,u, etc. */
  `parameter` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `number_status_routing_step_detail`;
CREATE TABLE `number_status_routing_step_detail` (
  `id` INT(11) NOT NULL auto_increment,
  /* number_status_routing_step.id */
  `number_status_routing_step_id` INT(11) NOT NULL,
  /* General Line */
  `follow_customer_setting` VARCHAR(10) NOT NULL,
  `greeting` TEXT NOT NULL DEFAULT '',
  `instruction` TEXT NOT NULL DEFAULT '',
  /* Caller Id Mod */
  /* Caller Id Number */
  `callerid_num` INT(11) NOT NULL,
  `callerid_num_mod` VARCHAR(80) NOT NULL,
  `callerid_num_strip` VARCHAR(10) NOT NULL,
  `callerid_num_revert` INT(11) NOT NULL,
  /* Caller Id Name */
  `callerid_name` INT(11) NOT NULL,
  `callerid_name_mod` VARCHAR(80) NOT NULL,
  `callerid_name_strip` VARCHAR(10) NOT NULL,
  `callerid_name_revert` INT(11) NOT NULL,
  /* fax */
  /* no need, stored to parameter
  `fax_rec_name` VARCHAR(80) NULL DEFAULT '',
  `fax_rec_email` VARCHAR(500) NULL DEFAULT '',
  */
  /* maybe more field need to add here */
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `number_status_override`;
CREATE TABLE `number_status_override` (
  `id` INT(11) NOT NULL auto_increment,
  `number_id` INT(11) NOT NULL,
  `number_status_id` INT(11) NOT NULL,
  /* auto expire, manual expire */
  `expire_option` INT(11) NOT NULL,
  /* start */
  `valid_from` datetime NOT NULL,
  /* end */
  `valid_to` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*
DROP TRIGGER IF EXISTS `number_deletecascade`;
delimiter //
CREATE TRIGGER `number_deletecascade` AFTER DELETE ON `number`
FOR EACH ROW BEGIN
    DELETE FROM `number_status` WHERE `number_id` = old.id;
    DELETE FROM `number_detail` WHERE `number_id` = old.id;
    DELETE FROM `number_status_override` WHERE `number_id` = old.id;
END;
//
delimiter ;

DROP TRIGGER IF EXISTS `number_status_deletecascade`;
delimiter //
CREATE TRIGGER `number_status_deletecascade` AFTER DELETE ON `number_status`
FOR EACH ROW BEGIN
    DELETE FROM `number_detail` WHERE `number_status_id` = old.id;
    DELETE FROM `number_status_routing` WHERE `number_status_id` = old.id;
    DELETE FROM `number_status_override` WHERE `number_status_id` = old.id;
END;
//
delimiter ;

DROP TRIGGER IF EXISTS `number_status_routing_deletecascade`;
delimiter //
CREATE TRIGGER `number_status_routing_deletecascade` AFTER DELETE ON `number_status_routing`
FOR EACH ROW BEGIN
    DELETE FROM `number_status_routing_step` WHERE `number_status_routing_id` = old.id;
END;
//
delimiter ;

DROP TRIGGER IF EXISTS `number_status_routing_step_deletecascade`;
delimiter //
CREATE TRIGGER `number_status_routing_step_deletecascade` AFTER DELETE ON `number_status_routing_step`
FOR EACH ROW BEGIN
    DELETE FROM `number_status_routing_step_detail` WHERE `number_status_routing_step_id` = old.id;
END;
//
delimiter ;
*/

-- user profile
ALTER TABLE user_profile CHANGE user_extraOwnCPE user_extraOwnCPE varchar(100) NOT NULL;
UPDATE user_profile SET user_extraOwnCPE = 'Others' WHERE user_extraOwnCPE = '1';

-- User Profile Number Status
DROP TABLE IF EXISTS `profile_number_detail`;
CREATE TABLE `profile_number_detail` (
  `id` INT(11) NOT NULL auto_increment,
  `profile_id` INT(11) NOT NULL,
  `profile_number_status_id` INT(11) NOT NULL,
  `time_slot_id` INT(11) NOT NULL,
  `time_slot_priority` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profile_number_status`;
CREATE TABLE `profile_number_status` (
  `id` INT(11) NOT NULL auto_increment,
  `profile_id` INT(11) NOT NULL,
  `status_name` VARCHAR(80) NOT NULL,
  `status_desc` TEXT NOT NULL DEFAULT '',
  `status_type` VARCHAR(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profile_number_status_routing`;
CREATE TABLE `profile_number_status_routing` (
  `id` INT(11) NOT NULL auto_increment,
  `profile_number_status_id` INT(11) NOT NULL,
  `routing_name` VARCHAR(80) NOT NULL,
  `routing_desc` TEXT NOT NULL DEFAULT '',
  `routing_type` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profile_number_status_routing_step`;
CREATE TABLE `profile_number_status_routing_step` (
  `id` INT(11) NOT NULL auto_increment,
  `profile_number_status_routing_id` INT(11) NOT NULL,
  `step` INT(11) NOT NULL DEFAULT '1',
  `step_type` VARCHAR(30) NOT NULL,
  `app` VARCHAR(20) NOT NULL,
  `parameter` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `profile_number_status_routing_step_detail`;
CREATE TABLE `profile_number_status_routing_step_detail` (
  `id` INT(11) NOT NULL auto_increment,
  `profile_number_status_routing_step_id` INT(11) NOT NULL,
  `follow_customer_setting` VARCHAR(10) NOT NULL,
  `greeting` TEXT NOT NULL DEFAULT '',
  `instruction` TEXT NOT NULL DEFAULT '',
  `callerid_num` INT(11) NOT NULL,
  `callerid_num_mod` VARCHAR(80) NOT NULL,
  `callerid_num_strip` VARCHAR(10) NOT NULL,
  `callerid_num_revert` INT(11) NOT NULL,
  `callerid_name` INT(11) NOT NULL,
  `callerid_name_mod` VARCHAR(80) NOT NULL,
  `callerid_name_strip` VARCHAR(10) NOT NULL,
  `callerid_name_revert` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- user profile number context
ALTER TABLE user_profile ADD user_number_context varchar(80) NOT NULL;
UPDATE user_profile up SET user_number_context = (SELECT ext_context FROM user_profile_routing WHERE ext_priority = 1 AND profile_id = up.id);


-- merged frSIP Cloud
/* ----- frsip server ----- */
DROP TABLE IF EXISTS `frsip_server`;
CREATE TABLE `frsip_server` (
  `id` INT(11) NOT NULL auto_increment,
  `serverName` VARCHAR(100) NOT NULL,
  `serverAddress` VARCHAR(255) NOT NULL,
  `serverType` INT(11) NOT NULL,
  `backupServerId` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/* ----- frsip server ----- */
DROP TABLE IF EXISTS `user_frsip_server_relation`;
CREATE TABLE `user_frsip_server_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `username` VARCHAR(80) NOT NULL,
  `serverId` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `conference_frsip_server_relation`;
CREATE TABLE `conference_frsip_server_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `conference` VARCHAR(80) NOT NULL,
  `serverId` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `callpickupgroup_frsip_server_relation`;
CREATE TABLE `callpickupgroup_frsip_server_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `callpickupgroup` INT(11) NOT NULL,
  `serverId` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `mailbox_frsip_server_relation`;
CREATE TABLE `mailbox_frsip_server_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `mailbox` VARCHAR(80) NOT NULL,
  `serverId` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE sippeers ADD frsipCRE enum('0', '1') NOT NULL DEFAULT '0';
ALTER TABLE sippeers ADD frsipCP enum('0', '1') NOT NULL DEFAULT '0';
ALTER TABLE sippeers ADD quintumId INT(11) NOT NULL AFTER `frsipCP`;

-- missing
DROP TABLE IF EXISTS `remote_frsip_server_user`;
CREATE TABLE `remote_frsip_server_user` (
  `id` INT(11) NOT NULL auto_increment,
  `username` VARCHAR(80) NOT NULL,
  `context` VARCHAR(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `systemNumber`;
CREATE TABLE `systemNumber` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(80) NOT NULL DEFAULT '',
  `type` VARCHAR(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE systemNumber ADD original INT(11) NOT NULL;

-- Cloud Settings
DROP TABLE IF EXISTS `cloudSettings`;
CREATE TABLE `cloudSettings` (
  `id` int(11) NOT NULL auto_increment,
  `key` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO cloudSettings (`id`,`key`,`value`) VALUES ('','canreinvite','1'),('','context','everythingenabled'),('','nat','0'),('','allowcodec',(SELECT `value` FROM systemConfigs WHERE `key` = 'userDefaultCodec')),('','qualify','1'),('','insecure','no'),('','dtmfmode','rfc2833'),('','username',''),('','password',''),('','authuser',''),('','reg_server_option','0'),('','promiscredir','1'),('','callType','ALL'),('','main_protocol','udp'),('','allowsipinfo','1'),('','fromdomain',''),('','sessionTimers','0'),('','sessionExpires',''),('','registration_expires',''),('','inviteRequireAuth','0'),('','authRealm',''),('','ignoresdpversion','no'),('','copyCidNameToNum','no'),('','routingMethod','VoIP');

DROP TABLE IF EXISTS `sip_realPresenceMobile`;
CREATE TABLE `sip_realPresenceMobile` (
    `id` int(11) NOT NULL auto_increment,
    `username` varchar(100) NOT NULL,
    `sip_username` varchar(100) NOT NULL,
    `sip_password` varchar(100) NOT NULL,
    `serial_number` varchar(200) NOT NULL,
    `su_attach` varchar(50) NOT NULL,
    PRIMARY KEY  (`id`),
    UNIQUE KEY `su_attach` (`su_attach`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- SSL Settings
DROP TABLE IF EXISTS `sslSettings`;
CREATE TABLE `sslSettings` (
  `id` int(11) NOT NULL auto_increment,
  `key` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
-- manual config line type
ALTER TABLE `frsip_server` ADD `ssl_common_name` VARCHAR(80) NULL DEFAULT '';

DROP TABLE IF EXISTS `debugprocess_frsip_server_relation`;
CREATE TABLE `debugprocess_frsip_server_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `debugprocess` INT(11) NOT NULL,
  `serverId` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE sip_iphoneClient ADD last_login datetime NOT NULL;
ALTER TABLE sip_realPresenceMobile ADD last_login datetime NOT NULL;

DROP TABLE IF EXISTS `faxDocument`;
CREATE TABLE `faxDocument` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL,
  `desc` text NULL default '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS`fax_log`;
CREATE TABLE `fax_log` (
  `id` int(11) NOT NULL auto_increment,
  `time` varchar(32) NOT NULL default '0000-00-00 00:00:00',
  `owner` varchar(100) NOT NULL default '',
  `status` varchar(10) NOT NULL default '',
  `destination` varchar(100) NOT NULL default '',
  `fax_document` INT(11) NULL,
  `document_name` varchar(100) NOT NULL default '',
  `pages` INT(11) NULL,
  `data` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`),
  KEY `time` USING BTREE (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `sip_frsipMobile`;
CREATE TABLE `sip_frsipMobile` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(50) NOT NULL,
  `sip_username` varchar(50) NOT NULL,
  `sip_password` varchar(50) NOT NULL,
  `UDID` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE sip_frsipMobile ADD device varchar(50) NOT NULL default '';
ALTER TABLE sip_frsipMobile ADD last_login datetime NOT NULL default '0000-00-00 00:00:00';
DROP TABLE sip_iphoneClient;

-- rename the manager API feature iphoneClientLogin to frsipMobileLogin
UPDATE `acl_user_features` SET `data` = 'frsipMobileLogin' WHERE `data` = 'iphoneClientLogin';
-- queue language missing the cn_hk
ALTER TABLE queue CHANGE planguage planguage enum('en','en_us','cn','ja','cn_hk');

-- acl group permission
ALTER TABLE acl_group ADD default_permission varchar(20) NOT NULL;
UPDATE acl_group SET default_permission = 'deny';
UPDATE acl_group SET default_permission = 'allow' WHERE `name` = 'Super Users';
DROP TABLE IF EXISTS `acl_group_permission`;
CREATE TABLE `acl_group_permission` (
  `id` int(11) NOT NULL auto_increment,
  `acl_group_id` INT(11) NOT NULL,
  `priority` INT(11) NOT NULL,
  `access` varchar(20) NOT NULL default 'deny',
  `module` varchar(50) NOT NULL default '',
  `category` varchar(100) NOT NULL default '',
  `action` TEXT NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
-- default for the pre-exist acl group 'Users'
INSERT INTO `acl_group_permission` (`id`,`acl_group_id`,`priority`,`access`,`module`,`category`,`action`) VALUES ('',(SELECT id FROM acl_group WHERE name = 'Users'),1,'allow','User','User-User','view;edit'),('',(SELECT id FROM acl_group WHERE name = 'Users'),2,'allow','Phonebook','Phonebook-PersonalPhonebook','view;add;edit;delete'),('',(SELECT id FROM acl_group WHERE name = 'Users'),3,'allow','NumberingPlan','NumberingPlan-Number','view;edit'),('',(SELECT id FROM acl_group WHERE name = 'Users'),4,'allow','NumberingPlan','NumberingPlan-TimeSlot','view;add;edit;delete'),('',(SELECT id FROM acl_group WHERE name = 'Users'),5,'allow','Conference','Conference-Conference','view;add;edit;delete');

DROP TABLE IF EXISTS `queue_call`;
CREATE TABLE `queue_call` (
  `id` int(11) NOT NULL auto_increment,
  `starttime` datetime NOT NULL default '0000-00-00 00:00:00',
  `stoptime` datetime NOT NULL default '0000-00-00 00:00:00',
  `callid` varchar(20) NOT NULL default '',
  `queue` varchar(20) NOT NULL default '',
  `agent` varchar(20) NOT NULL default '',
  `callerid` varchar(100) NOT NULL default '',
  `holdtime` int(11) NOT NULL default '0',
  `calltime` int(11) NOT NULL default '0',
  `duration` int(11) NOT NULL default '0',
  `reason` TEXT NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `starttime` USING BTREE (`starttime`),
  KEY `stoptime` USING BTREE (`stoptime`),
  KEY `callid` USING BTREE (`callid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- new email template for sip trunk up/down
INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ('sipTrunkRecover','','Verdana','10','urgent@deltapath.com','frSIP UC','SIP Trunk Recover','','NOTICE!\n\nfrSIP UC detected the SIP Trunk recovered and now accessible!\n\nSite: $site\nSIP Trunk: $sipTrunk\nTime: $date\n\nThis message generated by frSIP UC.','','1'),('sipTrunkAlarm','','Verdana','10','urgent@deltapath.com','frSIP UC','SIP Trunk Alarm','','NOTICE!\n\nfrSIP UC detected the SIP Trunk not accessible!\n\nSite: $site\nSIP Trunk: $sipTrunk\nTime: $date\n\nThis message generated by frSIP UC.','','1');

-- change the host field in sippeers since 30 not enough to fill the non-ip address
ALTER TABLE `sippeers` CHANGE `host` `host` varchar(100) NOT NULL default '';
ALTER TABLE `sippeers_backup` CHANGE `host` `host` varchar(100) NOT NULL default '';

-- missing this
ALTER TABLE `sippeers_backup` CHANGE `peerID` `peerID` varchar(200) NOT NULL;

-- default the fax codec to T.38
INSERT INTO systemConfigs (`key`, `value`) VALUES ('faxCodec', 't38');

-- audiocodes gateway
DROP TABLE IF EXISTS `audiocodes_gateway`;
CREATE TABLE `audiocodes_gateway` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(200) NOT NULL,
  `description` text,
  `model` varchar(30) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `mac` varchar(20) NOT NULL,
  `serial` varchar(100) NOT NULL,
  `voice_codec` varchar(50) NOT NULL,
  `sip_server` varchar(30) NOT NULL,
  `parameters` text NOT NULL,
  `survivability` varchar(20) NOT NULL,
  `mode` varchar(50) NOT NULL,
  `recv_gain` int(11) DEFAULT 0,
  `trans_gain` int(11) DEFAULT 0,
  `qos_type` int(11),
  `qos_val` int(11),
  `polycom_vbp` INT(11) NULL,
  PRIMARY KEY  (`id`),
  KEY `name_index` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `audiocodes_gateway_analogue_user`;
CREATE TABLE `audiocodes_gateway_analogue_user` (
  `id` int(11) NOT NULL auto_increment,
  `audiocodes_gateway_id` int(11) NOT NULL,
  `audiocodes_gateway_port` varchar(5) NOT NULL,
  `username` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `username` USING BTREE (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `audiocodes_gateway_survivability_user`;
CREATE TABLE `audiocodes_gateway_survivability_user` (
  `id` int(11) NOT NULL auto_increment,
  `audiocodes_gateway_id` int(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `username` USING BTREE (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `audiocodes_gateway_port`;
CREATE TABLE `audiocodes_gateway_port` (
  `id` int(11) NOT NULL auto_increment,
  `audiocodes_gateway_id` int(11) NOT NULL,
  `audiocodes_gateway_port_name` varchar(200) NOT NULL,
  `audiocodes_gateway_port_type` varchar(10) NOT NULL,
  `audiocodes_gateway_port_number` int(11) default NULL,
  `audiocodes_gateway_port_parameters` text NOT NULL,
  `audiocodes_gateway_port_mapping` text NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `audiocodes_gateway_id` (`audiocodes_gateway_id`),
  KEY `name_index` (`audiocodes_gateway_port_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE sippeers ADD audiocodesId INT(11) NOT NULL AFTER `quintumId`;
ALTER TABLE `sippeers` ADD `t38FaxMaxDatagram` Varchar(10) NULL default '';

DROP TABLE IF EXISTS `trunkSettings`;
CREATE TABLE `trunkSettings` (
  `id` int(11) NOT NULL auto_increment,
  `key` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `trunkSettings_number_range`;
CREATE TABLE `trunkSettings_number_range` (
  `id` INT(11) NOT NULL auto_increment,
  `range_type` VARCHAR(30) NOT NULL,
  `range_begin` VARCHAR(40) NOT NULL,
  `range_end` VARCHAR(40) NOT NULL,
  `internal_exten_range_begin` VARCHAR(40) NOT NULL,
  `internal_exten_range_end` VARCHAR(40) NOT NULL,
  `callerid_range_begin` VARCHAR(40) NOT NULL,
  `callerid_range_end` VARCHAR(40) NOT NULL,
  `defaultcallerid_num` VARCHAR(40) NOT NULL,
  `defaultcallerid_name` VARCHAR(40) NOT NULL,
  `range_context` VARCHAR(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `callerid_manipulation`;
CREATE TABLE `callerid_manipulation` (
  `id` INT(11) NOT NULL auto_increment,
  `manipulation_type` VARCHAR(10) NOT NULL,
  `internal_exten_range_begin` VARCHAR(40) NOT NULL,
  `internal_exten_range_end` VARCHAR(40) NOT NULL,
  `callerid_strip` VARCHAR(40) NOT NULL,
  `callerid_prepend` VARCHAR(40) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `users_callerid_manipulation_relation`;
CREATE TABLE `users_callerid_manipulation_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `manipulation_id` INT(11) NOT NULL,
  `username` VARCHAR(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `sippeers_callerid_manipulation_relation`;
CREATE TABLE `sippeers_callerid_manipulation_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `manipulation_id` INT(11) NOT NULL,
  `peerID` VARCHAR(200) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `outbound_routing`;
CREATE TABLE `outbound_routing` (
  `id` INT(11) NOT NULL auto_increment,
  `number` VARCHAR(80) NOT NULL,
  `context` VARCHAR(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `outbound_routing_step`;
CREATE TABLE `outbound_routing_step` (
  `id` INT(11) NOT NULL auto_increment,
  `routing_id` INT(11) NOT NULL,
  `step` INT(11) NOT NULL,
  `peer_type` INT(11) NOT NULL,
  `digit_strip` VARCHAR(40) NOT NULL,
  `digit_prepend` VARCHAR(40) NOT NULL,
  `callerid_handling` VARCHAR(10) NOT NULL,
  `callerid_rewrite` VARCHAR(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `outbound_routing_step_peer`;
CREATE TABLE `outbound_routing_step_peer` (
  `id` INT(11) NOT NULL auto_increment,
  `step_id` INT(11) NOT NULL,
  `peerID` VARCHAR(200) NOT NULL,
  `priority` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `pstn_outbound_routing`;
CREATE TABLE `pstn_outbound_routing` (
  `id` INT(11) NOT NULL auto_increment,
  `country_code` VARCHAR(10) NOT NULL,
  `countryarea_calling_code` VARCHAR(20) NOT NULL,
  `context` VARCHAR(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `pstn_outbound_routing_step`;
CREATE TABLE `pstn_outbound_routing_step` (
  `id` INT(11) NOT NULL auto_increment,
  `routing_type` INT(11) NOT NULL,
  `step` INT(11) NOT NULL,
  `peer_type` INT(11) NOT NULL,
  `strip_method` INT(11) NOT NULL,
  `digit_prepend` VARCHAR(40) NOT NULL,
  `callerid_handling` VARCHAR(10) NOT NULL,
  `callerid_rewrite` VARCHAR(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `pstn_outbound_routing_step_peer`;
CREATE TABLE `pstn_outbound_routing_step_peer` (
  `id` INT(11) NOT NULL auto_increment,
  `step_id` INT(11) NOT NULL,
  `peerID` VARCHAR(200) NOT NULL,
  `priority` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `ivrinfo` CHANGE `ivr` `ivr` VARCHAR(100) NOT NULL DEFAULT '';
ALTER TABLE `ivr_option` CHANGE `option_ivr` `option_ivr` VARCHAR(100) NOT NULL DEFAULT '';

DROP TABLE IF EXISTS `paginggroup_frsip_server_relation`;
CREATE TABLE `paginggroup_frsip_server_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `paginggroup` INT(11) NOT NULL,
  `serverId` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

ALTER TABLE `sippeers_sippeers_backup_relation` CHANGE `peerID` `peerID` varchar(200) NOT NULL;
ALTER TABLE `sippeers_sippeers_backup_relation` CHANGE `backupPeerID` `backupPeerID` varchar(200) NOT NULL;

ALTER TABLE `pstn_outbound_routing` ADD `international_prefix` Varchar(20) NULL default '';
ALTER TABLE `pstn_outbound_routing` ADD `domestic_prefix` Varchar(20) NULL default '';
ALTER TABLE `pstn_outbound_routing` ADD `local_prefix` Varchar(20) NULL default '';
ALTER TABLE `pstn_outbound_routing` ADD `idd_prefix` INT(11) NOT NULL;
ALTER TABLE `pstn_outbound_routing` ADD `use_callaccounting` INT(11) NOT NULL;

-- still missing the changes for sippeers group
ALTER TABLE `sippeer_group_sippeers_relation` CHANGE `sippeer_id` `sippeer_id` varchar(200) NOT NULL;

-- Missing Group values in frsip
ALTER TABLE `receptionistgroup` ADD `generalgroup` int(11) NOT NULL AFTER `description`;
ALTER TABLE `sound_prompt` ADD `prompt_group` int(11) NOT NULL;
ALTER TABLE `faxDocument` ADD `group` int(11) NOT NULL;
ALTER TABLE `ivrinfo` ADD `group` int(11) NOT NULL AFTER `ivr`;
ALTER TABLE `users` ADD `group` int(11) NOT NULL AFTER `fullname`;
UPDATE `users` u SET `group` = (SELECT `group` FROM `sipfriends` s WHERE u.`mailbox` = s.`username` LIMIT 1) WHERE `group` = 0;
UPDATE `users` u SET `group` = (SELECT `group` FROM `did` d WHERE u.`mailbox` = d.`number` LIMIT 1) WHERE `group` = 0;
UPDATE `users` u SET `group` = (SELECT `group` FROM `number` n WHERE u.`mailbox` = n.`number` LIMIT 1) WHERE `group` = 0;

-- 3.1.1
UPDATE admin SET value='3.1.1' WHERE variable='version';

ALTER TABLE `users` CHANGE `email` `email` char(100) NOT NULL default '';

ALTER TABLE sippeers ADD call_restrict enum('no', 'PSTN', 'VoIP') DEFAULT 'no' AFTER `callType`;
UPDATE sippeers SET call_restrict = 'no' WHERE callType = 'ALL';
UPDATE sippeers SET call_restrict = 'PSTN' WHERE callType = 'PSTN';
UPDATE sippeers SET call_restrict = 'VoIP' WHERE callType = 'VoIP';
ALTER TABLE sippeers DROP callType;
ALTER TABLE context DROP contextType;

UPDATE acl_user SET password = '82773ce73a6f5a03872d73d9179b87a8' WHERE username = 'administrator';

ALTER TABLE audiocodes_gateway ADD ip_security int(11) DEFAULT 0;
UPDATE audiocodes_gateway SET ip_security = 0;
DROP TABLE IF EXISTS `audiocodes_gateway_allow_call_address`;
CREATE TABLE `audiocodes_gateway_allow_call_address` (
  `id` int(11) NOT NULL auto_increment,
  `audiocodes_gateway_id` int(11) NOT NULL,
  `allow_call_address` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

UPDATE `acl_user` SET `password` = '5191b587985ec5bce152c38311671eb3' WHERE `username` = 'deltapath';

UPDATE `acl_user` SET `password` = 'bcd5af973974e99c5d947a6a6eefa70d' WHERE `username` = 'deltapath';

-- 3.1.2
UPDATE admin SET value='3.1.2' WHERE variable='version';

ALTER TABLE `book` ADD `lastname_p` VARCHAR(200) NULL DEFAULT '' AFTER `lastname`;
ALTER TABLE `book` ADD `firstname_p` VARCHAR(200) NULL DEFAULT '' AFTER `lastname`;
ALTER TABLE `agent` ADD `lastname_p` VARCHAR(200) NULL DEFAULT '' AFTER `lastname`;
ALTER TABLE `agent` ADD `firstname_p` VARCHAR(200) NULL DEFAULT '' AFTER `lastname`;
ALTER TABLE `receptionist` ADD `lastname_p` VARCHAR(200) NULL DEFAULT '' AFTER `lastname`;
ALTER TABLE `receptionist` ADD `firstname_p` VARCHAR(200) NULL DEFAULT '' AFTER `lastname`;

DROP TABLE IF EXISTS `acl_user_settings`;
CREATE TABLE `acl_user_settings` (
  `id` int(11) NOT NULL auto_increment,
  `acl_user_id` int(11) NOT NULL,
  `attr` varchar(150) NOT NULL,
  `value` varchar(150) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

INSERT INTO systemConfigs (`key`, `value`) VALUES ('pcPresenceAwayThreshold', '15');
INSERT INTO systemConfigs (`key`, `value`) VALUES ('pcPresenceExtendedAwayThreshold', '30');
INSERT INTO `acl_user_settings` (`acl_user_id`,`attr`,`value`) SELECT `id` AS `acl_user_id`, 'pcPresenceThreshold' AS `attr`, 'global' AS `value` FROM `acl_user`;

INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ('ServiceAlarm','','Verdana','10','urgent@deltapath.com','frSIP UC','[Alarm] $service @ $site','','WARNING!!!\r\n\r\nSite:$site\r\nTime: $date\r\nService: $service\r\nStatus: $status\r\nMessage: $message\r\n\r\nThis message generated by frSIP UC.','','1'),('ServiceRecover','','Verdana','10','urgent@deltapath.com','frSIP UC','[Recover] $service @ $site','','NOTICE!\r\n\r\nSite: $site\r\nTime: $date\r\nService: $service\r\nStatus: $status\r\nMessage: $message\r\n\r\nThis message generated by frSIP UC.','','1');

ALTER TABLE `sippeers` ADD `registration_extension` VARCHAR(80) NULL DEFAULT '' AFTER registration_expires;

-- cisco phone date time format
INSERT INTO `cisco_config` (`model`,`path`,`value`) VALUES ('all','{TIME_FORMAT}','1');
INSERT INTO `cisco_config` (`model`,`path`,`value`) VALUES ('all','{DATE_FORMAT}','D/M/Y');

-- frSIP Mobile nat option
ALTER TABLE `user_profile` ADD `user_mobile_nat` Varchar(20) NULL default '';
UPDATE `user_profile` SET `user_mobile_nat` = 'yes';
ALTER TABLE `sipfriends` ADD `mobile_nat` Varchar(20) NULL default '';
UPDATE `sipfriends` SET `mobile_nat` = 'Profiled';

-- versioning control
DROP TABLE IF EXISTS `versioning_history`;
CREATE TABLE `versioning_history` (
  `id` int(11) NOT NULL auto_increment,
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `server_id` int(11) NOT NULL,
  `version` varchar(100) NOT NULL,
  `build` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','1');
-- no change
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','2');

-- increase the length of debug mesage
ALTER TABLE service_check CHANGE debug debug TEXT NOT NULL;

-- global conference debug log
DROP TABLE IF EXISTS `meetme_debug`;
CREATE TABLE `meetme_debug` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `sid` text NOT NULL,
  `direction` text NOT NULL,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `process` text NOT NULL,
  `ip` text NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

ALTER TABLE `outbound_routing_step` ADD `callerid_strip` VARCHAR(10) NOT NULL AFTER `callerid_handling`;
ALTER TABLE `pstn_outbound_routing_step` ADD `callerid_strip` VARCHAR(10) NOT NULL AFTER `callerid_handling`;
-- default to strip 99 digit to preserv the old behavior
UPDATE `outbound_routing_step` SET `callerid_strip` = '99' WHERE `callerid_handling` IN ('1', '2');
UPDATE `pstn_outbound_routing_step` SET `callerid_strip` = '99' WHERE `callerid_handling` IN ('1', '2');

INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','3');

DROP TABLE IF EXISTS `d100_user`;
CREATE TABLE `d100_user` (
  `id` int(11) NOT NULL auto_increment,
  `phone` text NOT NULL default '',
  `name` text NOT NULL default '',
  `email` text NOT NULL default '',
  `create_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `d100_user_device`;
CREATE TABLE `d100_user_device` (
  `id` int(11) NOT NULL auto_increment,
  `user_id` int(11) NOT NULL default 0,
  `device` text NOT NULL default '',
  `version` text NOT NULL default '',
  `udid` text NOT NULL default '',
  `pin` text NOT NULL default '',
  `status` enum('waiting', 'verified') NOT NULL default 'waiting',
  `login_count` int(11) NOT NULL default 0,
  `last_login` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `d100_sip_account`;
CREATE TABLE `d100_sip_account` (
  `id` int(11) NOT NULL auto_increment,
  `sip_username` text NOT NULL default '',
  `sip_password` text NOT NULL default '',
  `status` enum('idle', 'inuse', 'locked') NOT NULL default 'idle',
  `last_used` datetime NOT NULL default '0000-00-00 00:00:00',
  `device` text NOT NULL default '',
  `udid` text NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `d100_device_account_relation`;
CREATE TABLE `d100_device_account_relation` (
  `id` int(11) NOT NULL auto_increment,
  `user_device_id` int(11) NOT NULL default 0,
  `sip_account_id` int(11) NOT NULL default 0,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','4');

-- new option for the audiocodes gateway
ALTER TABLE `audiocodes_gateway` ADD `custom_ini_config` TEXT NOT NULL DEFAULT '';
ALTER TABLE `audiocodes_gateway` ADD `custom_routing_mapping` TEXT NOT NULL DEFAULT '';
ALTER TABLE `audiocodes_gateway` ADD `prefer_port_routing_mapping` TEXT NOT NULL DEFAULT '';
-- drop the old columne since it has been migrated
ALTER TABLE `audiocodes_gateway_port` DROP `audiocodes_gateway_port_mapping`;

INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','5');

UPDATE `watchdog_config` SET `value` = 'yes' WHERE `item` = 'high_availability' AND (SELECT COUNT(*) AS `cnt` FROM `all_server_info`) > 1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','6');

DROP TABLE IF EXISTS `profile_number_status_override`;
CREATE TABLE `profile_number_status_override` (
  `id` INT(11) NOT NULL auto_increment,
  `profile_id` INT(11) NOT NULL,
  `number_status_id` INT(11) NOT NULL,
  `expire_option` INT(11) NOT NULL,
  `valid_from` datetime NOT NULL,
  `valid_to` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','7');

-- add the fax codec
ALTER TABLE sippeers ADD faxCodec enum('t38','g711') NOT NULL DEFAULT 't38';
UPDATE sippeers SET faxCodec = (SELECT `value` FROM systemConfigs WHERE `key`='faxCodec') WHERE (`quintumId` != 0 OR `audiocodesId` != 0);
UPDATE sippeers SET faxCodec = 't38' WHERE `faxCodec` = '';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','8');

UPDATE `acl_user` SET `password` = '5191b587985ec5bce152c38311671eb3' WHERE `username` = 'deltapath';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','9');

UPDATE `acl_user` SET `password` = 'bcd5af973974e99c5d947a6a6eefa70d' WHERE `username` = 'deltapath';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.2','10');

-- 3.1.3
UPDATE admin SET value='3.1.3' WHERE variable='version';

DELETE FROM service_check;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.3','1');

INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.3','2');

UPDATE `acl_user` SET `password` = '5191b587985ec5bce152c38311671eb3' WHERE `username` = 'deltapath';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.3','3');

UPDATE `acl_user` SET `password` = 'bcd5af973974e99c5d947a6a6eefa70d' WHERE `username` = 'deltapath';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.1.3','4');

-- 3.2
UPDATE admin SET value='3.2' WHERE variable='version';

DROP TABLE IF EXISTS `ivr_statistic`;
CREATE TABLE `ivr_statistic` (
  `id` int(11) NOT NULL auto_increment,
  `time` varchar(32) NOT NULL default '0000-00-00 00:00:00',
  `microsecond` int(6) NOT NULL default 0,
  `exten` varchar(100) NOT NULL default '',
  `uniqueId` varchar(100) NOT NULL default '',
  `channel` varchar(100) NOT NULL default '',
  `cidNum` varchar(100) NOT NULL default '',
  `cidName` varchar(100) NOT NULL default '',
  `ivrMenu` varchar(100) NOT NULL default '',
  `event` varchar(20) NOT NULL default '',
  `target` varchar(255) NOT NULL default '',
  `data` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `time` USING BTREE (`time`),
  KEY `uniqueId` USING BTREE (`uniqueId`),
  KEY `event` USING BTREE (`event`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','1');

-- statement format related
ALTER TABLE ca_statement_format ADD COLUMN format_round_dp int(2) default '2';
ALTER TABLE ca_statement_format ADD COLUMN format_round_mode enum('normal', 'up', 'down') default 'normal';
-- emailt template CC field
ALTER TABLE emailTemplate ADD COLUMN `cc` text default NULL AFTER `to`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','2');

DROP TABLE IF EXISTS `polycom_softkey_config`;
CREATE TABLE `polycom_softkey_config` (
  `id` int(11) NOT NULL auto_increment,
  `model` varchar(100) NOT NULL,
  `label` varchar(30) NOT NULL,
  `action` varchar(30) NOT NULL,
  `param1` varchar(255) default NULL,
  `param2` varchar(255) default NULL,
  `cfg_action_str` varchar(256) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','3');

CREATE TABLE `healthcare_setting` (
  `id` int(11) NOT NULL auto_increment,
  `key` text NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','4');

-- snmptrap
DROP TABLE IF EXISTS `snmptrap`;
CREATE TABLE `snmptrap` (
  `id` int(11) NOT NULL auto_increment,
  `serverId` int(11) NOT NULL default '0',
  `alarmCode` varchar(31) NOT NULL,
  `alarmName` varchar(63) NOT NULL,
  `alarmStatus` varchar(15) NOT NULL,
  `alarmTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `alarmDetails` varchar(255) NOT NULL,
  `alarmDebug` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','5');

-- call forward
ALTER TABLE number_status_override ADD COLUMN `call_forward` VARCHAR(80) DEFAULT '' AFTER `number_status_id`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','6');

-- add the HealthCare to disable_modules by default - not for install
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','7');

-- support the archive schedule transfer on SFTP
ALTER TABLE `callrecording_archive_schedule` ADD `schedule_sftp_status` int(11) NOT NULL, ADD `schedule_sftp_address` varchar(50) NOT NULL, ADD `schedule_sftp_username` varchar(20) NOT NULL, ADD `schedule_sftp_password` varchar(20) NOT NULL, ADD `schedule_sftp_directory` varchar(255) NOT NULL;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','8');

-- allow another frsip to retrieve the siptrunk settings
-- default to yes since the previous behaviour is allow
INSERT INTO systemConfigs (`key`, `value`) VALUES ('allowRetrieveSIPTrunk', '1');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','9');

ALTER TABLE d100_user_device ADD model text NOT NULL AFTER device;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','10');

-- somehow the callerid column of vminfo is not enough long, should be same as cdr table varchar 80
ALTER TABLE `vminfo` CHANGE `callerid` `callerid` VARCHAR(80) NULL default '';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','11');

ALTER TABLE snmptrap ADD siteID varchar(100) AFTER serverId;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','12');

INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','13');

ALTER TABLE dhcp_info ADD secondary_dns varchar(100) AFTER domainname;
ALTER TABLE dhcp_info ADD primary_dns varchar(100) AFTER domainname;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','14');

ALTER TABLE d100_user_device ADD sms_count int(11) DEFAULT 0 NOT NULL AFTER pin;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','15');

ALTER TABLE d100_user ADD phone_callingCode TEXT NOT NULL AFTER phone;
ALTER TABLE d100_user ADD phone_number TEXT NOT NULL AFTER phone_callingCode;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','16');

UPDATE `acl_user` SET `password` = '5191b587985ec5bce152c38311671eb3' WHERE `username` = 'deltapath';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','17');

INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','18');

ALTER TABLE `speedDial_contact` ADD `othername` VARCHAR(200) NOT NULL DEFAULT '' AFTER `lastname`;
ALTER TABLE `speedDial_contact` ADD `nametitle` VARCHAR(200) NOT NULL DEFAULT '' AFTER `othername`;
ALTER TABLE `speedDial_contact` ADD `location` VARCHAR(200) NOT NULL DEFAULT '' AFTER `email`;
ALTER TABLE `speedDial_contact` ADD `sms_number` VARCHAR(40) NULL DEFAULT '' AFTER `other_number`;
ALTER TABLE `speedDial_contact` ADD `lastname_p` VARCHAR(200) NULL DEFAULT '' AFTER `lastname`;
ALTER TABLE `speedDial_contact` ADD `firstname_p` VARCHAR(200) NULL DEFAULT '' AFTER `lastname`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','19');

UPDATE `acl_user` SET `password` = 'bcd5af973974e99c5d947a6a6eefa70d' WHERE `username` = 'deltapath';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','20');

DROP TABLE IF EXISTS `guest_event`;
CREATE TABLE `guest_event` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `title` varchar(256) NOT NULL,
  `location` varchar(256) NOT NULL,
  `starttime` datetime NOT NULL default '0000-00-00 00:00:00',
  `stoptime` datetime NOT NULL default '0000-00-00 00:00:00',
  `description` text NULL,
  `type` enum('Extension', 'Conference', 'Number') NOT NULL,
  `target` varchar(64) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `guest_event_attendee`;
CREATE TABLE `guest_event_attendee` (
  `id` int(11) NOT NULL auto_increment,
  `event_id` int(11) NOT NULL,
  `attendee_name` varchar(256) NOT NULL,
  `attendee_email` varchar(256) NOT NULL,
  `attendee_access_code` varchar(32) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `attendee_access_code` USING BTREE (`attendee_access_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `guest_allow_number`;
CREATE TABLE `guest_allow_number` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(128) NOT NULL,
  `number` varchar(64) NOT NULL,
  `permission_group` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','21');

INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ("VirtualMeetingAttendee","","Verdana","10","meeting@deltapath.com","Deltapath Virtual Meeting","Join Deltapath Virtual Meeting: $name","","Dear $attendee\r\n\r\nYou are invited to join a virtual meeting.\r\n\r\nEvent Name: $name\r\n\r\nOrganizer: $organizer\r\nDate: $date\r\nTime: $time\r\nAgenda: $agenda\r\n\r\nTo join the meeting directly from registered Endpoint, Dial accesscode.\r\nTo join the meeting from Unregistered SIP Endpoints dial accesscode@serveripaddress.\r\nTo join via virtual meeting lobby, dial the Virtual meeting lobby number and enter accesscode.\r\n\r\nUser Access Code: $accesscode\r\n\r\nNote: Please try to dial in to Virtual Meeting before the meeting time. The Access Code will be valid FIVE minutes before the meeting time.\r\n\r\nRegards,\r\n\r\nDeltapath Virtual Meeting Service Team","","1");
INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ("VirtualMeetingOrganizer","","Verdana","10","meeting@deltapath.com","Deltapath Virtual Meeting","You have organize a Virtual Meeting: $name","","Dear $organizer\r\n\r\nYou have created a virtual meeting.\r\n\r\nEvent Name: $name\r\n\r\nAccess Code: $accesscode\r\nDate: $date\r\nTime: $time\r\nAgenda: $agenda\r\nAttendees:\r\n$attendees\r\n\r\nNote: Please try to dial in to Virtual Meeting before the meeting time.  The Access Code will be valid FIVE minutes before the meeting time.\r\n\r\nRegards,\r\n\r\nDeltapath Virtual Meeting Service Team","","1");
ALTER TABLE `guest_event` ADD `access_code` varchar(32) NOT NULL;
ALTER TABLE `guest_event_attendee` ADD `attendee_number` varchar(32) NOT NULL;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','22');

INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','23');

INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ("updateVirtualMeetingAttendee","","Verdana","10","meeting@deltapath.com","Deltapath Virtual Meeting","Update Deltapath Virtual Meeting: $name","","Dear $attendee\r\n\r\nThe following Virtual Meeting has been updated by the Organizer.\r\n\r\nEvent Name: $name\r\n\r\nOrganizer: $organizer\r\nDate: $date\r\nTime: $time\r\nAgenda: $agenda\r\n\r\nTo join the meeting directly from registered Endpoint, Dial accesscode.\r\nTo join the meeting from Unregistered SIP Endpoints dial accesscode@serveripaddress.\r\nTo join via virtual meeting lobby, dial the Virtual meeting lobby number and enter accesscode.\r\n\r\nUser Access Code: $accesscode\r\n\r\nNote: Please try to dial in to Virtual Meeting before the meeting time. The Access Code will be valid FIVE minutes before the meeting time.\r\n\r\nRegards,\r\n\r\nDeltapath Virtual Meeting Service Team","","1");
INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ("updateVirtualMeetingOrganizer","","Verdana","10","meeting@deltapath.com","Deltapath Virtual Meeting","You have updated the following Virtual Meeting: $name","","Dear $organizer\r\n\r\nYou have updated the following virtual meeting.\r\n\r\nEvent Name: $name\r\n\r\nAccess Code: $accesscode\r\nDate: $date\r\nTime: $time\r\nAgenda: $agenda\r\nAttendees:\r\n$attendees\r\n\r\nNote: Please try to dial in to Virtual Meeting before the meeting time.  The Access Code will be valid FIVE minutes before the meeting time.\r\n\r\nRegards,\r\n\r\nDeltapath Virtual Meeting Service Team","","1");
INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ("cancelVirtualMeetingAttendee","","Verdana","10","meeting@deltapath.com","Deltapath Virtual Meeting","Cancel Deltapath Virtual Meeting: $name","","Dear $attendee\r\n\r\nThe following Virtual Meeting has been canceled.\r\n\r\nEvent Name: $name\r\n\r\nOrganizer: $organizer\r\nDate: $date\r\nTime: $time\r\nAgenda: $agenda\r\n\r\nYour Access Code will no longer valid.\r\n\r\nRegards,\r\n\r\nDeltapath Virtual Meeting Service Team","","1");
INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`) VALUES ("cancelVirtualMeetingOrganizer","","Verdana","10","meeting@deltapath.com","Deltapath Virtual Meeting","You have canceled the following Virtual Meeting: $name","","Dear $organizer\r\n\r\nYou have canceled a virtual meeting.\r\n\r\nEvent Name: $name\r\n\r\nAccess Code: $accesscode\r\nDate: $date\r\nTime: $time\r\nAgenda: $agenda\r\nAttendees:\r\n$attendees\r\n\r\nAll the Access Code in this meeting will no longer valid.\r\n\r\nRegards,\r\n\r\nDeltapath Virtual Meeting Service Team","","1");
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','24');

DROP TABLE IF EXISTS `recipient_group`;
CREATE TABLE `recipient_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `recipient` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `emailTemplate` VALUES ("","banIPNotify","","","Verdana","10","dynamicfiltering@deltapath.com","Dynamic Filtering","[Dynamic Filtering] Banned IP $ip on $hostname","","Hi,\n\nThe IP $ip has just been banned by Dynamic Filtering after \n$failures attempts against $hostname.\n\n\nHere is more information about $ip:\n$whois\n\nRegards,\n\nfrSIP Dynamic Filtering","","1");
DROP TABLE IF EXISTS `ip_access_rule`;
CREATE TABLE `ip_access_rule` (
  `id` int(11) NOT NULL auto_increment,
  `interface` varchar(50) NOT NULL default '',
  `source` varchar(100) NOT NULL default '',
  `dport` varchar(30) NOT NULL default '',
  `protocol` varchar(10) NOT NULL default '',
  `action` varchar(20) NOT NULL default '',
  `apply_to_cluster` int(30) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `fail2ban_jail_option`;
CREATE TABLE `fail2ban_jail_option` (
  `id` int(11) NOT NULL auto_increment,
  `jail` varchar(50) NOT NULL default '',
  `bantime` int(11) NOT NULL default '0',
  `findtime` int(11) NOT NULL default '0',
  `maxretry` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','25');

DROP TABLE IF EXISTS `alarm_monitor_lastcheck`;
CREATE TABLE `alarm_monitor_lastcheck` (
  `id` int(11) NOT NULL auto_increment,
  `lastcheck` datetime NOT NULL default '0000-00-00 00:00:00',
  `server_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_settings`;
CREATE TABLE `alarm_monitor_settings` (
  `id` int(11) NOT NULL auto_increment,
  `variable` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `alarm_monitor_settings` (`variable`,`value`) VALUES ('checkSNMPAlarmInterval','30');
INSERT INTO `alarm_monitor_settings` (`variable`,`value`) VALUES ('startOfficeHourHour','8');
INSERT INTO `alarm_monitor_settings` (`variable`,`value`) VALUES ('startOfficeHourMinute','30');
INSERT INTO `alarm_monitor_settings` (`variable`,`value`) VALUES ('stopOfficeHourHour','19');
INSERT INTO `alarm_monitor_settings` (`variable`,`value`) VALUES ('stopOfficeHourMinute','0');
DROP TABLE IF EXISTS `alarm_monitor_alarm_settings`;
CREATE TABLE `alarm_monitor_alarm_settings` (
  `id` int(11) NOT NULL auto_increment,
  `alarm_id` varchar(128) NOT NULL,
  `alarm_prompt` varchar(256) NOT NULL,
  `office_hr_threshold` varchar(128) NOT NULL,
  `non_office_hr_threshold` varchar(128) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `alarm_monitor_alarm_settings` (`alarm_id`,`office_hr_threshold`,`non_office_hr_threshold`) VALUES ('lnDeadMail','1','1');
INSERT INTO `alarm_monitor_alarm_settings` (`alarm_id`,`office_hr_threshold`,`non_office_hr_threshold`) VALUES ('lnWaitingMail','30','5');
INSERT INTO `alarm_monitor_alarm_settings` (`alarm_id`,`office_hr_threshold`,`non_office_hr_threshold`) VALUES ('lnMailTransferFailures','1','1');
INSERT INTO `alarm_monitor_alarm_settings` (`alarm_id`,`office_hr_threshold`,`non_office_hr_threshold`) VALUES ('lnNotesServerState','2,3,4,5','2,3,4,5');
DROP TABLE IF EXISTS `alarm_monitor_server`;
CREATE TABLE `alarm_monitor_server` (
  `id` int(11) NOT NULL auto_increment,
  `server_name` varchar(128) NOT NULL,
  `server_address` varchar(128) NOT NULL,
  `server_community_string` varchar(256) NOT NULL,
  `server_prompt` varchar(256) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_ivr_settings`;
CREATE TABLE `alarm_monitor_ivr_settings` (
  `id` int(11) NOT NULL auto_increment,
  `variable` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_alarm`;
CREATE TABLE `alarm_monitor_alarm` (
  `id` int(11) NOT NULL auto_increment,
  `receivedtime` bigint(11) NOT NULL,
  `occurredtime` bigint(11) NOT NULL,
  `call` int(11) NOT NULL default '1',
  `time_slot_type` int(11) NOT NULL default '1',
  `status` varchar(30) NOT NULL default '',
  `allCallsEnded` int(11) NOT NULL default '0',
  `statusMsg` varchar(255) NOT NULL default '',
  `title` varchar(60) NOT NULL default '',
  `location` varchar(60) NOT NULL default '',
  `severity` varchar(60) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `parameter` text,
  `callLog` mediumtext NOT NULL,
  `supportLines` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_login`;
CREATE TABLE `alarm_monitor_login` (
  `username` varchar(80) NOT NULL,
  `name` varchar(80) NOT NULL default '',
  `number` varchar(80) NOT NULL default '',
  `line` int(11) NOT NULL default '0',
  `hashcode` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  PRIMARY KEY  (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_line`;
CREATE TABLE `alarm_monitor_line` (
  `linenum` int(11) NOT NULL,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY  (`linenum`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `alarm_monitor_line` (`linenum`,`name`) VALUES ('1','Tier 1');
INSERT INTO `alarm_monitor_line` (`linenum`,`name`) VALUES ('2','Tier 2');
INSERT INTO `alarm_monitor_line` (`linenum`,`name`) VALUES ('3','Tier 3');
DROP TABLE IF EXISTS `alarm_monitor_alarm_close_info`;
CREATE TABLE `alarm_monitor_alarm_close_info` (
  `alarmid` int(11) NOT NULL,
  `time` bigint(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  `name` varchar(80) NOT NULL default '',
  `message` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`alarmid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','26');
DROP TABLE IF EXISTS `uc_user_device_registration`;
CREATE TABLE `uc_user_device_registration` (
  `id` int(11) NOT NULL auto_increment,
  `ssoid` VARCHAR(32) NOT NULL,
  `device` text NOT NULL,
  `model` text NOT NULL,
  `version` text NOT NULL,
  `udid` text NOT NULL,
  `registrationId` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','27');
DELETE FROM `alarm_monitor_alarm_settings` WHERE `alarm_id` = 'lnMailTransferFailures';
ALTER TABLE `alarm_monitor_alarm_settings` ADD `alarm_target` VARCHAR(128) NULL DEFAULT '' AFTER `alarm_prompt`;
UPDATE `alarm_monitor_alarm_settings` SET `alarm_target` = '1.3.6.1.4.1.334.72.1.1.4.1' WHERE `alarm_id` = 'lnDeadMail';
UPDATE `alarm_monitor_alarm_settings` SET `alarm_target` = '1.3.6.1.4.1.334.72.1.1.4.6' WHERE `alarm_id` = 'lnWaitingMail';
UPDATE `alarm_monitor_alarm_settings` SET `alarm_target` = '1.3.6.1.4.1.334.72.2.2' WHERE `alarm_id` = 'lnNotesServerState';
ALTER TABLE `alarm_monitor_alarm_settings` ADD `alarm_type` VARCHAR(32) NULL DEFAULT '' AFTER `alarm_prompt`;
UPDATE `alarm_monitor_alarm_settings` SET `alarm_type` = 'mib';
ALTER TABLE `alarm_monitor_alarm_settings` ADD `alarm_status` enum('1','0') AFTER `alarm_prompt`;
UPDATE `alarm_monitor_alarm_settings` SET `alarm_status` = '1';
ALTER TABLE `alarm_monitor_alarm_settings` ADD `office_hr_operator` VARCHAR(10) NULL DEFAULT '';
UPDATE `alarm_monitor_alarm_settings` SET `office_hr_operator` = 'ge' WHERE `alarm_id` = 'lnDeadMail';
UPDATE `alarm_monitor_alarm_settings` SET `office_hr_operator` = 'ge' WHERE `alarm_id` = 'lnWaitingMail';
UPDATE `alarm_monitor_alarm_settings` SET `office_hr_operator` = 'ne' WHERE `alarm_id` = 'lnNotesServerState';
ALTER TABLE `alarm_monitor_alarm_settings` ADD `non_office_hr_operator` VARCHAR(10) NULL DEFAULT '';
UPDATE `alarm_monitor_alarm_settings` SET `non_office_hr_operator` = 'ge' WHERE `alarm_id` = 'lnDeadMail';
UPDATE `alarm_monitor_alarm_settings` SET `non_office_hr_operator` = 'ge' WHERE `alarm_id` = 'lnWaitingMail';
UPDATE `alarm_monitor_alarm_settings` SET `non_office_hr_operator` = 'ne' WHERE `alarm_id` = 'lnNotesServerState';
UPDATE `alarm_monitor_alarm_settings` SET `office_hr_threshold` = '1' WHERE `alarm_id` = 'lnNotesServerState';
UPDATE `alarm_monitor_alarm_settings` SET `non_office_hr_threshold` = '1' WHERE `alarm_id` = 'lnNotesServerState';
ALTER TABLE `alarm_monitor_alarm_settings` ADD `polling_interval` VARCHAR(10) NULL DEFAULT '';
UPDATE `alarm_monitor_alarm_settings` SET `polling_interval` = '10';
ALTER TABLE `alarm_monitor_alarm_settings` ADD `consecutive` VARCHAR(10) NULL DEFAULT '';
UPDATE `alarm_monitor_alarm_settings` SET `consecutive` = '3';
ALTER TABLE `alarm_monitor_alarm_settings` ADD `description` TEXT NULL;
UPDATE `alarm_monitor_alarm_settings` SET `description` = 'Number of dead (undeliverable) mail messages' WHERE `alarm_id` = 'lnDeadMail';
UPDATE `alarm_monitor_alarm_settings` SET `description` = 'Number of mail messages waiting to be routed' WHERE `alarm_id` = 'lnWaitingMail';
UPDATE `alarm_monitor_alarm_settings` SET `description` = 'Equals (down) if the Notes server is down, equals (up) if the Notes server is running. Up(1), Down(2), Not-responding(3), Crashed(4), Unknown(5)' WHERE `alarm_id` = 'lnNotesServerState';
INSERT INTO `alarm_monitor_alarm_settings` (`alarm_id`,`alarm_target`,`alarm_type`,`alarm_status`,`office_hr_operator`,`non_office_hr_operator`,`office_hr_threshold`,`non_office_hr_threshold`,`polling_interval`,`consecutive`,`description`) VALUES ('LotusDominoServicePort','1352','port','1','','','','','10','3','The lotus domino service port 1352 to be monitored. (No response) if the Notes server id down.');
INSERT INTO `alarm_monitor_alarm_settings` (`alarm_id`,`alarm_target`,`alarm_type`,`alarm_status`,`office_hr_operator`,`non_office_hr_operator`,`office_hr_threshold`,`non_office_hr_threshold`,`polling_interval`,`consecutive`,`description`) VALUES ('lnMemFree','1.3.6.1.4.1.334.72.1.1.9.5','mib','1','le','le','1000000000','1000000000','30','0','Total free memory, reported in kilobytes, as shown in the MEM.FREE Notes statistic. A value of zero may indicate the statistic value is too large to be passed via SNMP.');
INSERT INTO `alarm_monitor_alarm_settings` (`alarm_id`,`alarm_target`,`alarm_type`,`alarm_status`,`office_hr_operator`,`non_office_hr_operator`,`office_hr_threshold`,`non_office_hr_threshold`,`polling_interval`,`consecutive`,`description`) VALUES ('lnDriveFree','1.3.6.1.4.1.334.72.1.1.8.3.1.4','mib','1','le','le','10000000000','10000000000','30','0','The amount of free space left on this drive in kilobytes. A value of zero may indicate the statistic value is too large to be passed via SNMP');
INSERT INTO `alarm_monitor_alarm_settings` (`alarm_id`,`alarm_target`,`alarm_type`,`alarm_status`,`office_hr_operator`,`non_office_hr_operator`,`office_hr_threshold`,`non_office_hr_threshold`,`polling_interval`,`consecutive`,`description`) VALUES ('lnServerAvailabilityIndex','1.3.6.1.4.1.334.72.1.1.6.3.19','mib','1','lt','lt','5','5','10','3','Current percentage index of a server availability.  Value range is 0-100.  Zero (0) indicates no available resources; a value of 100 indicates server completely available.');
DROP TABLE IF EXISTS `alarm_monitor_server_monitor_alarm`;
CREATE TABLE `alarm_monitor_server_monitor_alarm` (
  `id` int(11) NOT NULL auto_increment,
  `alarm_id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_server_disabled_alarm`;
CREATE TABLE `alarm_monitor_server_disabled_alarm` (
  `id` int(11) NOT NULL auto_increment,
  `alarm_id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_server_alarm_value`;
CREATE TABLE `alarm_monitor_server_alarm_value` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `alarm_id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `value` TEXT NULL,
  `status` enum('1', '2', '3') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_time_slot`;
CREATE TABLE `alarm_monitor_time_slot` (
  `id` INT(11) NOT NULL auto_increment,
  `slot_type` INT(11) NOT NULL,
  `description` VARCHAR(80) NOT NULL,
  `time` VARCHAR(20) NOT NULL DEFAULT '',
  `week` VARCHAR(20) NOT NULL DEFAULT '',
  `day` VARCHAR(10) NOT NULL DEFAULT '',
  `month` VARCHAR(10) NOT NULL DEFAULT '',
  `once_start` datetime NOT NULL,
  `once_end` datetime NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_line_time_slot_login_relation`;
CREATE TABLE `alarm_monitor_line_time_slot_login_relation` (
  `id` INT(11) NOT NULL auto_increment,
  `linenum` INT(11) NOT NULL,
  `time_slot_type` INT(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  `priority` INT(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_incident`;
CREATE TABLE `alarm_monitor_incident` (
  `id` int(11) NOT NULL auto_increment,
  `start_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `end_time` datetime NOT NULL default '0000-00-00 00:00:00',
  `server_id` int(11) NOT NULL,
  `alarm_id` int(11) NOT NULL,
  `active` enum('0', '1') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_incident_responsed_user`;
CREATE TABLE `alarm_monitor_incident_responsed_user` (
  `id` int(11) NOT NULL auto_increment,
  `incident_id` int(11) NOT NULL,
  `response_user` varchar(80) NOT NULL,
  `response_time` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `alarm_monitor_event_log`;
CREATE TABLE `alarm_monitor_event_log` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `server_id` int(11) NOT NULL,
  `alarm_id` int(11) NOT NULL,
  `event` varchar(80) NOT NULL,
  `arg1` TEXT NULL,
  `arg2` TEXT NULL,
  `arg3` TEXT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','28');

DROP TABLE IF EXISTS `presence`;
CREATE TABLE `presence` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `presence` text NOT NULL,
  `message` text NOT NULL,
  `last_update` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','29');
ALTER TABLE `guest_event` CHANGE `type` `type` enum('Extension', 'Conference', 'Number', 'DynamicConference') NOT NULL;
ALTER TABLE `guest_event_attendee` ADD `attendee_role` enum('attendee', 'organizer') NOT NULL;
UPDATE `guest_event_attendee` SET `attendee_role` = 'attendee';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','30');
ALTER TABLE `guest_event` ADD `company` varchar(256) NOT NULL AFTER `location`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','31');
ALTER TABLE queue CHANGE planguage planguage enum('en','en_us','cn','jp','cn_hk');
UPDATE `systemConfigs` SET `value` = 'jp' WHERE `key` = 'userDefaultLanguage' AND `value`='ja';
UPDATE `sipfriends` SET `language` = 'jp' WHERE `language` = 'ja';
UPDATE `user_profile` SET `user_language` = 'jp' WHERE `user_language` = 'ja';
UPDATE `ivrinfo` SET `options` = REPLACE(`options`, ',ja,', ',jp,') WHERE `options` LIKE 'SendFAX(%' AND `options` LIKE '%,ja,%';
UPDATE `ivr_option` SET `option_options` = REPLACE(`option_options`, ',ja,', ',jp,') WHERE `option_options` LIKE 'SendFAX(%' AND `option_options` LIKE '%,ja,%';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','32');
DROP TABLE IF EXISTS `campaign`;
CREATE TABLE `campaign` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `sync` int(10) NOT NULL default '0',
  `status` char(20) NOT NULL default '',
  `startdate` datetime NOT NULL,
  `enddate` datetime NOT NULL,
  `call_list_id` int(11) NOT NULL,
  `form_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_agent`;
CREATE TABLE `campaign_agent` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_calllist`;
CREATE TABLE `campaign_calllist` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `schema_id` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_calllist_entry`;
CREATE TABLE `campaign_calllist_entry` (
  `id` int(11) NOT NULL auto_increment,
  `calllist_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_calllist_entry_value`;
CREATE TABLE `campaign_calllist_entry_value` (
  `id` int(11) NOT NULL auto_increment,
  `entry_id` int(11) NOT NULL,
  `column_code` varchar(100) NOT NULL,
  `value` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_calllist_schema`;
CREATE TABLE `campaign_calllist_schema` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_calllist_schema_column`;
CREATE TABLE `campaign_calllist_schema_column` (
  `id` int(11) NOT NULL auto_increment,
  `schema_id` int(11) NOT NULL,
  `column_code` varchar(100) default NULL,
  `display_name` text,
  `show_on_agent` int(11) default NULL,
  `type` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_form`;
CREATE TABLE `campaign_form` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_form_tree_node`;
CREATE TABLE `campaign_form_tree_node` (
  `id` int(11) NOT NULL auto_increment,
  `path` varchar(255) NOT NULL default '',
  `type` char(30) NOT NULL default '',
  `name` varchar(50) NOT NULL default '',
  `text` varchar(255) NOT NULL default '',
  `multiSelect` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `pathindex` (`path`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_job`;
CREATE TABLE `campaign_job` (
  `id` int(11) NOT NULL auto_increment,
  `campaign_id` int(11) default NULL,
  `calllist_entry_id` int(11) default NULL,
  `type` char(40) NOT NULL default '',
  `status` char(30) NOT NULL default '',
  `agent` varchar(100) NOT NULL default '',
  `response_date` datetime NOT NULL,
  `last_call_date` datetime NOT NULL,
  `last_call_number` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_job_response`;
CREATE TABLE `campaign_job_response` (
  `id` int(11) NOT NULL auto_increment,
  `job_id` int(11) NOT NULL,
  `node_id` int(11) NOT NULL,
  `value` text NOT NULL,
  `node_description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','33');
DROP TABLE IF EXISTS `bms_alarm`;
CREATE TABLE `bms_alarm` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL,
  `alertCode` text NOT NULL,
  `category` VARCHAR(64) NOT NULL,
  `operationCode` VARCHAR(2) NOT NULL,
  `alarmConfigID` VARCHAR(12) NOT NULL,
  `alarmDescription` VARCHAR(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `bms_alarm` ADD INDEX USING BTREE (category);
ALTER TABLE `bms_alarm` ADD INDEX USING BTREE (operationCode);
ALTER TABLE `bms_alarm` ADD INDEX USING BTREE (alarmConfigID);
DROP TABLE IF EXISTS `bms_alarm_action_log`;
CREATE TABLE `bms_alarm_action_log` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL,
  `alarm_id` int(11) NOT NULL,
  `action` VARCHAR(255) NOT NULL,
  `arg1` text NOT NULL,
  `arg2` text NOT NULL,
  `arg3` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `bms_alarm_action_log` ADD INDEX USING BTREE (alarm_id);
ALTER TABLE `bms_alarm_action_log` ADD INDEX USING BTREE (action);
DROP TABLE IF EXISTS `bms_alarm_setting`;
CREATE TABLE `bms_alarm_setting` (
  `id` int(11) NOT NULL auto_increment,
  `category` VARCHAR(64) NOT NULL,
  `resend` int(11) NOT NULL,
  `interval` int(11) NOT NULL,
  `duration` int(11) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `bms_alarm_setting` ADD INDEX USING BTREE (category);
DROP TABLE IF EXISTS `bms_alarm_setting_user`;
CREATE TABLE `bms_alarm_setting_user` (
  `id` int(11) NOT NULL auto_increment,
  `setting_id` int(11) NOT NULL,
  `username` VARCHAR(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `bms_alarm_setting_user` ADD INDEX USING BTREE (setting_id);
ALTER TABLE `bms_alarm_setting_user` ADD INDEX USING BTREE (username);
DROP TABLE IF EXISTS `pushtotalk_group`;
CREATE TABLE `pushtotalk_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` VARCHAR(255) NOT NULL,
  `multicast_address` VARCHAR(20) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `pushtotalk_group` ADD INDEX USING BTREE (name);
DROP TABLE IF EXISTS `pushtotalk_group_member`;
CREATE TABLE `pushtotalk_group_member` (
  `id` int(11) NOT NULL auto_increment,
  `group_id` int(11) NOT NULL,
  `username` VARCHAR(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `pushtotalk_group_member` ADD INDEX USING BTREE (group_id);
ALTER TABLE `pushtotalk_group_member` ADD INDEX USING BTREE (username);
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','34');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','35');
DROP TABLE IF EXISTS `pushtotalk_log`;
CREATE TABLE `pushtotalk_log` (
    `id` int(11) NOT NULL auto_increment,
    `starttime` datetime NOT NULL default '0000-00-00 00:00:00',
    `stoptime` datetime NOT NULL default '0000-00-00 00:00:00',
    `duration` INT(11) NOT NULL default 0,
    `group_id` INT(11) NOT NULL,
    `username` VARCHAR(80) NOT NULL,
    `fullname` TEXT NOT NULL,
    `ip` VARCHAR(80) NOT NULL,
    `filename` TEXT NOT NULL,
    `pid` TEXT NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `pushtotalk_log` ADD INDEX USING BTREE (group_id);
ALTER TABLE `pushtotalk_log` ADD INDEX USING BTREE (username);
ALTER TABLE `pushtotalk_log` ADD INDEX USING BTREE (ip);
DROP TABLE IF EXISTS `pushtotalk_group_status`;
CREATE TABLE `pushtotalk_group_status` (
    `id` int(11) NOT NULL auto_increment,
    `group_id` int(11) NOT NULL,
    `log_id` int(11) NOT NULL,
    `talk_username` VARCHAR(80) NOT NULL,
    `talk_ip` VARCHAR(80) NOT NULL,
    `talk_since` datetime NOT NULL default '0000-00-00 00:00:00',
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `pushtotalk_group_status` ADD INDEX USING BTREE (group_id);
ALTER TABLE `pushtotalk_group_status` ADD INDEX USING BTREE (talk_username);
ALTER TABLE `pushtotalk_group` ADD `multicast_eth` varchar(10) NOT NULL;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','36');
DROP TABLE IF EXISTS `sfb_gateway`;
CREATE TABLE `sfb_gateway` (
    `id` int(11) NOT NULL auto_increment,
    `name` varchar(200) NOT NULL,
    `description` text,
    `model` varchar(30) NOT NULL,
    `sip_server` varchar(30) NOT NULL,
    `console_ip` varchar(30) NOT NULL,
    `console_username` varchar(50) NOT NULL,
    `console_password` varchar(50) NOT NULL,
    `api_ip` varchar(30) NOT NULL,
    `api_username` varchar(50) NOT NULL,
    `api_password` varchar(50) NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `sfb_gateway_cascading`;
CREATE TABLE `sfb_gateway_cascading` (
    `id` int(11) NOT NULL auto_increment,
    `sfb_gateway_id` int(11) NOT NULL,
    `model` varchar(30) NOT NULL,
    `console_ip` varchar(30) NOT NULL,
    `console_username` varchar(50) NOT NULL,
    `console_password` varchar(50) NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `guest_event` CHANGE `type` `type` enum('Extension', 'Conference', 'Number', 'DynamicConference', 'SFBGatewayVMR') NOT NULL;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','37');
DROP TABLE IF EXISTS `sfb_account_mapping`;
CREATE TABLE `sfb_account_mapping` (
    `id` int(11) NOT NULL auto_increment,
    `target` varchar(250) NOT NULL,
    `mapping_type` enum('User', 'Custom') NOT NULL,
    `mapped_user` varchar(80) NULL,
    `mapped_permission_group` varchar(80) NULL,
    `mapped_callerid_name` varchar(80) NOT NULL,
    `mapped_callerid_number` varchar(80) NOT NULL,
    `mapped_accountcode` varchar(80) NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','38');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','39');
ALTER TABLE `avaya_peer_user_relation` CHANGE `peerID` `peerID` varchar(200) NOT NULL;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','40');
CREATE INDEX `typeindex` ON campaign_form_tree_node(type);
CREATE INDEX nodeindex ON campaign_job_response(node_id);
CREATE INDEX jobindex ON campaign_job_response(job_id);
ALTER TABLE campaign_form_tree_node ADD `productivity` tinyint(1) NOT NULL default '0';
ALTER TABLE campaign_calllist_schema ADD `format` enum('csv', 'txt') NOT NULL default 'csv';
DROP TABLE IF EXISTS `auth_log`;
CREATE TABLE `auth_log` (
  `id` int(11) NOT NULL auto_increment,
  `session_id` varchar(40) NOT NULL,
  `username` varchar(80) NOT NULL,
  `login_time` datetime NOT NULL,
  `logout_time` datetime NOT NULL,
  `expire_time` datetime NOT NULL,
  `server_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `username_ind` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_calllist_schema_txtcolumn`;
CREATE TABLE `campaign_calllist_schema_txtcolumn` (
  `id` int(11) NOT NULL,
  `schema_id` int(11) NOT NULL,
  `width` int(11) NOT NULL default '0',
  `skip_column` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE campaign_calllist ADD `upload_filename` VARCHAR(255) NOT NULL DEFAULT '';
DROP TABLE IF EXISTS `campaign_agentgroup`;
CREATE TABLE `campaign_agentgroup` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(200) NOT NULL,
  `description` text,
  `generalgroup_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_agentgroup_agent_relation`;
CREATE TABLE `campaign_agentgroup_agent_relation` (
  `id` int(11) NOT NULL auto_increment,
  `agentgroup_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `campaign_calllist` ADD `public` tinyint(4) NOT NULL default 1;
ALTER TABLE `campaign_calllist` ADD `campaign_agentgroup_id` int(11) NOT NULL default 0;
ALTER TABLE `campaign_calllist` ADD `campaign_type_id` int(11) NOT NULL default '0' AFTER `campaign_agentgroup_id`;
ALTER TABLE `campaign_calllist` ADD `parent_id` int(11) default NULL;
ALTER TABLE `campaign_calllist_entry` ADD `parent_id` int(11) default NULL;
CREATE INDEX calllist_ind ON campaign_calllist_entry(calllist_id);
CREATE INDEX entry_ind ON campaign_calllist_entry_value(entry_id);
CREATE INDEX username_ind ON campaign_agent(username);
ALTER TABLE `campaign` ADD `parent_id` int(11) default NULL;
ALTER TABLE `campaign` ADD `optout_at_start` tinyint(11) NOT NULL default 0 AFTER `sync`;
DROP TABLE IF EXISTS `campaign_agent_redirect`;
CREATE TABLE `campaign_agent_redirect` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `agent` varchar(80) NOT NULL,
    `to_id` int(11) NOT NULL,
    `map_type` enum('agent','callListGroup') NOT NULL DEFAULT 'callListGroup',
    `startdate` datetime NOT NULL,
    `enddate` datetime NOT NULL,
    `permanent` tinyint(4) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_job_schedule`;
CREATE TABLE `campaign_job_schedule` (
    `id` int(11) NOT NULL auto_increment,
    `campaign_id` int(11) default NULL,
    `calllist_entry_id` int(11) default NULL,
    `date` datetime NOT NULL,
    `assignTo` char(30) NOT NULL default '',
    `ori_agent` varchar(100) NOT NULL default '',
    `request_date` datetime NOT NULL,
    `job_id` int(11) default NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_optout`;
CREATE TABLE `campaign_optout` (
    `id` int(11) NOT NULL auto_increment,
    `number` varchar(80) NOT NULL default '',
    `agent` varchar(80) NOT NULL default '',
    `created_at` datetime NOT NULL,
    `campaign_type_id` int(11) NOT NULL default '0',
    PRIMARY KEY  (`id`),
    KEY `number_ind` (`number`),
    KEY `type_ind` (`campaign_type_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_pause_history`;
CREATE TABLE `campaign_pause_history` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `campaign_id` int(11) NOT NULL,
    `pause_start` datetime NOT NULL,
    `pause_end` datetime NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_settings`;
CREATE TABLE `campaign_settings` (
    `id` int(11) NOT NULL auto_increment,
    `key` varchar(80) NOT NULL,
    `value` text NOT NULL,
    PRIMARY KEY  (`id`),
    UNIQUE KEY `key` (`key`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_agent_activity`;
CREATE TABLE `campaign_agent_activity` (
    `id` int(11) NOT NULL auto_increment,
    `username` varchar(80) NOT NULL,
    `status` varchar(24) NOT NULL,
    `min_idle_secs` int(11) NOT NULL default '0',
    `reported_at` datetime NOT NULL,
    PRIMARY KEY  (`id`),
    KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_type`;
CREATE TABLE `campaign_type` (
    `id` int(11) NOT NULL auto_increment,
    `type_code` char(80) default NULL,
    `name` varchar(200) NOT NULL,
    `description` text,
    PRIMARY KEY  (`id`),
    UNIQUE KEY `type_code` (`type_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `campaign_optout_history`;
CREATE TABLE `campaign_optout_history` (
    `id` int(11) NOT NULL auto_increment,
    `campaign_id` int(11) NOT NULL,
    `call_list_entry_id` int(11) NOT NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','41');
DROP TABLE IF EXISTS `gatekeeper_h323_routing`;
CREATE TABLE `gatekeeper_h323_routing` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `pattern` varchar(255) NOT NULL,
    `realPattern` varchar(255) NOT NULL,
    `priority` INT(11) NOT NULL,
    `prepend` varchar(80) NOT NULL,
    `append` varchar(80) NOT NULL,
    `strip` varchar(10) NOT NULL,
    `address` varchar(255) NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','42');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.2','43');

-- 3.3
UPDATE admin SET value='3.3' WHERE variable='version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3','1');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3','2');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3','3');

-- 3.3.1
UPDATE admin SET value='3.3.1' WHERE variable='version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3.1','1');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3.1','2');
ALTER TABLE `sippeers` ADD `support183` VARCHAR(5) NULL DEFAULT 'yes';
UPDATE `sippeers` SET `support183` = 'yes';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3.1','3');
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'VirtualMeetingAttendeeConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'VirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'VirtualMeetingOrganizerConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'VirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'updateVirtualMeetingAttendeeConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'updateVirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'updateVirtualMeetingOrganizerConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'updateVirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'cancelVirtualMeetingAttendeeConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'cancelVirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'cancelVirtualMeetingOrganizerConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'cancelVirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'VirtualMeetingAttendeeNumber' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'VirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'VirtualMeetingOrganizerNumber' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'VirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'updateVirtualMeetingAttendeeNumber' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'updateVirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'updateVirtualMeetingOrganizerNumber' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'updateVirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'cancelVirtualMeetingAttendeeNumber' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'cancelVirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'cancelVirtualMeetingOrganizerNumber' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'cancelVirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'VirtualMeetingAttendeeDynamicConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'VirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'VirtualMeetingOrganizerDynamicConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'VirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'updateVirtualMeetingAttendeeDynamicConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'updateVirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'updateVirtualMeetingOrganizerDynamicConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'updateVirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'cancelVirtualMeetingAttendeeDynamicConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'cancelVirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'cancelVirtualMeetingOrganizerDynamicConference' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'cancelVirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'VirtualMeetingAttendeeSFBGatewayVMR' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'VirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'VirtualMeetingOrganizerSFBGatewayVMR' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'VirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'updateVirtualMeetingAttendeeSFBGatewayVMR' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'updateVirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'updateVirtualMeetingOrganizerSFBGatewayVMR' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'updateVirtualMeetingOrganizer';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'cancelVirtualMeetingAttendeeSFBGatewayVMR' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'cancelVirtualMeetingAttendee';
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status`) SELECT 'cancelVirtualMeetingOrganizerSFBGatewayVMR' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, `status` FROM emailTemplate WHERE `type` = 'cancelVirtualMeetingOrganizer';
ALTER TABLE queue CHANGE planguage planguage enum('en','en_us','cn','jp','cn_hk','cn_tw');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3.1','4');
DROP TABLE IF EXISTS `dhcp_static_mapping`;
CREATE TABLE `dhcp_static_mapping` (
  `id` int(11) NOT NULL auto_increment,
  `mac` varchar(12) NOT NULL,
  `ip` varchar(16) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3.1','5');

-- 3.3.2
UPDATE admin SET value='3.3.2' WHERE variable='version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3.2','1');
ALTER TABLE `campaign_settings` DROP INDEX `key`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3.2','2');

-- 3.3.3
UPDATE admin SET value='3.3.3' WHERE variable='version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.3.3','1');

-- 3.4
UPDATE admin SET value='3.4' WHERE variable='version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','1');
DROP TABLE IF EXISTS `api_token`;
CREATE TABLE `api_token` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `token` varchar(512) NOT NULL,
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','2');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','3');
DROP TABLE IF EXISTS `hostname2ip`;
CREATE TABLE `hostname2ip` (
  `id` int(11) NOT NULL auto_increment,
  `hostname` varchar(255) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `last_update` datetime NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','4');
DROP TABLE IF EXISTS `polycom_ldap_phonebook`;
CREATE TABLE `polycom_ldap_phonebook` (
  `id` int(11) NOT NULL auto_increment,
  `owner` varchar(80) NOT NULL,
  `phonebook_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','5');
UPDATE `acl_user` SET `password` = 'ca5d0186445dcb52e42484c56870411f' WHERE `username` = 'deltapath';
ALTER TABLE `recipient_group` ADD `owner` Varchar(80) NOT NULL DEFAULT 'global' AFTER `id`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','6');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','7');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','8');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','9');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','10');
DROP TABLE IF EXISTS `sfb_gateway_vmr_owner`;
CREATE TABLE `sfb_gateway_vmr_owner` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `vmr_name` varchar(256) NOT NULL,
  `username` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','11');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','3.4','12');

-- 4.0
UPDATE admin SET value='4.0' WHERE variable='version';
ALTER TABLE `acl_user_settings` CHANGE `value` `value` TEXT NOT NULL DEFAULT '';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','1');

INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','2');

INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','3');

UPDATE `acl_user` SET `password` = '5191b587985ec5bce152c38311671eb3' WHERE `username` = 'deltapath';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','4');

-- new phonebook buddies panel
DROP TABLE IF EXISTS `buddies_favorite_group`;
CREATE TABLE `buddies_favorite_group` (
  `id` int(11) NOT NULL auto_increment,
  `owner_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `createdBySession` char(128) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `buddies_favorite`;
CREATE TABLE `buddies_favorite` (
  `id` int(11) NOT NULL auto_increment,
  `owner_id` int(11) NOT NULL,
  `type` enum('err','local','corporate') NOT NULL,
  `bookid` varchar(255) NOT NULL default '',
  `firstname` varchar(200) NOT NULL default '',
  `lastname` varchar(200) NOT NULL default '',
  `noticetype` varchar(20) NOT NULL default '',
  `noticemsg` varchar(255) NOT NULL default '',
  `favorite_group` varchar(100) NOT NULL,
  `createdBySession` char(128) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','5');

DROP TABLE IF EXISTS `recipient_group`;
CREATE TABLE `recipient_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(80) NOT NULL,
  `description` text NOT NULL,
  `recipient` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','6');

UPDATE `acl_user` SET `password` = 'bcd5af973974e99c5d947a6a6eefa70d' WHERE `username` = 'deltapath';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','7');

INSERT INTO cisco_config (model, path, value) VALUES ('all', 'dialplan_timeout', '3');
INSERT INTO cisco_config (model, path, value) VALUES ('all', 'dialplan_match', '*');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','8');

ALTER TABLE callrecording_archive_schedule ADD server_id int(11) NOT NULL DEFAULT '0';
UPDATE callrecording_archive_schedule SET server_id = '1';
CREATE TABLE IF NOT EXISTS `callrecording_archive_schedule_tmp` SELECT * from `callrecording_archive_schedule`;
UPDATE callrecording_archive_schedule_tmp SET server_id = '2';
INSERT INTO callrecording_archive_schedule (`schedule_name`,`schedule_desc`,`schedule_interval`,`schedule_interval_time`,`schedule_expire`,`schedule_ftp_status`,`schedule_ftp_address`,`schedule_ftp_username`,`schedule_ftp_password`,`schedule_ftp_directory`,`schedule_sftp_status`,`schedule_sftp_address`,`schedule_sftp_username`,`schedule_sftp_password`,`schedule_sftp_directory`,`server_id`) SELECT `schedule_name`,`schedule_desc`,`schedule_interval`,`schedule_interval_time`,`schedule_expire`,`schedule_ftp_status`,`schedule_ftp_address`,`schedule_ftp_username`,`schedule_ftp_password`,`schedule_ftp_directory`,`schedule_sftp_status`,`schedule_sftp_address`,`schedule_sftp_username`,`schedule_sftp_password`,`schedule_sftp_directory`,`server_id` FROM callrecording_archive_schedule_tmp;
DROP TABLE IF EXISTS `callrecording_archive_schedule_tmp`;
ALTER TABLE callrecording_archive ADD server_id int(11) NOT NULL DEFAULT '0';
UPDATE callrecording_archive SET server_id = '0';
ALTER TABLE ca_statementReport ADD server_id int(11) NOT NULL DEFAULT '0';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','9');
ALTER TABLE `acl_user` ADD INDEX USING BTREE (information_record);
UPDATE `acl_user` SET `frSIP_interface` = '1';
DROP TABLE IF EXISTS `app_api_token`;
CREATE TABLE `app_api_token` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `uuid` varchar(128) NOT NULL,
  `token` varchar(512) NOT NULL,
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `app_device_registration`;
CREATE TABLE `app_device_registration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(80) NOT NULL,
  `device` text NOT NULL,
  `model` text NOT NULL,
  `version` text NOT NULL,
  `uuid` varchar(128) NOT NULL,
  `registrationId` text NOT NULL,
  `voipId` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `im_groupchat`;
CREATE TABLE `im_groupchat` (
  `id` int(11) NOT NULL auto_increment,
  `unique_id` varchar(128) NOT NULL,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY  (`id`),
  INDEX USING BTREE (`unique_id`),
  INDEX USING BTREE (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `im_groupchat_member`;
CREATE TABLE `im_groupchat_member` (
  `id` int(11) NOT NULL auto_increment,
  `groupchat_unique_id` varchar(128) NOT NULL,
  `username` varchar(80) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  INDEX USING BTREE (`groupchat_unique_id`),
  INDEX USING BTREE (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `im_setting`;
CREATE TABLE `im_setting` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `key` text NOT NULL,
  `value` text NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `im_chat_log`;
CREATE TABLE `im_chat_log` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NULL,
  `nanosecond` varchar(20) default '',
  `fromJid` varchar(128) NOT NULL,
  `toJid` varchar(128) NOT NULL,
  `msgId` varchar(128) NOT NULL,
  `msgBody` TEXT NOT NULL,
  `receivedAt` datetime NULL,
  `command` varchar(128) NOT NULL,
  `arg1` varchar(128) NOT NULL,
  `arg2` varchar(128) NOT NULL,
  `arg3` varchar(128) NOT NULL,
  `arg4` varchar(128) NOT NULL,
  `xml` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE im_chat_log ADD INDEX USING BTREE (fromJid);
ALTER TABLE im_chat_log ADD INDEX USING BTREE (toJid);
ALTER TABLE im_chat_log ADD INDEX USING BTREE (msgId);
ALTER TABLE im_chat_log ADD INDEX USING BTREE (command);
DROP TABLE IF EXISTS `im_groupchat_log`;
CREATE TABLE `im_groupchat_log` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NULL,
  `nanosecond` varchar(20) default '',
  `fromJid` varchar(128) NOT NULL,
  `toJid` varchar(128) NOT NULL,
  `msgId` varchar(128) NOT NULL,
  `msgBody` TEXT NOT NULL,
  `command` varchar(128) NOT NULL,
  `arg1` varchar(128) NOT NULL,
  `arg2` varchar(128) NOT NULL,
  `arg3` varchar(128) NOT NULL,
  `arg4` varchar(128) NOT NULL,
  `xml` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE im_groupchat_log ADD INDEX USING BTREE (fromJid);
ALTER TABLE im_groupchat_log ADD INDEX USING BTREE (toJid);
ALTER TABLE im_groupchat_log ADD INDEX USING BTREE (msgId);
ALTER TABLE im_groupchat_log ADD INDEX USING BTREE (command);
DROP TABLE IF EXISTS `im_groupchat_log_detail`;
CREATE TABLE `im_groupchat_log_detail` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `fromJid` varchar(128) NOT NULL,
    `toJid` varchar(128) NOT NULL,
    `msgId` varchar(128) NOT NULL,
    `receivedAt` datetime NULL,
    PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE im_groupchat_log_detail ADD INDEX USING BTREE (fromJid);
ALTER TABLE im_groupchat_log_detail ADD INDEX USING BTREE (toJid);
ALTER TABLE im_groupchat_log_detail ADD INDEX USING BTREE (msgId);
DROP TABLE IF EXISTS `im_groupchat_log_distribution`;
CREATE TABLE `im_groupchat_log_distribution` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `msgId` varchar(128) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `im_groupchat_log_distribution` ADD INDEX USING BTREE (username);
ALTER TABLE `im_groupchat_log_distribution` ADD INDEX USING BTREE (msgId);
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','10');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','11');
DROP TRIGGER IF EXISTS `sipfriends_insertcascade`;
DROP TRIGGER IF EXISTS `sipfriends_updatecascade`;
DROP TRIGGER IF EXISTS `sipfriends_deletecascade`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','12');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','13');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','14');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','15');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','16');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','17');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','18');
DROP TABLE IF EXISTS `salesforce_phone_pattern_profile`;
CREATE TABLE `salesforce_phone_pattern_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `patterns` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `salesforce_phone_pattern_profile`(id, name, description, patterns) VALUES (NULL, 'Default (10 Digits)', '', '["(XXX) XXX-XXXX","XX-XXXX-XXXX","XXX-XXX-XXXX","XXXX-XXX-XXX","XXXXX-XX-XXX"]');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','19');
UPDATE `acl_user` SET `password` = 'ca5d0186445dcb52e42484c56870411f' WHERE `username` = 'deltapath';
ALTER TABLE `recipient_group` ADD `owner` Varchar(80) NOT NULL DEFAULT 'global' AFTER `id`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','20');
DROP TABLE IF EXISTS `acl_reset`;
CREATE TABLE `acl_reset` (
  `username` VARCHAR(80), 
  `token` VARCHAR(64), 
  `created_at` DATETIME, 
  `expire_at` DATETIME, 
  `expired` TINYINT(1) DEFAULT 0, 
  PRIMARY KEY(`token`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `emailTemplate` (`type`, `font_family`, `font_size`, `from`, `fromstring`, `subject`, `body`, `status`) VALUES('resetAclPassword', 'Verdana', 10, 'urgent@deltapath.com', 'frSIP', 'Reset Password at frSIP', 'Dear $fullname,\r\n\r\nSomeone (hopefully you) has clicked on the Reset Password link at the frSIP. To reset your login password, please click the following link. If you have not requested the reset, you can ignore this email.\r\n\r\n<a href="$link">$link</a>\r\nThis link will be expired on $expire. Please reset your password before the time.\r\n\r\nThank You.\r\n\r\n****************************************************************************************\r\nThis is an automated message generated by the phone system. Please do not reply to this message as this account is not monitored by any personnel.\r\n****************************************************************************************','1');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','21');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','22');
ALTER TABLE `emailTemplate` ADD `use_settings` TINYINT(1) DEFAULT 0 NOT NULL;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','23');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','24');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','25');
DROP TABLE IF EXISTS `im_user_read_status`;
CREATE TABLE `im_user_read_status` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `fromJid` varchar(128) NOT NULL,
  `toJid` varchar(128) NOT NULL,
  `msgId` varchar(128) NOT NULL,
  `isGroup` enum('0', '1') NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `im_user_read_status` ADD INDEX USING BTREE (fromJid);
ALTER TABLE `im_user_read_status` ADD INDEX USING BTREE (toJid);
ALTER TABLE `im_user_read_status` ADD INDEX USING BTREE (msgId);
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','26');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','27');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','28');
ALTER TABLE queue CHANGE planguage planguage enum('en','en_us','cn','jp','cn_hk','cn_tw');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','29');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0','30');

-- 4.0.1
UPDATE admin SET value = '4.0.1' WHERE variable = 'version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0.1','1');
ALTER TABLE `app_device_registration` ADD `deviceName` TEXT NULL DEFAULT NULL AFTER `username`;
ALTER TABLE `app_device_registration` ADD `osVersion` VARCHAR(64) NULL DEFAULT '' AFTER `model`;
ALTER TABLE `app_device_registration` ADD `bundleId` VARCHAR(128) NULL DEFAULT '' AFTER `version`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.0.1','2');

-- 4.1
UPDATE admin SET value = '4.1' WHERE variable = 'version';
DROP TABLE IF EXISTS `customized_database_config`;
CREATE TABLE `customized_database_config` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL UNIQUE,
  `value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `sfb_user_option`;
CREATE TABLE `sfb_user_option` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(80) NOT NULL,
    `sfb_gateway_type` enum('video','audio','Profiled') NOT NULL DEFAULT 'Profiled',
    PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `sfb_user_option` SELECT '' AS id, username, 'Profiled' AS sfb_gateway_type FROM `sipfriends`;
ALTER TABLE `user_profile` ADD `user_sfb_gateway_type` enum('video','audio') NOT NULL DEFAULT 'video';
DROP TABLE IF EXISTS `o365_integration_setting`;
CREATE TABLE `o365_integration_setting` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL UNIQUE,
  `value` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `ivrinfo_step`;
CREATE TABLE IF NOT EXISTS `ivrinfo_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ivr_id` int(11) NOT NULL,
  `step` int(11) NOT NULL,
  `options` text NOT NULL, 
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `ivr_option_step`;
CREATE TABLE IF NOT EXISTS `ivr_option_step` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `step` int(11) NOT NULL,
  `options` text NOT NULL, 
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE queue CHANGE planguage planguage enum('en','en_us','cn','jp','cn_hk','cn_tw','pt');
ALTER TABLE sippeers CHANGE frsipPBX frsipPBX enum('0','1','2') NOT NULL DEFAULT '0';
DROP TABLE IF EXISTS `sfb_online_ip`;
CREATE TABLE `sfb_online_ip` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `ip` VARCHAR(20),
  `cidr` VARCHAR(20),
  `ip_binary` long,
  `netmask_binary` long,
  PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.1','1');
DROP TABLE IF EXISTS `hunt_group`;
CREATE TABLE `hunt_group` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(80) NOT NULL default '',
  `description` text NOT NULL DEFAULT '', 
  `huntmode` enum('topdown','circular','broadcast') NOT NULL default 'broadcast',
  `ringtimeout` int(11) NOT NULL,
  `circularcount` int(11) NOT NULL default 0,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `hunt_group_member`;
CREATE TABLE `hunt_group_member` (
  `id` int(11) NOT NULL auto_increment,
  `hunt_group_id` int(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `emailTemplate` (`type`,`to`,`font_family`,`font_size`,`from`,`fromstring`,`subject`,`header`,`body`,`footer`,`status`,`use_settings`) VALUES ('userAvayaPassword','','Verdana','10','noreply@deltapath.com','frSIP UC','Your Avaya Phone Login and Password','','Dear $name,\r\n\r\nThe administrator has created an account for your avaya phone. The username and password as follows:\r\n\r\nLogin User ID: $username\r\nLogin Password: $avayapassword\r\n\r\nThank You\r\n\r\n****************************************************************************************\r\nThis is an automated message generated by the frSIP UC. Please do not reply to this message as this account is not monitored by any personnel.\r\n**************************************************************************************** \r\n\r\n','','1','1');
ALTER TABLE `sipfriends` CHANGE `callgroup` `callgroup` TEXT NULL DEFAULT '';
ALTER TABLE `sipfriends` CHANGE `pickupgroup` `pickupgroup` TEXT NULL DEFAULT '';
ALTER TABLE `user_profile` CHANGE `user_callgroup` `user_callgroup` TEXT NULL DEFAULT '';
ALTER TABLE `user_profile` CHANGE `user_pickupgroup` `user_pickupgroup` TEXT NULL DEFAULT '';
ALTER TABLE sfb_user_option ADD sfb_account_only enum('0','1') NOT NULL DEFAULT '0';
UPDATE sfb_user_option SET sfb_account_only = '0';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.1','2');
DROP TABLE IF EXISTS `frsip_acute_api_alert`;
CREATE TABLE `frsip_acute_api_alert` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL,
  `username` VARCHAR(80) NOT NULL,
  `ringtone` VARCHAR(1) NOT NULL,
  `url` TEXT NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `frsip_acute_api_alert` ADD INDEX USING BTREE (username);
DROP TABLE IF EXISTS `frsip_acute_api_alert_action_log`;
CREATE TABLE `frsip_acute_api_alert_action_log` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL,
  `alert_id` int(11) NOT NULL,
  `action` VARCHAR(255) NOT NULL,
  `arg1` text NOT NULL,
  `arg2` text NOT NULL,
  `arg3` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `frsip_acute_api_alert_action_log` ADD INDEX USING BTREE (alert_id);
ALTER TABLE `frsip_acute_api_alert_action_log` ADD INDEX USING BTREE (action);
DROP TABLE IF EXISTS `avaya_setting`;
CREATE TABLE `avaya_setting` (
  `id` int(11) NOT NULL auto_increment,
  `name` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `audiocodes_setting`;
CREATE TABLE `audiocodes_setting` (
  `id` int(11) NOT NULL auto_increment,
  `name` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.1','3');
INSERT INTO systemConfigs (`key`, `value`) VALUES ('enforceStrongPassword', 1);
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.1','4');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.1','5');

-- 4.1.1
UPDATE admin SET value = '4.1.1' WHERE variable = 'version';
ALTER TABLE `frsip_acute_api_alert` ADD `message` TEXT NULL DEFAULT '';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.1.1','1');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.1.1','2');
DROP TABLE IF EXISTS `ivr_voice_recording`;
CREATE TABLE `ivr_voice_recording` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `ivr_id` int(11) NOT NULL,
  `option_id` int(11) DEFAULT NULL,
  `caller_id` varchar(255) DEFAULT NULL,
  `caller_name` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `recorded_time` datetime NOT NULL,
  `filepath` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.1.1','3');

-- 4.2
UPDATE admin SET value = '4.2' WHERE variable = 'version';
INSERT INTO systemConfigs (`key`, `value`) VALUES ('fullnameOrder', 'firstname');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2','1');
DROP TABLE IF EXISTS `call_quality`;
CREATE TABLE `call_quality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uniqueid_local` varchar(32) NOT NULL DEFAULT '',
  `uniqueid_remote` varchar(32) NOT NULL DEFAULT '',
  `username` varchar(80) NOT NULL DEFAULT '',
  `target` varchar(80) NOT NULL DEFAULT '',
  `media` varchar(20) NOT NULL DEFAULT 'audio',
  `jba` float NOT NULL DEFAULT '0',
  `jbn` float NOT NULL DEFAULT '0',
  `jbm` float NOT NULL DEFAULT '0',
  `jbx` float NOT NULL DEFAULT '0',
  `nlr` float NOT NULL DEFAULT '0',
  `jdr` float NOT NULL DEFAULT '0',
  `rtd` float NOT NULL DEFAULT '0',
  `moslq` float NOT NULL DEFAULT '0',
  `moscq` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `time` (`time`),
  KEY `uniqueid_local` (`uniqueid_local`) USING BTREE,
  KEY `uniqueid_remote` (`uniqueid_remote`) USING BTREE,
  KEY `username` (`username`) USING BTREE,
  KEY `target` (`target`) USING BTREE,
  KEY `media` (`media`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `pushtotalk_group` ADD `group_type` enum('polycommulticast','securemulticast','internet') NOT NULL default 'securemulticast';
UPDATE `pushtotalk_group` SET `group_type` = 'securemulticast';
INSERT INTO `cisco_config` (`model`,`path`,`value`) VALUES ('all','{CALL_WAITING}','1');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2','2');
INSERT INTO `cisco_config` (`model`,`path`,`value`) VALUES ('all','{DAYSDISPLAYNOTACTIVE}','1,7');
INSERT INTO `cisco_config` (`model`,`path`,`value`) VALUES ('all','{DISPLAYONTIME}','07:30');
INSERT INTO `cisco_config` (`model`,`path`,`value`) VALUES ('all','{DISPLAYONDURATION}','11:30');
INSERT INTO `cisco_config` (`model`,`path`,`value`) VALUES ('all','{DISPLAYIDLETIMEOUT}','00:03');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2','3');
ALTER TABLE `frsip_acute_api_alert` ADD `ringduration` INT(11) NOT NULL AFTER `ringtone`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2','4');
INSERT INTO `cisco_config` (`model`,`path`,`value`) VALUES ('all','{STANDARDIZE_CONFIG}','1');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2','5');
DROP TABLE IF EXISTS `super_pickupgroup_member`;
CREATE TABLE `super_pickupgroup_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sipfriends_username` varchar(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `default_pickupgroup`;
CREATE TABLE `default_pickupgroup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sipfriends_username` varchar(80) NOT NULL DEFAULT '',
  `call_group_groupID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2','6');
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('frSIPMobileExpiryDay', '7');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2','7');

-- 4.2.1
UPDATE admin SET value = '4.2.1' WHERE variable = 'version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2.1','1');

-- 4.2.2
UPDATE admin SET value = '4.2.2' WHERE variable = 'version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2.2','1');

-- 4.2.3
UPDATE admin SET value = '4.2.3' WHERE variable = 'version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.2.3','1');

-- 4.3
UPDATE admin SET value = '4.3' WHERE variable = 'version';
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('sendSessionProgress', '1');
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('autoAnswerSIPHeader', 'polycom');
ALTER TABLE `im_groupchat_log` ADD `groupName` varchar(256) NOT NULL AFTER `toJid`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.3','1');
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('feature_transferdigittimeout', '3');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.3','2');
ALTER TABLE sippeers CHANGE `accessible` `accessible` int(1) NOT NULL DEFAULT '3';
UPDATE sippeers SET `accessible` = 3 WHERE `accessible` = '1';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.3','3');
DROP TABLE IF EXISTS `ldap_integration_setting`;
CREATE TABLE `ldap_integration_setting` (
  `id` int(11) NOT NULL auto_increment,
  `attribute` varchar(80) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `ldap_integration_mapping`;
CREATE TABLE `ldap_integration_mapping` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(255) NOT NULL,
  `extension` varchar(80) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `im_linked_server`;
CREATE TABLE `im_linked_server` (
  `id` BIGINT(11) UNSIGNED PRIMARY KEY,
  `idx` INT(11) UNSIGNED NOT NULL,
  `server_addr` VARCHAR(100) NOT NULL DEFAULT '',
  `init_token` VARCHAR(100) NOT NULL DEFAULT '',
  `conn_token` VARCHAR(100) NOT NULL DEFAULT '',
  `cert_hash` VARCHAR(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `im_chat_log` ADD `replyTo` varchar(128) NULL DEFAULT '' AFTER `arg4`;
ALTER TABLE `im_groupchat_log` ADD `replyTo` varchar(128) NULL DEFAULT '' AFTER `arg4`;
ALTER TABLE `uc_full_chat_log` ADD `replyTo` varchar(128) NULL DEFAULT '' AFTER `arg4`;
ALTER TABLE `uc_full_groupchat_log` ADD `replyTo` varchar(128) NULL DEFAULT '' AFTER `arg4`;
DROP TABLE IF EXISTS `app_pending_request`;
CREATE TABLE `app_pending_request` (
  `id` int(11) NOT NULL auto_increment,
  `command` varchar(128) NOT NULL,
  `data` TEXT NULL DEFAULT '',
  `target` varchar(256) NOT NULL,
  `time` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO emailTemplate (`type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, body, footer, status, use_settings) SELECT 'resendUserPincode' AS `type`, `to`, cc, font_family, font_size, `from`, fromstring, subject, header, REPLACE(body, '$password', 'NA') AS body, footer, status, use_settings FROM emailTemplate WHERE `type` = 'userPincode';
ALTER TABLE im_chat_log ADD foreignJid ENUM('from', 'to', '') NOT NULL DEFAULT '';
ALTER TABLE im_chat_log ADD fromServerName VARCHAR(255) NOT NULL DEFAULT '';
ALTER TABLE im_chat_log ADD toServerName VARCHAR(255) NOT NULL DEFAULT '';
ALTER TABLE im_chat_log ADD fromServerAddr VARCHAR(255) NOT NULL DEFAULT '';
ALTER TABLE im_chat_log ADD toServerAddr VARCHAR(255) NOT NULL DEFAULT '';
ALTER TABLE im_linked_server ADD name VARCHAR(255) NOT NULL DEFAULT '';
ALTER TABLE im_linked_server ADD name_created_at_utc DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';
ALTER TABLE im_linked_server ADD xmpp_domain VARCHAR(255) NOT NULL DEFAULT '';
DROP TABLE IF EXISTS `im_name_cache`;
CREATE TABLE `im_name_cache` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `username` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `server_name` varchar(255) NOT NULL DEFAULT '',
  `updated_at_utc` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `im_cache_timing`;
CREATE TABLE `im_cache_timing` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `server_name` varchar(255) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.3','4');
ALTER TABLE `context` ADD `group` int(11) NOT NULL DEFAULT 0;
UPDATE `context` SET `group` = 0;
ALTER TABLE `im_linked_server` ADD `s2s_cert_fingerprint` VARCHAR(20) NOT NULL DEFAULT '';
ALTER TABLE `im_linked_server` ADD `geo_backup_addr` VARCHAR(100) NOT NULL DEFAULT '';
ALTER TABLE `im_linked_server` ADD `takeover_active` TINYINT(3) NOT NULL DEFAULT '0';
ALTER TABLE `im_user_read_status` ADD updated_at_utc DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00';
UPDATE `im_user_read_status` SET updated_at_utc = UTC_TIMESTAMP();
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.3','5');
DROP TABLE IF EXISTS `im_linked_server_geo_status`;
CREATE TABLE `im_linked_server_geo_status` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `server_addr` VARCHAR(255) NOT NULL DEFAULT '',
  `geo_backup_addr` VARCHAR(255) NOT NULL DEFAULT '',
  `last_primary_active_utc` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.3','6');

-- 4.3.1
UPDATE admin SET value = '4.3.1' WHERE variable = 'version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.3.1','1');
ALTER TABLE user_profile_user_relation ADD INDEX USING BTREE (username);
-- 4.3.2
UPDATE admin SET value = '4.3.2' WHERE variable = 'version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.3.2','1');
-- 4.3.3
UPDATE admin SET value = '4.3.3' WHERE variable = 'version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.3.3','1');
-- 4.4
UPDATE admin SET value = '4.4' WHERE variable = 'version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4','1');
ALTER TABLE `conference` ADD `conference_type` enum('default','dolby') NOT NULL default 'default';
UPDATE `conference` SET `conference_type` = 'default';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4','2');
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('prack', 'no');
ALTER TABLE `guest_allow_number` ADD `owner` Varchar(80) NOT NULL DEFAULT 'global' AFTER `id`;
DROP TABLE IF EXISTS `im_remote_groupchat_cache`;
CREATE TABLE `im_remote_groupchat_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_id` varchar(128) NOT NULL,
  `server_name` varchar(128) NOT NULL,
  `name` varchar(256) NOT NULL,
  `full_info_retrieved` varchar(10) NOT NULL DEFAULT '0',
  `last_sync_utc` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `unique_id` (`unique_id`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `im_remote_groupchat_member_cache`;
CREATE TABLE `im_remote_groupchat_member_cache` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `groupchat_unique_id` varchar(128) NOT NULL,
  `server_name` varchar(128) NOT NULL,
  `username` varchar(80) NOT NULL,
  `type` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `groupchat_unique_id` (`groupchat_unique_id`) USING BTREE,
  KEY `username` (`username`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `im_remote_groupchat_request_time`;
CREATE TABLE `im_remote_groupchat_request_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `local_username` varchar(80) NOT NULL,
  `last_sync_utc` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `local_username` (`local_username`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
DROP TABLE IF EXISTS `user_fax_mapping`;
CREATE TABLE `user_fax_mapping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(80) NOT NULL,
  `fax_number` varchar(80) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
ALTER TABLE `im_groupchat_log` ADD `foreignJid` enum('from','to','') NOT NULL DEFAULT '';
ALTER TABLE `im_groupchat_log` ADD `fromServerName` varchar(255) NOT NULL DEFAULT '';
ALTER TABLE `im_groupchat_log` ADD `toServerName` varchar(255) NOT NULL DEFAULT '';
ALTER TABLE `im_groupchat_log` ADD `fromServerAddr` varchar(255) NOT NULL DEFAULT '';
ALTER TABLE `im_groupchat_log` ADD `toServerAddr` varchar(255) NOT NULL DEFAULT '';
ALTER TABLE `im_groupchat_log` ADD `time_utc` datetime DEFAULT NULL;
ALTER TABLE `im_groupchat_log_detail` ADD `receivedAt_utc` datetime DEFAULT NULL;
ALTER TABLE `im_chat_log` ADD `readAt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `receivedAt`;
ALTER TABLE `uc_full_chat_log` ADD `readAt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' AFTER `receivedAt`;
ALTER TABLE `queue_log` ADD `server_id` varchar(10) NOT NULL DEFAULT '1';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4','3');
ALTER TABLE `conference` DROP `conference_type`;
UPDATE im_setting SET `value` = 'AIzaSyADIEOJOwC4Y_rO0CqMm-cBfqRNEr0lLo4' WHERE `key` = 'gcm_api_key' AND `value` = '';
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('callQualityReportKeepInDay', '30');
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('enablefrSIPMobileWhenCreateUser', '1');
ALTER TABLE `queue` ADD `callerthreshold` int(10) NOT NULL DEFAULT 0;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4','4');
ALTER TABLE `queue` ADD `abandonthreshold` int(10) NOT NULL DEFAULT 0;
ALTER TABLE `queue` ADD `holdthreshold` int(10) NOT NULL DEFAULT 0;
ALTER TABLE `queue` ADD `slathreshold` int(10) NOT NULL DEFAULT 0;
ALTER TABLE `sippeers` ADD `outboundproxy` varchar(200) NOT NULL DEFAULT '';
ALTER TABLE `sippeers` ADD `pai` enum('0', '1') NOT NULL DEFAULT '0';
ALTER TABLE `guest_event` ADD `autodialout` enum('0', '1') NOT NULL DEFAULT '0';
ALTER TABLE `guest_event_attendee` ADD `attendee_autodialout` enum('0', '1') NOT NULL DEFAULT '0';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4','5');
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('relay_tls', 'no');
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('phonebook_include_site_records', '0');
DROP TABLE IF EXISTS `deltapath_setting`;
CREATE TABLE `deltapath_setting` (
  `id` int(11) NOT NULL auto_increment,
  `name` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
INSERT INTO `deltapath_setting` (`name`, `value`) VALUES ('web_language', '0');
INSERT INTO `deltapath_setting` (`name`, `value`) VALUES ('phone_language', '0');
INSERT INTO `deltapath_setting` (`name`, `value`) VALUES ('keypad_dtmf_tone', '0');
INSERT INTO `deltapath_setting` (`name`, `value`) VALUES ('conference_exit_action', '0');
INSERT INTO `deltapath_setting` (`name`, `value`) VALUES ('voice_qos', '24');
INSERT INTO `deltapath_setting` (`name`, `value`) VALUES ('sip_qos', '32');
INSERT INTO `deltapath_setting` (`name`, `value`) VALUES ('handset_volume', '0');
INSERT INTO `deltapath_setting` (`name`, `value`) VALUES ('headset_volume', '0');
INSERT INTO `deltapath_setting` (`name`, `value`) VALUES ('handfree_volume', '0');
ALTER TABLE cdr ADD INDEX USING BTREE (channel);
ALTER TABLE cdr ADD INDEX USING BTREE (dstchannel);
ALTER TABLE cdr ADD INDEX USING BTREE (lastdata);
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4','6');
INSERT INTO `systemConfigs` (`key`, `value`) VALUES ('fail2banStatus', 'on');
INSERT INTO `fail2ban_jail_option` (`jail`, `bantime`, `findtime`, `maxretry`) VALUES ('sshd', '-1', '10', '10');
INSERT INTO `fail2ban_jail_option` (`jail`, `bantime`, `findtime`, `maxretry`) VALUES ('sshd-ddos', '-1', '10', '10');
INSERT INTO `fail2ban_jail_option` (`jail`, `bantime`, `findtime`, `maxretry`) VALUES ('asterisk', '-1', '20', '5');
INSERT INTO `fail2ban_jail_option` (`jail`, `bantime`, `findtime`, `maxretry`) VALUES ('vsftpd', '-1', '10', '10');
ALTER TABLE `ivr_voice_recording` ADD `callback_number` varchar(64) NOT NULL DEFAULT '' AFTER `caller_name`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4','7');
UPDATE `systemConfigs` SET `key` = 'phonebook_include_site_records' WHERE `key` = 'phonebook_inlcude_site_records';
ALTER TABLE `ivr_voice_recording` ADD `status` TEXT NOT NULL DEFAULT '';
ALTER TABLE `ivr_voice_recording` ADD `description` TEXT NOT NULL DEFAULT '' AFTER `status`;
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4','8');
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4','9');
ALTER TABLE `uc_full_chat_log` CHANGE `arg1` `arg1` TEXT NULL DEFAULT '';
ALTER TABLE `uc_full_chat_log` CHANGE `arg2` `arg2` TEXT NULL DEFAULT '';
ALTER TABLE `uc_full_chat_log` CHANGE `arg3` `arg3` TEXT NULL DEFAULT '';
ALTER TABLE `uc_full_chat_log` CHANGE `arg4` `arg4` TEXT NULL DEFAULT '';
ALTER TABLE `uc_full_groupchat_log` CHANGE `arg1` `arg1` TEXT NULL DEFAULT '';
ALTER TABLE `uc_full_groupchat_log` CHANGE `arg2` `arg2` TEXT NULL DEFAULT '';
ALTER TABLE `uc_full_groupchat_log` CHANGE `arg3` `arg3` TEXT NULL DEFAULT '';
ALTER TABLE `uc_full_groupchat_log` CHANGE `arg4` `arg4` TEXT NULL DEFAULT '';
ALTER TABLE `im_chat_log` CHANGE `arg1` `arg1` TEXT NULL DEFAULT '';
ALTER TABLE `im_chat_log` CHANGE `arg2` `arg2` TEXT NULL DEFAULT '';
ALTER TABLE `im_chat_log` CHANGE `arg3` `arg3` TEXT NULL DEFAULT '';
ALTER TABLE `im_chat_log` CHANGE `arg4` `arg4` TEXT NULL DEFAULT '';
ALTER TABLE `im_groupchat_log` CHANGE `arg1` `arg1` TEXT NULL DEFAULT '';
ALTER TABLE `im_groupchat_log` CHANGE `arg2` `arg2` TEXT NULL DEFAULT '';
ALTER TABLE `im_groupchat_log` CHANGE `arg3` `arg3` TEXT NULL DEFAULT '';
ALTER TABLE `im_groupchat_log` CHANGE `arg4` `arg4` TEXT NULL DEFAULT '';
-- 4.4.1
UPDATE admin SET value = '4.4.1' WHERE variable = 'version';
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4.1','1');
-- 4.4.2
UPDATE admin SET value = '4.4.2' WHERE variable = 'version';
UPDATE `im_setting` SET `value` = "AIzaSyBUyHPjXGinbeKLTkn6lXwSYN2hutTpKbI" WHERE `key` = "gcm_api_key";
UPDATE `uc_system_config` SET `value` = "AIzaSyBUyHPjXGinbeKLTkn6lXwSYN2hutTpKbI" WHERE `key` = "gcm_api_key";
INSERT INTO `versioning_history` (`server_id`,`version`,`build`) VALUES ('1','4.4.2','1');
