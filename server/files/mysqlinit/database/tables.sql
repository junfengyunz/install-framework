-- MySQL dump 10.11
--
-- Host: localhost    Database: frsip
-- ------------------------------------------------------
-- Server version	5.0.70-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `RoamingInAccount`
--

DROP TABLE IF EXISTS `RoamingInAccount`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `RoamingInAccount` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `sip_username` varchar(100) NOT NULL,
  `sip_secret` varchar(100) NOT NULL,
  `sip_peer` varchar(100) NOT NULL,
  `expirytime` datetime NOT NULL,
  `mac` varchar(100) NOT NULL,
  `dockingStation` varchar(100) NOT NULL,
  `isLocal` enum('0','1') NOT NULL default '1',
  `ack` enum('0','1') NOT NULL default '0',
  `hash` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `RoamingOutAccount`
--

DROP TABLE IF EXISTS `RoamingOutAccount`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `RoamingOutAccount` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `sip_username` varchar(100) NOT NULL,
  `sip_secret` varchar(100) NOT NULL,
  `sip_peer` varchar(100) NOT NULL,
  `expirytime` datetime NOT NULL,
  `mac` varchar(100) NOT NULL,
  `dockingStation` varchar(100) NOT NULL,
  `isLocal` enum('0','1') NOT NULL default '1',
  `ack` enum('0','1') NOT NULL default '0',
  `hash` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `SMSMessage`
--

DROP TABLE IF EXISTS `SMSMessage`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `SMSMessage` (
  `id` int(11) NOT NULL auto_increment,
  `smsm_from` varchar(100) NOT NULL,
  `smsm_fromSMSC` varchar(100) NOT NULL,
  `smsm_subject` varchar(100) NOT NULL,
  `smsm_sendTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `smsm_receiveTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `smsm_message` text NOT NULL,
  `smsm_messageID` int(11) NOT NULL,
  `smms_dischargeTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `smsm_status` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `SMSRouting`
--

DROP TABLE IF EXISTS `SMSRouting`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `SMSRouting` (
  `id` int(11) NOT NULL auto_increment,
  `sms_callerID` varchar(100) NOT NULL,
  `sms_destination` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `account` (
  `accountcode` varchar(20) NOT NULL default '',
  `pincode` varchar(50) default NULL,
  `status` varchar(20) default NULL,
  `username` varchar(80) NOT NULL default '',
  `usepincode` varchar(20) NOT NULL default 'Y',
  KEY `username` USING BTREE (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `acl_group`
--

DROP TABLE IF EXISTS `acl_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `acl_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(80) NOT NULL,
  `description` text,
  `role` varchar(1) NOT NULL,
  `group_privilege` varchar(100) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `acl_group_acl`
--

DROP TABLE IF EXISTS `acl_group_acl`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `acl_group_acl` (
  `id` int(11) NOT NULL auto_increment,
  `acl_group_id` int(11) NOT NULL,
  `grouping` int(11) NOT NULL,
  `topology_page` varchar(20) NOT NULL,
  `access` enum('0','1','2') NOT NULL default '1',
  `priority` int(11) NOT NULL default '1',
  `actionType` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `acl_group_topology_relation`
--

DROP TABLE IF EXISTS `acl_group_topology_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `acl_group_topology_relation` (
  `id` int(11) NOT NULL auto_increment,
  `acl_group_id` int(11) NOT NULL,
  `topology_page` varchar(10) NOT NULL,
  `access` enum('0','1','2') NOT NULL default '1',
  `priority` int(11) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `acl_group_user_relation`
--

DROP TABLE IF EXISTS `acl_group_user_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `acl_group_user_relation` (
  `id` int(11) NOT NULL auto_increment,
  `acl_group_id` varchar(20) NOT NULL,
  `acl_user_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `acl_user_id` USING BTREE (`acl_user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `acl_user`
--

DROP TABLE IF EXISTS `acl_user`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `acl_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `password` varchar(80) NOT NULL,
  `group` varchar(100) NOT NULL,
  `privileges` varchar(100) NOT NULL,
  `information_record` int(11) NOT NULL,
  `frSIP_interface` int(11) NOT NULL,
  `empolyee_id` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `username` USING BTREE (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `acl_user_features`
--

DROP TABLE IF EXISTS `acl_user_features`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `acl_user_features` (
  `id` int(11) NOT NULL auto_increment,
  `acl_user_id` int(11) NOT NULL,
  `feature` varchar(100) NOT NULL,
  `data` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `acl_user_login_seat`
--

DROP TABLE IF EXISTS `acl_user_login_seat`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `acl_user_login_seat` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `seat` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `acl_user_monitor_group`
--

DROP TABLE IF EXISTS `acl_user_monitor_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `acl_user_monitor_group` (
  `id` int(11) NOT NULL auto_increment,
  `acl_user_id` int(11) NOT NULL,
  `group_id` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `additional_device`
--

DROP TABLE IF EXISTS `additional_device`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `additional_device` (
  `username` varchar(80) NOT NULL default '',
  `owner` varchar(80) NOT NULL default '',
  `name` varchar(80) NOT NULL default '',
  `callerid` varchar(80) NOT NULL default '',
  `nat` varchar(40) NOT NULL default 'no',
  `sip_password` varchar(100) NOT NULL default '',
  `language` varchar(20) NOT NULL default 'GLOBAL',
  `expiretime` datetime NOT NULL default '9999-12-31 23:59:59',
  `allowcodec` varchar(100) default 'GLOBAL',
  `phoneLabel` varchar(40) default '',
  `disableMWI` varchar(20) NOT NULL default '',
  `autoAnswer` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `admin` (
  `variable` varchar(20) NOT NULL default '',
  `value` text,
  `server_id` int(11) NOT NULL default '0',
  `id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `agent`
--

DROP TABLE IF EXISTS `agent`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `agent` (
  `id` int(11) NOT NULL auto_increment,
  `agentid` varchar(20) default NULL,
  `agentpwd` varchar(80) default NULL,
  `aclgroupid` int(11) NOT NULL,
  `generalgroupid` int(11) default NULL,
  `firstname` varchar(200) default NULL,
  `lastname` varchar(200) default NULL,
  `callrecording` enum('yes','no') default 'no',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `agent_location`
--

DROP TABLE IF EXISTS `agent_location`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `agent_location` (
  `id` int(11) NOT NULL auto_increment,
  `agentid` varchar(80) NOT NULL,
  `location` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `agentgroup`
--

DROP TABLE IF EXISTS `agentgroup`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `agentgroup` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(200) NOT NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `agentgroup_agent`
--

DROP TABLE IF EXISTS `agentgroup_agent`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `agentgroup_agent` (
  `id` int(11) NOT NULL auto_increment,
  `agentgroupid` int(11) NOT NULL,
  `agentid` int(11) NOT NULL,
  `agentpriority` int(8) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `all_server_info`
--

DROP TABLE IF EXISTS `all_server_info`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `all_server_info` (
  `server_id` int(11) NOT NULL auto_increment,
  `isMain` enum('0','1') NOT NULL default '0',
  `ip` varchar(100) NOT NULL default '',
  `status` enum('-1','0','1','2') NOT NULL default '0',
  `ipTakeOver` enum('0','1') NOT NULL default '0',
  `autoFallBack` enum('0','1') NOT NULL default '0',
  PRIMARY KEY  (`server_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `appliance_service_check`
--

DROP TABLE IF EXISTS `appliance_service_check`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `appliance_service_check` (
  `id` int(11) NOT NULL auto_increment,
  `server_id` int(11) NOT NULL default '0',
  `service` varchar(63) NOT NULL default '',
  `service_code` varchar(63) NOT NULL default '',
  `timestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  `status` varchar(31) NOT NULL default '',
  `message` varchar(31) NOT NULL default '',
  `remarks` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `appliance_system_log`
--

DROP TABLE IF EXISTS `appliance_system_log`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `appliance_system_log` (
  `id` int(11) NOT NULL auto_increment,
  `server_id` int(11) NOT NULL default '0',
  `timestamp` datetime NOT NULL default '0000-00-00 00:00:00',
  `triggered_by` varchar(63) NOT NULL,
  `type` varchar(63) NOT NULL,
  `component` varchar(31) NOT NULL,
  `message` varchar(255) NOT NULL,
  `remarks` varchar(127) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `avaya_peer_user_relation`
--

DROP TABLE IF EXISTS `avaya_peer_user_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `avaya_peer_user_relation` (
  `id` int(11) NOT NULL auto_increment,
  `peerID` varchar(15) NOT NULL,
  `mailbox` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `book` (
  `id` int(11) NOT NULL auto_increment,
  `firstname` varchar(200) NOT NULL default '',
  `lastname` varchar(200) NOT NULL default '',
  `othername` varchar(200) NOT NULL default '',
  `nametitle` varchar(200) NOT NULL default '',
  `company` varchar(255) NOT NULL default '',
  `department` varchar(255) NOT NULL default '',
  `jobTitle` varchar(255) NOT NULL default '',
  `phone_number` varchar(35) NOT NULL default '',
  `mobile_number` varchar(35) NOT NULL default '',
  `other_number` varchar(35) NOT NULL default '',
  `fax` varchar(35) NOT NULL default '',
  `email` varchar(150) NOT NULL default '',
  `location` varchar(200) NOT NULL default '',
  `recordOwner` varchar(35) NOT NULL default '',
  `recordType` varchar(35) NOT NULL default '',
  `buddy` varchar(35) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_billingaccount`
--

DROP TABLE IF EXISTS `ca_billingaccount`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_billingaccount` (
  `billingaccount_id` int(11) NOT NULL auto_increment,
  `billingaccount_code` varchar(100) NOT NULL,
  `billingaccount_name` varchar(100) NOT NULL,
  `billingaccount_desc` text,
  `billingaccount_type` enum('Postpaid','Prepaid','Wholesale') NOT NULL,
  `billingaccount_balance` float NOT NULL default '0',
  `billingaccount_status` enum('0','1') NOT NULL default '0',
  `billingaccount_owner` varchar(80) NOT NULL default '',
  `billingaccount_fixed_basis` varchar(20) default '0',
  `billingaccount_user_basis` varchar(20) default '0',
  `billingaccount_virtualuser_basis` varchar(20) default '0',
  `billingaccount_generalline_basis` varchar(20) default '0',
  `billingaccount_directline_basis` varchar(20) default '0',
  `billingaccount_threshold` varchar(20) default '0',
  `billingaccount_dl_play_caller` enum('0','1') NOT NULL default '0',
  `billingaccount_dl_play_callee` enum('0','1') NOT NULL default '0',
  `billingaccount_dl_play_timeout` enum('0','1') NOT NULL default '0',
  `billingaccount_dl_play_connect` enum('0','1') NOT NULL default '0',
  `billingaccount_dl_remaining_threshold` varchar(20) NOT NULL default '0',
  `billingaccount_dl_warning_threshold` varchar(20) NOT NULL default '0',
  `billingaccount_dl_warning_repeat` varchar(20) NOT NULL default '0',
  `billingaccount_connection_limit` int(11) NOT NULL default '0',
  `billingaccount_statement_format` varchar(20) default '',
  PRIMARY KEY  (`billingaccount_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_billingaccount_rule`
--

DROP TABLE IF EXISTS `ca_billingaccount_rule`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_billingaccount_rule` (
  `ba_rule_id` int(11) NOT NULL auto_increment,
  `ba_rule_ba_id` int(11) NOT NULL,
  `ba_rule_minute_begin` varchar(100) NOT NULL default '0',
  `ba_rule_minute_end` varchar(100) NOT NULL default '-1',
  `ba_rule_adjust_percentage` varchar(10) NOT NULL default '100',
  PRIMARY KEY  (`ba_rule_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_call`
--

DROP TABLE IF EXISTS `ca_call`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_call` (
  `id` int(11) NOT NULL auto_increment,
  `starttime` datetime NOT NULL,
  `stoptime` datetime NOT NULL,
  `src_num` varchar(200) default NULL,
  `src_name` varchar(200) default NULL,
  `dst_num` varchar(200) default NULL,
  `terminatecause` varchar(200) default NULL,
  `country_code` varchar(200) default NULL,
  `countryarea_code` varchar(200) default NULL,
  `duration` float default NULL,
  `bill` float default NULL,
  `cost` float default NULL,
  `useraccount_code` varchar(200) NOT NULL,
  `useraccount_name` varchar(200) default NULL,
  `billingaccount_code` varchar(200) NOT NULL,
  `billingaccount_name` varchar(200) default NULL,
  `tariff_code` varchar(200) NOT NULL,
  `tariff_name` varchar(200) default NULL,
  `tariff_adjustment` float default NULL,
  `provider_code` varchar(200) NOT NULL,
  `provider_name` varchar(200) default NULL,
  `uniqueid` varchar(200) NOT NULL,
  `channel` varchar(200) NOT NULL,
  `iddprefix` varchar(200) NOT NULL,
  `calling_code` varchar(200) NOT NULL,
  `dialed_num` varchar(200) NOT NULL,
  `sippeer` varchar(200) default NULL,
  `nasipaddress` varchar(200) default NULL,
  `status` varchar(5) default NULL,
  PRIMARY KEY  (`id`),
  KEY `id` USING BTREE (`id`),
  KEY `starttime` USING BTREE (`starttime`),
  KEY `stoptime` USING BTREE (`stoptime`),
  KEY `uniqueid` USING BTREE (`uniqueid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_country`
--

DROP TABLE IF EXISTS `ca_country`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_country` (
  `country_id` int(11) NOT NULL auto_increment,
  `country_code` varchar(20) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  `country_calling_code` varchar(100) NOT NULL,
  PRIMARY KEY  (`country_id`),
  KEY `country_code` (`country_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_countryarea`
--

DROP TABLE IF EXISTS `ca_countryarea`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_countryarea` (
  `countryarea_id` int(11) NOT NULL auto_increment,
  `countryarea_country_id` int(11) NOT NULL,
  `countryarea_code` varchar(20) NOT NULL,
  `countryarea_calling_code` varchar(100) NOT NULL,
  `countryarea_desc` text,
  PRIMARY KEY  (`countryarea_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_iddprefix`
--

DROP TABLE IF EXISTS `ca_iddprefix`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_iddprefix` (
  `iddprefix_id` int(11) NOT NULL auto_increment,
  `iddprefix_prefix` varchar(100) NOT NULL,
  `iddprefix_name` varchar(100) NOT NULL,
  `iddprefix_desc` text,
  PRIMARY KEY  (`iddprefix_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_iddprefix_account_relation`
--

DROP TABLE IF EXISTS `ca_iddprefix_account_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_iddprefix_account_relation` (
  `iar_id` int(11) NOT NULL auto_increment,
  `iar_percentage` varchar(100) NOT NULL default '100',
  `iar_billingaccount_id` int(11) NOT NULL,
  `iar_tariff_id` int(11) default NULL,
  `iar_iddprefix_id` int(11) NOT NULL,
  PRIMARY KEY  (`iar_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_postpaid`
--

DROP TABLE IF EXISTS `ca_postpaid`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_postpaid` (
  `postpaid_id` int(11) NOT NULL auto_increment,
  `postpaid_ba_id` int(11) NOT NULL,
  `postpaid_billday` varchar(10) NOT NULL default '1',
  `postpaid_billperiod` tinyint(4) NOT NULL default '1',
  PRIMARY KEY  (`postpaid_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_provider`
--

DROP TABLE IF EXISTS `ca_provider`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_provider` (
  `provider_id` int(11) NOT NULL auto_increment,
  `provider_code` varchar(100) NOT NULL,
  `provider_name` varchar(100) NOT NULL,
  `provider_desc` text NOT NULL,
  `provider_prefix` varchar(20) NOT NULL default '',
  `provider_strip` int(11) NOT NULL default '0',
  `provider_connection_limit` int(11) NOT NULL default '-1',
  `provider_sippeer` varchar(100) NOT NULL default '',
  `provider_preferredCodec` varchar(100) default NULL,
  PRIMARY KEY  (`provider_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_providerrate`
--

DROP TABLE IF EXISTS `ca_providerrate`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_providerrate` (
  `providerrate_id` int(11) NOT NULL auto_increment,
  `providerrate_name` varchar(100) default '',
  `providerrate_desc` text,
  `providerrate_provider_id` int(11) NOT NULL,
  `providerrate_country_code` varchar(20) NOT NULL,
  `providerrate_countryarea_code` varchar(20) NOT NULL,
  `providerrate_valid_period_begin` datetime default '0000-00-00 00:00:00',
  `providerrate_valid_period_end` datetime default '9999-12-31 23:59:59',
  `providerrate_period_day_begin` varchar(100) default '',
  `providerrate_period_day_end` varchar(100) default '',
  `providerrate_period_month_begin` varchar(100) default '',
  `providerrate_period_month_end` varchar(100) default '',
  `providerrate_period_weekday` varchar(100) default '',
  `providerrate_period_time_begin` varchar(100) default '',
  `providerrate_period_time_end` varchar(100) default '',
  `providerrate_period_default` varchar(100) default '',
  PRIMARY KEY  (`providerrate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_providerrate_info`
--

DROP TABLE IF EXISTS `ca_providerrate_info`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_providerrate_info` (
  `providerrate_info_id` int(11) NOT NULL auto_increment,
  `providerrate_info_providerrate_id` int(11) NOT NULL,
  `providerrate_info_rate` float NOT NULL,
  `providerrate_info_rounduptime` float NOT NULL,
  `providerrate_info_begin` int(11) NOT NULL default '0',
  `providerrate_info_end` int(11) NOT NULL default '-1',
  PRIMARY KEY  (`providerrate_info_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_routing`
--

DROP TABLE IF EXISTS `ca_routing`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_routing` (
  `routing_id` int(11) NOT NULL auto_increment,
  `routing_country_code` varchar(20) NOT NULL,
  `routing_countryarea_code` varchar(20) NOT NULL,
  `routing_name` varchar(100) default '',
  `routing_desc` text,
  `routing_lcr` enum('0','1') NOT NULL default '1',
  `routing_period_time_begin` varchar(100) default '',
  `routing_period_time_end` varchar(100) default '',
  `routing_period_weekday` varchar(100) default '',
  `routing_period_month_begin` varchar(100) default '',
  `routing_period_month_end` varchar(100) default '',
  `routing_period_day_begin` varchar(100) default '',
  `routing_period_day_end` varchar(100) default '',
  `routing_valid_period_begin` datetime default '0000-00-00 00:00:00',
  `routing_valid_period_end` datetime default '9999-12-31 23:59:59',
  PRIMARY KEY  (`routing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_routing_group`
--

DROP TABLE IF EXISTS `ca_routing_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_routing_group` (
  `routing_group_id` int(11) NOT NULL auto_increment,
  `routing_group_name` varchar(100) NOT NULL,
  `routing_group_desc` text,
  PRIMARY KEY  (`routing_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_routing_group_iddprefix_account_relation`
--

DROP TABLE IF EXISTS `ca_routing_group_iddprefix_account_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_routing_group_iddprefix_account_relation` (
  `rgiar_id` int(11) NOT NULL auto_increment,
  `rgiar_routing_group_id` int(11) NOT NULL,
  `rgiar_iar_id` int(11) NOT NULL,
  PRIMARY KEY  (`rgiar_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_routing_group_routing_relation`
--

DROP TABLE IF EXISTS `ca_routing_group_routing_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_routing_group_routing_relation` (
  `rgrr_id` int(11) NOT NULL auto_increment,
  `rgrr_routing_group_id` int(11) NOT NULL,
  `rgrr_routing_id` int(11) NOT NULL,
  PRIMARY KEY  (`rgrr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_routing_provider_relation`
--

DROP TABLE IF EXISTS `ca_routing_provider_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_routing_provider_relation` (
  `rpr_id` int(11) NOT NULL auto_increment,
  `rpr_routing_id` int(11) NOT NULL,
  `rpr_provider_id` int(11) NOT NULL,
  PRIMARY KEY  (`rpr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_skippinnumber`
--

DROP TABLE IF EXISTS `ca_skippinnumber`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_skippinnumber` (
  `spn_id` int(11) NOT NULL auto_increment,
  `spn_number` varchar(100) NOT NULL,
  `spn_provider_id` int(11) NOT NULL,
  `spn_use_provider_config` enum('0','1') NOT NULL default '0',
  `spn_prepend` varchar(20) NOT NULL default '',
  `spn_strip` int(11) NOT NULL default '0',
  PRIMARY KEY  (`spn_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_statementReport`
--

DROP TABLE IF EXISTS `ca_statementReport`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_statementReport` (
  `id` int(11) NOT NULL auto_increment,
  `creator_code` varchar(200) NOT NULL,
  `creator_name` varchar(200) NOT NULL,
  `createTime` datetime NOT NULL default '0000-00-00 00:00:00',
  `customer_code` varchar(100) NOT NULL,
  `path` text NOT NULL,
  `actual_start` varchar(100) default '',
  `actual_end` varchar(100) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_statement_format`
--

DROP TABLE IF EXISTS `ca_statement_format`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_statement_format` (
  `format_id` int(11) NOT NULL auto_increment,
  `format_name` varchar(100) NOT NULL,
  `format_desc` text,
  `format_columns` varchar(200) default '',
  PRIMARY KEY  (`format_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_statement_format_category`
--

DROP TABLE IF EXISTS `ca_statement_format_category`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_statement_format_category` (
  `category_id` int(11) NOT NULL auto_increment,
  `category_format_id` int(11) NOT NULL,
  `category_name` varchar(100) NOT NULL,
  `category_desc` text,
  `category_priority` varchar(10) NOT NULL,
  `category_others` varchar(10) default '0',
  `category_log` varchar(10) default '0',
  PRIMARY KEY  (`category_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_statement_format_category_info`
--

DROP TABLE IF EXISTS `ca_statement_format_category_info`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_statement_format_category_info` (
  `cat_info_id` int(11) NOT NULL auto_increment,
  `cat_info_category_id` int(11) NOT NULL,
  `cat_info_country_code` varchar(20) NOT NULL,
  `cat_info_countryarea_code` varchar(20) NOT NULL,
  PRIMARY KEY  (`cat_info_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_tariff`
--

DROP TABLE IF EXISTS `ca_tariff`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_tariff` (
  `tariff_id` int(11) NOT NULL auto_increment,
  `tariff_code` varchar(100) NOT NULL,
  `tariff_name` varchar(100) NOT NULL,
  `tariff_desc` text NOT NULL,
  PRIMARY KEY  (`tariff_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_tariffrate`
--

DROP TABLE IF EXISTS `ca_tariffrate`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_tariffrate` (
  `tariffrate_id` int(11) NOT NULL auto_increment,
  `tariffrate_name` varchar(100) default '',
  `tariffrate_desc` text,
  `tariffrate_tariff_id` int(11) NOT NULL,
  `tariffrate_country_code` varchar(20) NOT NULL,
  `tariffrate_countryarea_code` varchar(20) NOT NULL,
  `tariffrate_valid_period_begin` datetime default '0000-00-00 00:00:00',
  `tariffrate_valid_period_end` datetime default '9999-12-31 23:59:59',
  `tariffrate_period_day_begin` varchar(100) default '',
  `tariffrate_period_day_end` varchar(100) default '',
  `tariffrate_period_month_begin` varchar(100) default '',
  `tariffrate_period_month_end` varchar(100) default '',
  `tariffrate_period_weekday` varchar(100) default '',
  `tariffrate_period_time_begin` varchar(100) default '',
  `tariffrate_period_time_end` varchar(100) default '',
  `tariffrate_period_default` varchar(100) default '',
  PRIMARY KEY  (`tariffrate_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_tariffrate_info`
--

DROP TABLE IF EXISTS `ca_tariffrate_info`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_tariffrate_info` (
  `tariffrate_info_id` int(11) NOT NULL auto_increment,
  `tariffrate_info_tariffrate_id` int(11) NOT NULL,
  `tariffrate_info_rate` float NOT NULL,
  `tariffrate_info_rounduptime` float NOT NULL,
  `tariffrate_info_begin` int(11) NOT NULL default '0',
  `tariffrate_info_end` int(11) NOT NULL default '-1',
  PRIMARY KEY  (`tariffrate_info_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ca_useraccount`
--

DROP TABLE IF EXISTS `ca_useraccount`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ca_useraccount` (
  `useraccount_id` int(11) NOT NULL auto_increment,
  `useraccount_username` varchar(100) NOT NULL,
  `useraccount_callerID` varchar(100) NOT NULL,
  `useraccount_validate_pin` enum('0','1','Profiled') NOT NULL default '0',
  `useraccount_allow_others` enum('0','1','Profiled') NOT NULL default '0',
  `useraccount_status` enum('0','1','Profiled') NOT NULL default '0',
  `useraccount_custom_callerID_num` varchar(100) default '',
  `useraccount_custom_callerID_name` varchar(100) default '',
  PRIMARY KEY  (`useraccount_id`),
  KEY `useraccount_username` USING BTREE (`useraccount_username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `call_centre_admin_member`
--

DROP TABLE IF EXISTS `call_centre_admin_member`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `call_centre_admin_member` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `member` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `username` (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `call_group`
--

DROP TABLE IF EXISTS `call_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `call_group` (
  `groupID` int(10) unsigned NOT NULL auto_increment,
  `groupTitle` varchar(50) NOT NULL default '',
  `groupDesc` text,
  `isCallGroup` char(1) NOT NULL default '1',
  PRIMARY KEY  (`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `call_recording`
--

DROP TABLE IF EXISTS `call_recording`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `call_recording` (
  `id` int(11) NOT NULL auto_increment,
  `member` varchar(80) NOT NULL,
  `type` varchar(1) NOT NULL,
  `email` varchar(1) NOT NULL default '0',
  `canView` varchar(1) NOT NULL default '0',
  `canDelete` varchar(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `member` USING BTREE (`member`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `call_recording_file`
--

DROP TABLE IF EXISTS `call_recording_file`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `call_recording_file` (
  `id` int(11) NOT NULL auto_increment,
  `owner` varchar(80) NOT NULL,
  `callerID` varchar(80) NOT NULL,
  `calleeID` varchar(80) NOT NULL,
  `type` varchar(20) NOT NULL,
  `starttime` datetime NOT NULL,
  `stoptime` datetime NOT NULL,
  `md5hash` varchar(32) NOT NULL,
  `md5file` varchar(32) NOT NULL,
  `uniqueid` varchar(32) NOT NULL,
  `status` varchar(10) NOT NULL default 'exists',
  `fixed` varchar(2) NOT NULL default '0',
  `reference` varchar(255) default NULL,
  `outage` datetime default NULL,
  `comment` text,
  `customized_column_1` text,
  `customized_column_2` text,
  `customized_column_3` text,
  `customized_column_4` text,
  `customized_column_5` text,
  `customized_column_6` text,
  PRIMARY KEY  (`id`),
  KEY `owner` USING BTREE (`owner`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `call_recording_file_attribute`
--

DROP TABLE IF EXISTS `call_recording_file_attribute`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `call_recording_file_attribute` (
  `id` int(2) NOT NULL auto_increment,
  `status` tinyint(1) default '0',
  `name` varchar(30) default NULL,
  `type` enum('textarea','textfield','combobox') default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `call_recording_file_combobox_option`
--

DROP TABLE IF EXISTS `call_recording_file_combobox_option`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `call_recording_file_combobox_option` (
  `option_id` int(10) NOT NULL auto_increment,
  `name` varchar(255) default NULL,
  `combobox_id` int(2) default NULL,
  PRIMARY KEY  (`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `call_recording_webadmin_member`
--

DROP TABLE IF EXISTS `call_recording_webadmin_member`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `call_recording_webadmin_member` (
  `id` int(11) NOT NULL auto_increment,
  `adminID` varchar(20) NOT NULL,
  `member` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `callernumber`
--

DROP TABLE IF EXISTS `callernumber`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `callernumber` (
  `accountcode` varchar(20) default NULL,
  `callerid` varchar(20) default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `cdr`
--

DROP TABLE IF EXISTS `cdr`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cdr` (
  `calldate` datetime NOT NULL default '0000-00-00 00:00:00',
  `clid` varchar(80) NOT NULL default '',
  `src` varchar(80) NOT NULL default '',
  `dst` varchar(80) NOT NULL default '',
  `dcontext` varchar(80) NOT NULL default '',
  `channel` varchar(80) NOT NULL default '',
  `dstchannel` varchar(80) NOT NULL default '',
  `lastapp` varchar(80) NOT NULL default '',
  `lastdata` varchar(80) NOT NULL default '',
  `duration` int(11) NOT NULL default '0',
  `billsec` int(11) NOT NULL default '0',
  `disposition` varchar(45) NOT NULL default '',
  `amaflags` int(11) NOT NULL default '0',
  `accountcode` varchar(20) NOT NULL default '',
  `uniqueid` varchar(32) NOT NULL default '',
  `userfield` varchar(255) NOT NULL default '',
  KEY `calldate` (`calldate`),
  KEY `dst` (`dst`),
  KEY `accountcode` (`accountcode`),
  KEY `uniqueid` USING BTREE (`uniqueid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `cisco_config`
--

DROP TABLE IF EXISTS `cisco_config`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `cisco_config` (
  `id` int(11) NOT NULL auto_increment,
  `model` varchar(10) default '',
  `path` varchar(255) default '',
  `attribute` text,
  `value` varchar(100) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `conference`
--

DROP TABLE IF EXISTS `conference`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `conference` (
  `id` int(11) NOT NULL auto_increment,
  `number` varchar(50) NOT NULL default '',
  `title` varchar(100) NOT NULL default '',
  `description` text NOT NULL,
  `pincode` varchar(50) NOT NULL default '',
  `adminPincode` varchar(50) default NULL,
  `limit` varchar(20) NOT NULL default '',
  `startday` datetime default NULL,
  `endday` datetime default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `conference_owner_relation`
--

DROP TABLE IF EXISTS `conference_owner_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `conference_owner_relation` (
  `id` int(11) NOT NULL auto_increment,
  `conference_number` varchar(50) NOT NULL,
  `third_party_user_id` int(11) default NULL,
  `sipfriends_id` varchar(80) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `context`
--

DROP TABLE IF EXISTS `context`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `context` (
  `contextID` varchar(50) NOT NULL default '',
  `contextTitle` varchar(50) NOT NULL default '',
  `contextDesc` text,
  `isPGroup` char(1) NOT NULL default '1',
  PRIMARY KEY  (`contextID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `corporate_phonebook_attribute`
--

DROP TABLE IF EXISTS `corporate_phonebook_attribute`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `corporate_phonebook_attribute` (
  `id` int(11) NOT NULL auto_increment,
  `attributeName` varchar(80) NOT NULL,
  `attributeLabel` varchar(80) NOT NULL,
  `attributeMapTo` varchar(80) NOT NULL,
  `attributeFilter` text NOT NULL,
  `sticky` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `corporate_phonebook_setting`
--

DROP TABLE IF EXISTS `corporate_phonebook_setting`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `corporate_phonebook_setting` (
  `id` int(11) NOT NULL auto_increment,
  `attribute` varchar(80) NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `customer`
--

DROP TABLE IF EXISTS `customer`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `customer` (
  `id` int(11) NOT NULL auto_increment,
  `engName` varchar(255) NOT NULL default '',
  `chiName` varchar(255) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `greeting` text NOT NULL,
  `instruction` text NOT NULL,
  `code` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `customer_generalline_relation`
--

DROP TABLE IF EXISTS `customer_generalline_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `customer_generalline_relation` (
  `id` int(11) NOT NULL auto_increment,
  `customer_id` varchar(11) NOT NULL,
  `generalline_number` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `dhcp_info`
--

DROP TABLE IF EXISTS `dhcp_info`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `dhcp_info` (
  `id` int(11) NOT NULL auto_increment,
  `eth` varchar(10) NOT NULL,
  `domainname` varchar(100) NOT NULL,
  `lease_time` varchar(100) NOT NULL,
  `tftp_option` varchar(20) NOT NULL,
  `range_start` varchar(100) NOT NULL,
  `range_end` varchar(100) NOT NULL,
  `gateway` varchar(80) NOT NULL,
  `server_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `did`
--

DROP TABLE IF EXISTS `did`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `did` (
  `number` varchar(80) NOT NULL default '',
  `name` varchar(80) NOT NULL default '',
  `secret` varchar(80) NOT NULL default '',
  `group` varchar(100) NOT NULL default '',
  `dnmode` text NOT NULL,
  `notifyUser` text NOT NULL,
  `mailbox` varchar(80) NOT NULL default '',
  PRIMARY KEY  (`number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `did_range`
--

DROP TABLE IF EXISTS `did_range`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `did_range` (
  `id` int(11) NOT NULL auto_increment,
  `extension_begin` varchar(40) NOT NULL,
  `extension_end` varchar(40) NOT NULL,
  `did_context` varchar(80) NOT NULL,
  `extension_context` varchar(80) NOT NULL,
  `did_begin` varchar(40) NOT NULL,
  `did_end` varchar(40) NOT NULL,
  `callerid_begin` varchar(40) NOT NULL,
  `callerid_end` varchar(40) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `dnmode`
--

DROP TABLE IF EXISTS `dnmode`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `dnmode` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `description` text,
  `rule` varchar(100) NOT NULL default '',
  `isGlobal` char(1) NOT NULL default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `emailTemplate`
--

DROP TABLE IF EXISTS `emailTemplate`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `emailTemplate` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(50) NOT NULL default '',
  `to` text NOT NULL,
  `font_family` varchar(200) default NULL,
  `font_size` varchar(3) default NULL,
  `from` text NOT NULL,
  `fromstring` text,
  `subject` text NOT NULL,
  `header` text NOT NULL,
  `body` text NOT NULL,
  `footer` text NOT NULL,
  `status` varchar(10) default '1',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `equipment`
--

DROP TABLE IF EXISTS `equipment`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `equipment` (
  `username` varchar(80) NOT NULL default '',
  `mac` varchar(20) default NULL,
  `linenum` char(1) default NULL,
  `type` varchar(20) default NULL,
  `parameters` text,
  `model` varchar(50) NOT NULL default '',
  `hotdeskphone` varchar(1) NOT NULL default '0',
  `monitor` varchar(200) default NULL,
  `exp` varchar(2) NOT NULL default '0',
  `services` varchar(2) NOT NULL default '1',
  `disableMissedCall` varchar(2) NOT NULL default '0',
  `callsPerLineKey` varchar(10) NOT NULL default '',
  `isManualConfig` varchar(10) NOT NULL default '',
  `eq_id` int(11) NOT NULL auto_increment,
  `eyebeam_license` text,
  PRIMARY KEY  (`eq_id`),
  KEY `username` USING BTREE (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `equipment_manual_config`
--

DROP TABLE IF EXISTS `equipment_manual_config`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `equipment_manual_config` (
  `id` int(11) NOT NULL auto_increment,
  `mac` varchar(100) NOT NULL,
  `linenum` varchar(10) NOT NULL,
  `username` varchar(100) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `sip_password` varchar(100) NOT NULL,
  `sip_server` varchar(100) NOT NULL,
  `sip_server_port` varchar(100) NOT NULL,
  `backup_sip_server` varchar(100) NOT NULL,
  `backup_sip_server_port` varchar(100) NOT NULL,
  `outbound_proxy_server` varchar(100) NOT NULL,
  `outbound_proxy_server_port` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `equipment_speeddial`
--

DROP TABLE IF EXISTS `equipment_speeddial`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `equipment_speeddial` (
  `id` int(11) NOT NULL auto_increment,
  `mac` varchar(20) NOT NULL,
  `mode` varchar(30) NOT NULL,
  `line` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `exten_include`
--

DROP TABLE IF EXISTS `exten_include`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `exten_include` (
  `context` varchar(50) NOT NULL default '',
  `include` varchar(50) NOT NULL default '',
  `priority` int(11) NOT NULL,
  PRIMARY KEY  (`context`,`include`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `extension`
--

DROP TABLE IF EXISTS `extension`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `extension` (
  `ex_id` int(11) NOT NULL auto_increment,
  `ext` varchar(80) NOT NULL default '',
  `user_ext` varchar(80) default NULL,
  `context` varchar(80) NOT NULL default '',
  `exten` varchar(80) NOT NULL default '',
  `wild_matching` char(1) default '0',
  `exact_matching` varchar(20) default NULL,
  `ivr` varchar(25) default NULL,
  `priority` int(11) NOT NULL default '0',
  `options` text NOT NULL,
  `system` varchar(20) NOT NULL default '0',
  `type` varchar(20) NOT NULL default 'Extension',
  `ref` varchar(100) default NULL,
  `group` varchar(100) NOT NULL default '',
  `setCDRuserfield` text NOT NULL,
  PRIMARY KEY  (`ex_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `extension_default`
--

DROP TABLE IF EXISTS `extension_default`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `extension_default` (
  `ex_id` int(11) NOT NULL auto_increment,
  `context` varchar(80) NOT NULL default '',
  `priority` int(11) NOT NULL default '0',
  `options` varchar(255) NOT NULL default '',
  `setCDRuserfield` text NOT NULL,
  PRIMARY KEY  (`ex_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `extension_dnmode_relation`
--

DROP TABLE IF EXISTS `extension_dnmode_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `extension_dnmode_relation` (
  `id` int(11) NOT NULL auto_increment,
  `exten` varchar(15) NOT NULL,
  `dnmode` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `extension_extended_info`
--

DROP TABLE IF EXISTS `extension_extended_info`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `extension_extended_info` (
  `id` int(11) NOT NULL auto_increment,
  `exten` varchar(80) NOT NULL,
  `name` varchar(80) default NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `extension_reserved`
--

DROP TABLE IF EXISTS `extension_reserved`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `extension_reserved` (
  `id` int(11) NOT NULL auto_increment,
  `exten` varchar(80) NOT NULL,
  `options` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `fax`
--

DROP TABLE IF EXISTS `fax`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `fax` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `callerid` varchar(100) NOT NULL default '',
  `machine` varchar(100) NOT NULL default '',
  `number` varchar(100) NOT NULL default '',
  `destname` varchar(100) NOT NULL default '',
  `destemail` varchar(100) NOT NULL default '',
  `filename` varchar(100) NOT NULL default '',
  `status` char(1) NOT NULL default '0',
  `pages` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `general_group`
--

DROP TABLE IF EXISTS `general_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `general_group` (
  `groupID` int(11) NOT NULL auto_increment,
  `groupCode` varchar(100) NOT NULL,
  `groupTitle` text NOT NULL,
  `groupDesc` text NOT NULL,
  `mohNum` varchar(5) NOT NULL default '0',
  PRIMARY KEY  (`groupID`),
  KEY `groupID` USING BTREE (`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `generalline_info`
--

DROP TABLE IF EXISTS `generalline_info`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `generalline_info` (
  `id` int(11) NOT NULL auto_increment,
  `exten` varchar(80) NOT NULL,
  `greeting` text NOT NULL,
  `instruction` text NOT NULL,
  `use_customer_greeting` int(11) NOT NULL,
  `calleridnum` int(11) NOT NULL,
  `calleridname` int(11) NOT NULL,
  `calleridnum_custom` varchar(80) NOT NULL,
  `calleridname_custom` varchar(80) NOT NULL,
  `with_extension` int(11) NOT NULL,
  `number_context` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `incomingSMS`
--

DROP TABLE IF EXISTS `incomingSMS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `incomingSMS` (
  `id` int(11) NOT NULL auto_increment,
  `smsm_id` int(11) NOT NULL,
  `isms_to` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `smsm_id` (`smsm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ivr_option`
--

DROP TABLE IF EXISTS `ivr_option`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ivr_option` (
  `option_id` int(11) NOT NULL auto_increment,
  `option_exten` varchar(80) NOT NULL,
  `option_context` varchar(80) NOT NULL,
  `option_ivr` varchar(25) NOT NULL,
  `option_beforeaction_prompt` int(11) default '0',
  `option_dial_with_dtmf` int(11) default '0',
  `option_options` text NOT NULL,
  `setcustomcalleridname` int(11) NOT NULL,
  `setcustomcalleridnum` int(11) NOT NULL,
  `customcalleridname` varchar(100) default '',
  `customcalleridnum` varchar(100) default '',
  PRIMARY KEY  (`option_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ivrinfo`
--

DROP TABLE IF EXISTS `ivrinfo`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ivrinfo` (
  `id` int(11) NOT NULL auto_increment,
  `ivr` varchar(25) NOT NULL,
  `includePG` varchar(100) NOT NULL,
  `options` text NOT NULL,
  `loop_count` int(11) NOT NULL,
  `wait_second` int(11) NOT NULL,
  `wait_digit` int(11) NOT NULL,
  `intro_prompt` int(11) default '0',
  `invalidoption_prompt` int(11) default '0',
  `timeout_prompt` int(11) default '0',
  `tryagain_prompt` int(11) default '0',
  `toomanyerror_prompt` int(11) default '0',
  `toomanytimeout_prompt` int(11) default '0',
  `beforeexit_prompt` int(11) default '0',
  `dial_with_dtmf` int(11) default '0',
  `setcustomcalleridname` int(11) NOT NULL,
  `setcustomcalleridnum` int(11) NOT NULL,
  `customcalleridname` varchar(100) default '',
  `customcalleridnum` varchar(100) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `log`
--

DROP TABLE IF EXISTS `log`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `log` (
  `id` bigint(20) unsigned NOT NULL auto_increment,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `user` text NOT NULL,
  `category` text NOT NULL,
  `action` text NOT NULL,
  `description` text NOT NULL,
  `data` text NOT NULL,
  `ip` varchar(20) NOT NULL,
  `server_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `message` (
  `id` int(11) NOT NULL auto_increment,
  `customerID` int(11) NOT NULL default '0',
  `callerName` varchar(255) NOT NULL default '',
  `returnNumber` varchar(50) NOT NULL default '',
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `receptionist` varchar(100) NOT NULL default '',
  `details` text NOT NULL,
  `recipient` varchar(80) NOT NULL,
  `company` varchar(200) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `messageEmail`
--

DROP TABLE IF EXISTS `messageEmail`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `messageEmail` (
  `id` int(11) NOT NULL auto_increment,
  `message_id` int(11) NOT NULL,
  `email_to` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `myfeed`
--

DROP TABLE IF EXISTS `myfeed`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `myfeed` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(35) NOT NULL default '',
  `feedTitle` varchar(100) NOT NULL default '',
  `feedLink` varchar(100) NOT NULL default '',
  `feedUsername` varchar(30) NOT NULL default '',
  `feedPassword` varchar(100) NOT NULL default '',
  `defaulted` enum('yes','no') NOT NULL default 'no',
  `systemUsing` varchar(5) NOT NULL default '',
  `cachedTime` varchar(20) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `nameserver`
--

DROP TABLE IF EXISTS `nameserver`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `nameserver` (
  `id` int(11) NOT NULL auto_increment,
  `nameserver` text NOT NULL,
  `server_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `network`
--

DROP TABLE IF EXISTS `network`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `network` (
  `id` int(11) NOT NULL auto_increment,
  `eth` varchar(10) NOT NULL default '',
  `ip` varchar(80) NOT NULL default '',
  `gateway` varchar(80) NOT NULL default '',
  `netmask` varchar(80) NOT NULL default '',
  `broadcast` varchar(80) NOT NULL default '',
  `routing` text NOT NULL,
  `description` text NOT NULL,
  `virtual` enum('0','1') NOT NULL default '0',
  `eth_state` varchar(100) NOT NULL default '',
  `hasDHCP` enum('0','1') NOT NULL default '0',
  `needRestart` int(11) default '0',
  `server_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `outgoingSMS`
--

DROP TABLE IF EXISTS `outgoingSMS`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `outgoingSMS` (
  `id` int(11) NOT NULL auto_increment,
  `message_id` int(11) NOT NULL,
  `osms_to` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `message_id` (`message_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `paging_group`
--

DROP TABLE IF EXISTS `paging_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `paging_group` (
  `groupID` int(11) NOT NULL auto_increment,
  `groupTitle` varchar(255) NOT NULL,
  `groupDesc` text,
  `timeout` varchar(10) NOT NULL default '30',
  `type` varchar(1) NOT NULL default '1',
  `callerID` varchar(255) default NULL,
  `key` varchar(4) default NULL,
  PRIMARY KEY  (`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `paging_group_member`
--

DROP TABLE IF EXISTS `paging_group_member`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `paging_group_member` (
  `id` int(11) NOT NULL auto_increment,
  `groupID` int(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `patch`
--

DROP TABLE IF EXISTS `patch`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `patch` (
  `id` int(11) NOT NULL auto_increment,
  `version` varchar(80) NOT NULL,
  `number` int(11) NOT NULL,
  `time` datetime NOT NULL,
  `status` enum('success','fail') NOT NULL default 'fail',
  `message` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `polycom_config`
--

DROP TABLE IF EXISTS `polycom_config`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `polycom_config` (
  `id` int(11) NOT NULL auto_increment,
  `model` varchar(10) default '',
  `path` varchar(255) default '',
  `attribute` text,
  `value` varchar(100) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `polycom_phonebook_attribute`
--

DROP TABLE IF EXISTS `polycom_phonebook_attribute`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `polycom_phonebook_attribute` (
  `id` int(2) NOT NULL auto_increment,
  `store_name` varchar(20) NOT NULL default '',
  `display_name` varchar(30) NOT NULL default '',
  `status` tinyint(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `polycom_setting`
--

DROP TABLE IF EXISTS `polycom_setting`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `polycom_setting` (
  `id` int(11) NOT NULL auto_increment,
  `key` text NOT NULL,
  `data` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `polycom_user_config`
--

DROP TABLE IF EXISTS `polycom_user_config`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `polycom_user_config` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `path` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `queue`
--

DROP TABLE IF EXISTS `queue`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `queue` (
  `id` int(11) NOT NULL auto_increment,
  `noc_queuename` varchar(200) NOT NULL default '',
  `description` text,
  `general_group` int(11) default NULL,
  `agentgroup` enum('0','1') NOT NULL default '0',
  `callernamerewrite` char(1) NOT NULL default '0',
  `callernamerewritewith` varchar(200) default NULL,
  `callernameappendto` varchar(200) default NULL,
  `callernumrewrite` char(1) NOT NULL default '0',
  `callernumrewritewith` varchar(80) default NULL,
  `callernumappendto` varchar(200) default NULL,
  `strategy` enum('ringall','leastrecent','fewestcalls','random','rrmemory','linear','wrandom') default NULL,
  `servicelevel` int(11) default NULL,
  `exitdigit` varchar(5) default NULL,
  `exitvm` varchar(80) default NULL,
  `timeout` int(5) default NULL,
  `retry` int(5) default NULL,
  `weight` int(8) NOT NULL default '0',
  `wrapuptime` int(5) default NULL,
  `autopause` enum('yes','no') NOT NULL default 'yes',
  `autopauseexpiry` int(5) NOT NULL default '0',
  `maxlen` int(5) NOT NULL default '0',
  `announcefre` int(5) default NULL,
  `minannouncefre` int(5) default NULL,
  `periannouncecefre` int(5) default NULL,
  `announceholdtime` enum('yes','no','once') default NULL,
  `announceposilimit` int(5) default NULL,
  `announcerousecs` enum('5','10','15','20','25','30') default NULL,
  `announceposi` enum('yes','no','limit','more') default NULL,
  `callrecordings` enum('yes','no') NOT NULL default 'no',
  `joinempty` enum('yes','no','strict','loose') default NULL,
  `leavewhenemp` enum('yes','no','strict','loose') default NULL,
  `reportholdtime` enum('yes','no') NOT NULL default 'no',
  `proseconds` int(11) default NULL,
  `proreporthold` int(11) default NULL,
  `proannounce` int(11) default NULL,
  `properiodicannounce` int(11) default NULL,
  `prothankyou` int(11) default NULL,
  `proyouarenext` int(11) default NULL,
  `prothereare` int(11) default NULL,
  `procallswaiting` int(11) default NULL,
  `proholdtime` int(11) default NULL,
  `prominutes` int(11) default NULL,
  `planguage` enum('en','en_us','cn') default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `queue_agentgroup`
--

DROP TABLE IF EXISTS `queue_agentgroup`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `queue_agentgroup` (
  `id` int(11) NOT NULL auto_increment,
  `queueid` int(11) default NULL,
  `agentgroupid` int(11) default NULL,
  `agpriority` int(8) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `queue_log`
--

DROP TABLE IF EXISTS `queue_log`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `queue_log` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `callid` varchar(20) NOT NULL default '',
  `queuename` varchar(20) NOT NULL default '',
  `agent` varchar(20) NOT NULL default '',
  `event` varchar(20) NOT NULL default '',
  `arg1` varchar(100) NOT NULL default '',
  `arg2` varchar(100) NOT NULL default '',
  `arg3` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `queue_url_api`
--

DROP TABLE IF EXISTS `queue_url_api`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `queue_url_api` (
  `id` int(11) NOT NULL auto_increment,
  `queue_id` int(11) NOT NULL,
  `event_type` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `url` text NOT NULL,
  `post` text,
  `get` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `quintum`
--

DROP TABLE IF EXISTS `quintum`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `quintum` (
  `quintum_id` int(11) NOT NULL auto_increment,
  `quintum_name` varchar(200) NOT NULL,
  `quintum_model` varchar(30) NOT NULL,
  `quintum_ip` varchar(30) NOT NULL,
  `quintum_username` varchar(50) NOT NULL,
  `quintum_password` varchar(50) NOT NULL,
  `quintum_mac` varchar(20) NOT NULL,
  `quintum_serial` varchar(100) NOT NULL,
  `quintum_voiceCodec` varchar(50) NOT NULL,
  `quintum_sipServer` varchar(30) NOT NULL,
  `quintum_description` text,
  `quintum_parameters` text NOT NULL,
  `quintum_survivability` varchar(20) NOT NULL,
  PRIMARY KEY  (`quintum_id`),
  KEY `name_index` (`quintum_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `quintum_analogue_gateway`
--

DROP TABLE IF EXISTS `quintum_analogue_gateway`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `quintum_analogue_gateway` (
  `id` int(11) NOT NULL auto_increment,
  `quintum_id` varchar(50) NOT NULL,
  `physicalPort` varchar(5) NOT NULL,
  `username` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `username` USING BTREE (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `quintum_user_survivability_models_relation`
--

DROP TABLE IF EXISTS `quintum_user_survivability_models_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `quintum_user_survivability_models_relation` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `quintum_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `quintumport`
--

DROP TABLE IF EXISTS `quintumport`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `quintumport` (
  `quintumport_id` int(11) NOT NULL auto_increment,
  `quintum_id` int(11) NOT NULL,
  `quintumport_name` varchar(200) NOT NULL,
  `quintumport_number` int(11) default NULL,
  `quintumport_lineType` varchar(50) NOT NULL,
  `quintumport_parameters` text NOT NULL,
  `quintumport_mapping` text NOT NULL,
  PRIMARY KEY  (`quintumport_id`),
  KEY `quintum_id` (`quintum_id`),
  KEY `name_index` (`quintumport_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `receptionist`
--

DROP TABLE IF EXISTS `receptionist`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `receptionist` (
  `id` int(11) NOT NULL auto_increment,
  `receptionistid` varchar(80) NOT NULL,
  `receptionistpwd` varchar(80) NOT NULL,
  `aclgroup` int(11) NOT NULL,
  `generalgroup` int(11) default NULL,
  `firstname` varchar(200) default NULL,
  `lastname` varchar(200) default NULL,
  `callrecording` enum('yes','no') default 'no',
  `autoclosepopup` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `receptionist_location`
--

DROP TABLE IF EXISTS `receptionist_location`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `receptionist_location` (
  `id` int(11) NOT NULL auto_increment,
  `receptionistid` varchar(80) NOT NULL,
  `location` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `receptionistgroup`
--

DROP TABLE IF EXISTS `receptionistgroup`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `receptionistgroup` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(200) NOT NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `receptionistgroup_receptionist`
--

DROP TABLE IF EXISTS `receptionistgroup_receptionist`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `receptionistgroup_receptionist` (
  `id` int(11) NOT NULL auto_increment,
  `receptionistgroupid` int(11) NOT NULL,
  `receptionistid` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `redundancy_network_mapping`
--

DROP TABLE IF EXISTS `redundancy_network_mapping`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `redundancy_network_mapping` (
  `id` int(11) NOT NULL auto_increment,
  `server_id_1` int(11) NOT NULL,
  `eth_1` varchar(10) NOT NULL,
  `server_id_2` int(11) NOT NULL,
  `eth_2` varchar(10) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `redundancy_sync_dir`
--

DROP TABLE IF EXISTS `redundancy_sync_dir`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `redundancy_sync_dir` (
  `id` int(11) NOT NULL auto_increment,
  `path` varchar(256) NOT NULL,
  `desc` text,
  `ignore` varchar(256) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `reporting_saved_report`
--

DROP TABLE IF EXISTS `reporting_saved_report`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `reporting_saved_report` (
  `id` int(11) NOT NULL auto_increment,
  `report_name` varchar(100) NOT NULL,
  `report_desc` text,
  `report_data` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ring_group`
--

DROP TABLE IF EXISTS `ring_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `ring_group` (
  `groupID` int(11) NOT NULL auto_increment,
  `groupTitle` varchar(50) NOT NULL default '',
  `groupDesc` text,
  `members` text,
  `isReceptionist` char(1) NOT NULL default '0',
  PRIMARY KEY  (`groupID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `route`
--

DROP TABLE IF EXISTS `route`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `route` (
  `id` int(11) NOT NULL auto_increment,
  `exten` varchar(80) NOT NULL,
  `priority` varchar(5) NOT NULL,
  `trunkid` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `rss_news`
--

DROP TABLE IF EXISTS `rss_news`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `rss_news` (
  `id` int(11) NOT NULL auto_increment,
  `rss_id` varchar(80) NOT NULL,
  `title` varchar(80) default '',
  `description` text,
  `pubdate` varchar(80) default '',
  `cachedTime` varchar(20) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `schedule_plan`
--

DROP TABLE IF EXISTS `schedule_plan`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `schedule_plan` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `parameters` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `schedule_report`
--

DROP TABLE IF EXISTS `schedule_report`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `schedule_report` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(50) NOT NULL default '',
  `billingDay` int(11) NOT NULL default '0',
  `day` int(11) NOT NULL default '0',
  `hour` int(11) NOT NULL default '0',
  `parameters` text NOT NULL,
  `plan` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `scheduled_task`
--

DROP TABLE IF EXISTS `scheduled_task`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `scheduled_task` (
  `id` int(11) NOT NULL auto_increment,
  `server_id` int(11) NOT NULL default '0',
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `job` varchar(100) NOT NULL,
  `data` text,
  `processing` int(11) NOT NULL default '0',
  `done` int(11) NOT NULL default '0',
  `trial` int(11) NOT NULL default '0',
  `timestamp` decimal(16,5) NOT NULL default '0.00000',
  PRIMARY KEY  (`id`,`server_id`),
  KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `session` (
  `id` int(11) NOT NULL auto_increment,
  `session_id` varchar(40) NOT NULL,
  `username` varchar(80) NOT NULL,
  `current_page` varchar(20) NOT NULL,
  `last_access` int(11) NOT NULL,
  `ip` varchar(16) NOT NULL,
  `credential` varchar(100) default NULL,
  `server_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sipfriends`
--

DROP TABLE IF EXISTS `sipfriends`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sipfriends` (
  `name` varchar(80) NOT NULL default '',
  `username` varchar(80) NOT NULL default '',
  `secret` varchar(80) NOT NULL default '',
  `context` varchar(80) NOT NULL default '',
  `callerid` varchar(80) NOT NULL default '',
  `musiconhold` varchar(40) NOT NULL default 'default',
  `dtmfmode` varchar(40) NOT NULL default 'rfc2833',
  `incominglimit` varchar(20) NOT NULL default '0',
  `mailbox` varchar(80) NOT NULL default '',
  `callgroup` varchar(50) default NULL,
  `pickupgroup` varchar(50) default NULL,
  `nat` varchar(40) NOT NULL default 'no',
  `canreinvite` varchar(40) NOT NULL default 'yes',
  `ipaddr` varchar(20) NOT NULL default 'dynamic',
  `port` varchar(20) NOT NULL default '5060',
  `regseconds` int(11) NOT NULL default '0',
  `qualify` varchar(10) NOT NULL default 'NO',
  `accessExtplan` varchar(10) default 'yes',
  `group` varchar(100) NOT NULL default '',
  `sip_password` varchar(100) NOT NULL default '',
  `language` varchar(20) NOT NULL default 'GLOBAL',
  `allowcodec` varchar(100) default 'GLOBAL',
  `call_waiting` varchar(20) NOT NULL default '1',
  `call_recording` varchar(20) NOT NULL default '0',
  `paginggroup` varchar(50) default '',
  `phoneLabel` varchar(40) default '',
  `disableMWI` varchar(20) NOT NULL default '',
  `autoAnswer` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`username`),
  KEY `username` USING BTREE (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sippeer_group`
--

DROP TABLE IF EXISTS `sippeer_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sippeer_group` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(80) NOT NULL,
  `description` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sippeer_group_sippeers_relation`
--

DROP TABLE IF EXISTS `sippeer_group_sippeers_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sippeer_group_sippeers_relation` (
  `id` int(11) NOT NULL auto_increment,
  `group_name` varchar(80) NOT NULL,
  `sippeer_id` varchar(30) NOT NULL,
  `priority` int(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sippeers`
--

DROP TABLE IF EXISTS `sippeers`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sippeers` (
  `peerID` varchar(30) NOT NULL default '',
  `canreinvite` char(1) NOT NULL default '0',
  `host` varchar(30) NOT NULL default '',
  `port` int(6) NOT NULL default '0',
  `context` varchar(50) NOT NULL default 'default',
  `nat` char(1) NOT NULL default '1',
  `allowcodec` varchar(100) NOT NULL default '',
  `qualify` char(1) NOT NULL default '0',
  `avayaGateway` char(1) NOT NULL default '0',
  `insecure` varchar(20) NOT NULL,
  `dtmfmode` char(10) NOT NULL default '',
  `pronunciation` text,
  `frsipPBX` enum('0','1') NOT NULL default '0',
  `username` varchar(80) default '',
  `password` varchar(80) default '',
  `promiscredir` varchar(10) NOT NULL,
  PRIMARY KEY  (`peerID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sippeers_backup`
--

DROP TABLE IF EXISTS `sippeers_backup`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sippeers_backup` (
  `id` int(11) NOT NULL auto_increment,
  `peerID` varchar(30) NOT NULL default '',
  `host` varchar(30) NOT NULL default '',
  `port` int(6) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sla_group`
--

DROP TABLE IF EXISTS `sla_group`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sla_group` (
  `id` int(11) NOT NULL auto_increment,
  `group_name` varchar(80) NOT NULL,
  `group_desc` text,
  `generalgroup` int(11) NOT NULL,
  `max_appearance` int(11) NOT NULL,
  `group_type` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sla_group_user`
--

DROP TABLE IF EXISTS `sla_group_user`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sla_group_user` (
  `id` int(11) NOT NULL auto_increment,
  `sla_group_id` int(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `sound_prompt`
--

DROP TABLE IF EXISTS `sound_prompt`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `sound_prompt` (
  `prompt_id` int(11) NOT NULL auto_increment,
  `prompt_name` varchar(100) NOT NULL,
  `prompt_desc` text,
  `prompt_script_text` text,
  PRIMARY KEY  (`prompt_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `speedDial_contact`
--

DROP TABLE IF EXISTS `speedDial_contact`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `speedDial_contact` (
  `id` int(11) NOT NULL auto_increment,
  `owner` varchar(80) NOT NULL,
  `contactType` int(11) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `company` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `jobTitle` varchar(255) NOT NULL,
  `phone_number` varchar(35) NOT NULL,
  `mobile_number` varchar(35) NOT NULL,
  `other_number` varchar(35) NOT NULL,
  `fax` varchar(35) NOT NULL,
  `email` varchar(150) NOT NULL,
  `buddy` varchar(35) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `speedDial_setting`
--

DROP TABLE IF EXISTS `speedDial_setting`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `speedDial_setting` (
  `id` int(11) NOT NULL auto_increment,
  `owner` varchar(80) NOT NULL,
  `contactType` int(11) NOT NULL,
  `contactID` varchar(80) NOT NULL,
  `contactRef` text NOT NULL,
  `selectedNumber` varchar(80) NOT NULL,
  `buddy` varchar(35) NOT NULL,
  `sdIndex` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `static_route`
--

DROP TABLE IF EXISTS `static_route`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `static_route` (
  `id` int(11) NOT NULL auto_increment,
  `eth` varchar(10) NOT NULL,
  `ip` varchar(80) NOT NULL,
  `gateway` varchar(80) NOT NULL,
  `netmask` varchar(80) NOT NULL,
  `server_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `switchboard_popup_number`
--

DROP TABLE IF EXISTS `switchboard_popup_number`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `switchboard_popup_number` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `number` varchar(100) NOT NULL,
  `text` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `switchboard_user_buddy`
--

DROP TABLE IF EXISTS `switchboard_user_buddy`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `switchboard_user_buddy` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `buddy` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `switchboard_user_status`
--

DROP TABLE IF EXISTS `switchboard_user_status`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `switchboard_user_status` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `status_name` varchar(100) NOT NULL,
  `status_sound` int(11) NOT NULL,
  `status_type` int(11) NOT NULL,
  `status_ans_cc_call` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `switchboard_user_status_callflow`
--

DROP TABLE IF EXISTS `switchboard_user_status_callflow`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `switchboard_user_status_callflow` (
  `id` int(11) NOT NULL auto_increment,
  `status_id` varchar(100) NOT NULL,
  `priority` int(11) NOT NULL,
  `options` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `switchboard_user_status_currentstatus`
--

DROP TABLE IF EXISTS `switchboard_user_status_currentstatus`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `switchboard_user_status_currentstatus` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `status_id` varchar(100) NOT NULL,
  `status_type` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `switchboard_user_status_default`
--

DROP TABLE IF EXISTS `switchboard_user_status_default`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `switchboard_user_status_default` (
  `id` int(11) NOT NULL auto_increment,
  `status_name` varchar(100) NOT NULL,
  `status_sound` int(11) NOT NULL,
  `status_ans_cc_call` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `options` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `systemConfigs`
--

DROP TABLE IF EXISTS `systemConfigs`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `systemConfigs` (
  `id` int(11) NOT NULL auto_increment,
  `key` text NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `system_check`
--

DROP TABLE IF EXISTS `system_check`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `system_check` (
  `id` int(11) NOT NULL auto_increment,
  `service` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `system_error_log`
--

DROP TABLE IF EXISTS `system_error_log`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `system_error_log` (
  `id` int(11) NOT NULL auto_increment,
  `time` datetime NOT NULL default '0000-00-00 00:00:00',
  `code` varchar(10) NOT NULL,
  `title` text NOT NULL,
  `desc` text,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `system_time_zone`
--

DROP TABLE IF EXISTS `system_time_zone`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `system_time_zone` (
  `id` int(11) NOT NULL auto_increment,
  `location` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `time_offset_ref` varchar(100) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `telecommuting_device`
--

DROP TABLE IF EXISTS `telecommuting_device`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `telecommuting_device` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `device_number` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `third_party_user`
--

DROP TABLE IF EXISTS `third_party_user`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `third_party_user` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) default '',
  `group` varchar(100) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `topology`
--

DROP TABLE IF EXISTS `topology`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `topology` (
  `topology_id` int(11) NOT NULL auto_increment,
  `topology_modules` varchar(255) default NULL,
  `topology_name` varchar(255) default NULL,
  `topology_text` varchar(255) default NULL,
  `topology_icon` varchar(255) default NULL,
  `topology_parent` varchar(10) default NULL,
  `topology_page` varchar(10) default NULL,
  `topology_order` int(11) default NULL,
  `topology_url` varchar(255) default NULL,
  `topology_url_opt` varchar(255) default NULL,
  `topology_url_opt_m` varchar(10) default NULL,
  `topology_url_opt_a` varchar(10) default NULL,
  `topology_actionType` varchar(5) default NULL,
  `topology_show` enum('0','1') default '1',
  `topology_have_child` enum('0','1') default '1',
  `topology_activate` enum('0','1') default '1',
  PRIMARY KEY  (`topology_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `trunk`
--

DROP TABLE IF EXISTS `trunk`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `trunk` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(80) NOT NULL,
  `description` text,
  `calleridprefix` varchar(80) default NULL,
  `cutcalleridnum` varchar(80) default NULL,
  `forcecallerid` varchar(80) default NULL,
  `extensionprefix` varchar(80) default NULL,
  `cutextensionnum` varchar(80) default NULL,
  `sippeer` varchar(30) NOT NULL,
  `autoresolvecalleridprefix` char(1) NOT NULL default '1',
  `preferredCodec` varchar(100) default NULL,
  `dtmfmode` varchar(100) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `trunk_sippeer_relation`
--

DROP TABLE IF EXISTS `trunk_sippeer_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `trunk_sippeer_relation` (
  `id` int(11) NOT NULL auto_increment,
  `trunk_name` varchar(80) NOT NULL,
  `sippeer_id` varchar(30) default '',
  `sippeer_group_id` varchar(80) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_profile`
--

DROP TABLE IF EXISTS `user_profile`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_profile` (
  `id` int(11) NOT NULL auto_increment,
  `profile_name` varchar(200) NOT NULL,
  `profile_desc` text NOT NULL,
  `user_context` varchar(80) NOT NULL default '',
  `user_musiconhold` varchar(40) NOT NULL default 'default',
  `user_dtmfmode` varchar(40) NOT NULL default 'rfc2833',
  `user_incominglimit` int(11) NOT NULL default '0',
  `user_callgroup` varchar(100) default NULL,
  `user_pickupgroup` varchar(100) default NULL,
  `user_paginggroup` varchar(100) default NULL,
  `user_nat` varchar(40) NOT NULL default 'no',
  `user_canreinvite` varchar(40) NOT NULL default 'yes',
  `user_ipaddr` varchar(20) NOT NULL default 'dynamic',
  `user_port` int(6) NOT NULL default '5060',
  `user_qualify` varchar(10) NOT NULL default 'NO',
  `user_accessExtplan` varchar(10) default 'no',
  `user_language` varchar(20) NOT NULL default 'GLOBAL',
  `user_allowcodec` varchar(100) default 'GLOBAL',
  `user_vmDeliveryOpt` varchar(10) NOT NULL default '0',
  `user_acl_group_id` int(11) NOT NULL,
  `user_IDD_callerid` varchar(100) NOT NULL,
  `user_IDD_callerid_custom_name` varchar(100) default '',
  `user_IDD_callerid_custom_number` varchar(100) default '',
  `user_IDD_valid_PIN` enum('0','1') NOT NULL default '0',
  `user_IDD_allow_other` enum('0','1') NOT NULL default '0',
  `user_IDD_status` enum('0','1') NOT NULL default '0',
  `user_callrecording` varchar(10) NOT NULL,
  `user_call_waiting` varchar(10) NOT NULL,
  `user_disa_status` varchar(10) NOT NULL,
  `user_disa_usePIN` varchar(10) NOT NULL default 'Y',
  `user_extraOwnCPE` varchar(10) NOT NULL,
  `user_conferenceOption` varchar(10) NOT NULL,
  `user_maxmsg` varchar(10) NOT NULL default '',
  `user_maxsecs` varchar(10) NOT NULL default '',
  `user_envelope` varchar(10) NOT NULL default '',
  `user_saycid` varchar(10) NOT NULL default '',
  `user_delnewafterday` varchar(10) NOT NULL default '',
  `user_deloldafterday` varchar(10) NOT NULL default '',
  `user_disableMWI` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_profile_routing`
--

DROP TABLE IF EXISTS `user_profile_routing`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_profile_routing` (
  `id` int(11) NOT NULL auto_increment,
  `profile_id` int(11) NOT NULL,
  `ext_context` varchar(80) NOT NULL default '',
  `ext_priority` int(11) NOT NULL default '0',
  `ext_options` text NOT NULL,
  `ext_type` varchar(20) NOT NULL default 'Extension',
  `ext_setCDRuserfield` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_profile_routing_dnmode`
--

DROP TABLE IF EXISTS `user_profile_routing_dnmode`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_profile_routing_dnmode` (
  `id` int(11) NOT NULL auto_increment,
  `profile_id` int(11) NOT NULL,
  `ext_routing_dnmode` varchar(80) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `user_profile_user_relation`
--

DROP TABLE IF EXISTS `user_profile_user_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `user_profile_user_relation` (
  `id` int(11) NOT NULL auto_increment,
  `profile_id` int(11) NOT NULL,
  `username` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `users` (
  `context` char(80) NOT NULL default 'default',
  `mailbox` char(80) NOT NULL default '',
  `password` char(80) NOT NULL default '',
  `fullname` char(80) NOT NULL default '',
  `email` char(80) NOT NULL default '',
  `pager` char(80) NOT NULL default '',
  `options` char(255) NOT NULL default '',
  `vmDeliveryOpt` varchar(20) NOT NULL default '0',
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `maxmsg` varchar(10) NOT NULL default '',
  `maxsecs` varchar(10) NOT NULL default '',
  `envelope` varchar(10) NOT NULL default '',
  `saycid` varchar(10) NOT NULL default '',
  `delnewafterday` varchar(10) NOT NULL default '',
  `deloldafterday` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`context`,`mailbox`),
  KEY `mailbox` USING BTREE (`mailbox`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `vminfo`
--

DROP TABLE IF EXISTS `vminfo`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `vminfo` (
  `id` int(11) NOT NULL auto_increment,
  `mailbox` varchar(80) NOT NULL,
  `message_id` varchar(200) NOT NULL,
  `iv` varchar(200) NOT NULL,
  `key` varchar(200) NOT NULL,
  `digest` text NOT NULL,
  `status` varchar(10) NOT NULL default 'new',
  `callerid` varchar(50) default '',
  `timestamp` varchar(50) default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `voicemail_notifyuser_relation`
--

DROP TABLE IF EXISTS `voicemail_notifyuser_relation`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `voicemail_notifyuser_relation` (
  `id` int(11) NOT NULL auto_increment,
  `mailbox` varchar(80) NOT NULL,
  `notifyUser` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `weather_content`
--

DROP TABLE IF EXISTS `weather_content`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `weather_content` (
  `id` int(11) NOT NULL auto_increment,
  `username` int(11) NOT NULL,
  `cityid` varchar(20) default '',
  `title` varchar(40) default '',
  `current_des` varchar(20) default '',
  `current_deg` varchar(20) default '',
  `today` varchar(40) default '',
  `tomorrow` varchar(40) default '',
  `cachedTime` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `xmlServicePA`
--

DROP TABLE IF EXISTS `xmlServicePA`;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
CREATE TABLE `xmlServicePA` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(80) NOT NULL,
  `paWeatherDefaultCityCode` varchar(80) NOT NULL default '',
  `paWeatherDefaultCityName` varchar(200) default '',
  `paWeatherEnabled` enum('yes','no') NOT NULL default 'no',
  `paMyFeedEnabled` enum('yes','no') NOT NULL default 'no',
  `paNewsEnabled` enum('yes','no') NOT NULL default 'no',
  `paPhonebookEnabled` enum('yes','no') NOT NULL default 'yes',
  `paIdleDisplay` enum('myfeed','phonebook','news','weather') NOT NULL default 'news',
  `paIdleDisplayRefresh` varchar(80) NOT NULL default '15',
  `paIdleDisplayEnabled` enum('yes','no') NOT NULL default 'no',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
SET character_set_client = @saved_cs_client;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2009-12-14  2:06:23
