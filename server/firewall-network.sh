#!/bin/bash
#
firewall-network.sh(){
	firewall-cmd --zone=public --permanent --add-service={sip,sips}
	firewall-cmd --zone=public --permanent --add-port=2727/udp
	firewall-cmd --zone=public --permanent --add-port=10000-20000/udp
	firewall-cmd --zone=public --permanent --add-service={http,https}
	firewall-cmd --zone=public --permanent --add-port=4569/udp
	yum install -y dhcp
	touch /etc/conf.d/dhcpd
	chgrp apache /etc/conf.d/dhcpd
	chmod 664 /etc/conf.d/dhcpd
	chgrp apache /etc/dhcp -R
	chmod 774 /etc/dhcp -R
	
        yum install -y ntp
        touch /etc/ntp.conf
	chgrp apache /etc/ntp.conf
	chmod 774 /etc/ntp.conf

	#install some network utilities
	yum install -y tcpdump nmap fping

	#include tftp
        yum install -y tftp tftp-server* xinetd*
	mkdir /var/log/tftpd
	mkdir -p /frSIP_backup/current/log/tftpd
        
	#include vsftpd 	
	yum install vsftpd ftp -y 
	#  configration missing

	# install snmpd
	yum install net-snmp net-snmp-utils 
	#configration missing
        systemctl restart snmpd

 	yum install arpwatch
	sed -i 's/^IFACES=.*$/IFACES="eth5 eth4 eth3 eth2 eth1 eth0"/g' /etc/sysconfig/arpwatch
	chkconfig --level 35 arpwatch on
	service arpwatch restart       	
	
	yum install -y openvpn
	chown apache /etc/openvpn
	
}

